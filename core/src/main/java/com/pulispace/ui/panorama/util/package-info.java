/**
 * This package contains classes for handling very large image files.  Originally from MCFS
 * (Mission Control - Flight Software) a development of Team Puli Space, official Google Lunar XPRIZE contestant.
 */
package com.pulispace.ui.panorama.util;