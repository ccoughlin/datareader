/*
 * com.emphysic.myriad.core.data.ops.math.Histogram
 *
 * Copyright (c) 2018 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops.math;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.NormalizeSignalOperation;

/**
 * Simple histogram functions.
 */
public class Histogram {
    /**
     * Calculates the one-dimensional histogram for a given Dataset.
     * @param input Dataset to examine
     * @param normalize if true, input is normalized prior to calculating the histogram
     * @param max_intensity maximum intensity to use
     * @return array of size [max_intensity + 1] with a count of the number of elements in input with that value
     */
    public static int[] hist1d(Dataset input, boolean normalize, int max_intensity) {
        Dataset normalized;
        if (normalize) {
            normalized = grayScale(input, max_intensity);
        } else {
            normalized = new Dataset(input);
        }
        int[] hist = new int[max_intensity + 1];

        for (int i=0; i<normalized.getSize(); i++) {
            int val = (int)normalized.getData()[i];
            hist[val] += 1;
        }
        return hist;
    }

    /**
     * Converts a Dataset into a gray scale representation by normalizing between 0 and a maximum value.
     * @param input input to normalize
     * @param max_intensity maximum value to use
     * @return Dataset with scaled amplitudes
     */
    public static Dataset grayScale(Dataset input, int max_intensity) {
        Dataset normalized = new NormalizeSignalOperation().run(input);
        for(int i=0; i < normalized.getWidth(); i++) {
            for (int j = 0; j < normalized.getHeight(); j++) {
                normalized.set(i, j, max_intensity * normalized.get(i, j));
            }
        }
        return normalized;
    }

    /**
     * Converts a Dataset into a gray scale representation by normalizing between 0 and 255.
     * @param input input to normalize
     * @return Dataset with values between 0 and 255.
     */
    public static Dataset grayScale(Dataset input) {
        return grayScale(input, 255);
    }

    /**
     * Calculates the histogram for a given Dataset.
     * @param input Dataset to examine
     * @return 255-level histogram
     */
    public static int[] hist1d(Dataset input) {
        return hist1d(input, true, 255);
    }

    /**
     * Calculates the histogram for a given Dataset.
     * @param input Dataset to examine
     * @param normalize if true, normalize the data between 0-255 prior to calculating the histogram
     * @return histogram of data
     */
    public static int[] hist1d(Dataset input, boolean normalize) {
        return hist1d(input, normalize, 255);
    }
}
