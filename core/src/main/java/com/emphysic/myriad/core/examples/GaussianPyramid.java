/*
 * com.emphysic.myriad.core.examples.GaussianPyramid
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.examples;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.GaussianPyramidOperation;
import com.emphysic.myriad.core.data.util.FileSniffer;

import java.io.File;

/**
 * Demonstrates usage of the Gaussian Pyramid operation
 * Created by ccoughlin on 9/9/16.
 */
public class GaussianPyramid {
    public static void main(String[] args) {
        Dataset input;
        if (args == null || args.length == 0) {
            System.out.println("Using default input data");
            input = getDefaultDataset();
        } else {
            System.out.print("Attempting to read " + args[0] + "...");
            try {
                input = FileSniffer.read(new File(args[0]), true);
                System.out.println("ok");
            } catch (Exception e) {
                System.out.println("failed!  Using default input data.");
                input = getDefaultDataset();
            }
        }
        System.out.println();
        System.out.println("Input data " + input.getWidth() + "x" + input.getHeight() + ":");
        input.prettyPrint();
        System.out.println();
        GaussianPyramidOperation po = new GaussianPyramidOperation();
        Dataset res = new Dataset(input);
        int pstep = 1;
        while ((res = po.run(res)) != null) {
            System.out.println("Step " + pstep + " " + res.getWidth() + "x" + res.getHeight() + ":");
            res.prettyPrint();
            System.out.println();
        }
    }

    /**
     * Default data for demonstration if not provided an input file to read
     */
    private static double[] defaultSampleData = {
            0.0,  0.0,  0.0,  0.0, 0.0,
            0.0,  0.0,  0.0,  0.0, 0.0,
            0.0, 50.0, 60.0, 50.0, 0.0,
            0.0, 60.0, 90.0, 60.0, 0.0,
            0.0, 50.0, 60.0, 50.0, 0.0,
            0.0,  0.0,  0.0,  0.0, 0.0,
            0.0,  0.0,  0.0,  0.0, 0.0
    };

    /**
     * Creates a Dataset from the default sample data
     * @return new Dataset
     */
    private static Dataset getDefaultDataset() {
        return new Dataset(defaultSampleData, 5, 7);
    }
}
