/*
 * com.emphysic.myriad.core.data.ops.AbsoluteValueOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;

import java.util.Map;

/**
 * AbsoluteValueOperation - returns absolute value of a Dataset's elements.
 */
public class AbsoluteValueOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Returns a new Dataset comprised of the absolute value of the input's elements.
     * @param input input dataset.
     * @return new Dataset of same dimensions as input, or null if input was null.
     */
    @Override
    public Dataset run(Dataset input) {
        if (input != null) {
            double[] orig = input.getData();
            double[] abs = new double[orig.length];
            for (int i = 0; i < orig.length; i++) {
                abs[i] = Math.abs(orig[i]);
            }
            return new Dataset(abs, input.getWidth(), input.getHeight());
        }
        return null;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {}
}
