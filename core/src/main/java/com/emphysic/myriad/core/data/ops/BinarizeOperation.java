/*
 * com.emphysic.myriad.core.data.ops.BinarizeOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;

import java.util.Map;

/**
 * BinarizeOperation - returns a new Dataset with binarized values: values less than the threshold are set to 0,
 * values greater than the threshold are set to 1.
 */
public class BinarizeOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version
    /**
     * Threshold for binarization
     */
    private double threshold;

    /**
     * Creates a new BinarizeOperation with the specified threshold.
     * @param threshold binarization threshold.
     */
    public BinarizeOperation(double threshold) {
        this.threshold = threshold;
    }

    /**
     * Creates a new BinarizeOperation with a threshold of 0.
     */
    public BinarizeOperation() {
        this(0);
    }

    /**
     * Binarizes a Dataset - elements less than the threshold are set to 0, elements greater than the threshold
     * are set to 1.
     * @param input input dataset
     * @return new binarized Dataset or null if input is null
     */
    @Override
    public Dataset run(Dataset input) {
        if (input != null) {
            double[] inputData = input.getData();
            double[] binarizedData = new double[inputData.length];
            for (int i = 0; i < inputData.length; i++) {
                if (inputData[i] < threshold) {
                    binarizedData[i] = 0;
                } else {
                    binarizedData[i] = 1;
                }
            }
            return new Dataset(binarizedData, input.getWidth(), input.getHeight());
        }
        return null;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("threshold", threshold);
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        threshold = (double) objectMap.getOrDefault("threshold", threshold);
    }

    /**
     * Threshold for binarization
     */
    public double getThreshold() {
        return threshold;
    }

    /**
     * Threshold for binarization
     */
    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }
}
