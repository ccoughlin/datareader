/*
 * com.emphysic.myriad.core.data.roi.MLROIFinder
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;

/**
 * MLROIFinder - ROIFinder based on machine learning techniques.
 */
public interface MLROIFinder extends ROIFinder {
    /**
     * Trains the Region Of Interest finder on new data
     * @param X N examples with M features per example
     * @param y N labels for the N examples in X
     * @throws Exception if an error occurs
     */
    void train(double[][] X, int[] y) throws Exception;

    /**
     * Returns the predicted class/value of the sample
     *
     * @param data sample to predict
     * @return predicted label / value
     * @throws Exception if an error occurs
     */
    double predict(double[] data) throws Exception;

    /**
     * Returns the predicted class/value of the sample
     *
     * @param data sample to predict
     * @return predicted label / value
     * @throws Exception if an error occurs
     */
    double predict(Dataset data) throws Exception;

    /**
     * The numeric label assigned to positive samples i.e. samples that contain ROI
     *
     * @return the value assigned to positive samples
     */
    double positiveClass();

    /**
     * The numeric label assigned to negative samples i.e. samples that do not contain ROI
     *
     * @return the value assigned to negative samples
     */
    double negativeClass();
}
