/*
 * com.emphysic.myriad.core.data.roi.PassiveAggressiveROIFinder
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import lombok.extern.slf4j.Slf4j;
import org.apache.mahout.classifier.sgd.PassiveAggressive;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.Vector;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * PassiveAggressiveROIFinder - machine learning ROI detector that uses the Passive-Aggressive algorithm outlined in
 * "Online Passive-Aggressive Algorithms" K. Crammer, O. Dekel, J. Keshat, S. Shalev-Shwartz, Y. Singer - JMLR 7 (2006).
 */
@Slf4j
public class PassiveAggressiveROIFinder extends MLROIConfFinder {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Number of categories to learn (default 2)
     */
    private int numCategories = 2;
    /**
     * Number of features in the model.  In Myriad, equivalent to the total size of the Dataset the model was trained
     * on i.e. #Dataset.getSize().
     */
    private int numFeatures;
    /**
     * The "step size" to use during learning - smaller learning rates can provide a better final result at the
     * cost of increased training time.
     */
    private double learningRate;

    /**
     * The underlying passive-aggressive ROI model
     */
    private PassiveAggressive model;

    /**
     * Creates a new PassiveAggressiveROIFinder with the specified learning rate.
     * @param learningRate learning rate
     */
    public PassiveAggressiveROIFinder(double learningRate) {
        this.learningRate = learningRate;
    }

    /**
     * Creates a new PassiveAggressiveROIFinder with a learning rate of 0.1.
     */
    public PassiveAggressiveROIFinder() {
        this(0.1);
    }

    /**
     * Incrementally trains the model with new samples, creating the model if required.
     * @param X N examples with M features per example
     * @param y N labels for the N examples in X
     */
    @Override
    public void train(double[][] X, int[] y) {
        if (model == null) {
            numFeatures = X[0].length;
            model = new PassiveAggressive(numCategories, numFeatures);
            model.learningRate(learningRate);
        }
        for (int i = 0; i < y.length; i++) {
            model.train(y[i], new DenseVector(X[i]));
        }
    }

    /**
     * Returns the numeric value of the positive class of a two-category model.
     * @return positive class label
     */
    @Override
    public double positiveClass() {
        return 1;
    }

    /**
     * Returns the numeric value of the negative class of a two-category model.
     * @return negative class label
     */
    @Override
    public double negativeClass() {
        return 0;
    }

    /**
     * Predict if a sample contains a region of interest.
     * @param data data to examine
     * @return true if the sample appears to contain an ROI, false otherwise.
     */
    @Override
    public boolean isROI(double[] data) {
        return (int)predict(data) != (int)negativeClass();
    }

    /**
     * Predict if a sample contains a region of interest.
     * @param dataset data to examine
     * @return true if the sample appears to contain an ROI, false otherwise.
     */
    @Override
    public boolean isROI(Dataset dataset) {
        return isROI(dataset.getData());
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = PassiveAggressiveROIFinder.super.getObjectMap();
        map.put("categories", numCategories);
        map.put("features", numFeatures);
        map.put("learning_rate", learningRate);
        map.put("confidence_threshold", confThr);
        map.put("model_fields", writableWriteToBytes(model));
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        numCategories = (int)objectMap.getOrDefault("categories", numCategories);
        numFeatures = (int) objectMap.getOrDefault("features", numFeatures);
        confThr = (double) objectMap.getOrDefault("confidence_threshold", confThr);
        learningRate = (double) objectMap.getOrDefault("learning_rate", learningRate);
        if (objectMap.containsKey("model_fields")) {
            byte[] arr = (byte[]) objectMap.get("model_fields");
            ByteArrayInputStream bais = new ByteArrayInputStream(arr);
            DataInputStream dis = new DataInputStream(bais);
            model = new PassiveAggressive(numCategories, numFeatures);
            model.learningRate(learningRate);
            try {
                model.readFields(dis);
            } catch (IOException ioe) {
                log.error("Error encountered reading model: " + ioe.getMessage());
            }
        }
    }

    /**
     * Returns the number of labels known by the current model.
     * @return number of categories
     */
    public int getNumCategories() {
        return numCategories;
    }

    /**
     * Sets the number of categories for the model.  Will require creating a new model.
     * @param numCategories number of categories
     */
    public void setNumCategories(int numCategories) {
        this.numCategories = numCategories;
    }

    /**
     * Get the number of features in the data i.e. the size of each Dataset used to train the model.
     * @return number of features
     */
    public int getNumFeatures() {
        return numFeatures;
    }

    /**
     * Sets the number of features in the sample data.  Changing this number will require training a new model.
     * @param numFeatures number of features in the data
     */
    public void setNumFeatures(int numFeatures) {
        this.numFeatures = numFeatures;
    }

    /**
     * Returns the current model.
     * @return current model
     */
    public PassiveAggressive getModel() {
        return model;
    }

    /**
     * Sets the ROI model.
     * @param model model to predict regions of interest
     */
    public void setModel(PassiveAggressive model) {
        this.model = model;
    }

    /**
     * Returns the current learning rate.
     * @return learning rate
     */
    public double getLearningRate() {
        return learningRate;
    }

    /**
     * Sets the learning rate.
     * @param learningRate new learning rate
     */
    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public void legacyWrite(Kryo kryo, Output output) {
        try {
            kryo.writeObject(output, this);
            Double ct = new Double(getConfidenceThreshold());
            kryo.writeObject(output, ct);
            if (model != null) {
                model.write(new DataOutputStream(output));
            }
        } catch (IOException ioe) {
            log.error("Error writing model: {}", ioe.getMessage());
        }
    }

    public void legacyRead(Kryo kryo, Input input) {
        try {
            PassiveAggressiveROIFinder paf = kryo.readObject(input, PassiveAggressiveROIFinder.class);
            Double ct = kryo.readObject(input, Double.class);
            setConfidenceThreshold(ct);
            setLearningRate(paf.getLearningRate());
            PassiveAggressive model = new PassiveAggressive(getNumCategories(), getNumFeatures());
            model.readFields(new DataInputStream(input));
            setModel(model);
            setNumFeatures(model.numFeatures());
            setNumCategories(model.numCategories());
        } catch (IOException ioe) {
            log.error("Error reading model: " + ioe.getMessage());
        }
    }

    @Override
    public Vector classify(DenseVector d) {
        return model.classifyFull(d);
    }
}
