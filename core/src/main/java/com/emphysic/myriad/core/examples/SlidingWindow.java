/*
 * com.emphysic.myriad.core.examples.SlidingWindow
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.examples;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.SlidingWindowOperation;

/**
 * Demonstrates use of the "sliding window" algorithm to examine small subsets of a larger Dataset.
 * Created by ccoughlin on 9/12/2016.
 */
public class SlidingWindow {

    public static void main(String[] args) throws Exception {
        int numRows = 5;
        int numCols = 7;
        Dataset inp = new Dataset(numCols, numRows);
        double val = 1;
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                inp.set(j, i, val);
                val++;
            }
        }
        System.out.println("Input:");
        inp.prettyPrint(System.out);
        System.out.println("");

        int step = 1;
        int windowWidth = 2;
        int windowHeight = 3;
        System.out.println("Sliding across input with window " + windowWidth + "x" + windowHeight + ", stepsize " + step + ".\n");
        SlidingWindowOperation slider = new SlidingWindowOperation(step, windowWidth, windowHeight);
        Dataset res;
        int win = 1;
        while ((res = slider.run(inp)) != null) {
            System.out.println("Window #" + win + ", UL corner at (" + slider.getXoffset() + ", " + slider.getYoffset() + ")");
            res.prettyPrint(System.out);
            System.out.println();
            win++;
        }
    }
}
