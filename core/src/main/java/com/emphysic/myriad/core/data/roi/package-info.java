/**
 * This package contains classes for detecting Regions Of Interest (ROI) such as NDE flaw signals, faces, or
 * object recognition.
 * Copyright (c) 2016 Emphysic LLC.  All rights reserved.
 */
package com.emphysic.myriad.core.data.roi;