/*
 * com.emphysic.myriad.core.experimental.roi.GradMachineROIFinder
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.experimental.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.roi.MLROIConfFinder;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import lombok.extern.slf4j.Slf4j;
import org.apache.mahout.classifier.sgd.GradientMachine;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.Vector;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Random;

/**
 * Region Of Interest (ROI) finder based on Mahout's GradientMachine, an online gradient machine with one hidden
 * sigmoid layer that minimizes the hinge loss.
 * Created by ccoughlin on 11/2/2016.
 */
@Slf4j
public class GradMachineROIFinder extends MLROIConfFinder {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * The ROI detection model
     */
    private GradientMachine model;
    /**
     * Number of features in the feature space
     */
    private int numFeatures;
    /**
     * Number of classes (defaults to 2, i.e. ROI and notROI)
     */
    private int numCategories = 2;
    /**
     * Number of nodes in the hidden layer (default 100)
     */
    private int numHidden = 100;
    /**
     * Learning rate (default 0.1)
     */
    private double learningRate = 0.1;
    /**
     * Sparsity - a positive number between 0-1 that controls the sparsity of the hidden layer (default 0.1)
     */
    private double sparsity = 0.1;
    /**
     * Regularization parameter - controls the size of the weight vector (default 0.1)
     */
    private double regularization = 0.1;

    /**
     * Default no-arg constructor for serialization
     */
    public GradMachineROIFinder() {}

    /**
     * Constructor.
     * @param numCats number of categories in the data
     * @param numHidden number of nodes in the hidden layer
     * @param learningRate learning rate (0-1)
     * @param sparsity sparsity of hidden layer (0-1)
     * @param regularization reqularization of weight vector
     */
    public GradMachineROIFinder(int numCats, int numHidden, double learningRate, double sparsity, double regularization) {
        this.numCategories = numCats;
        this.numHidden = numHidden;
        this.learningRate = learningRate;
        this.sparsity = sparsity;
        this.regularization = regularization;
    }

    /**
     * Constructor.
     * @param numCats number of categories in the data
     */
    public GradMachineROIFinder(int numCats) {
        this.numCategories = numCats;
    }

    /**
     * Trains the flaw finder on new data.  Initializes the model if required and sets the number of features.
     *
     * @param X N examples with M features per example
     * @param y N labels for the N examples in X
     * @throws Exception if an error occurs
     */
    @Override
    public void train(double[][] X, int[] y) throws Exception {
        if (model == null) {
            numFeatures = X[0].length;
            initModel();
        }
        for (int i = 0; i < y.length; i++) {
            model.train(y[i], new DenseVector(X[i]));
        }
    }

    /**
     * Initializes the model.
     */
    private void initModel() {
        log.info("Initializing model");
        model = new GradientMachine(numFeatures, numHidden, numCategories);
        model.initWeights(new Random());
        model.learningRate(learningRate).sparsity(sparsity).regularization(regularization);
    }

    /**
     * The numeric label assigned to positive samples i.e. samples that contain ROI
     *
     * @return the value assigned to positive samples
     */
    @Override
    public double positiveClass() {
        return 1;
    }

    /**
     * The numeric label assigned to negative samples i.e. samples that do not contain ROI
     *
     * @return the value assigned to negative samples
     */
    @Override
    public double negativeClass() {
        return 0;
    }

    /**
     * Examine an array of data and report whether it appears to contain a region of interest (ROI)
     *
     * @param data raw data to examine
     * @return true if data appears to contain an ROI, false otherwise
     */
    @Override
    public boolean isROI(double[] data) {
        return false;
    }

    /**
     * Examine a dataset and return whether or not it seems to contain a region of interest (ROI)
     *
     * @param dataset data to examine
     * @return true if data appears to contain an ROI, false otherwise
     */
    @Override
    public boolean isROI(Dataset dataset) {
        return false;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = GradMachineROIFinder.super.getObjectMap();
        map.put("categories", numCategories);
        map.put("features", numFeatures);
        map.put("hidden", numHidden);
        map.put("learning_rate", learningRate);
        map.put("regularization", regularization);
        map.put("confidence_threshold", confThr);
        map.put("model_fields", writableWriteToBytes(model));
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        numCategories = (int)objectMap.getOrDefault("categories", numCategories);
        numFeatures = (int) objectMap.getOrDefault("features", numFeatures);
        numHidden = (int) objectMap.getOrDefault("hidden", numHidden);
        learningRate = (double) objectMap.getOrDefault("learning_rate", learningRate);
        regularization = (double) objectMap.getOrDefault("regularization", regularization);
        confThr = (double) objectMap.getOrDefault("confidence_threshold", confThr);
        if (objectMap.containsKey("model_fields")) {
            byte[] arr = (byte[]) objectMap.get("model_fields");
            ByteArrayInputStream bais = new ByteArrayInputStream(arr);
            DataInputStream dis = new DataInputStream(bais);
            model = new GradientMachine(numFeatures, numHidden, numCategories);
            try {
                model.readFields(dis);
            } catch (IOException ioe) {
                log.error("Error encountered reading model: " + ioe.getMessage());
            }
        }
    }

    public void legacyWrite(Kryo kryo, Output output) {
        try {
            kryo.writeObject(output, new Integer(numFeatures));
            kryo.writeObject(output, new Integer(numCategories));
            kryo.writeObject(output, new Integer(numHidden));
            kryo.writeObject(output, new Double(learningRate));
            kryo.writeObject(output, new Double(sparsity));
            kryo.writeObject(output, new Double(regularization));
            kryo.writeObject(output, new Double(getConfidenceThreshold()));
            model.write(new DataOutputStream(output));
        } catch (IOException ioe) {
            log.error("Unable to serialize: {}", ioe);
        }
    }

    public void legacyRead(Kryo kryo, Input input) {
        try {
            numFeatures = kryo.readObject(input, Integer.class);
            numCategories = kryo.readObject(input, Integer.class);
            numHidden = kryo.readObject(input, Integer.class);
            learningRate = kryo.readObject(input, Double.class);
            sparsity = kryo.readObject(input, Double.class);
            regularization = kryo.readObject(input, Double.class);
            confThr = kryo.readObject(input, Double.class);
            initModel();
            model.readFields(new DataInputStream(input));
        } catch (IOException ioe) {
            log.error("Unable to deserialize: {}", ioe);
        }
    }

    public Vector classify(DenseVector d) {
        return model.classifyFull(d);
    }

    public GradientMachine getModel() {
        return model;
    }

    public void setModel(GradientMachine model) {
        this.model = model;
    }

    public int getNumFeatures() {
        return numFeatures;
    }

    public int getNumCategories() {
        return numCategories;
    }

    public int getNumHidden() {
        return numHidden;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public double getSparsity() {
        return sparsity;
    }

    public double getRegularization() {
        return regularization;
    }

}
