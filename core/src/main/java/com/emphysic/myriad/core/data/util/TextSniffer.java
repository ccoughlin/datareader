/*
 * com.emphysic.myriad.core.data.util.TextSniffer
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

import com.emphysic.myriad.core.data.io.IODataset;
import com.emphysic.myriad.core.data.io.TextDataset;
import org.apache.commons.lang3.ArrayUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

/**
 * TextSniffer - attempts to read a delimited text file automatically
 */
public class TextSniffer implements Sniffer {

    /**
     * Commonly-used delimiters
     */
    private String[] commonDelims = {"\\s", ","};
    /**
     * Commonly-used comment characters
     */
    private String[] commonCommentChars = {"#", ";", "'", "/"};

    /**
     * Examines the specified file and attempts to automatically create a Dataset from the contents.
     *
     * @param inputFile file to examine
     * @return best guess at the corresponding Dataset, or null if unable to parse
     * @throws IOException if an I/O error occurs
     */
    @Override
    public TextDataset sniff(File inputFile) throws IOException {
        for (String delim : commonDelims) {
            int width = 0;
            int length = 0;
            String commentChar = null;
            BufferedReader br = new BufferedReader(new FileReader(inputFile));
            String line;
            while ((line = br.readLine()) != null) {
                if (ArrayUtils.indexOf(commonCommentChars, line.charAt(0)) == -1) {
                    double[] els = enumerate(tokenize(line, delim));
                    if (els == null) {
                        continue;
                    }
                    if (els.length > width) {
                        // Attempt to square off the dataset if req'd
                        width = els.length;
                    }
                    length++;
                } else {
                    commentChar = String.valueOf(line.charAt(0));
                }
            }
            if (width > 0 && length > 0) {
                return new TextDataset(inputFile, delim, commentChar, width, length);
            }
        }
        return null;
    }

    /**
     * Splits a line into tokens.
     * @param line line to split
     * @param delimRegex regular expression representing the delimiter
     * @return array of tokens
     */
    private String[] tokenize(String line, String delimRegex) {
        return line.split(delimRegex);
    }

    /**
     * Converts an array of String tokens into their numeric values.
     * @param elements String tokens to convert
     * @return array of doubles or null if one or more elements couldn't be converted.
     */
    private double[] enumerate(String[] elements) {
        try {
            double[] vals = new double[elements.length];
            for (int i = 0; i < elements.length; i++) {
                vals[i] = Double.valueOf(elements[i]);
            }
            return vals;
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    // Basic demo of usage
    public static void main(String[] args) throws Exception {
        URL cscanUrl = Thread.currentThread().getContextClassLoader().getResource("data/io/sampledata/140.csv");
        File inputFile = new File(cscanUrl.getPath());
        TextSniffer sniffer = new TextSniffer();
        IODataset tds = sniffer.sniff(inputFile);
        if (tds != null) {
            tds.read();
            tds.prettyPrint();
            System.out.println(tds.getWidth() + "x" + tds.getHeight());
        } else {
            System.out.println("Couldn't automatically read file.");
        }
    }
}
