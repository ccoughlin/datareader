/*
 * Copyright (c) 2016 Emphysic LLC.  All rights reserved.
 */

/**
 * This package contains code that for one reason or another should be considered experimental.  Use with caution!
 * Created by chris on 9/1/2016.
 */
package com.emphysic.myriad.core.experimental;