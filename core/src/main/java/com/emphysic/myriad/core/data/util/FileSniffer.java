/*
 * com.emphysic.myriad.core.data.util.FileSniffer
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

import com.emphysic.myriad.core.data.io.DicomDataset;
import com.emphysic.myriad.core.data.io.IODataset;
import com.emphysic.myriad.core.data.io.ImageDataset;
import com.emphysic.myriad.core.data.io.TextDataset;
import com.pixelmed.dicom.DicomException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * FileSniffer - reads Datasets from files
 */
@Slf4j
public class FileSniffer implements Sniffer {
    /**
     * Default text file extensions
     */
    private String[] defaultTextExtensions = {"txt", "dat", "asc", "csv", "tab", "text"};
    /**
     * Case-insensitive text file extensions
     */
    private Set<String> textExtensions = new HashSet<>(Arrays.asList(defaultTextExtensions));

    /**
     * Default image file extensions
     */
    private String[] defaultImageExtensions = ImageIO.getWriterFormatNames();
    /**
     * Case-insensitive image file extensions
     */
    private Set<String> imageExtensions = new HashSet<>(Arrays.asList(defaultImageExtensions));
    /**
     * Default DICOM/DICONDE file extensions
     */
    private String[] defaultDICOMExtensions = {"dcm"};
    /**
     * Case-insensitive DICOM/DICONDE file extensions
     */
    private Set<String> dicomExtensions = new HashSet<>(Arrays.asList(defaultDICOMExtensions));

    /**
     * If true, attempt to match by every type.  If false (default), base on filename extension only.
     */
    private boolean exhaustive = false;

    /**
     * List of printable ASCII codes - files that consist only of these characters are assumed to be ASCII files
     */
    private final static int[] asciiCodes = {
              9,  10,  11,  12,  13,  32,  33,  34,  35,  36,
             37,  38,  39,  40,  41,  42,  43,  44,  45,  46,
             47,  48,  49,  50,  51,  52,  53,  54,  55,  56,
             57,  58,  59,  60,  61,  62,  63,  64,  65,  66,
             67,  68,  69,  70,  71,  72,  73,  74,  75,  76,
             77,  78,  79,  80,  81,  82,  83,  84,  85,  86,
             87,  88,  89,  90,  91,  92,  93,  94,  95,  96,
             97,  98,  99, 100, 101, 102, 103, 104, 105, 106,
            107, 108, 109, 110, 111, 112, 113, 114, 115, 116,
            117, 118, 119, 120, 121, 122, 123, 124, 125, 126
    };

    /**
     * Instantiate a new sniffer
     *
     * @param exhaustive whether to match by filename only (false) or to attempt every match (true).
     */
    public FileSniffer(boolean exhaustive) {
        this.exhaustive = exhaustive;
    }

    /**
     * Default constructor - exhaustive search is disabled
     */
    public FileSniffer() {
        this(false);
    }

    /**
     * Attempts to automatically return a Dataset from a supported file format.  Initial search is conducted
     * based on filename extension; if exhaustive search was enabled on instantiation it will also attempt
     * to parse the file regardless of name.
     *
     * @param inputFile File to examine
     * @return Dataset of the first matching type found, or null if a match couldn't be found.
     * @throws IOException if an I/O error occurs
     * @see FileSniffer#exhaustive
     */
    @Override
    public IODataset sniff(File inputFile) throws IOException {
        TextDataset tds;
        ImageDataset ids;
        DicomDataset dds;
        String ext = FilenameUtils.getExtension(inputFile.getAbsolutePath());
        boolean hasExtension = ext.length() > 0;
        // 1. Check extension
        if (hasExtension) {
            ext = ext.toLowerCase();
            if (textExtensions.contains(ext)) {
                tds = readAsText(inputFile);
                if (tds != null) {
                    return tds;
                }
            }
            if (imageExtensions.contains(ext)) {
                ids = readAsImage(inputFile);
                if (ids != null) {
                    return ids;
                }
            }
            if (dicomExtensions.contains(ext)) {
                dds = readAsDICOM(inputFile);
                if (dds != null) {
                    return dds;
                }
            }
            log.info("Couldn't identify " + inputFile + " by extension");
        }
        // 2. Check if appears to be text
        if (isText(inputFile)) {
            log.info("File " + inputFile + " identified as text");
            return readAsText(inputFile);
        }
        // 3.  Try them all
        if (exhaustive) {
            log.info("Attempting exhaustive match of file " + inputFile);
            dds = readAsDICOM(inputFile);
            if (dds != null) {
                log.info("Matched as DICOM");
                return dds;
            }
            ids = readAsImage(inputFile);
            if (ids != null) {
                log.info("Matched as image");
                return ids;
            }
            tds = readAsText(inputFile);
            if (tds != null) {
                log.info("Matched as text");
                return tds;
            }
            log.info("Exhaustive match for file " + inputFile + " failed!");
        }
        log.warn("Couldn't determine filetype for " + inputFile + " returning null");
        return null;
    }

    /**
     * Attempts to automatically identify the contents of the specified file.  Does not load the
     * file contents i.e. use IODataset.read() to access.
     *
     * @param inputFile file to read
     * @return IODataset instance, or null if the file couldn't be read
     * @throws IOException if an error occurs
     */
    public static IODataset read(File inputFile) throws IOException {
        return read(inputFile, false);
    }

    /**
     * Attempts to automatically identify the contents in the specified file.
     *
     * @param inputFile    file to read
     * @param loadContents whether to load the contents (true) or just initialize (false)
     * @return the dataset or null if unable to read
     * @throws IOException if an error occurs
     */
    public static IODataset read(File inputFile, boolean loadContents) throws IOException {
        FileSniffer sniffer = new FileSniffer();
        IODataset dataset = sniffer.sniff(inputFile);
        if (dataset != null && loadContents) {
            dataset.read();
        }
        return dataset;
    }

    /**
     * Attempts to determine whether a file consists of text or is binary.  Based on the approach detailed at
     * http://stackoverflow.com/a/277568/151744 .  Attempts to read the first four bytes for an initial check against
     * known UTF headers, then falls back to reading the entire contents looking for non-ASCII characters.
     * @param file file to examine
     * @return true if the file appears to be text, false otherwise
     * @throws IOException if an I/O error occurs
     */
    public static boolean isText(File file) throws IOException {
        // Check against common UTF headers
        RandomAccessFile f = new RandomAccessFile(file, "r");
        byte[] header = new byte[4];
        f.read(header);
        if (((int) header[0] == 0xFF && (int) header[1] == 0xFE) || // Suggests the file is UTF-16 LE or UTF-32 LE
                ((int) header[0] == 0x00 && (int) header[1] == 0x00 && (int) header[2] == 0xFE && (int) header[3] == 0xFF) || // Suggests the file is UTF-32 BE
                ((int) header[0] == 0xFE && (int) header[1] == 0xFF) ) { // Suggests the file is UTF-16 BE
            return true;
        }
        // Check for non-printable ASCII content
        byte[] data = Files.readAllBytes(file.toPath());
        for (byte b: data) {
            if (!ArrayUtils.contains(asciiCodes, (int)b)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Reads a text file
     *
     * @param inputFile file to read
     * @return Dataset of contents, or null if the file couldn't be read
     */
    public TextDataset readAsText(File inputFile) {
        try {
            return new TextSniffer().sniff(inputFile);
        } catch (StringIndexOutOfBoundsException e) {
            // Couldn't interpret as String (binary, etc.)
            log.error("Couldn't read file " + inputFile + ": " + e.getMessage());
        } catch (IOException ioe) {
            log.error("General I/O error reading " + inputFile + ": " + ioe.getMessage());
        }
        return null;
    }

    /**
     * Reads an image.
     *
     * @param inputFile file to read
     * @return Dataset of pixels, or null if the file couldn't be read
     */
    public ImageDataset readAsImage(File inputFile) {
        try {
            return new ImageDataset(inputFile);
        } catch (IOException ioe) {
            // Couldn't recognize file, not supported, etc.
            log.error("Couldn't read file " + inputFile + ": " + ioe.getMessage());
        }
        return null;
    }

    /**
     * Reads a DICOM/DICONDE file.
     *
     * @param inputFile file to read
     * @return Dataset or null if the file couldn't be read.
     */
    public DicomDataset readAsDICOM(File inputFile) {
        try {
            DicomDataset dds = new DicomDataset(inputFile);
            // DICOM reader doesn't automatically fail - need to see if it can actually read the data
            // TODO: closer look at com.pixelmed.validate.DicomSRValidator - seems to fail on current test dataset
            dds.read();
            if (dds.getData() != null) {
                return dds;
            }
        } catch (DicomException e) {
            log.warn("Couldn't read " + inputFile + " as DICOM/DICONDE: " + e.getMessage());
        } catch (IOException ioe) {
            log.error("General I/O error reading " + inputFile + ": " + ioe.getMessage());
        }
        return null;
    }

    /**
     * Retrieves current list of text file extensions (no leading ".")
     *
     * @return text file extensions
     */
    public Set<String> getTextExtensions() {
        return textExtensions;
    }

    /**
     * Adds a text file extension to the case-insensitive set e.g. addTextExtension("TXT").
     *
     * @param extension extension to add
     */
    public void addTextExtension(String extension) {
        textExtensions.add(extension.toLowerCase());
    }

    /**
     * Removes a text file extension if present
     *
     * @param extension extension to remove
     */
    public void delTextExtension(String extension) {
        textExtensions.remove(extension);
    }

    /**
     * Retrieves the current list of case-insensitive image file extensions (no leading ".")
     *
     * @return image file extensions
     */
    public Set<String> getImageExtensions() {
        return imageExtensions;
    }

    /**
     * Adds an image file extensions to the case-insensitive set e.g. addImageExtension("JPEG").
     *
     * @param extension extension to add
     */
    public void addImageExtension(String extension) {
        imageExtensions.add(extension.toLowerCase());
    }

    /**
     * Removes an image file extension if present.
     *
     * @param extension extension to remove
     */
    public void delImageExtension(String extension) {
        imageExtensions.remove(extension);
    }

    /**
     * Retrieves the current set of case-insensitive DICOM/DICONDE file extensions (no leading ".")
     *
     * @return DICOM extensions
     */
    public Set<String> getDicomExtensions() {
        return dicomExtensions;
    }

    /**
     * Adds a DICOM file extension to the current case-insensitive set e.g. addDICOMExtension("DCM")
     *
     * @param extension extension to add
     */
    public void addDICOMExtension(String extension) {
        dicomExtensions.add(extension.toLowerCase());
    }

    /**
     * Removes a DICOM/DICONDE extension if present.
     *
     * @param extension extension to remove
     */
    public void delDICOMExtension(String extension) {
        dicomExtensions.remove(extension.toLowerCase());
    }

    /**
     * Sets whether to perform exhaustive matching or not.
     *
     * @param exhaustive false to rely on filename extension alone, or true to attempt to read the file regardless of
     *                   extension.
     */
    public void setExhaustive(boolean exhaustive) {
        this.exhaustive = exhaustive;
    }

    /**
     * Whether an input file is read based on extension or by an exhaustive match.
     *
     * @return true if the file is read by exhaustive match, false if the file is read by extension alone
     */
    public boolean isExhaustive() {
        return this.exhaustive;
    }
}
