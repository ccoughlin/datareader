/*
 * com.emphysic.myriad.core.data.ops.GaussianBlur
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;

/**
 * GaussianBlur - a fast approximation of Gaussian Blur performed by multiple passes of a Box Blur.  Typically 3
 * passes are used, which is ~97% of a "real" Gaussian Blur.
 */
public class GaussianBlur extends BoxBlur {
    /**
     * Number of box blur passes (3 passes is ~ 97% of a "real" Gaussian blur)
     */
    private int numPasses = 3;

    /**
     * Creates a new Gaussian Blur of the specified radius.  Uses 3 Box Blur passes.
     * @param radius radius of the blur
     */
    public GaussianBlur(int radius) {
        super(radius);
    }

    /**
     * Creates a new Gaussian Blur of radius 5.
     */
    public GaussianBlur() {
        this(5);
    }

    /**
     * Creates a new Gaussian Blur of the specified radius and number of Box Blur passes.
     * @param radius radius of the blur
     * @param numPasses number of Box Blurs to perform to approximate a single Gaussian Blur
     */
    public GaussianBlur(int radius, int numPasses) {
        super(radius);
        this.numPasses = numPasses;
        if (this.numPasses < 1) {
            this.numPasses = 1;
        }
    }

    /**
     * Performs a Gaussian blur on the input.
     * @param dataSet Dataset to blur
     * @return blurred result, or null if input was null.
     */
    @Override
    public Dataset run(Dataset dataSet) {
        if (dataSet != null) {
            Dataset result = super.run(dataSet);
            for (int i = 0; i < numPasses - 1; i++) {
                result = super.run(result);
            }
            return result;
        }
        return null;
    }
}
