/*
 * com.emphysic.myriad.core.data.io.DicomDataset
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.io;

import com.pixelmed.dicom.DicomException;
import com.pixelmed.display.SourceImage;
import lombok.extern.slf4j.Slf4j;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * DicomDataset - ImageDataset read from DICOM/DICONDE input files.
 */
@Slf4j
public class DicomDataset extends ImageDataset {
    /**
     * DICOM / DICONDE reader
     */
    private SourceImage reader;

    /**
     * Creates a new DicomDataset from the specified file
     * @param dataFileName DICOM file to read
     * @throws IOException if an I/O error occurs
     * @throws DicomException if the file doesn't appear to be valid DICOM/DICONDE, etc.
     */
    public DicomDataset(String dataFileName) throws IOException, DicomException {
        this.source = new File(dataFileName);
        reader = new SourceImage(dataFileName);
    }

    /**
     * Creates a new DicomDataset from the specified file
     * @param dataSource DICOM file to read
     * @throws IOException if an I/O error occurs
     * @throws DicomException if the file doesn't appear to be valid DICOM/DICONDE
     */
    public DicomDataset(File dataSource) throws IOException, DicomException {
        this(dataSource.getAbsolutePath());
    }

    /**
     * Reads a specified frame from the DICOM file
     * @param idx index to read
     * @return contents of the given frame
     * @throws IOException if an I/O error occurs
     * @throws IndexOutOfBoundsException if idx &lt; 0 || idx &gt; number of frames in the file
     */
    @Override
    public double[] read(int idx) throws IOException {
        if (idx >= 0 && idx < getNumFrames()) {
            BufferedImage image = reader.getBufferedImage(idx);
            this.width = image.getWidth();
            this.height = image.getHeight();
            double[] ddata = getData(image, 0, 0, width, height);
            return ddata;
        } else {
            throw new IndexOutOfBoundsException("Invalid frame specified, must be between 0 " + " and " + getNumFrames());
        }
    }

    /**
     * Returns the number of frames in the file.
     * @return number of frames
     */
    @Override
    public int getNumFrames() {
        return reader.getNumberOfFrames();
    }
}
