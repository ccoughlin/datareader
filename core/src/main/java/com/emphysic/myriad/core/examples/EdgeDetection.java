/*
 * com.emphysic.myriad.core.examples.EdgeDetection
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.examples;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.io.IODataset;
import com.emphysic.myriad.core.data.ops.*;
import com.emphysic.myriad.core.data.util.FileSniffer;

import java.io.File;
import java.net.URL;

/**
 * Demonstrates the edge detection operations.
 * Created by ccoughlin on 9/9/16.
 */
public class EdgeDetection {
    public static void main(String[] args) {
        IODataset input = null;
        try {
            URL testJpgURL = Thread.currentThread().getContextClassLoader().getResource("data/io/test.jpg");
            File cannyOut = new File("canny.txt");
            File dogOut = new File("dog.txt");
            File pOut = new File("prewitt.txt");
            File scOut = new File("scharr.txt");
            File soOut = new File("sobel.txt");
            if (args == null || args.length == 0) {
                System.out.print("No input file specified, attempting to read demo image...");
                input = FileSniffer.read(new File(testJpgURL.getPath()), true);
                System.out.println("ok");
            } else {
                System.out.println("Attempting to read file " + args[0]);
                input = FileSniffer.read(new File(args[0]), true);
            }
            if (input != null) {
                System.out.println("Input file " + input.getWidth() + "x" + input.getHeight());

                System.out.println("Canny edge detection saved to " + cannyOut.getAbsolutePath());
                canny(input).write(cannyOut);

                System.out.println("Difference of Gaussians (DOG) edge detection saved to " + dogOut.getAbsolutePath());
                dog(input).write(dogOut);

                System.out.println("Prewitt edge detection saved to " + pOut.getAbsolutePath());
                prewitt(input).write(pOut);

                System.out.println("Scharr edge detection saved to " + scOut.getAbsolutePath());
                scharr(input).write(scOut);

                System.out.println("Sobel edge detection saved to " + soOut.getAbsolutePath());
                sobel(input).write(soOut);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Performs Canny edge detection on a dataset https://en.wikipedia.org/wiki/Canny_edge_detector
     * @param input input data
     * @return results of operation or null if input was null
     */
    public static Dataset canny(Dataset input) {
        return new CannyOperation().run(input);
    }

    /**
     * Performs Difference of Gaussian (DOG) edge detection (technically more of a feature enhancer) on a dataset
     * https://en.wikipedia.org/wiki/Difference_of_Gaussians
     * @param input input data
     * @return results of operation or null if input was null
     */
    public static Dataset dog(Dataset input) {
        return new DifferenceOfGaussiansOperation(1, 2).run(input);
    }

    /**
     * Performs Prewitt edge detection on a dataset https://en.wikipedia.org/wiki/Prewitt_operator
     * @param input input data
     * @return results of operation or null if input was null
     */
    public static Dataset prewitt(Dataset input) {
        return new PrewittOperation().run(input);
    }

    /**
     * Performs Scharr edge detection on a dataset
     * https://en.wikipedia.org/wiki/Sobel_operator#Alternative_operators
     * @param input input data
     * @return results of operation or null if input was null
     */
    public static Dataset scharr(Dataset input) {
        return new ScharrOperation().run(input);
    }

    /**
     * Performs Sobel edge detection on a dataset https://en.wikipedia.org/wiki/Sobel_operator
     * @param input input data
     * @return results of operation or null if input was null
     */
    public static Dataset sobel(Dataset input) {
        return new SobelOperation().run(input);
    }
}
