/**
 * This package contains classes for reading and manipulating data.
 * Copyright (c) 2016 Emphysic LLC.  All rights reserved.
 */
package com.emphysic.myriad.core.data;