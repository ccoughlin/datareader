/*
 * com.emphysic.myriad.core.data.util.ExternalProcess
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ExternalProcess - wrapper class for execution and interaction with an external process
 */
@Slf4j
public class ExternalProcess {
    /**
     * Environment variables for the process.  Defaults to the current environment variables.
     */
    private Map<String, String> env;
    /**
     * Command line arguments for the process
     */
    private List<String> args;
    /**
     * Working folder for the process.  Defaults to the user's working directory.
     */
    transient private File workingFolder;
    /**
     * User's working directory.
     */
    transient private static File defaultWorkingFolder;
    /**
     * Command to run
     */
    private String cmd;
    /**
     * Handle to the external process
     */
    transient private Process process;

    /**
     * Empty constructor for serialization
     */
    public ExternalProcess() {
        defaultWorkingFolder = new File(System.getProperty("user.dir"));
    }

    /**
     * Creates a new ExternalProcess
     * @param cmd command to execute
     * @param args command line arguments
     * @param environment environment variables
     * @param workingFolder working folder
     */
    public ExternalProcess(String cmd, List<String> args, Map<String, String> environment, File workingFolder) {
        this.cmd = cmd;
        if (args != null) {
            this.args = new ArrayList<>(args);
        } else {
            this.args = new ArrayList<>();
        }
        this.args.add(0, cmd);
        if (environment != null) {
            this.env = new HashMap<>(environment);
        } else {
            this.env = null;
        }
        if (workingFolder != null) {
            this.workingFolder = new File(workingFolder.getAbsolutePath());
        } else {
            this.workingFolder = defaultWorkingFolder;
        }
    }

    /**
     * Creates a new ExternalProcess with no command line arguments, default working folder, and the current
     * environment variables.
     * @param cmd command
     */
    public ExternalProcess(String cmd) {
        this(cmd, new ArrayList<>(), null, defaultWorkingFolder);
    }

    /**
     * Creates a new ExternalProcess with the default working folder and the current environment variables.
     * @param cmd command
     * @param args command line arguments
     */
    public ExternalProcess(String cmd, List<String> args) {
        this(cmd, args, null, defaultWorkingFolder);
    }

    /**
     * Copy constructor
     * @param orig original ExternalProcess to copy
     */
    public ExternalProcess(ExternalProcess orig) {
        this.cmd = orig.getCmd();
        this.args = new ArrayList<>(orig.getArgs());
        if (orig.getEnv() != null) {
            this.env = new HashMap<>(orig.getEnv());
        }
        this.workingFolder = new File(orig.getWorkingFolder().getAbsolutePath());
    }

    /**
     * Adds/overwrites an environment variable.
     * @param key environment variable name
     * @param value environment variable value
     */
    public void addEnvironmentVariable(String key, String value) {
        env.put(key, value);
    }

    /**
     * Removes an environment variable if it exists.
     * @param key key to remove
     */
    public void delEnvironmentVariable(String key) {
        if (env.containsKey(key)) {
            env.remove(key);
        }
    }

    /**
     * Adds a command line argument
     * @param arg argument to add
     * @param index position of the argument
     */
    public void addArgument(String arg, int index) {
        args.add(index, arg);
    }

    /**
     * Appends a command line argument to the end of the current list.
     * @param arg argument to append
     */
    public void addArgument(String arg) {
        args.add(arg);
    }

    /**
     * Removes a command line argument if it exists.
     * @param arg argument to remove
     */
    public void delArgument(String arg) {
        args.remove(arg);
    }

    /**
     * Starts the external process
     *
     * @throws IOException if an error occurs
     * @throws NullPointerException if the command or any argument was passed as null
     */
    public void start() throws IOException {
        ProcessBuilder pb = new ProcessBuilder(args);
        if (env != null) {
            pb.environment().putAll(env);
        }
        if (workingFolder != null) {
            pb.directory(workingFolder);
        }
        pb.redirectErrorStream(true);
        try {
            process = pb.start();
        } catch (NullPointerException npe) {
            log.error("One or more arguments was null, args=" + args);
        }
    }

    /**
     * Returns the environment variable settings for the external process.
     * @return process' environment
     */
    public Map<String, String> getEnv() {
        return env;
    }

    /**
     * Sets the environment for the external process.
     * @param env new environment
     */
    public void setEnv(Map<String, String> env) {
        this.env = env;
    }

    /**
     * Returns the list of arguments, where the first element is the command to run i.e. args.get(0) = cmd.
     *
     * @return command line arguments
     */
    public List<String> getArgs() {
        return args;
    }

    /**
     * Sets the command line arguments.
     * @param args arguments
     */
    public void setArgs(List<String> args) {
        this.args = new ArrayList<>(args);
    }

    /**
     * Sets the working folder for the external process.
     * @param workingFolder new working folder
     */
    public void setWorkingFolder(File workingFolder) {
        this.workingFolder = workingFolder;
    }

    /**
     * Gets the current working folder for the external process.
     * @return working folder
     */
    public File getWorkingFolder() {
        return this.workingFolder;
    }

    /**
     * Gets the current command
     * @return command
     */
    public String getCmd() {
        return cmd;
    }

    /**
     * Sets the command to execute.
     * @param cmd command
     */
    public void setCmd(String cmd) {
        this.cmd = cmd;
        args.set(0, cmd);
    }

    /**
     * Returns a handle to the external process
     * @return handle, or null if the process is not running @see #ExternalProcess.start()
     */
    public Process getProcess() {
        return process;
    }
}
