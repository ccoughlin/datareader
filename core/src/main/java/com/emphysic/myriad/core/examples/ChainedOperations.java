/*
 * com.emphysic.myriad.core.examples.ChainedOperations
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.examples;

import com.emphysic.myriad.core.data.io.IODataset;
import com.emphysic.myriad.core.data.ops.ChainedDatasetOperation;
import com.emphysic.myriad.core.data.ops.GaussianBlur;
import com.emphysic.myriad.core.data.ops.NormalizeSignalOperation;
import com.emphysic.myriad.core.data.ops.SobelOperation;
import com.emphysic.myriad.core.data.util.FileSniffer;

import java.io.File;
import java.net.URL;

/**
 * Demonstrates how to "chain" Dataset operations by running a blur, edge detection, and normalization.
 * A ChainedDatasetOperation performs multiple DatasetOperations in sequence but acts like a single operation.
 * Operations can be added at any time.
 * Created by ccoughlin on 9/9/16.
 */
public class ChainedOperations {
    public static void main(String[] args) {
        IODataset input = null;
        try {
            URL testJpgURL = Thread.currentThread().getContextClassLoader().getResource("data/io/test.jpg");
            File output = new File("blur_sobel_norm.txt");
            if (args == null || args.length == 0) {
                System.out.print("No input file specified, attempting to read demo image...");
                input = FileSniffer.read(new File(testJpgURL.getPath()), true);
                System.out.println("ok");
            } else {
                System.out.println("Attempting to read file " + args[0]);
                input = FileSniffer.read(new File(args[0]), true);
            }
            if (input != null) {
                System.out.println("Input file " + input.getWidth() + "x" + input.getHeight());

                ChainedDatasetOperation chain = new ChainedDatasetOperation();
                chain.withOp(new GaussianBlur(1)).withOp(new SobelOperation()).withOp(new NormalizeSignalOperation());
                System.out.println("Performing Gaussian Blur --> Sobel edge detection --> normalization...");
                chain.run(input).write(output);

                System.out.println("Output saved to " + output.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
