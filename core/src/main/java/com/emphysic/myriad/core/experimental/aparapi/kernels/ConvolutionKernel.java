/*
 * com.emphysic.myriad.core.experimental.aparapi.kernels.ConvolutionKernel
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.experimental.aparapi.kernels;

import com.aparapi.Kernel;

/**
 * ConvolutionKernel - Aparapi kernel for performing convolution operations.
 * Created by ccoughlin on 8/31/16.
 */
public class ConvolutionKernel extends Kernel {
    private double[] idata;
    private double[] odata;
    private double[][] knl;
    private int w;
    private int h;

    public ConvolutionKernel(double[][] kern, double[] input, double[] output, int width, int height) {
        this.knl = kern;
        this.idata = input;
        this.odata = output;
        this.w = width;
        this.h = height;
    }

    public ConvolutionKernel() {}

    @Override
    public void run() {
        int i = getGlobalId(0);
        int j = getGlobalId(1);
        double val = 0.0;
        for (int kw = 0; kw < knl.length; kw++) {
            for (int kh = 0; kh < knl[0].length; kh++) {
                int x = safeIdx(i + kw - knl.length / 2, w);
                int y = safeIdx(j + kh - knl[0].length / 2, h);
                val += knl[kw][kh] * idata[y * w + x];
            }
        }
        odata[j * w + i] = val;
    }

    /**
     * Returns a safe index within bounds.  Identical to {@link com.emphysic.myriad.core.data.util.DatasetUtils}
     * implementation, required because Aparapi does not allow Java objects inside kernels.
     * @param value proposed index
     * @param endIndex end of safe values of indices
     * @return proposed index if safe, 0 if proposed is less than 0, or endIndex-1 if value is greater than endIndex.
     */
    private int safeIdx(int value, int endIndex) {
        if (value < 0)
            return 0;
        if (value < endIndex)
            return value;
        return endIndex - 1;
    }

    /**
     * Input data
     */ /**
     * Retrieves the input data array
     * @return input array
     */
    public double[] getIdata() {
        return idata;
    }

    /**
     * Output data
     */ /**
     * Retrieves the output data array
     * @return output array
     */
    public double[] getOdata() {
        return odata;
    }

    /**
     * Convolution kernel
     */ /**
     * Retrieves the convolution kernel
     * @return convolution kernel
     */
    public double[][] getKnl() {
        return knl;
    }

    /**
     * Width of data
     */ /**
     * Retrieves the width of the data
     * @return width of data
     */
    public int getW() {
        return w;
    }

    /**
     * Height of data
     */ /**
     * Retrieves the height of the dat
     * @return height of data
     */
    public int getH() {
        return h;
    }

    public void setIdata(double[] idata) {
        this.idata = idata;
    }

    public void setOdata(double[] odata) {
        this.odata = odata;
    }

    public void setKnl(double[][] knl) {
        this.knl = knl;
    }

    public void setW(int w) {
        this.w = w;
    }

    public void setH(int h) {
        this.h = h;
    }
}
