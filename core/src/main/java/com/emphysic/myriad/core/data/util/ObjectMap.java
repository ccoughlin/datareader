/*
 * com.emphysic.myriad.core.data.util.ObjectMap
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * ObjectMap - Kryo serialization of an instance's object graph.
 * Created by ccoughlin on 4/4/2017.
 */
public interface ObjectMap extends KryoSerializable {
    /**
     * Returns the current version of the serialization format.  Increments when the serialization format changes, i.e.
     * backwards compatibility is broken.
     * @return serialization version
     */
    long getSerializationVersion();

    /**
     * Returns the current class version.  Class version increments when the class changes but doesn't necessarily
     * break compatibility with serialization.
     * @return class version
     */
    int getVersion();
    /**
     * Creates a map of the important fields for the instance, suitable for serialization.
     * @return "object map" of the instance
     */
    default Map<String, Object> getObjectMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("VERSION", getVersion());
        map.put("SERVERSION", getSerializationVersion());
        return map;
    }

    /**
     * Initializes an instance with an object map.
     * @param objectMap Map of field:value pairs
     * @throws UnsupportedOperationException if serialization versions don't match
     */
    default void init(Map<String, Object> objectMap) {
        long serializationVersion = (long) objectMap.get("SERVERSION");
        if (serializationVersion != getSerializationVersion()) {
            throw new UnsupportedOperationException("Serialization formats do not match, expected "
                    + getSerializationVersion() + " read " + serializationVersion);
        }
        int version = (Integer) objectMap.get("VERSION");
        if (version == getVersion()) {
            // Current version
            initCurrentVersion(objectMap);
        } else if (version < getVersion()) {
            // Previous version(s)
            initPreviousVersion(objectMap, version);
        } else {
            // Future version, not specified, etc.
            initUnknownVersion(objectMap, version);
        }
    }

    /**
     * Initializes an instance with a current-version object graph.
     * @param objectMap object graph for initialization
     */
    void initCurrentVersion(Map<String, Object> objectMap);

    /**
     * Handles requests to initialize an instance with a previous version of an object graph.  Default action is no-op.
     * @param objectMap object graph for initialization
     * @param version version of object graph
     */
    default void initPreviousVersion(Map<String, Object> objectMap, int version) {}

    /**
     * Handles requests to initialize an instance with an unknown (future, undefined, etc.) version of an object graph.
     * Default action is to throw UnsupportedOperationException.
     * @param objectMap object graph
     * @param version version
     * @throws UnsupportedOperationException by default
     */
    default void initUnknownVersion(Map<String, Object> objectMap, int version) {
        throw new UnsupportedOperationException("Version " + version + " not supported.");
    }

    /**
     * Saves a model to disk
     * @param outFile destination file
     * @throws IOException if an I/O error occurs (file not found, insufficient permissions, etc.)
     */
    default void save(File outFile) throws IOException {
        Kryo kryo = new Kryo();
        Output output = new Output(new FileOutputStream(outFile));
        write(kryo, output);
        output.close();
    }

    /**
     * Reads a model from disk
     * @param inFile source file
     * @throws IOException if an I/O error occurs (file not found, insufficient permissions, etc.)
     */
    default void load(File inFile) throws IOException {
        Kryo kryo = new Kryo();
        Input input = new Input(new FileInputStream(inFile));
        read(kryo, input);
    }

    /**
     * Reads a Kryo-serialized object graph.
     * @param kryo Kryo instance
     * @param input Input
     */
    default void read(Kryo kryo, Input input) {
        init((Map<String, Object>) kryo.readClassAndObject(input));
    }

    /**
     * Serializes the current instance as a Kryo object.
     * @param kryo Kryo instance
     * @param output Output
     */
    default void write(Kryo kryo, Output output) {
        Map<String, Object> objectGraph = getObjectMap();
        kryo.writeClassAndObject(output, objectGraph);
        output.flush();
    }
}
