/*
 * com.emphysic.myriad.core.data.ops.BlurOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import java.util.Map;

/**
 * BlurOperation - blurs a Dataset.
 */
public abstract class BlurOperation implements DatasetOperation {
    /**
     * Radius of the blur in points.  Blur window is 2r+1 wide and 2r+1 tall.
     */
    protected int radius;

    /**
     * Creates a new BlurOperation with the specified radius.
     * @param radius of the blur
     */
    public BlurOperation(int radius) {
        this.radius = radius;
    }

    /**
     * Returns the radius of the blur operation
     * @return radius
     */
    public int getRadius() {
        return radius;
    }

    /**
     * Sets the blur radius.
     * @param radius radius of blur operation in points.
     */
    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("radius", radius);
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        radius = (int) objectMap.getOrDefault("radius", radius);
    }
}
