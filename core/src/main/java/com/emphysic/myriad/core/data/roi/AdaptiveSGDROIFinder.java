/*
 * com.emphysic.myriad.core.data.roi.AdaptiveSGDROIFinder
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import lombok.extern.slf4j.Slf4j;
import org.apache.mahout.classifier.sgd.AdaptiveLogisticRegression;
import org.apache.mahout.classifier.sgd.ElasticBandPrior;
import org.apache.mahout.classifier.sgd.PriorFunction;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.Vector;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * AdaptiveSGDROIFinder - machine learning ROI detector that locates regions of interest by maintaining a pool of SGD
 * classifiers.
 */
@Slf4j
public class AdaptiveSGDROIFinder extends MLROIConfFinder {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Number of categories of ROI e.g. 2 for is/isn't a region of interest.
     */
    private int numCategories;
    /**
     * Number of features in each sample.  In Myriad, equivalent to the size of a Dataset @see #Dataset.getSize()
     */
    private int numFeatures;
    /**
     * Regularization function used to limit overfitting
     */
    private PriorFunction priorFunction;
    /**
     * The meta-model that manages the SGD pool
     */
    private AdaptiveLogisticRegression model;
    /**
     * The number of simultaneous threads to use during training.  Defaults to the number of cores on the system.
     */
    private int threadCount = Runtime.getRuntime().availableProcessors();
    /**
     * The number of SGD learners to use.  Defaults to 20.
     */
    private int poolSize = 20;

    /**
     * Creates a new AdaptiveSGDROIFinder with the specified number of categories and the specified
     * regularization function.  Uses all available CPU cores and 20 SGD learners.
     * @param numCats number of categories
     * @param priorFunction prior function to use
     */
    public AdaptiveSGDROIFinder(int numCats, PriorFunction priorFunction) {
        this.numCategories = numCats;
        this.priorFunction = priorFunction;
    }

    /**
     * @param numCats       number of labels in the data (e.g. 2 for is/is not a flaw)
     * @param priorFunction Regularization function to penalize complex models
     * @param threadCount   number of cores to use during training.  Defaults to number of cores on system.
     * @param poolSize      number of learners to train.  Defaults to 20.
     */
    public AdaptiveSGDROIFinder(int numCats, PriorFunction priorFunction, int threadCount, int poolSize) {
        this(numCats, priorFunction);
        this.threadCount = threadCount;
        this.poolSize = poolSize;
    }

    /**
     * Create a flaw finder with 2 categories - is / isn't a flaw, and a linear combination of Laplacian and
     * Gaussian regularization.  Twenty learners are trained, using all the available cores on the system.
     */
    public AdaptiveSGDROIFinder() {
        this(2, new ElasticBandPrior());
    }

    /**
     * Trains the current model with additional samples, creating the model if required.
     * @param X N examples with M features per example
     * @param y N labels for the N examples in X
     * @throws Exception if an error occurs
     */
    @Override
    public void train(double[][] X, int[] y) throws Exception {
        if (model == null) {
            numFeatures = X[0].length;
            model = new AdaptiveLogisticRegression(numCategories, numFeatures, priorFunction, threadCount, poolSize);
        }
        int numPasses = 1000 / y.length + 1;
        for (int pass = 0; pass < numPasses; pass++) {
            // AdaptiveLogisticRegression buffers samples until it has at least 1000
            for (int i = 0; i < y.length; i++) {
                model.train(y[i], new DenseVector(X[i]));
            }
        }
    }

    @Override
    public Vector classify(DenseVector d) {
        return model.getBest().getPayload().getLearner().classifyFull(d);
    }

    /**
     * Predict if a sample contains a region of interest.
     * @param data raw data to examine
     * @return true if the sample appears to contain an ROI, false otherwise.
     */
    @Override
    public boolean isROI(double[] data) {
        return (int)predict(data) != (int)negativeClass();
    }

    /**
     * Predict if a sample contains a region of interest.
     * @param dataset data to examine
     * @return true if the sample appears to contain an ROI, false otherwise.
     */
    @Override
    public boolean isROI(Dataset dataset) {
        return isROI(dataset.getData());
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = AdaptiveSGDROIFinder.super.getObjectMap();
        map.put("categories", numCategories);
        map.put("features", numFeatures);
        map.put("priorfunction", priorFunction);
        map.put("confidence_threshold", confThr);
        map.put("thread_count", threadCount);
        map.put("pool_size", poolSize);
        map.put("model_fields", writableWriteToBytes(model));
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        numCategories = (int)objectMap.getOrDefault("categories", numCategories);
        numFeatures = (int) objectMap.getOrDefault("features", numFeatures);
        confThr = (double) objectMap.getOrDefault("confidence_threshold", confThr);
        threadCount = (int) objectMap.getOrDefault("thread_count", threadCount);
        poolSize = (int) objectMap.getOrDefault("pool_size", poolSize);
        if (objectMap.containsKey("priorfunction")) {
            priorFunction = (PriorFunction) objectMap.get("priorfunction");
        }
        if (objectMap.containsKey("model_fields")) {
            byte[] arr = (byte[]) objectMap.get("model_fields");
            ByteArrayInputStream bais = new ByteArrayInputStream(arr);
            DataInputStream dis = new DataInputStream(bais);
            model = new AdaptiveLogisticRegression();
            try {
                model.readFields(dis);
            } catch (IOException ioe) {
                log.error("Error encountered reading model: " + ioe.getMessage());
            }
        }
    }

    /**
     * Return the number of categories for this model.  Typically 2 for does / does not contain a region of interest.
     * @return number of categories
     */
    public int getNumCategories() {
        return numCategories;
    }

    /**
     * Sets the number of categories for this model.  Note that it may be necessary to retrain the model if this
     * number is changed.
     * @param numCategories number of categories
     */
    public void setNumCategories(int numCategories) {
        this.numCategories = numCategories;
    }

    /**
     * Return the number of features for this model.  In Myriad, this is equivalent to the size of the data the model
     * was trained on i.e. #Dataset.getSize().
     * @return number of features
     */
    public int getNumFeatures() {
        return numFeatures;
    }

    /**
     * Sets the number of features for this model.  Note that changes to this number will invalidate the current model.
     * @param numFeatures number of features
     */
    public void setNumFeatures(int numFeatures) {
        this.numFeatures = numFeatures;
    }

    /**
     * Returns the regularization function used to limit overfitting.
     * @return regularization function.
     */
    public PriorFunction getPriorFunction() {
        return priorFunction;
    }

    /**
     * Sets the regularization function used to limit overfitting.
     * @param priorFunction new regularization function.
     */
    public void setPriorFunction(PriorFunction priorFunction) {
        this.priorFunction = priorFunction;
    }

    /**
     * Returns the current meta-model.
     * @return meta-model
     */
    public AdaptiveLogisticRegression getModel() {
        return model;
    }

    /**
     * Sets the current meta-model
     * @param model new meta-model
     */
    public void setModel(AdaptiveLogisticRegression model) {
        this.model = model;
    }

    /**
     * Returns the numeric value of the positive class of a two-category model.
     * @return positive class label
     */
    @Override
    public double positiveClass() {
        return 1;
    }

    /**
     * Returns the numeric value of the negative class of a two-category model.
     * @return negative class label
     */
    @Override
    public double negativeClass() {
        return 0;
    }

    /**
     * Shuts down the worker pool used during training and best model determination.
     */
    public void close() {
        if (model != null) {
            model.close();
        }
    }

    /**
     * Returns the number of threads used in training the meta-model learners
     * @return number of threads (cores)
     */
    public int getThreadCount() {
        return threadCount;
    }

    /**
     * Sets the number of threads to use during training.
     * @param threadCount new number of threads (cores)
     */
    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    /**
     * Returns the number of SGD learners in the meta-model.
     * @return number of SGD learners
     */
    public int getPoolSize() {
        return poolSize;
    }

    /**
     * Sets the number of SGD learners in the meta-model.
     * @param poolSize new number of SGD learners
     */
    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public void legacyWrite(Kryo kryo, Output output) {
        try {
            kryo.writeObject(output, this);
            kryo.writeObject(output, new Double(getConfidenceThreshold()));
            if (model != null) {
                model.write(new DataOutputStream(output));
            }
        } catch (IOException ioe) {
            log.error("Error encountered writing model: " + ioe.getMessage());
        }
    }

    public void legacyRead(Kryo kryo, Input input) {
        try {
            AdaptiveSGDROIFinder read = kryo.readObject(input, AdaptiveSGDROIFinder.class);
            setConfidenceThreshold(kryo.readObject(input, Double.class));
            setPriorFunction(read.getPriorFunction());
            setThreadCount(read.getThreadCount());
            setPoolSize(read.getPoolSize());
            AdaptiveLogisticRegression model = new AdaptiveLogisticRegression();
            model.readFields(new DataInputStream(input));
            setModel(model);
            setNumFeatures(model.numFeatures());
            setNumCategories(model.getNumCategories());
        } catch (IOException ioe) {
            log.error("Error encountered reading model: " + ioe.getMessage());
        }
    }
}
