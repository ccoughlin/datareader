/*
 * com.emphysic.myriad.core.data.util.DatasetUtils
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

/**
 * DatasetUtils - general purpose Dataset utility functions
 */
public class DatasetUtils {
    /**
     * Safe handling of out of bound index requests - returns
     * index requested if 0 &lt; value &lt; endIndex, 0 if value &lt; 0, endIndex - 1 otherwise
     *
     * @param value    proposed index
     * @param endIndex upper limit of valid index values
     * @return value if within valid bounds, 0 if negative, endIndex - 1 otherwise
     */
    public static int safeIdx(int value, int endIndex) {
        if (value < 0)
            return 0;
        if (value < endIndex)
            return value;
        return endIndex - 1;
    }
}
