/*
 * com.emphysic.myriad.core.data.ops.PowerOfTwoOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;

import java.util.Map;

/**
 * PowerOfTwoOperation - resizes an input as required such that its width and height are both powers of 2.
 * Primarily used in blending pyramids.
 */
public class PowerOfTwoOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Returns the next power of two i.e. returns x such that 2^x &gt;= a
     *
     * @param a number to pad
     * @return the power x, or 0 if a = 0
     */
    public static int nextPowerOf2(int a) {
        return a == 0 ? 0 : 32 - Integer.numberOfLeadingZeros(a - 1);
    }

    /**
     * Returns the next number &gt;= the input which is a power of two
     * i.e. returns 2^x such that 2^x &gt;= a
     *
     * @param a number to pad
     * @return next power of two greater than or equal to a
     */
    public static int pad(int a) {
        if (!isPowerOf2(a)) {
            return (int) Math.pow(2, nextPowerOf2(a));
        }
        return a;
    }

    /**
     * Determines whether a number is a power of 2
     *
     * @param a number to examine
     * @return true if a is greater than 0 and a power of 2, false otherwise
     */
    public static boolean isPowerOf2(int a) {
        return (a > 0) && ((a & (a - 1)) == 0);
    }

    /**
     * For each dimension in the input, if the dimension is not a power of 2 add elements of value 0 until this
     * condition is satisified.  Thus if both width and height are increased, the original data will be in the
     * lower-left corner of the new Dataset, surrounded by an L-shaped sequence of 0's.
     * @param input Dataset on which to operate
     * @return possibly-resized Dataset, or null if input was null.
     */
    @Override
    public Dataset run(Dataset input) {
        if (input == null) {
            return null;
        }
        if (isPowerOf2(input.getWidth()) && isPowerOf2(input.getHeight())) {
            return input;
        }
        int newWidth = pad(input.getWidth());
        int newHeight = pad(input.getHeight());
        Dataset padded = new Dataset(newWidth, newHeight);
        for (int i = 0; i < input.getWidth(); i++) {
            for (int j = 0; j < input.getHeight(); j++) {
                padded.set(i, j, input.get(i, j));
            }
        }
        return padded;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {}
}
