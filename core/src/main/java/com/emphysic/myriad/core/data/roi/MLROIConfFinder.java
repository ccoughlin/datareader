/*
 * com.emphysic.myriad.core.data.roi.MLROIConfFinder
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.io.Writable;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.Vector;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * A Region of Interest (ROI) finder based on machine learning that provides both probabilities of its
 * classifications and the ability to define a confidence threshold.
 * Created by ccoughlin on 9/21/2016.
 */
@Slf4j
public abstract class MLROIConfFinder implements MLROIFinder, ROIProbability {
    /**
     * A confidence threshold between 0 and 1 to be met for labelling a sample as containing ROI.  The higher the
     * threshold the fewer ROI will be reported.  Defaults to 0 i.e. no confidence thresholding is performed.
     */
    protected double confThr = 0;

    /**
     * Returns the confidence threshold for labelling ROI.
     * @return confidence threshold between 0 and 1
     */
    public double getConfidenceThreshold() {
        return confThr;
    }

    /**
     * Sets the confidence threshold for labelling ROI samples - the model must be (confThr * 100)% confident a
     * sample contains ROI for it to be labelled as such.
     * @param confThr new confidence threshold
     * @throws IllegalArgumentException if the confidence threshold is not in the range 0-1 inclusive.
     */
    public void setConfidenceThreshold(double confThr) throws IllegalArgumentException {
        if (confThr < 0 || confThr > 1) {
            throw new IllegalArgumentException("Confidence threshold must be between 0 and 1 inclusive.");
        }
        this.confThr = confThr;
    }

    /**
     * Predicts the label for a sample.  Prediction is the category index with the maximum value.
     * @param data sample to predict
     * @return integer index with the maximum value
     */
    @Override
    public double predict(double[] data) {
        DenseVector d = new DenseVector(data);
        Vector scores = classify(d);
        int idx = scores.maxValueIndex();
        double maxConf = scores.maxValue();
        if (idx > 0) {
            if (maxConf < confThr) {
                // Only report ROI if the model's confidence > threshold
                log.info("Predicted ROI with confidence ", maxConf, " below threshold ", confThr, ". Reporting as ", negativeClass());
                return negativeClass();
            }
        }
        return idx;
    }

    /**
     * Predicts the label for a sample.  Prediction is the category index with the maximum value.
     * @param data sample to predict
     * @return integer index with the maximum value
     */
    @Override
    public double predict(Dataset data) {
        return predict(data.getData());
    }

    /**
     * Convenience method for writing a Mahout Writable to a byte array
     * @param writable Mahout object that implements the Writable interface
     * @return byte array
     */
    public static byte[] writableWriteToBytes(Writable writable) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream daos = new DataOutputStream(baos);
        if (writable != null) {
            try {
                writable.write(daos);
                daos.flush();
                return baos.toByteArray();
            } catch (IOException ioe) {
                log.error("Error encountered writing model: " + ioe.getMessage());
            }
        }
        return null;
    }
}
