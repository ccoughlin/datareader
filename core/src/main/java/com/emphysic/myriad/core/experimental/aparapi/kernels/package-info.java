/*
 * Copyright (c) 2016 Emphysic LLC.  All rights reserved.
 */

/**
 * This package contains Aparapi kernels for performing calculations on OpenCL devices such as GPUs or accelerators
 * such as Intel's Xeon Phi.
 * Created by chris on 9/1/2016.
 */
package com.emphysic.myriad.core.experimental.aparapi.kernels;