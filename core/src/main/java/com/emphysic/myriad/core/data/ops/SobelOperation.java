/*
 * com.emphysic.myriad.core.data.ops.SobelOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.DatasetUtils;

/**
 * SobelOperation - applies the Sobel operator to an input.  Primarily used for edge detection.
 */
public class SobelOperation extends ConvolutionOperation {
    protected double[][] Gx = {
            {-1.0, 0, 1.0},
            {-2.0, 0, 2.0},
            {-1.0, 0, 1.0}
    };

    protected double[][] Gy = {
            {-1.0, -2.0, -1.0},
            {0.0, 0.0, 0.0},
            {1.0, 2.0, 1.0}
    };

    protected ConvolutionOperation horizontal;
    protected ConvolutionOperation vertical;

    /**
     * Creates a new SobelOperation.
     */
    public SobelOperation() {
        this.kernel = null;
        horizontal = new ConvolutionOperation(Gx);
        vertical = new ConvolutionOperation(Gy);
    }

    /**
     * Runs the Sobel operator on the input.
     * @param input Dataset to convolve
     * @return edge-enhanced Dataset, or null if input was null.
     */
    @Override
    public Dataset run(Dataset input) {
        if (input == null) {
            return null;
        }
        Dataset gx = horizontal.run(input);
        Dataset gy = vertical.run(input);
        Dataset output = new Dataset(input.getWidth(), input.getHeight());
        for (int i = 0; i < input.getWidth(); i++) {
            for (int j = 0; j < input.getHeight(); j++) {
                double xval = 0.0;
                double yval = 0.0;
                for (int x = 0; x < 3; x++) {
                    for (int y = 0; y < 3; y++) {
                        int ix = DatasetUtils.safeIdx(i + x, input.getWidth());
                        int iy = DatasetUtils.safeIdx(j + y, input.getHeight());
                        xval += input.get(ix, iy) * gx.get(ix, iy);
                        yval += input.get(ix, iy) * gy.get(ix, iy);
                    }
                }
                output.set(i, j, Math.sqrt(xval * xval + yval * yval));
            }
        }
        return output;
    }

    /**
     * Horizontal gradient kernel
     * @return horizontal gradient kernel
     */
    public double[][] getGx() {
        return Gx;
    }

    /**
     * Vertical gradient kernel
     * @return vertical gradient kernel
     */
    public double[][] getGy() {
        return Gy;
    }

    /**
     * Horizontal ConvolutionOperation - applies the Gx kernel.
     * @return convolution operation that applies the horizontal gradient
     */
    public ConvolutionOperation getHorizontal() {
        return horizontal;
    }

    /**
     * Vertical ConvolutionOperation - applies the Gy kernel.
     * @return convolution operation that applies the vertical gradient
     */
    public ConvolutionOperation getVertical() {
        return vertical;
    }
}
