/*
 * com.emphysic.myriad.core.data.ops.math.OnlineStats
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops.math;

import com.emphysic.myriad.core.data.io.Dataset;

/**
 * OnlineStats - approximations for incremental mean and standard deviation for large / streaming data.  Based
 * on the algorithms outlined in:
 *
 * Chan, Tony F., Gene H. Golub, and Randall J. LeVeque.
 * "Algorithms for computing the sample variance: Analysis and recommendations." The American Statistician 37.3 (1983),
 * pp 242-247.
 */
public class OnlineStats {
    /**
     * Number of points seen so far
     */
    private int n;
    /**
     * Current mean value
     */
    private double currentMean;
    /**
     * Sum of squares of differences from the current mean
     */
    private double sumSquaredMeanDiff;

    /**
     * Updates the current mean, variance, and standard deviation.
     *
     * @param data new elements
     */
    public void update(double[] data) {
        if (data != null) {
            for (int i = 0; i < data.length; i++) {
                n++;
                double delta = data[i] - currentMean;
                currentMean += delta / n;
                sumSquaredMeanDiff += delta * (data[i] - currentMean);
            }
        }
    }

    /**
     * Updates the current mean, variance, and standard deviation approximations.
     * @param input new elements
     */
    public void update(Dataset input) {
        update(input.getData());
    }

    /**
     * Returns the current mean of the numbers.
     *
     * @return current mean
     */
    public double mean() {
        return currentMean;
    }

    /**
     * Updates the statistics then returns the current mean
     *
     * @param data new elements
     * @return current mean
     */
    public double mean(double[] data) {
        update(data);
        return mean();
    }

    /**
     * Updates the approximations then returns the current mean
     * @param input new elements
     * @return current mean
     */
    public double mean(Dataset input) {
        return mean(input.getData());
    }

    /**
     * Returns the current approximation of the variance of the data.
     *
     * @return current variance, or NaN if less than 2 elements have been seen.
     */
    public double variance() {
        if (n < 2) {
            return Double.NaN;
        } else {
            return sumSquaredMeanDiff / (n - 1);
        }
    }

    /**
     * Updates the statistics then returns the current variance.
     *
     * @param data new elements
     * @return current variance, or NaN if less than 2 elements have been seen.
     */
    public double variance(double[] data) {
        update(data);
        return variance();
    }

    /**
     * Updates the statistics with new values then returns the current variance.
     * @param input new elements
     * @return current variance, or NaN if less than 2 elements have been seen.
     */
    public double variance(Dataset input) {
        return variance(input.getData());
    }

    /**
     * Returns the current approximation of the standard deviation of the data.
     *
     * @return current standard devication, or NaN if less than 2 elements have been seen.
     */
    public double stddev() {
        return Math.sqrt(variance());
    }

    /**
     * Updates the statistics then returns the current standard deviation.
     *
     * @param data new elements
     * @return current standard deviation, or NaN if less than 2 elements have been seen.
     */
    public double stddev(double[] data) {
        update(data);
        return stddev();
    }

    /**
     * Updates the statistics with the supplied values then returns the current standard deviation.
     * @param input new elements
     * @return current standard deviation, or NaN if less than 2 elements have been seen.
     */
    public double stddev(Dataset input) {
        return stddev(input.getData());
    }

    /**
     * The number of elements seen so far.
     *
     * @return number of elements included in statistics
     */
    public int getN() {
        return n;
    }

    /**
     * The current sum of squares of differences from the current mean.
     *
     * @return current value
     */
    public double getSumSquaredMeanDiff() {
        return sumSquaredMeanDiff;
    }

    public void setN(int n) {
        this.n = n;
    }

    /**
     * Current mean value
     */
    public double getCurrentMean() {
        return currentMean;
    }

    public void setCurrentMean(double currentMean) {
        this.currentMean = currentMean;
    }

    public void setSumSquaredMeanDiff(double sumSquaredMeanDiff) {
        this.sumSquaredMeanDiff = sumSquaredMeanDiff;
    }

    // Demo of usage
    public static void main(String[] args) {
        double[][] samples = new double[][]{
                {1, -1, 2},
                {2, 0, 0},
                {0, 1, -1}
        };
        double[] samples1d = new double[samples.length * samples[0].length];
        OnlineStats online = new OnlineStats();
        System.out.println("Incremental calculation:");
        for (int i = 0; i < samples.length; i++) {
            online.update(samples[i]);
            System.out.println("Current mean: " + online.mean());
            System.out.println("Current variance: " + online.variance());
            System.out.println("Current standard deviation: " + online.stddev());
            double[] row = new double[samples[i].length];
            for (int j = 0; j < row.length; j++) {
                samples1d[i * samples[i].length + j] = samples[i][j];
            }
        }
        System.out.println("---");
        System.out.println("Actual values:");
        System.out.println("Mean: " + Stats.mean(samples1d));
        System.out.println("Variance: " + Stats.variance(samples1d));
        System.out.println("Standard Deviation: " + Stats.stddev(samples1d));
    }
}
