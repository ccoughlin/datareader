/**
 * This package contains demonstrations of using Myriad and its components.
 * Copyright (c) 2016 Emphysic LLC.  All rights reserved.
 */
package com.emphysic.myriad.core.examples;