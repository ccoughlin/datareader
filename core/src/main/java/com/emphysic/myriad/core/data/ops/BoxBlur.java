/*
 * com.emphysic.myriad.core.data.ops.BoxBlur
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.DatasetUtils;

import java.util.Map;

/**
 * BoxBlur - fast implementation of a box blur based on its separability.
 */
public class BoxBlur extends BlurOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Creates a new BoxBlur of the specified rank (aka radius).
     * @param rank radius of the blur operation
     */
    public BoxBlur(int rank) {
        super(rank);
    }

    /**
     * Creates a new BoxBlur of radius 5.
     */
    public BoxBlur() {
        this(5);
    }

    /**
     * Performs a fast 2D box blur by implementing a separate vertical and horizontal blur.
     * @param dataSet Dataset to blur
     * @return blurred Dataset, or null if input Dataset was null.
     */
    @Override
    public Dataset run(Dataset dataSet) {
        return VBlur(HBlur(dataSet));
    }

    /**
     * Performs a vertical blur on the dataset.  One component of the two-step fast Box Blur.
     * @param dataSet Dataset to blur
     * @return blurred Dataset, or null if input Dataset was null.
     */
    public Dataset VBlur(Dataset dataSet) {
        if (dataSet != null) {
            int w = dataSet.getWidth();
            int h = dataSet.getHeight();
            Dataset result = new Dataset(w, h);
            Double factor = 1 / ((double) (2 * getRadius() + 1));
            for (int y = 0; y < h; y++) {
                for (int x = 0; x < w; x++) {
                    double newVal = 0.0;
                    for (int kx = -getRadius(); kx <= getRadius(); kx++) {
                        newVal += dataSet.get(DatasetUtils.safeIdx(x + kx, w), y);
                    }
                    result.set(x, y, newVal * factor);
                }
            }
            return result;
        }
        return null;
    }

    /**
     * Performs a horizontal blur on the dataset.  One component of the two-step fast Box Blur.
     * @param dataSet Dataset to blur
     * @return blurred Dataset, or null if input was null.
     */
    public Dataset HBlur(Dataset dataSet) {
        if (dataSet != null) {
            int w = dataSet.getWidth();
            int h = dataSet.getHeight();
            Dataset result = new Dataset(w, h);
            Double factor = 1 / ((double) (2 * getRadius() + 1));
            for (int y = 0; y < h; y++) {
                for (int x = 0; x < w; x++) {
                    double newVal = 0.0;
                    for (int ky = -getRadius(); ky <= getRadius(); ky++) {
                        newVal += dataSet.get(x, DatasetUtils.safeIdx(y + ky, h));
                    }
                    result.set(x, y, newVal * factor);
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }
}
