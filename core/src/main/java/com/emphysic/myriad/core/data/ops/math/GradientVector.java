/*
 * com.emphysic.myriad.core.data.ops.math.GradientVector
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops.math;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.ConvolutionOperation;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * GradientVector - container class for gradient of a matrix
 * Created by chris on 8/25/2016.
 */
@Slf4j
public class GradientVector implements Serializable {
    /**
     * Magnitudes of vector
     */
    private Dataset magnitudes;
    /**
     * Angles of vector
     */
    private Dataset angles;
    /**
     * Default kernel for calculating horizontal gradient
     */
    private double[][] hKernel = new double[][] {
            {-1, 0, 1}
    };
    /**
     * Default kernel for calculating vertical gradient
     */
    private double[][] vKernel = new double[][] {
            {-1},
            {0},
            {1}
    };

    /**
     * Computes the gradient vector from the input data
     * @param input input data
     */
    public GradientVector(Dataset input) {
        calc(input);
    }

    /**
     * Computes the gradient vector from the horizontal and vertical gradients
     * @param dx horizontal gradient
     * @param dy vertical gradient
     */
    public GradientVector(Dataset dx, Dataset dy) {
        calc(dx, dy);
    }

    /**
     * Calculates the horizontal gradient of the input
     * @param input input Dataset
     * @return horizontal gradient
     */
    public Dataset hGrad(Dataset input) {
        ConvolutionOperation conv = new ConvolutionOperation(hKernel);
        return conv.run(input);
    }

    /**
     * Calculates the vertical gradient of the input
     * @param input input Dataset
     * @return vertical gradient
     */
    public Dataset vGrad(Dataset input) {
        ConvolutionOperation conv = new ConvolutionOperation(vKernel);
        return conv.run(input);
    }

    /**
     * Calculates the magnitude and direction of the gradient vectors for the given input.
     * @param input input Dataset
     */
    void calc(Dataset input) {
        Dataset h = hGrad(input);
        try {
            Dataset v = vGrad(input);
            calc(h, v);
        } catch (Exception e) {
            log.info("Error calculating gradient vectors: " + e.getMessage());
        }

    }

    /**
     * Calculates the magnitude and direction of the gradient vectors for the given horizontal and
     * vertical gradients.
     * @param dx horizontal gradient
     * @param dy vertical gradient
     */
    void calc(Dataset dx, Dataset dy) {
        assert (dx.getWidth() == dy.getWidth());
        assert (dx.getHeight() == dy.getHeight());
        magnitudes = new Dataset(dx.getWidth(), dx.getHeight());
        angles = new Dataset(dx.getWidth(), dx.getHeight());
        for (int i=0; i<dx.getWidth(); i++) {
            for (int j=0; j<dx.getHeight(); j++) {
                double mag = Math.sqrt(
                        Math.pow(dx.get(i, j), 2) +
                                Math.pow(dy.get(i, j), 2)
                );
                magnitudes.set(i, j, mag);
                double angle = Math.atan2(dy.get(i, j), dx.get(i, j));
                angles.set(i, j, angle);
            }
        }
    }

    /**
     * Magnitudes of gradient vectors
     * @return magnitudes
     */
    public Dataset getMagnitudes() {
        return magnitudes;
    }

    /**
     * Angles of gradient vectors
     * @return angles in radians
     */
    public Dataset getAngles() {
        return angles;
    }

    /**
     * Horizontal gradient kernel
     * @return horizontal gradient kernel
     */
    public double[][] gethKernel() {
        return hKernel;
    }

    /**
     * Sets the kernel for calculating horizontal gradient
     * @param hKernel new kernel
     */
    public void sethKernel(double[][] hKernel) {
        this.hKernel = hKernel;
    }

    /**
     * Vertical gradient kernel
     * @return vertical gradient kernel
     */
    public double[][] getvKernel() {
        return vKernel;
    }

    /**
     * Sets the kernel for calculating vertical gradient
     * @param vKernel new kernel
     */
    public void setvKernel(double[][] vKernel) {
        this.vKernel = vKernel;
    }
}