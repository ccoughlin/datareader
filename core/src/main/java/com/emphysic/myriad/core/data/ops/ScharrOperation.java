/*
 * com.emphysic.myriad.core.data.ops.ScharrOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

/**
 * ScharrOperation - variation of the Sobel operator, an optimization minimizing weighted mean squared angular error
 * in the Fourier domain.  As with the conventional SobelOperation, used in edge detection.
 */
public class ScharrOperation extends SobelOperation {
    /**
     * Horizontal gradient kernel
     */
    private double[][] Gx = {
            {3.0, 0, -3.0},
            {10.0, 0, -10.0},
            {3.0, 0, -3.0}
    };

    /**
     * Vertical gradient kernel
     */
    private double[][] Gy = {
            {3.0, 10.0, 3.0},
            {0.0, 0.0, 0.0},
            {-3.0, -10.0, 3.0}
    };

    /**
     * Creates a new ScharrOperation
     */
    public ScharrOperation() {
        this.kernel = null;
        horizontal = new ConvolutionOperation(Gx);
        vertical = new ConvolutionOperation(Gy);
    }
}
