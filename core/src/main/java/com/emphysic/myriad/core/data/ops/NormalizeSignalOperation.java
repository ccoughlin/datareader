/*
 * com.emphysic.myriad.core.data.ops.NormalizeSignalOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;

import java.util.Map;

/**
 * NormalizeSignalOperation - normalizes an input by subtracting the minimum value in the input and dividing by
 * the total range i.e. for an input array M the output is Oij = (Mij - min(M))/(max(M) - min(M)).
 */
public class NormalizeSignalOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Normalizes a Dataset by subtracting its minimum and dividing by the total range (max - min).
     * @param input Dataset on which to operate
     * @return normalized Dataset, or null if input was null
     */
    @Override
    public Dataset run(Dataset input) {
        if (input == null) {
            return null;
        }
        Dataset result = new Dataset(input.getWidth(), input.getHeight());
        double max = Double.MIN_VALUE;
        double min = Double.MAX_VALUE;
        double[] data = input.getData();
        for (int i = 0; i < data.length; i++) {
            if (data[i] > max) {
                max = data[i];
            }
            if (data[i] < min) {
                min = data[i];
            }
        }
        for (int i = 0; i < input.getWidth(); i++) {
            for (int j = 0; j < input.getHeight(); j++) {
                result.set(i, j, (input.get(i, j) - min) / (max - min));
            }
        }
        return result;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {}
}
