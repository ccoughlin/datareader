/*
 * com.emphysic.myriad.core.data.roi.RESTROIFinder
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.json.JSONObject;

import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * RESTROIFinder - makes calls to REST servers for detecting Regions Of Interest (ROI).  Supports basic auth.
 *
 * By default sends REST POST requests of the form
 *
 * "POST [url]?data=WzIzLjM3OTY4MDI4NjA3ODc3NCw5LjQyNzQ3NzQzNDk1M..."
 *
 * where [url] is the REST API URL e.g. http://localhost:8080/api/isroi and
 * data is the Base64-encoded array to search for ROI.
 *
 * Expected server response is a JSON object of (at least) the form {"isROI": boolean} i.e.
 * {"isROI": true} if the data contains an ROI and {"isROI": false} if it does not.
 *
 * Created by ccoughlin on 10/17/16.
 */
@Slf4j
public class RESTROIFinder implements ROIFinder, AutoCloseable {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Server isROI API endpoint e.g. http://127.0.0.1:8080/api/isroi
     */
    private String url;
    /**
     * The header fields to include in the REST request.  Defaults:
     * "accept" : "application/json"
     */
    private Map<String, String> header;
    /**
     * The username for basic authentication.  Defaults to no authentication if both username and password are empty.
     */
    private String username;
    /**
     * The password for basic authentication.  Defaults to no authentication if both username and password are empty.
     */
    private String password;
    /**
     * The timeout in milliseconds for making a connection to the server (default 10000).
     */
    private long connectionTimeout;
    /**
     * The timeout in milliseconds for receiving data from a server (default 60000).
     */
    private long socketTimeout;
    /**
     * JSON serializer / de-serializer
     */
    private Gson gson;

    /**
     * Save credentials flag - if true basic auth username and password are serialized.  Default is false, i.e.
     * credentials are _not_ saved.
     */
    private boolean saveCredentials = false;

    /**
     * Constructor
     * @param url REST API endpoint e.g. http://127.0.0.1:8080/api/isroi
     */
    public RESTROIFinder(String url) {
        this.url = url;
        header = new HashMap<>();
        header.put("accept", "application/json");
        gson = new Gson();
        username = "";
        password = "";
    }

    /**
     * Default constructor for serialization.
     */
    public RESTROIFinder() {}

    /**
     * Examine an array of data and report whether it appears to contain a region of interest (ROI)
     *
     * @param data raw data to examine
     * @return true if data appears to contain an ROI, false otherwise
     */
    @Override
    public boolean isROI(double[] data) {
        HttpResponse<JsonNode> response;
        try {
            if (!username.isEmpty() && !password.isEmpty()) {
                response = Unirest.post(url).headers(header).queryString("data", encodeData(data))
                        .basicAuth(username, password).asJson();
            } else {
                response = Unirest.post(url).headers(header).queryString("data", encodeData(data)).asJson();
            }
            int statusCode = response.getStatus();
            if (statusCode >= 200 && statusCode < 300) {
                // Server responded OK
                JSONObject payload = response.getBody().getObject();
                return payload.getBoolean("isROI");
            } else {
                // Something went wrong
                log.error("Server returned {}: {}", statusCode, response.getStatusText());
            }
        } catch (UnirestException ure) {
            log.error("Error making REST call: {}", ure);
        } catch (IOException e) {
            log.error("Unable to encode data: {}", e);
        }
        return false;
    }

    /**
     * Examine a dataset and return whether or not it seems to contain a region of interest (ROI)
     *
     * @param dataset data to examine
     * @return true if data appears to contain an ROI, false otherwise
     */
    @Override
    public boolean isROI(Dataset dataset) {
        return isROI(dataset.getData());
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = ROIFinder.super.getObjectMap();
        map.put("url", url);
        map.put("header", header);
        map.put("timeout", connectionTimeout);
        map.put("socket_timeout", socketTimeout);
        map.put("save_credentials", saveCredentials);
        if (saveCredentials) {
            map.put("username", username);
            map.put("password", password);
        }
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        url = (String) objectMap.getOrDefault("url", url);
        header = (Map<String, String>) objectMap.getOrDefault("header", header);
        connectionTimeout = (long) objectMap.getOrDefault("timeout", connectionTimeout);
        socketTimeout = (long) objectMap.getOrDefault("socket_timeout", socketTimeout);
        if (objectMap.containsKey("save_credentials")) {
            saveCredentials = (boolean) objectMap.get("save_credentials");
            if (saveCredentials) {
                username = (String) objectMap.getOrDefault("username", username);
                password = (String) objectMap.getOrDefault("password", password);
            }
        }
    }

    public void legacyWrite(Kryo kryo, Output output) {
        kryo.writeObject(output, getUrl());
        kryo.writeObject(output, getConnectionTimeout());
        kryo.writeObject(output, getSocketTimeout());
        kryo.writeObject(output, saveCredentials);
        if (saveCredentials) {
            kryo.writeObject(output, getUsername());
            kryo.writeObject(output, getPassword());
        }
    }

    public void legacyRead(Kryo kryo, Input input) {
        setUrl(kryo.readObject(input, String.class));
        setConnectionTimeout(kryo.readObject(input, long.class));
        setSocketTimeout(kryo.readObject(input, long.class));
        setSaveCredentials(kryo.readObject(input, boolean.class));
        if (saveCredentials) {
            setUsername(kryo.readObject(input, String.class));
            setPassword(kryo.readObject(input, String.class));
        }
    }

    /**
     * Closes this resource, relinquishing any underlying resources.
     * This method is invoked automatically on objects managed by the
     * {@code try}-with-resources statement.
     * <p>
     * <p>While this interface method is declared to throw {@code
     * Exception}, implementers are <em>strongly</em> encouraged to
     * declare concrete implementations of the {@code close} method to
     * throw more specific exceptions, or to throw no exception at all
     * if the close operation cannot fail.
     * <p>
     * <p> Cases where the close operation may fail require careful
     * attention by implementers. It is strongly advised to relinquish
     * the underlying resources and to internally <em>mark</em> the
     * resource as closed, prior to throwing the exception. The {@code
     * close} method is unlikely to be invoked more than once and so
     * this ensures that the resources are released in a timely manner.
     * Furthermore it reduces problems that could arise when the resource
     * wraps, or is wrapped, by another resource.
     * <p>
     * <p><em>Implementers of this interface are also strongly advised
     * to not have the {@code close} method throw {@link
     * InterruptedException}.</em>
     * <p>
     * This exception interacts with a thread's interrupted status,
     * and runtime misbehavior is likely to occur if an {@code
     * InterruptedException} is {@linkplain Throwable#addSuppressed
     * suppressed}.
     * <p>
     * More generally, if it would cause problems for an
     * exception to be suppressed, the {@code AutoCloseable.close}
     * method should not throw it.
     * <p>
     * <p>Note that unlike the {@link Closeable#close close}
     * method of {@link Closeable}, this {@code close} method
     * is <em>not</em> required to be idempotent.  In other words,
     * calling this {@code close} method more than once may have some
     * visible side effect, unlike {@code Closeable.close} which is
     * required to have no effect if called more than once.
     * <p>
     * However, implementers of this interface are strongly encouraged
     * to make their {@code close} methods idempotent.
     *
     * @throws Exception if this resource cannot be closed
     */
    @Override
    public void close() throws Exception {
        Unirest.shutdown();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

    public void addHeader(String key, String val) {
        header.put(key, val);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(long connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
        setTimeouts();
    }

    public long getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(long socketTimeout) {
        this.socketTimeout = socketTimeout;
        setTimeouts();
    }

    /**
     * Updates Unirest with current socket and connection timeouts
     */
    private void setTimeouts() {
        Unirest.setTimeouts(connectionTimeout, socketTimeout);
    }

    /**
     * Base64 encoding of an input array
     * @param data data to encode
     * @return encoded version of the array
     * @throws IOException if unable to encode data or an I/O error occurs
     */
    public String encodeData(double[] data) throws IOException {
        return Base64.getEncoder().encodeToString(gson.toJson(data).getBytes("utf-8"));
    }

    /**
     * Decodes a Base64 string into its input array
     * @param enc encoded string
     * @return decoded array
     * @throws UnsupportedEncodingException UTF-8 Encoding is not supported (!).
     */
    public double[] decodeData(String enc) throws UnsupportedEncodingException {
        byte[] asBytes = Base64.getDecoder().decode(enc);
        return gson.fromJson(new String(asBytes, "utf-8"), double[].class);
    }

    /**
     * Sets the HTTPClient implementation to use.
     * @param newClient new client
     */
    public void setHttpClient(HttpClient newClient) {
        Unirest.setHttpClient(newClient);
    }

    /**
     * Reports the current basic auth serialization settings.
     * @return true if crentidals are serialized, false otherwise.
     */
    public boolean saveCredentials() {
        return saveCredentials;
    }

    /**
     * Sets whether or not to save basic auth credentials during serialization.  Default is to not save credentials -
     * please consider carefully the security ramifications before enabling this feature.
     * @param save true to save username and password, false otherwise.
     */
    public void setSaveCredentials(boolean save) {
        saveCredentials = save;
    }
}
