/*
 * com.emphysic.myriad.core.experimental.aparapi.OCLConvolutionOperation
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.experimental.aparapi;

import com.aparapi.Range;
import com.aparapi.device.Device;
import com.aparapi.internal.kernel.KernelManager;
import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.ConvolutionOperation;
import com.emphysic.myriad.core.data.ops.DatasetOperation;
import com.emphysic.myriad.core.experimental.aparapi.kernels.ConvolutionKernel;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * OCLConvolutionOperation - performs ConvolutionOperation on OpenCL devices.
 * Created by chris on 9/1/2016.
 */
@Slf4j
public class OCLConvolutionOperation extends ConvolutionOperation {
    /**
     * OpenCL convolution kernel.  Uses a 2D range so don't dispose()! https://github.com/tdeneau/aparapi/issues/144
     */
    ConvolutionKernel kern = new ConvolutionKernel();

    public OCLConvolutionOperation() {
        super();
        kern.setKnl(this.kernel);
    }

    public OCLConvolutionOperation(double[][] k) {
        super(k);
        kern.setKnl(this.kernel);
    }

    public OCLConvolutionOperation(Dataset k) {
        super(k);
        kern.setKnl(this.kernel);
    }

    /**
     * Runs the convolution operator on the specified input file.  The base ConvolutionOperator runs the
     * identity kernel i.e. input and output data should be identical.
     *
     * If an accelerator that supports OpenCL is found, the convolution operation is
     * performed on the "best" (as determined by number of compute units
     * https://software.intel.com/sites/landingpage/opencl/optimization-guide/Basic_Concepts.htm ) device available.
     *
     * If no accelerator is found, the convolution operation is performed on the CPU using Java Thread Pools.  This is
     * also the case if the best device is a CPU - currently OpenCL on a CPU greatly underperforms native JTP.
     *
     * @param input Dataset on which to operate
     * @return convolved data
     */
    @Override
    public Dataset run(Dataset input) {
        if (input == null) {
            return null;
        }
        Device best = KernelManager.instance().bestDevice();
        if (best == null || best.getType() == Device.TYPE.CPU) {
            log.info("No OpenCL compatible device found, defaulting to JTP");
            ConvolutionOperation fallBack = new ConvolutionOperation(kernel);
            return fallBack.run(input);
        }
        kern.setIdata(input.getData());
        int w = input.getWidth();
        kern.setW(w);
        int h = input.getHeight();
        kern.setH(h);
        Dataset output = new Dataset(w, h);
        kern.setOdata(output.getData());
        Range range = Range.create2D(best, w, h);
        kern.execute(range);
        output.setData(kern.getOdata(), w, h);
        kern.dispose();
        return output;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        kernel = (double[][]) objectMap.getOrDefault("kernel", kernel);
        kern.setKnl(kernel);
    }
}
