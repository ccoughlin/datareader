/*
 * com.emphysic.myriad.core.data.ops.FeatureScalingOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.math.OnlineStats;
import com.emphysic.myriad.core.data.ops.math.Stats;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * FeatureScalingOperation - scales features for machine learning by subtracting the mean and dividing by the
 * standard deviation for each feature (index position).
 */
@Slf4j
public class FeatureScalingOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Means of the features
     */
    private List<Double> featureMeans;
    /**
     * Standard deviations of the features
     */
    private List<Double> featureStds;
    /**
     * Online approximation to mean and standard deviation calculations
     */
    private OnlineStats onlineStats;

    /**
     * Creates a new FeatureScalingOperation.
     */
    public FeatureScalingOperation() {
        onlineStats = new OnlineStats();
    }

    /**
     * Creates a new scaler and fits with the provided data.
     * @param datasets data with which to fit the scaler
     */
    public FeatureScalingOperation(List<? extends Dataset> datasets) {
        fit(datasets);
    }

    /**
     * Scales the input data by substracting the feature means and dividing by their standard deviations.
     *
     * @param input Dataset to scale
     * @return scaled Dataset
     * @throws RuntimeException if the scaler hasn't been trained yet
     * @see FeatureScalingOperation#fit
     */
    @Override
    public Dataset run(Dataset input) {
        if (featureMeans == null || featureStds == null) {
            log.error("Tried scaling data without fitting scaler");
            throw new RuntimeException("Must call fit prior to scaling a dataset");
        }
        if (input != null) {
            int width = input.getWidth();
            int height = input.getHeight();
            Dataset standardized = new Dataset(width, height);
            for (int i = 0; i < standardized.getWidth(); i++) {
                for (int j = 0; j < standardized.getHeight(); j++) {
                    int idx = j * width + i;
                    standardized.set(i, j, (input.get(i, j) - featureMeans.get(idx)) / featureStds.get(idx));
                }
            }
            return standardized;
        }
        return null;
    }

    /**
     * "Trains" (i.e. calculates feature means and standard deviations) from the specified data.  Used when the
     * entire sample set is available up front as it replaces the current values.
     * @param datasets data to use
     */
    public void fit(List<? extends Dataset> datasets) {
        featureMeans = new ArrayList<>();
        featureStds = new ArrayList<>();
        onlineStats = new OnlineStats();
        for (int i = 0; i < datasets.get(0).getSize(); i++) {
            // ith feature
            double[] features = new double[datasets.size()];
            for (int j = 0; j < datasets.size(); j++) {
                // jth sample
                features[j] = datasets.get(j).getData()[i];
            }
            featureMeans.add(Stats.mean(features));
            featureStds.add(Stats.stddev(features));
        }
    }

    /**
     * Incremental training - keeps approximations of feature means and standard deviations.  Used when not all of the
     * samples are available up front.
     *
     * @param datasets new batch of samples of size &gt;=2 (approximations require 2 or more elements)
     */
    public void partial_fit(List<? extends Dataset> datasets) {
        assert (datasets.size() >= 2); // Online approximations require 2+ elements
        if (featureMeans == null) {
            featureMeans = new ArrayList<>();
        }
        if (featureStds == null) {
            featureStds = new ArrayList<>();
        }
        if (onlineStats == null) {
            onlineStats = new OnlineStats();
        }
        for (int i = 0; i < datasets.get(0).getSize(); i++) {
            // ith feature
            double[] features = new double[datasets.size()];
            for (int j = 0; j < datasets.size(); j++) {
                // jth sample
                features[j] = datasets.get(j).getData()[i];
            }
            featureMeans.add(onlineStats.mean(features));
            featureStds.add(onlineStats.stddev(features));
        }
    }

    /**
     * Returns the feature means
     * @return feature means
     */
    public List<Double> getFeatureMeans() {
        return featureMeans;
    }

    /**
     * Sets the feature means
     * @param featureMeans means of the features
     */
    public void setFeatureMeans(List<Double> featureMeans) {
        this.featureMeans = featureMeans;
    }

    /**
     * Returns the feature standard deviations
     * @return feature standard deviations
     */
    public List<Double> getFeatureStds() {
        return featureStds;
    }

    /**
     * Sets the feature standard deviations
     * @param featureStds standard deviations of the features
     */
    public void setFeatureStds(List<Double> featureStds) {
        this.featureStds = featureStds;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("feature_means", featureMeans);
        map.put("feature_stds", featureStds);
        map.put("stats_numpoints", onlineStats.getN());
        map.put("stats_mean", onlineStats.getCurrentMean());
        map.put("stats_ssmd", onlineStats.getSumSquaredMeanDiff());
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        if (objectMap.containsKey("feature_means")) {
            featureMeans = (List<Double>) objectMap.get("feature_means");
        }
        if (objectMap.containsKey("feature_stds")) {
            featureStds = (List<Double>) objectMap.get("feature_stds");
        }
        onlineStats = new OnlineStats();
        if (objectMap.containsKey("stats_numpoints")) {
            onlineStats.setN((Integer) objectMap.get("stats_numpoints"));
        }
        if (objectMap.containsKey("stats_mean")) {
            onlineStats.setCurrentMean((Double) objectMap.get("stats_mean"));
        }
        if (objectMap.containsKey("stats_ssmd")) {
            onlineStats.setSumSquaredMeanDiff((Double) objectMap.get("stats_ssmd"));
        }
    }

    /**
     * Returns the online statistics approximator.
     * @return OnlineStats instance
     */
    public OnlineStats getOnlineStats() {
        return onlineStats;
    }

    // Demo of usage.  In production you'd also want to save the trained scaler so it could apply the same
    // transformation to incoming data.
    public static void main(String[] args) {
        List<Dataset> samples = new ArrayList<>();
        samples.add(new Dataset(new double[]{1, -1, 2}, 3, 1));
        samples.add(new Dataset(new double[]{2, 0, 0}, 3, 1));
        samples.add(new Dataset(new double[]{0, 1, -1}, 3, 1));
        FeatureScalingOperation scaler = new FeatureScalingOperation(samples);
        log.info("Feature means: " + scaler.getFeatureMeans());
        log.info("Feature standard deviations: " + scaler.getFeatureStds());
        for (Dataset d : samples) {
            Dataset scaled = scaler.run(d);
            StringBuilder sb = new StringBuilder("Scaled data: [");
            double[] data = scaled.getData();
            for (double el : data) {
                sb.append(el).append(" ");
            }
            sb.append("]");
            log.info(sb.toString());
        }
        System.out.println("\nDemonstrating scaling\nSample data:");
        Dataset test = new Dataset(new double[]{-1., 1., 0.}, 3, 1);
        test.prettyPrint();
        System.out.println();
        System.out.println("Conventional scaling:");
        scaler.run(test).prettyPrint();
        System.out.println();
        System.out.println("Scaled with online approximations:");
        FeatureScalingOperation scaler2 = new FeatureScalingOperation();
        scaler2.partial_fit(samples);
        scaler2.run(test).prettyPrint();
    }
}
