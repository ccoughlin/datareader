/*
 * com.emphysic.myriad.core.data.roi.SGDROIFinder
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import lombok.extern.slf4j.Slf4j;
import org.sgdtk.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * SGDROIFinder - machine learning region of interest detection based on the Stochastic Gradient Descent (SGD)
 * algorithm.
 */
@Slf4j
public class SGDROIFinder implements MLROIFinder {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Trains the SGD model
     */
    private Learner learner;
    /**
     * The actual SGD model
     */
    private Model linearModel;
    /**
     * Number of features in the sample data.  In Myriad this is equivalent to the size of each sample used
     * to train the model @see #Dataset.getSize().
     */
    private int numFeatures;

    /**
     * Creates a new SGD ROI finder using the Hinge loss function and a regularization set to 1.0E-5.
     */
    public SGDROIFinder() {
        learner = new SGDLearner();
    }

    /**
     * Creates a new SGD ROI finder using the specified loss function and regularization.
     * @param lossFunction e.g. HingeLoss, LogLoss, SquaredHingeLoss,
     * @param regularization regularization parameter to penalize model complexity
     */
    public SGDROIFinder(Loss lossFunction, double regularization) {
        learner = new SGDLearner(lossFunction, regularization);
    }

    /**
     * Instantiates a new SGD ROI finder from an existing SGD model.
     * @param mdl SGD model
     */
    public SGDROIFinder(Model mdl) {
        linearModel = mdl;
        learner = new SGDLearner();
    }

    /**
     * Trains the model with another set of samples, creating the model if required.
     * @param X N examples with M features per example
     * @param y N labels for the N examples in X
     * @throws Exception if an error occurs
     */
    @Override
    public void train(double[][] X, int[] y) throws Exception {
        if (linearModel == null) {
            numFeatures = X[0].length;
            linearModel = learner.create(numFeatures);
        }
        List<FeatureVector> featureVectors = new ArrayList<>();
        for (int i=0; i<y.length;i++) {
            if (!validDims(X[i])) {
                throw new IllegalArgumentException("Invalid dimensions: got " + X[i].length + " expected " + numFeatures);
            }
            DenseVectorN vectorN = new DenseVectorN(X[i]);
            FeatureVector featureVector = new FeatureVector(y[i], vectorN);
            featureVectors.add(featureVector);
        }
        learner.trainEpoch(linearModel, featureVectors);
    }

    /**
     * Predict whether a sample contains a region of interest
     * @param data raw data to examine
     * @return true if the sample contains an ROI, false otherwise
     */
    @Override
    public boolean isROI(double[] data) {
        double prediction = predict(data);
        double posDelta = Math.abs(positiveClass() - prediction);
        double negDelta = Math.abs(negativeClass() - prediction);
        return posDelta < negDelta;
    }

    /**
     * Predict whether a sample contains a region of interest
     * @param dataset raw data to examine
     * @return true if the sample contains an ROI, false otherwise
     */
    @Override
    public boolean isROI(Dataset dataset) {
        return isROI(dataset.getData());
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = MLROIFinder.super.getObjectMap();
        map.put("features", numFeatures);
        map.put("learner", learner);
        map.put("model", getLinearModel());
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        numFeatures = (int) objectMap.getOrDefault("features", numFeatures);
        learner = (Learner) objectMap.getOrDefault("learner", learner);
        linearModel = (Model) objectMap.getOrDefault("model", linearModel);
    }

    /**
     * Returns the prediction of the specified sample.
     *
     * @param data sample
     * @return predicted label / value
     */
    public double predict(double[] data) {
        if (!validDims(data)) {
            throw new IllegalArgumentException("Invalid dimensions: got " + data.length + " expected " + numFeatures);
        }
        FeatureVector featureVector = new FeatureVector(0.0, new DenseVectorN(data));
        return linearModel.predict(featureVector);
    }

    /**
     * Returns the prediction of the specified sample.
     *
     * @param dataset sample
     * @return predicted label / value
     */
    public double predict(Dataset dataset) {
        return predict(dataset.getData());
    }

    /**
     * Ensure a given sample has correct number of features.
     *
     * @param data sample
     * @return true if the number of features in the sample = number of features the model was trained on
     */
    public boolean validDims(double[] data) {
        return data.length == numFeatures;
    }

    /**
     * Returns the model trainer.
     * @return current model trainer
     */
    public Learner getLearner() {
        return learner;
    }

    /**
     * Sets the model trainer.
     * @param learner new model trainer
     */
    public void setLearner(Learner learner) {
        this.learner = learner;
    }

    /**
     * Returns the current SGD model.
     * @return current model
     */
    public Model getLinearModel() {
        return linearModel;
    }

    /**
     * Sets the SGD model
     * @param linearModel new model
     */
    public void setLinearModel(Model linearModel) {
        this.linearModel = linearModel;
    }

    /**
     * Returns the number of features used to train this model
     * @return number of features @see #Dataset.getSize()
     */
    public int getNumFeatures() {
        return numFeatures;
    }

    /**
     * Returns the value of the positive label.
     * @return positive label value
     */
    @Override
    public double positiveClass() {
        return 1;
    }

    /**
     * Returns the value of the negative label.
     * @return negative label value
     */
    @Override
    public double negativeClass() {
        return -1;
    }

    public void legacyWrite(Kryo kryo, Output output) {
        kryo.writeObject(output, new Integer(numFeatures));
        kryo.writeObject(output, linearModel);
    }

    public void legacyRead(Kryo kryo, Input input) {
        this.numFeatures = kryo.readObject(input, Integer.class);
        setLinearModel(kryo.readObject(input, LinearModel.class));
    }

    // Basic demo of train and predict cycle
    public static void main(String[] args) throws Exception {
        double[][] X = {{-1.0, -1.0}, {-2.0, -1.0}, {1.0, 1.0}, {2.0, 1.0}};
        int[] y = {0, 0, 1, 1};
        SGDROIFinder sgdFlawFinder = new SGDROIFinder();
        sgdFlawFinder.train(X, y);
        for (int i = 0; i < X.length; i++) {
            System.out.println(sgdFlawFinder.predict(X[i]) + " vs. " + (y[i] > 0));
        }
        sgdFlawFinder.save(new File("sgdcankill"));
        SGDROIFinder read = new SGDROIFinder();
        read.load(new File("sgdcankill"));
        for (int i = 0; i < X.length; i++) {
            System.out.println(read.isROI(X[i]) + " vs. " + (y[i] > 0));
        }
    }
}
