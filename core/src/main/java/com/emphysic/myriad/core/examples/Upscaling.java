/*
 * com.emphysic.myriad.core.examples.Upscaling
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.examples;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.io.ImageDataset;
import com.emphysic.myriad.core.data.ops.UpscaleOperation;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;

/**
 * Demonstrates upscaling data.
 * Created by ccoughlin on 9/12/2016.
 */
public class Upscaling {
    // Demo of usage - double the size of a test image
    public static void main(String[] args) throws Exception {
        URL sampleImageUrl = Thread.currentThread().getContextClassLoader().getResource("data/io/test.jpg");
        File inputFile = new File(sampleImageUrl.getPath());
        ImageDataset imageDataset = new ImageDataset(inputFile);
        imageDataset.read();
        System.out.println("Original Dataset: " + imageDataset.getWidth() + "x" + imageDataset.getHeight() + "\n");

        System.out.print("Upscaling...");
        UpscaleOperation upscaleOperation = new UpscaleOperation();
        Dataset upscaled = upscaleOperation.run(imageDataset);
        System.out.println("done");
        System.out.flush();
        System.out.println("Upscaled Dataset: " + upscaled.getWidth() + "x" + upscaled.getHeight() + "\n");

        System.out.print("Saving Datasets...");
        File outFolder = new File(System.getProperty("java.io.tmpdir"), "myriad");
        if (!outFolder.exists()) {
            Files.createDirectories(outFolder.toPath());
        }
        File orig = new File(outFolder, "originalDataset.txt");
        File big = new File(outFolder, "upscaledDataset.txt");
        imageDataset.write(orig);
        upscaled.write(big);
        System.out.println("done");
        System.out.flush();
        System.out.println("Original data saved to " + orig.getAbsolutePath());
        System.out.println("Upscaled data saved to " + big.getAbsolutePath());
    }
}
