/*
 * com.emphysic.myriad.core.data.ops.HOGOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.math.GradientVector;
import com.emphysic.myriad.core.data.util.FileSniffer;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.BasicConfigurator;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * HOGOperation - an implementation of the Histogram of Oriented Gradients (HOG) algorithm as outlined in
 * Dalal, Navneet, and Bill Triggs. "Histograms of oriented gradients for human detection."
 * 2005 IEEE Computer Society Conference on Computer Vision and Pattern Recognition (CVPR'05). Vol. 1. IEEE, 2005.
 *
 * Based in part on the Chris McCormick MATLAB implementation discussed at
 * http://mccormickml.com/2013/05/09/hog-person-detector-tutorial/ .
 *
 * Created by chris on 8/5/2016.
 */
@Slf4j
public class HOGOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version
    /**
     * Number of data points in a cell's radius.
     */
    private int cellRadius = 4;
    /**
     * Total width of a square cell
     */
    private int cellWidth = 2 * cellRadius + 1;
    /**
     * Number of cells in a block in one dimension i.e. total cells = blockSize * blockSize
     */
    private int blockSize = 2;
    /**
     * Number of bins in the local histgram
     */
    private int nbins = 9;

    /**
     * Construct a HOG operation with a cell radius of 4, 4 cells per block, and 9 histogram bins.
     * Each cell contains (2*4+1)^2 = 81 pixels, 324 pixels per block, and the final descriptor vector is
     * [number of blocks in input] * 9 elements.
     */
    public HOGOperation() {}

    /**
     * Construct a HOG operation.
     * @param cellRadius radius of a cell (number of points in one cell = (2*r + 1)^2)
     * @param blockSize radius of a block (number of cells in one block = bs * bs)
     * @param nbins number of histogram bins
     */
    public HOGOperation(int cellRadius, int blockSize, int nbins) {
        setCellRadius(cellRadius);
        setBlockSize(blockSize);
        setNbins(nbins);
    }

    /**
     * Maximum angle in the histogram in radians.  Original paper used PI.
     */
    private double MAXANGLE = 2 * Math.PI;
    /**
     * Minimum angle in the histogram in radians.  Original paper used 0.
     */
    private double MINANGLE = 0;

    @Override
    public Dataset run(Dataset input) {
        if (input != null) {
            return descriptors(input);
        } else {
            log.info("Received null Dataset, returning same");
            return null;
        }
    }

    /**
     * Calculates the gradient vector of the input
     * @param input input Dataset
     * @return gradient vector
     */
    public GradientVector gradientVector(Dataset input) {
        return new GradientVector(input);
    }

    /**
     * Computes the normalized HOG descriptors for the specified input
     * @param input data to inspect
     * @return a descriptor Dataset of size (number of blocks * number of bins * number of cells / block) X 1, or
     * null if no data
     */
    public Dataset descriptors(Dataset input) {
        SlidingWindowOperation blockSlider = new SlidingWindowOperation(cellWidth / 2,
                blockSize * cellWidth);
        Dataset block;
        List<Double> normalizedBlockHists = new ArrayList<>();
        double w = (MAXANGLE - MINANGLE) / nbins;
        while ((block = blockSlider.run(input)) != null) {
            SlidingWindowOperation cellSlider = new SlidingWindowOperation(cellWidth, cellWidth);
            Dataset cell;
            double[] h = new double[blockSize * blockSize * nbins];
            int idx = 0;
            while ((cell = cellSlider.run(block)) != null) {
                GradientVector gradient = gradientVector(cell);
                double[] mags = gradient.getMagnitudes().getData();
                double[] angles = gradient.getAngles().getData();
                double[] d = new double[nbins];
                for (int i=0; i<angles.length; i++) {
                    double angle = angles[i];
                    if (angle < 0) {
                        angle += Math.PI;
                    }
                    int leftBinIndex = Math.toIntExact(Math.round((angle - MINANGLE) / w));
                    int rightBinIndex = leftBinIndex + 1;
                    double leftBinCenter = ((leftBinIndex - 0.5) * w) - MINANGLE;
                    double rfrac = angle - leftBinCenter;
                    double lfrac = w - rfrac;
                    rfrac /= w;
                    lfrac /= w;
                    if (leftBinIndex < 0) {
                        leftBinIndex = nbins - 1;
                    } else if (leftBinIndex > nbins - 1) {
                        leftBinIndex = 0;
                    }
                    if (rightBinIndex > nbins - 1) {
                        rightBinIndex = 0;
                    }
                    d[leftBinIndex] += lfrac * mags[i];
                    d[rightBinIndex] += rfrac * mags[i];
                }
                System.arraycopy(d, 0, h, idx * d.length, d.length);
                idx++;
            }
            h = normalize(h);
            for (double el: h) {
                normalizedBlockHists.add(el);
            }
        }
        if (normalizedBlockHists.isEmpty()) {
            log.info("No block histograms found, returning null");
            return null;
        }
        Dataset H = new Dataset(
                normalizedBlockHists.stream().mapToDouble(Double::doubleValue).toArray(), 1,
                normalizedBlockHists.size());
        return H;
    }

    /**
     * Normalizes an array by dividing each element by the magnitude of the vector.
     * @param data data to normalize
     * @return normalized data
     */
    public double[] normalize(double[] data) {
        double[] norm = new double[data.length];
        double mag = 0.01;
        for (double aData : data) {
            mag += Math.pow(aData, 2);
        }
        mag = Math.sqrt(mag);
        for (int i=0; i<data.length; i++) {
            norm[i] = data[i] / mag;
        }
        return norm;
    }

    /**
     * Number of data points in a cell's radius.
     * @return cell's radius in data points.
     */
    public int getCellRadius() {
        return cellRadius;
    }

    public void setCellRadius(int cellRadius) {
        this.cellRadius = cellRadius;
        this.cellWidth = 2 * cellRadius + 1;
    }

    /**
     * Total width of a square cell
     * @return cell's width i.e. 2 * cellRadius + 1
     */
    public int getCellWidth() {
        return cellWidth;
    }

    public void setCellWidth(int cellWidth) {
        this.cellWidth = cellWidth;
        this.cellRadius = (cellWidth - 1) / 2;
    }

    /**
     * Number of cells in a block in one dimension i.e. total cells = blockSize * blockSize
     * @return number of cells per block
     */
    public int getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }

    /**
     * Number of bins in the local histgram
     * @return number of bins
     */
    public int getNbins() {
        return nbins;
    }

    /**
     * Sets the number of bins to use in the histogram
     * @param nbins number of bins
     */
    public void setNbins(int nbins) {
        this.nbins = nbins;
    }

    /**
     * Maximum angle in the histogram in radians.  Original paper used PI.
     * @return maximum angle in radians
     */
    public double getMAXANGLE() {
        return MAXANGLE;
    }

    /**
     * Sets the maximum angle in the histogram in radians.  Original paper used PI.
     * @param MAXANGLE maximum angle in radians
     */
    public void setMAXANGLE(double MAXANGLE) {
        this.MAXANGLE = MAXANGLE;
    }

    /**
     * Minimum angle in the histogram in radians.  Original paper used 0.
     * @return minimum angle in radians
     */
    public double getMINANGLE() {
        return MINANGLE;
    }

    /**
     * Sets the minimum angle in the histogram in radians.  Original paper used 0.
     * @param MINANGLE minimum angle in radians
     */
    public void setMINANGLE(double MINANGLE) {
        this.MINANGLE = MINANGLE;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("cell_radius", cellRadius);
        map.put("cell_width", cellWidth);
        map.put("block_size", blockSize);
        map.put("nbins", nbins);
        map.put("angle_max", MAXANGLE);
        map.put("angle_min", MINANGLE);
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        cellRadius = (int) objectMap.getOrDefault("cell_radius", cellRadius);
        cellWidth = (int) objectMap.getOrDefault("cell_width", cellWidth);
        blockSize = (int) objectMap.getOrDefault("block_size", blockSize);
        nbins = (int) objectMap.getOrDefault("nbins", nbins);
        MAXANGLE  = (double) objectMap.getOrDefault("angle_max", MAXANGLE);
        MINANGLE  = (double) objectMap.getOrDefault("angle_min", MAXANGLE);
    }

    /*
     * Demo of scanning over a larger dataset
     */
    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();
        URL testJpgURL = Thread.currentThread().getContextClassLoader().getResource("data/io/test.jpg");
        File img = new File(testJpgURL.getPath());
        Dataset orig = FileSniffer.read(img, true);
        SlidingWindowOperation slide = new SlidingWindowOperation(63, 128);
        Dataset s;
        int i = 0;
        while ((s = slide.run(orig)) != null) {
            HOGOperation hog = new HOGOperation(1, 2, 9);
            Dataset descriptor = hog.run(s);
            i++;
        }
    }
}
