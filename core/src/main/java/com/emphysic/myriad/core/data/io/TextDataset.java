/*
 * com.emphysic.myriad.core.data.io.TextDataset
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.io;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * TextDataset - IODatasets read from delimited ASCII files.
 */
@Slf4j
public class TextDataset extends IODataset {
    /**
     * The default token delimiter
     */
    private static final String defaultDelimiter = ",";
    /**
     * The default comment character
     */
    private static final String defaultCommentChar = "#";
    /**
     * The token delimiter - elements in the data are separated with this token.  Initialized to #TextDataset
     * .defaultDelimiter.
     */
    private String delim = defaultDelimiter;
    /**
     * The comment character - lines beginning with this character are ignored.  Initialized to #TextDataset
     * .defaultCommentChar.
     */
    private String commentChar = defaultCommentChar;
    /**
     * The underlying String tokens in the data.
     */
    private List<List<String>> stringData;

    /**
     * Creates a new TextDataset from the specified file.  Default delimiter and comment character assumed.
     * @param dataSource file to read
     * @param width width of data
     * @param height height of data
     * @throws IOException if an I/O error occurs
     */
    public TextDataset(File dataSource, int width, int height) throws IOException {
        super(dataSource, width, height);
    }

    /**
     * Creates a new TextDataset from the specified file.  Default delimiter and comment character assumed.
     * @param dataFileName file to read
     * @param width width of data
     * @param height height of data
     * @throws IOException if an I/O error occurs
     */
    public TextDataset(String dataFileName, int width, int height) throws IOException {
        super(dataFileName, width, height);
    }

    /**
     * Creates a new TextDataset from the specified file.
     * @param dataSource file to read
     * @param delim token delimiter
     * @param commentChar comment character
     * @param width width of data
     * @param height height of data
     * @throws IOException if an I/O error occurs
     */
    public TextDataset(File dataSource, String delim, String commentChar, int width, int height) throws IOException {
        super(dataSource, width, height);
        this.delim = delim;
        this.commentChar = commentChar;
    }

    /**
     * Creates a new TextDataset from the specified file.
     * @param dataFileName file to read
     * @param delim token delimiter
     * @param commentChar comment character
     * @param width width of data
     * @param height height of data
     * @throws IOException if an I/O error occurs
     */
    public TextDataset(String dataFileName, String delim, String commentChar, int width, int height) throws IOException {
        this(new File(dataFileName), delim, commentChar, width, height);
    }

    /**
     * Creates a new TextDataset from the specified file.  Uses default comment character.
     * @param dataSource file to read
     * @param delim token delimiter
     * @param width width of data
     * @param height height of data
     * @throws IOException if an I/O error occurs
     */
    public TextDataset(File dataSource, String delim, int width, int height) throws IOException {
        this(dataSource, delim, defaultCommentChar, width, height);
    }

    /**
     * Creates a new TextDataset from the specified file.  Uses default comment character.
     * @param dataFileName file to read
     * @param delim token delimiter
     * @param width width of data
     * @param height height of data
     * @throws IOException if an I/O error occurs
     */
    public TextDataset(String dataFileName, String delim, int width, int height) throws IOException {
        this(new File(dataFileName), delim, width, height);
    }

    /**
     * Reads and returns an array of elements from a delimited ASCII stringData file.
     *
     * @param dataFile    file to read
     * @param delim       character used to delimit elements in the file
     * @param commentChar lines beginning with this character are treated as comments and ignored
     * @return stringData array
     * @throws IOException if an error occurs reading the file
     */
    public static List<List<String>> readAsString(File dataFile, String delim, String commentChar) throws IOException {
        ArrayList<List<String>> data = new ArrayList<>();
        Scanner scanner = new Scanner(dataFile);
        while (scanner.hasNextLine()) {
            try {
                String line = scanner.nextLine();
                if (commentChar == null || !line.startsWith(commentChar)) {
                    String[] tokens = line.split(delim);
                    data.add(Arrays.asList(tokens));
                }
            } catch (NoSuchElementException nsee) {
                log.warn("Error reading " + dataFile.getAbsolutePath() + nsee.getLocalizedMessage());
            }
        }
        return data;
    }

    /**
     * Reads the data file.
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void read() throws IOException {
        stringData = readAsString(source, delim, commentChar);
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                set(i, j, Double.valueOf(stringData.get(j).get(i)));
            }
        }
    }

    /**
     * Returns the token delimiter.
     * @return token delimiter
     */
    public String getDelim() {
        return delim;
    }

    /**
     * Sets the token delimiter.
     * @param delim token delimiter
     */
    public void setDelim(String delim) {
        this.delim = delim;
    }

    /**
     * Gets the comment character.
     * @return comment character
     */
    public String getCommentChar() {
        return commentChar;
    }

    /**
     * Sets the comment character.
     * @param commentChar comment character
     */
    public void setCommentChar(String commentChar) {
        this.commentChar = commentChar;
    }

    /**
     * Returns the String tokens from the data file, i.e. its contents before being converted to numeric values.
     * @return tokenized contents of file.
     */
    public List<List<String>> getStringData() {
        return stringData;
    }

}
