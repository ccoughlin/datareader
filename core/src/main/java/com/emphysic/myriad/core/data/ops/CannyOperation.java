/*
 * com.emphysic.myriad.core.data.ops.CannyOperation
 *
 * Copyright (c) 2018 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.math.GradientVector;
import com.emphysic.myriad.core.data.ops.math.Stats;
import com.emphysic.myriad.core.data.util.DatasetUtils;
import lombok.extern.slf4j.Slf4j;
import smile.math.Math;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * CannyOperation - Canny Edge detection
 * Created by chris on 8/25/2016.
 */
@Slf4j
public class CannyOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Conversion factor for converting radians to degrees
     */
    private static final double RADIANS2DEGREES = 57.2958;
    /**
     * Smoothing operation to de-noise input data prior to edge detection
     */
    private BlurOperation blurOperation;
    /**
     * Operation to use for calculating horizontal and vertical gradients
     */
    private SobelOperation gradientOperation;
    /**
     * Upper threshold for element values - elements above this value are "strong" and considered edges.
     * Elements between the upper and lower thresholds are "candidates".
     */
    private double upperThreshold = 2;
    /**
     * Lower threshold for element values - elements below this value are "weak" and are rejected (set to 0)
     * Elements between the upper and lower thresholds are "candidates".
     */
    private double lowerThreshold = 1;

    /**
     * Whether and how to attempt automatic calculation of the lower and upper thresholds.
     * NONE - do not attempt to automatically calculate
     * MEAN - use the mean of the input data to calculate lower and upper thresholds
     * MEDIAN - use the median of the input data to calculate lower and upper thresholds
     * OTSU1D - use Otsu's Method (https://en.wikipedia.org/wiki/Otsu%27s_method ) to calculate lower and upper thresholds
     */
    public enum AutoThreshold {NONE, MEAN, MEDIAN, OTSU1D}

    /**
     * Whether and how to automatically calculate thresholds
     */
    private AutoThreshold autoThreshold = AutoThreshold.NONE;

    /**
     * Standard deviation threshold for mean and median calculation of thresholds
     */
    private double sigmaThreshold = 0.33;

    /**
     * Cardinal points - horizontal, vertical, and diagonal.  Local gradient directions are rounded to one of
     * these directions: 0, 45, 90, 135 (i.e. horizontal, vertical, or diagonal).
     */
    int[] cardinals = {0, 45, 90, 135};

    /**
     * Constructs a new Canny Edge detector with the specified denoise operation and gradient operation
     * @param blurOperation BlurOperation used to smooth and denoise input data
     * @param convolutionOperation ConvolutionOperation used to calculate horizontal and vertical gradients
     * @param autoThreshold whether/how to automatically determine thresholds
     */
    public CannyOperation(BlurOperation blurOperation, SobelOperation convolutionOperation, AutoThreshold autoThreshold) {
        this.blurOperation = blurOperation;
        this.gradientOperation = convolutionOperation;
        this.autoThreshold = autoThreshold;
    }

    /**
     * Constructs a new Canny Edge detector with the specified denoise operation and gradient operation
     * @param blurOperation BlurOperation used to smooth and denoise input data
     * @param convolutionOperation ConvolutionOperation used to calculate horizontal and vertical gradients
     */
    public CannyOperation(BlurOperation blurOperation, SobelOperation convolutionOperation) {
        this(blurOperation, convolutionOperation, AutoThreshold.NONE);
    }

    /**
     * Constructs a new Canny Edge detector, denoising input with a Gaussian Blur of radius 5 and calculating
     * gradients with a standard Sobel edge detector.
     */
    public CannyOperation() {
        this(new GaussianBlur(), new SobelOperation());
    }

    /**
     * Constructs a new Canny Edge detector, denoising input with a Gaussian Blur of the specified radius and
     * calculating gradients with a standard Sobel edge detector.
     * @param blurRadius radius of smoothing operation
     */
    public CannyOperation(int blurRadius) {
        this(new GaussianBlur(blurRadius), new SobelOperation());
    }

    @Override
    public Dataset run(Dataset input) {
        if (input != null) {
            switch (autoThreshold) {
                case MEAN:
                    // Set thresholds w. mean
                    // (1-sigma)*[mean value] and set the high threshold to (1+sigma)*[mean value]
                    Double mean = Math.mean(input.getData());
                    setLowerThreshold((1 - sigmaThreshold) * mean);
                    setUpperThreshold((1 + sigmaThreshold) * mean);
                    break;
                case MEDIAN:
                    // Set thresholds w. median
                    // (1-sigma)*[median value] and (1+sigma)*[median value]
                    Double median = Math.median(input.getData());
                    setLowerThreshold((1 - sigmaThreshold) * median);
                    setUpperThreshold((1 + sigmaThreshold) * median);
                    break;
                case OTSU1D:
                    // Sets upper threshold to Otsu's 1D threshold and
                    // lower to one half this value
                    int t = Otsu1dOperation.calcThreshold(input, true);
                    // Thresholds are calculated w. normalized grayscale data between 0-255
                    // so we need to scale back to our original input
                    Double max = Math.max(input.getData());
                    Double min = Math.min(input.getData());
                    Double threshold = (t / 255) * (max - min) + min;
                    setLowerThreshold(threshold / 2);
                    setUpperThreshold(threshold);
                    break;
                default:
                    break;
            }
            // Denoise the input data
            Dataset smoothed = blurOperation.run(input);
            // Calculate the grdadient
            GradientVector G = new GradientVector(smoothed);
            Dataset origAngles = G.getAngles();
            double[] angles = origAngles.getData();
            // Round angles to cardinals
            for (int i=0; i<angles.length; i++) {
                angles[i] = roundToCardinal(angles[i]);
            }
            Dataset roundedAngles = new Dataset(angles, origAngles.getWidth(), origAngles.getHeight());
            Dataset origMags = G.getMagnitudes();
            Dataset result = new Dataset(input.getWidth(), input.getHeight());
            // Non-Maximum Suppression - thin edges by only keeping the local maximum along the gradient direction
            for (int i=0; i<origMags.getWidth(); i++) {
                for (int j=0; j<origMags.getHeight(); j++) {
                    double current = origMags.get(i, j);
                    double localMax = gradMax(origMags, i, j, roundedAngles.get(i, j));
                    result.set(i, j, current < localMax ? 0: localMax);
                }
            }
            return hysteresis(result);
        } else {
            log.info("Received null input, sending the same");
            return null;
        }
    }

    /**
     * Calculates the "hysteresis thresholding" of the input.  For each element in the input:
     *
     * 1. If element &gt; upper threshold, it's an edge: set to 1
     * 2. If element &lt; lower threshold, it's a reject: set to 0
     * 3. Otherwise it's a candidate: if at least one of its 8 neighbors is an edge it's also an edge, otherwise reject
     * @param input Dataset to threshold
     * @return thresholded Dataset : edges set to 1, all else to 0
     */
    public Dataset hysteresis(Dataset input) {
        Dataset result = null;
        // TODO:  think about a parameter to set newVal to 1 (normal Canny) or current pixel value.  Keeping magnitudes might be interesting...
        if (input != null) {
            result = new Dataset(input.getWidth(), input.getHeight());
            for (int i=0; i<input.getWidth(); i++) {
                for (int j=0; j<input.getHeight(); j++) {
                    double pixel = input.get(i, j);
                    double newVal = 0; // Assume "weak" pixel and reject
                    if (pixel > upperThreshold) {
                        // "Strong" pixel - Edge
                        newVal = 1;
                    } else if (pixel > lowerThreshold){
                        // Candidate - call an edge IFF it borders at least one strong pixel
                        int lowX = DatasetUtils.safeIdx(i - 1, input.getWidth());
                        int hiX = DatasetUtils.safeIdx(i + 2, input.getWidth());
                        int lowY = DatasetUtils.safeIdx(j - 1, input.getHeight());
                        int hiY = DatasetUtils.safeIdx(j + 2, input.getHeight());
                        neighborloop:
                        for (int x=lowX; x<hiX; x++) {
                            for (int y=lowY; y<hiY; y++) {
                                if (input.get(x, y) != 0) {
                                    newVal = 1;
                                    break neighborloop;
                                }
                            }
                        }
                    }
                    result.set(i, j, newVal);
                }
            }
        }
        return result;
    }

    /**
     * Rounds the specified angle in radians to the closest cardinal direction
     * @param radians angle in radians
     * @return closest angle in degrees: one of 0, 45, 90, or 135
     */
    public double roundToCardinal(double radians) {
        double nearestCardinal = 0;
        double angle = radians;
        if (radians < 0) {
            angle += 6.283185307179586;
        }
        angle = RADIANS2DEGREES * angle;
        if (angle >= 0 && angle < 22.5) {
            nearestCardinal = 0;
        } else if (angle >= 22.5 && angle < 67.5) {
            nearestCardinal = 45;
        } else if (angle >= 67.5 && angle < 112.5) {
            nearestCardinal = 90;
        } else if (angle >= 112.5 && angle < 157.5) {
            nearestCardinal = 135;
        } else if (angle >= 157.5 && angle < 202.5) {
            nearestCardinal = 0;
        } else if (angle >= 202.5 && angle < 247.5) {
            nearestCardinal = 45;
        } else if (angle >= 247.5 && angle < 292.5) {
            nearestCardinal = 90;
        } else if (angle >= 292.5 && angle < 337.5) {
            nearestCardinal = 135;
        } else if (angle >= 337.5 && angle < 360) {
            nearestCardinal = 0;
        }
        return nearestCardinal;
    }

    /**
     * Calculates the local maximum along the gradient.  For a point (i, j) under consideration, find the perpendicular
     * to the gradient angle at (i, j).  Take the two points closes to (i, j) that lie on this perpendicular, and
     * calculate the maximum of the three points.
     *
     * Used to thin edges - keep the largest value and scrap the others.
     *
     * @param input input dataset
     * @param i center element's horizontal index within input
     * @param j center element's vertical index within input
     * @param angle angle of gradient at point i, j
     * @return local maximum value of the three points along a line perpendicular to the angle.
     */
    public double gradMax(Dataset input, int i, int j, double angle) {
        int ang = (int)Math.round(angle);
        double[] points = new double[3];
        points[1] = input.get(i, j);
        int i0=i, i2=i, j0=j, j2=j;
        switch (ang) {
            case 0:
                i0 = i - 1;
                i2 = i + 1;
                j0 = j;
                j2 = j;
                break;
            case 45:
                i0 = i + 1;
                i2 = i - 1;
                j0 = j + 1;
                j2 = j - 1;
                break;
            case 90:
                i0 = i;
                i2 = i;
                j0 = j - 1;
                j2 = j + 1;
                break;
            case 135:
                i0 = i - 1;
                i2 = i + 1;
                j0 = j + 1;
                j2 = j - 1;
                break;
            default:
                break;
        }
        points[0] = input.get(
                DatasetUtils.safeIdx(i0, input.getWidth()),
                DatasetUtils.safeIdx(j0, input.getHeight()));
        points[2] = input.get(
                DatasetUtils.safeIdx(i2, input.getWidth()),
                DatasetUtils.safeIdx(j2, input.getHeight()));
        return Stats.max(points);
    }

    /**
     * Returns the smoothing operation used to denoise input data.
     * @return smoothing operation
     */
    public BlurOperation getBlurOperation() {
        return blurOperation;
    }

    /**
     * Sets the operation used to denoise data prior to edge detection.
     * @param blurOperation smoothing operation
     */
    public void setBlurOperation(BlurOperation blurOperation) {
        this.blurOperation = blurOperation;
    }

    /**
     * Returns the operation used to calculate horizontal and vertical gradients.
     * @return gradient operation
     */
    public SobelOperation getGradientOperation() {
        return gradientOperation;
    }

    /**
     * Sets the operation used to calculate horizontal and vertical gradients
     * @param gradientOperation gradient operation to use
     */
    public void setGradientOperation(SobelOperation gradientOperation) {
        this.gradientOperation = gradientOperation;
    }

    /**
     * Gets the "strong" or upper threshold for edge detection - elements with values greater than or equal to
     * this value are considered edges.
     * @return upper threshold
     */
    public double getUpperThreshold() {
        return upperThreshold;
    }

    /**
     * Sets the "strong" or upper threshold for edge detection - elements with values greater than or equal to
     * this value are considered edges.  Typically 2-3 times the size of the lower threshold.
     * @param upperThreshold strong threshold value
     */
    public void setUpperThreshold(double upperThreshold) {
        this.upperThreshold = upperThreshold;
    }

    /**
     * Gets the "weak" or lower threshold for edge detection - elements with values less than this value are not
     * considered edges and are rejected.
     * @return lower threshold
     */
    public double getLowerThreshold() {
        return lowerThreshold;
    }

    /**
     * Sets the "weak" or lower threshold for edge detection - elements with values greater than or equal to
     * this value are considered edges.  Typically the upper threshold is 2-3 times the size of the lower threshold.
     * @param lowerThreshold weak threshold value
     */
    public void setLowerThreshold(double lowerThreshold) {
        this.lowerThreshold = lowerThreshold;
    }


    /**
     * Returns the current settings for auto thresholding
     * @return AutoThreshold enum
     */
    public AutoThreshold getAutoThreshold() {
        return autoThreshold;
    }

    /**
     * Sets the auto thresholding
     * @param autoThreshold new thresholding setting
     */
    public void setAutoThreshold(AutoThreshold autoThreshold) {
        this.autoThreshold = autoThreshold;
    }

    /**
     * Returns the current threshold parameter for auto-thresholding based on the mean or median.
     * @return threshold parameter (0-1)
     */
    public double getSigmaThreshold() {
        return sigmaThreshold;
    }

    /**
     * Sets the threshold parameter for auto-thresholding based on the mean or median
     * @param sigmaThreshold parameter between 0 and 1
     */
    public void setSigmaThreshold(double sigmaThreshold) {
        this.sigmaThreshold = sigmaThreshold;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("upper_threshold", upperThreshold);
        map.put("lower_threshold", lowerThreshold);
        map.put("auto_threshold", autoThreshold);
        map.put("sigma_threshold", sigmaThreshold);
        if (blurOperation != null) {
            map.put("blurclz", blurOperation.getClass());
            map.put("blurmap", blurOperation.getObjectMap());
        }
        if (gradientOperation != null) {
            map.put("gradclz", gradientOperation.getClass());
            map.put("gradmap", gradientOperation.getObjectMap());
        }
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        upperThreshold = (double) objectMap.getOrDefault("upper_threshold", upperThreshold);
        lowerThreshold = (double) objectMap.getOrDefault("lower_threshold", lowerThreshold);
        autoThreshold = (AutoThreshold) objectMap.getOrDefault("auto_threshold", autoThreshold);
        sigmaThreshold = (double) objectMap.getOrDefault("sigma_threshold", sigmaThreshold);
        if (objectMap.containsKey("blurclz")) {
            Class<? extends BlurOperation> clz = (Class<? extends BlurOperation>) objectMap.get("blurclz");
            try {
                blurOperation = clz.getConstructor().newInstance();
                if (objectMap.containsKey("blurmap")) {
                    blurOperation.init((Map<String, Object>) objectMap.get("blurmap"));
                }
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                log.error("Unable to instantiate blur operation: ", e, " defaulting to standard Gaussian");
                blurOperation = new GaussianBlur();
            }
        }
        if (objectMap.containsKey("gradclz")) {
            Class<? extends SobelOperation> clz = (Class<? extends SobelOperation>) objectMap.get("gradclz");
            try {
                gradientOperation = clz.getConstructor().newInstance();
                if (objectMap.containsKey("gradmap")) {
                    gradientOperation.init((Map<String, Object>) objectMap.get("gradmap"));
                }
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                log.error("Unable to instantiate gradient operation: ", e, " defaulting to standard Sobel");
                gradientOperation = new SobelOperation();
            }
        }
    }
}
