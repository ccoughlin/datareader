/*
 * com.emphysic.myriad.core.data.ops.UpscaleOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * UpscaleOperation - upscales an input by doubling its size and approximating (blurring).
 */
@Slf4j
public class UpscaleOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Convolution kernel to use for approximating the new elements in the upscaled data.
     */
    private ConvolutionOperation approximator;

    /**
     * Creates a new UpscaleOperation
     * @param convolutionOperation convolution operation to use
     */
    public UpscaleOperation(ConvolutionOperation convolutionOperation) {
        this.approximator = convolutionOperation;
    }

    /**
     * Creates a new ConvolutionOperation with the standard upscale kernel.
     */
    public UpscaleOperation() {
        double[][] defaultConvolutionKernel = {
                {0.25, 1.0, 1.25, 1.0, 0.25},
                {1.0, 4.0, 6.0, 4.0, 1.0},
                {1.5, 6.0, 9.0, 6.0, 1.5},
                {1.0, 4.0, 6.0, 4.0, 1.0},
                {0.25, 1.0, 1.25, 1.0, 0.25}
        };
        this.approximator = new ConvolutionOperation(defaultConvolutionKernel);
    }

    /**
     * Upscales an input - doubles its size, approximates the new elements based on their neighbors.
     * @param input Dataset on which to operate
     * @return upscaled representation of the input, or null if input was null.
     */
    @Override
    public Dataset run(Dataset input) {
        if (input == null) {
            return null;
        }
        Dataset upscaled = new Dataset(input.getWidth() * 2, input.getHeight() * 2);
        for (int i = 0; i < upscaled.getWidth(); i++) {
            for (int j = 0; j < upscaled.getHeight(); j++) {
                if (i % 2 == 0 || j % 2 == 0) {
                    int X = i / 2;
                    int Y = j / 2;
                    upscaled.set(i, j, input.get(X, Y));
                }
            }
        }
        return approximator.run(upscaled);
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("approximatorclz", approximator.getClass());
        map.put("approximatormap", approximator.getObjectMap());
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        if (objectMap.containsKey("approximatorclz")) {
            Class<? extends ConvolutionOperation> clz = (Class<? extends ConvolutionOperation>) objectMap.get("approximatorclz");
            try {
                approximator = clz.newInstance();
                if (objectMap.containsKey("approximatormap")) {
                    Map<String, Object> approximatorGraph = (Map<String, Object>) objectMap.get("approximatormap");
                    approximator.init(approximatorGraph);
                }
            } catch (IllegalAccessException | InstantiationException e) {
                log.error("Unable to instantiate convolution operation: ", e, " defaulting to identity convolution operator");
                approximator = new ConvolutionOperation();
            }
        }
    }
}
