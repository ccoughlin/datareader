/*
 * com.emphysic.myriad.core.data.ops.SlidingWindowOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.DatasetUtils;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * SlidingWindowOperation - moves a "window" across a Dataset.
 */
@Slf4j
public class SlidingWindowOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version
    /**
     * Current offset in the horizontal direction
     */
    private int xoffset;
    /**
     * Current offset in the vertical direction
     */
    private int yoffset;
    /**
     * Number of points to move for each new window position
     */
    private int stepSize;
    /**
     * Size of the window (width)
     */
    private int windowWidth;

    /**
     * Size of the window (height)
     */
    private int windowHeight;
    /**
     * An optional DatasetOperation that can be performed on each window.
     */
    private DatasetOperation op;

    /**
     * Creates a new SlidingWindowOperation.
     * @param stepSize points to move between windows
     * @param windowWidth width of window
     * @param windowHeight height of window
     * @param op for each window in the Dataset, perform this operation on the contents
     */
    public SlidingWindowOperation(int stepSize, int windowWidth, int windowHeight, DatasetOperation op) {
        this.xoffset = 0;
        this.yoffset = 0;
        this.stepSize = stepSize;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        this.op = op;
    }

    public SlidingWindowOperation(int stepSize, int windowSize, DatasetOperation op) {
        this(stepSize, windowSize, windowSize, op);
    }

    public SlidingWindowOperation(int stepSize, int windowWidth, int windowHeight) {
        this(stepSize, windowWidth, windowHeight, null);
    }

    /**
     * Creates a new SlidingWindowOperation.  No operations performed on the windows.
     * @param stepSize points to move between windows
     * @param windowWidth use a window of windowWidth x windowWidth points
     */
    public SlidingWindowOperation(int stepSize, int windowWidth) {
        this(stepSize, windowWidth, windowWidth, null);
    }

    /**
     * Creates a new SlidingWindowOperation with a 5x5 window and stepsize 5.  No operations performed on the windows.
     */
    public SlidingWindowOperation() {
        this(5, 5);
    }

    /**
     * Slides across the input Dataset.  If the instance's op is not null, returns the results of performing
     * the operation on the window, otherwise returns the window itself.
     *
     * @return Dataset of window contents, or null if slide is complete or input was null.
     */
    @Override
    public Dataset run(Dataset input) {
        Dataset window = null;
        if (input != null) {
            int width = input.getWidth();
            int height = input.getHeight();

            if (height > 1 && width > 1) {
                // 2D data
                if (yoffset < height - windowWidth + 1) {
                    window = new Dataset(windowWidth, windowHeight);
                    for (int i = 0; i < windowWidth; i++) {
                        for (int j = 0; j < windowHeight; j++) {
                            int X = DatasetUtils.safeIdx(i + xoffset, width);
                            int Y = DatasetUtils.safeIdx(j + yoffset, height);
                            window.set(i, j, input.get(X, Y));
                        }
                    }
                    xoffset += stepSize;
                    if (xoffset >= width - windowWidth + 1) {
                        yoffset += stepSize;
                        xoffset = 0;
                    }
                }
            } else {
                // 1D data
                if (height == 1) {
                    window = slideWidth(input);
                } else if (width == 1) {
                    window = slideHeight(input);
                }
            }
        }
        if (op != null && window != null) {
            return op.run(window);
        }
        return window;
    }

    /**
     * Sliding window across 1D (horizontal) Dataset.
     *
     * @return current window, or null if complete.
     */
    private Dataset slideWidth(Dataset input) {
        Dataset window = null;
        if (xoffset < input.getWidth() - windowWidth + 1) {
            window = new Dataset(windowWidth, 1);
            for (int i = 0; i < windowWidth; i++) {
                window.set(i, 0, input.get(i + xoffset, 0));
            }
            xoffset += stepSize;
        }
        return window;
    }

    /**
     * Sliding window across a 1D (vertical) Dataset.
     *
     * @return current window, or null if complete.
     */
    private Dataset slideHeight(Dataset input) {
        Dataset window = null;
        if (yoffset < input.getHeight() - windowHeight + 1) {
            window = new Dataset(1, windowHeight);
            for (int i = 0; i < windowHeight; i++) {
                window.set(0, i, input.get(0, i + yoffset));
            }
            yoffset += stepSize;
        }
        return window;
    }

    /**
     * Returns the current horizontal offset of the sliding window.
     * @return current offset
     */
    public int getXoffset() {
        return xoffset;
    }

    /**
     * Returns the current vertical offset of the sliding window.
     * @return current offset
     */
    public int getYoffset() {
        return yoffset;
    }

    /**
     * Returns the number of points moved between windows.
     * @return step size
     */
    public int getStepSize() {
        return stepSize;
    }

    /**
     * Returns the size of the square window in points.
     * @return window size
     */
    public int getWindowWidth() {
        return windowWidth;
    }

    /**
     * Returns the size of the square window in points.
     * @return window size
     */
    public int getWindowHeight() {
        return windowHeight;
    }

    /**
     * Returns the DatasetOperation performed on each window.
     * @return DatasetOperation or null if not set
     */
    public DatasetOperation getOp() {
        return op;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("xoffset", xoffset);
        map.put("yoffset", yoffset);
        map.put("step_size", stepSize);
        map.put("window_width", windowWidth);
        map.put("window_height", windowHeight);
        if (op != null) {
            map.put("opclz", op.getClass());
            map.put("opmap", op.getObjectMap());
        }
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        xoffset = (int) objectMap.getOrDefault("xoffset", xoffset);
        yoffset = (int) objectMap.getOrDefault("yoffset", yoffset);
        stepSize = (int) objectMap.getOrDefault("step_size", stepSize);
        windowHeight = (int) objectMap.getOrDefault("window_height", windowHeight);
        windowWidth = (int) objectMap.getOrDefault("window_width", windowWidth);
        if (objectMap.containsKey("opclz")) {
            Class<? extends DatasetOperation> clz = (Class<? extends DatasetOperation>) objectMap.get("opclz");
            try {
                op = clz.getConstructor().newInstance();
                if (objectMap.containsKey("opmap")) {
                    op.init((Map<String, Object>) objectMap.get("opmap"));
                }
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                log.error("Unable to instantiate sliding window operation: ", e);
                op = null;
            }
        }
    }
}
