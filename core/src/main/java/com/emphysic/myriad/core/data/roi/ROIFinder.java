/*
 * com.emphysic.myriad.core.data.roi.ROIFinder
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.ObjectMap;

import java.io.File;
import java.io.IOException;

public interface ROIFinder extends ObjectMap {
    /**
     * Examine an array of data and report whether it appears to contain a region of interest (ROI)
     * @param data raw data to examine
     * @return true if data appears to contain an ROI, false otherwise
     */
    boolean isROI(double[] data);

    /**
     * Examine a dataset and return whether or not it seems to contain a region of interest (ROI)
     * @param dataset data to examine
     * @return true if data appears to contain an ROI, false otherwise
     */
    boolean isROI(Dataset dataset);

    /**
     * Loads a Region of Interest finder from disk.
     * @param inFile input file
     * @param clz class of ROIFinder
     * @return new ROIFinder
     * @throws InstantiationException error instantiating the ROIFinder (abstract, interface, etc.)
     * @throws IllegalAccessException constructor isn't accessible
     * @throws IOException if an I/O error occurs reading the input file
     */
    static ROIFinder fromFile(File inFile, Class<? extends ROIFinder> clz)
            throws InstantiationException, IllegalAccessException, IOException {
        ROIFinder r = clz.newInstance();
        r.load(inFile);
        return r;
    }
}
