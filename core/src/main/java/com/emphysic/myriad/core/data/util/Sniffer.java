/*
 * com.emphysic.myriad.core.data.util.Sniffer
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

import com.emphysic.myriad.core.data.io.IODataset;

import java.io.File;
import java.io.IOException;

/**
 * Sniffer - an interface for attempting to automatically read an input file
 */
public interface Sniffer {

    /**
     * Attempts to read an input file automatically
     * @param inputFile file to read
     * @return Dataset of contents or null if a problem occurred
     * @throws IOException if an I/O error occurred
     */
    IODataset sniff(File inputFile) throws IOException;
}
