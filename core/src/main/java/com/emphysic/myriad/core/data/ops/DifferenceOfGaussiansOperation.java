/*
 * com.emphysic.myriad.core.data.ops.DifferenceOfGaussiansOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * DifferenceOfGaussiansOperation - performs the Difference of Gaussians (DOG) operation on an input.
 * Primarily used as a Region of Interest (ROI) detector or for feature enhancement.
 */
@Slf4j
public class DifferenceOfGaussiansOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * First blur operation
     */
    BlurOperation firstBlur;
    /**
     * Second blur operation
     */
    BlurOperation secondBlur;

    /**
     * Creates a new DOGOperation with the specified blur operations.
     * @param first first blur operation.
     * @param second second blur operation.
     */
    public DifferenceOfGaussiansOperation(BlurOperation first, BlurOperation second) {
        firstBlur = first;
        secondBlur = second;
    }

    /**
     * Creates a new DOGOperation with two GaussianBlurOperations.
     * @param firstRadius radius of first GaussianOperation
     * @param secondRadius radius of second GaussianOperation
     */
    public DifferenceOfGaussiansOperation(int firstRadius, int secondRadius) {
        this(new GaussianBlur(firstRadius), new GaussianBlur(secondRadius));
    }

    /**
     * Creates a new DOGOperation with blur radii of 1 and 2.
     */
    public DifferenceOfGaussiansOperation() {
        this(1, 2);
    }

    /**
     * Performs both blur operations on an input, and returns a new Dataset consisting of the difference between the
     * two.
     * @param input Dataset on which to operate
     * @return resultant Dataset, or null if input was null
     */
    @Override
    public Dataset run(Dataset input) {
        if (input != null) {
            Dataset first = firstBlur.run(input);
            Dataset second = secondBlur.run(input);
            int w1 = first.getWidth();
            int w2 = second.getWidth();
            int h1 = first.getHeight();
            int h2 = second.getHeight();
            if (w1 != w2 || h1 != h2) {
                throw new IllegalArgumentException("Input dimensions do not match: " + w1 + "x" + h1 + " vs. " + w2 + "x" + h2);
            }
            Dataset result = new Dataset(first.getWidth(), first.getHeight());
            for (int i = 0; i < w1; i++) {
                for (int j = 0; j < h1; j++) {
                    result.set(i, j, first.get(i, j) - second.get(i, j));
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("firstblurclz", firstBlur.getClass());
        map.put("secondblurclz", secondBlur.getClass());
        map.put("firstblurgraph", firstBlur.getObjectMap());
        map.put("secondblurgraph", secondBlur.getObjectMap());
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        if (objectMap.containsKey("firstblurclz")) {
            Class<? extends BlurOperation> clz = (Class<? extends BlurOperation>) objectMap.get("firstblurclz");
            try {
                firstBlur = clz.getConstructor().newInstance();
                firstBlur.init((Map<String, Object>) objectMap.get("firstblurgraph"));
            } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                log.error("Error instantiating blur operation ", clz, e);
            }
        }
        if (objectMap.containsKey("secondblurclz")) {
            Class<? extends BlurOperation> clz = (Class<? extends BlurOperation>) objectMap.get("secondblurclz");
            try {
                secondBlur = clz.getConstructor().newInstance();
                secondBlur.init((Map<String, Object>) objectMap.get("secondblurgraph"));
            } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                log.error("Error instantiating blur operation ", clz, e);
            }
        }
    }
}
