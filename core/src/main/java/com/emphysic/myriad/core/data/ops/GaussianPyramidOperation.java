/*
 * com.emphysic.myriad.core.data.ops.GaussianPyramidOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * GaussianPyramidOperation - repeated smoothing and subsampling of an input.  Primarily
 * used to allow other operations to consider input data at multiple scales.
 */
@Slf4j
public class GaussianPyramidOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version
    private BlurOperation blur;
    private int scaleFactor;
    private int windowSize;

    /**
     * Creates a new GaussianPyramidOperation
     * @param blurOp blur operation
     * @param scaleFactor scaling factor to use e.g. 2 to halve the size at each step
     * @param windowSize size of the smallest step
     */
    public GaussianPyramidOperation(BlurOperation blurOp, int scaleFactor, int windowSize) {
        if (scaleFactor < 2) {
            throw new IllegalArgumentException("Scaling factor must be 2 or greater.");
        }
        if (windowSize < 1) {
            throw new IllegalArgumentException("Window size must be 1 or greater.");
        }
        blur = blurOp;
        this.scaleFactor = scaleFactor;
        this.windowSize = windowSize;
    }

    /**
     * Creates a new GaussianPyramidOperation with a GaussianBlur of radius 5, halves the size of the input at each
     * step, and stops if the width and/or height of a step is less than 1.
     */
    public GaussianPyramidOperation() {
        this(new GaussianBlur(5), 2, 1);
    }

    /**
     * Creates a new GaussianPyramidOperation with the specified blur op, halves the size of the input at each step,
     * and stops if the width and/or height of a step is less than 1.
     * @param blurOperation blur operation to use
     */
    public GaussianPyramidOperation(BlurOperation blurOperation) {
        this(blurOperation, 2, 1);
    }

    /**
     * Creates a new GaussianPyramidOperation with no blur, halving the size at each step, and stopping if the width
     * and/or height of a step is less than the specified window size.  Primarily used for testing.
     * @param scaleFactor scaling factor between successive steps, typically 2 for halving at each step
     * @param windowSize size of the window
     */
    public GaussianPyramidOperation(int scaleFactor, int windowSize) {
        this(null, scaleFactor, windowSize);
    }

    /**
     * Blurs and subsamples the input data.  Returns the results or null if the result is smaller than
     * the operation's window in width or height.
     */
    @Override
    public Dataset run(Dataset input) {
        if (input == null) {
            return null;
        }
        int w = input.getWidth();
        int h = input.getHeight();

        if (w > 1 && h > 1) {
            // 2-D Dataset
            int wFactor = w / scaleFactor + w % scaleFactor;
            int hFactor = h / scaleFactor + h % scaleFactor;
            Dataset output = new Dataset(wFactor, hFactor);

            int ix = 0;
            int iy = 0;
            for (int i = 0; i < w; i += scaleFactor) {
                for (int j = 0; j < h; j += scaleFactor) {
                    double val = input.get(i, j);
                    output.set(ix, iy, val);
                    iy++;
                }
                ix++;
                iy = 0;
            }
            if (blur != null) {
                output = blur.run(output);
                blur.setRadius(2 * blur.getRadius());
            }
            if (output.getHeight() > windowSize && output.getWidth() > windowSize) {
                return output;
            }
            return null;
        }
        // Dataset is 1-D
        return pyramid1D(input);
    }

    /**
     * Runs the Pyramid operation on a 1-D Dataset
     *
     * @param input input Dataset
     * @return results of operation or null if done / not 1-D
     */
    private Dataset pyramid1D(Dataset input) {
        int w = input.getWidth();
        int h = input.getHeight();
        if (w == 1 && h == 1) {
            return null;
        } else if (w == 1) {
            return pyramidH(input);
        } else if (h == 1) {
            return pyramidW(input);
        }
        return null;
    }

    /**
     * Pyramid operation across 1D (horizontal) Dataset.
     *
     * @return current window, or null if complete.
     */
    private Dataset pyramidW(Dataset input) {
        int w = input.getWidth();
        int ix = 0;
        Dataset output = new Dataset(w / scaleFactor + w % scaleFactor, 1);
        for (int i = 0; i < w; i += scaleFactor) {
            double val = input.get(i, 0);
            output.set(ix, 0, val);
            ix++;
        }
        if (blur != null) {
            output = blur.run(output);
        }
        if (output.getWidth() > windowSize) {
            return output;
        }
        return null;
    }

    /**
     * Sliding window across a 1D (vertical) Dataset.
     *
     * @return current window, or null if complete.
     */
    private Dataset pyramidH(Dataset input) {
        int h = input.getHeight();
        int iy = 0;
        Dataset output = new Dataset(1, h / scaleFactor + h % scaleFactor);
        for (int j = 0; j < h; j += scaleFactor) {
            double val = input.get(0, j);
            output.set(0, iy, val);
            iy++;
        }
        if (blur != null) {
            output = blur.run(output);
        }
        if (output.getHeight() > windowSize) {
            return output;
        }
        return null;
    }

    /**
     * Blur operation
     */
    public BlurOperation getBlur() {
        return blur;
    }

    public void setBlur(BlurOperation blur) {
        this.blur = blur;
    }

    /**
     * Decimation factor - typically 2 to make each level of the pyramid 1/2 the size of its predecessor.
     */
    public int getScaleFactor() {
        return scaleFactor;
    }

    /**
     * Decimation factor - typically 2 to make each level of the pyramid 1/2 the size of its predecessor.
     * @param scaleFactor scaling factor between succesive levels of the pyramid
     */
    public void setScaleFactor(int scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    /**
     * Size of the smallest level of the pyramid - stops when this threshold is reached.
     */
    public int getWindowSize() {
        return windowSize;
    }

    /**
     * Sets the cutoff size of the pyramid - operation is complete when this threshold is reached
     * @param windowSize new threshold
     */
    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("scale_factor", scaleFactor);
        map.put("window_size", windowSize);
        if (blur != null) {
            map.put("blurclz", blur.getClass());
            map.put("blurmap", blur.getObjectMap());
        }
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        scaleFactor = (int) objectMap.getOrDefault("scale_factor", scaleFactor);
        windowSize = (int) objectMap.getOrDefault("window_size", windowSize);
        if (objectMap.containsKey("blurclz")) {
            Class<? extends BlurOperation> clz = (Class<? extends BlurOperation>) objectMap.get("blurclz");
            try {
                blur = clz.getConstructor().newInstance();
                if (objectMap.containsKey("blurmap")) {
                    blur.init((Map<String, Object>) objectMap.get("blurmap"));
                }
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                log.error("Unable to instantiate blur operation: ", e, " defaulting to standard Gaussian");
                blur = new GaussianBlur();
            }
        }
    }
}
