/*
 * com.emphysic.myriad.core.data.ops.ChainedDatasetOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ChainedDatasetOperation - performs multiple DatasetOperations on an input.
 */
@Slf4j
public class ChainedDatasetOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * List of DatasetOperations to perform on incoming data
     */
    private List<DatasetOperation> operations;

    /**
     * Creates a new empty ChainedDatasetOperation.  @see #ChainedDatasetOperation.withOp.
     */
    public ChainedDatasetOperation() {
        operations = new ArrayList<>();
    }

    /**
     * Runs each DatasetOperation in turn and returns the final result.
     *
     * @param input original Dataset
     * @return result of running operations.get(0), operations.get(1), ... operations.get(N)
     */
    @Override
    public Dataset run(Dataset input) {
        if (input != null) {
            Dataset result = new Dataset(input);
            for (DatasetOperation op : operations) {
                result = op.run(result);
            }
            return result;
        }
        return null;
    }

    /**
     * Adds a new DatasetOperation to the chain.  Returns this instance for chaining,
     * e.g. chain.withOp(Operation1).withOp(Operation2)...
     *
     * @param idx position of the operation - the 0th operation being the first operation performed.
     * @param op  DatasetOperation to add to the chain
     * @return this
     */
    public ChainedDatasetOperation withOp(int idx, DatasetOperation op) {
        if (operations.size() == 0) {
            return withOp(op);
        }
        operations.add(idx, op);
        return this;
    }

    /**
     * Appends a new DatasetOperation as the last operation in the chain.  Returns this instance for chaining,
     * e.g. chain.withOp(Operation1).withOp(Operation2)...
     *
     * @param op DatasetOperation to add to the chain
     * @return this
     */
    public ChainedDatasetOperation withOp(DatasetOperation op) {
        operations.add(op);
        return this;
    }

    /**
     * Returns the current list of DatasetOperations.
     *
     * @return current chain
     */
    public List<DatasetOperation> getOps() {
        return operations;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        List<Map<String, Object>> opGraphs = new ArrayList<>();
        for (DatasetOperation op : operations) {
            Map<String, Object> opMap = op.getObjectMap();
            opMap.put("opclz", op.getClass());
            opGraphs.add(opMap);
        }
        map.put("operations", opGraphs);
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        if (objectMap.containsKey("operations")) {
            operations = new ArrayList<>();
            List<Map<String, Object>> opGraphs = (List<Map<String, Object>>) objectMap.get("operations");
            for (Map<String, Object> graph : opGraphs) {
                DatasetOperation op;
                Class<? extends DatasetOperation> opclz = (Class<? extends DatasetOperation>) graph.get("opclz");
                try {
                    op = opclz.getConstructor().newInstance();
                    op.init(graph);
                    operations.add(op);
                } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                    log.error("Error instantiating DatasetOperation ", opclz, e);
                }
            }
        }
    }
}
