/*
 * com.emphysic.myriad.core.data.ops.DatasetOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.ObjectMap;

/**
 * DatasetOperation - an operation on Datasets that returns a resultant Dataset.
 */
public interface DatasetOperation extends ObjectMap {
    /**
     * Runs the operation on an input.
     * @param input Dataset on which to operate
     * @return result
     */
    Dataset run(Dataset input);
}
