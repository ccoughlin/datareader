/*
 * com.emphysic.myriad.core.ml.CrossValidation
 *
 * Copyright (c) 2018 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.ml;

import com.emphysic.myriad.core.data.roi.MLROIFinder;
import smile.math.distance.Distance;
import smile.math.distance.EuclideanDistance;
import smile.neighbor.LinearSearch;
import smile.neighbor.Neighbor;
import smile.validation.Accuracy;

import java.util.*;

/**
 * CrossValidation - splits data into train/test and evaluates a machine learning model.
 */
public abstract class CrossValidation {
    /**
     * RNG
     */
    protected Random random;
    /**
     * Original samples
     */
    protected double[][] X;
    /**
     * Original sample labels
     */
    protected int[] y;

    /**
     * Create a new cross validator.
     *
     * @param samples N samples of M features
     * @param labels  N labels
     */
    public CrossValidation(double[][] samples, int[] labels) {
        random = new Random();
        this.X = samples;
        this.y = labels;
    }

    /**
     * Create a new cross validator
     * @param data samples and labels to use
     */
    public CrossValidation(Data data) {
        random = new Random();
        this.X = data.samples;
        this.y = data.labels;
    }

    /**
     * Creates a new empty cross-validator.
     */
    public CrossValidation() {
        random = new Random();
    }
    /**
     * Partition the input data into a training and a test set
     *
     * @return input data partitioned into a training and a test set
     */
    public abstract TrainTestSubsets getTrainTestSubset();

    /**
     * Partition of input data
     */
    public static class Data {
        public double[][] samples;
        public int[] labels;

        public Data(int numSamples, int numFeatures) {
            samples = new double[numSamples][numFeatures];
            labels = new int[numSamples];
        }

        public Data(double[][] samples, int[] labels) {
            this.samples = samples;
            this.labels = labels;
        }

        public int numSamples() { return samples.length; }
        public int numFeatures() {
            if (samples != null && samples.length > 0) {
                return samples[0].length;
            }
            return 0;
        }
    }

    /**
     * Tallies the frequencies of each label in a dataset.
     * @param data data to tally
     * @return Map where keys are each label encountered and values are total number of samples in data observed with
     * that label.
     */
    public static Map<Integer, Integer> labelFrequencies(Data data) {
        HashMap<Integer, Integer> labelFreqs = new HashMap<>();
        for (int i=0; i<data.numSamples(); i++) {
            int label = data.labels[i];
            int value = 0;
            if (labelFreqs.containsKey(label)) {
                value = labelFreqs.get(label);
            }
            value++;
            labelFreqs.put(label, value);
        }
        return labelFreqs;
    }

    /**
     * Finds the minority label in a sample set, i.e. the label for which the fewest observations are found.
     * @param labelFreqs label frequencies map
     * @return label with fewest number of samples
     */
    public static Integer findMinorityLabel(Map<Integer, Integer> labelFreqs) {
        Set<Integer> keys = labelFreqs.keySet();
        int minorityLabel = keys.iterator().next();
        int minValue = labelFreqs.get(minorityLabel);
        for (Integer label : labelFreqs.keySet()) {
            int labelCount = labelFreqs.get(label);
            if (labelCount < minValue) {
                minValue = labelCount;
                minorityLabel = label;
            }
        }
        return minorityLabel;
    }

    /**
     * Finds the minority label in a sample set, i.e. the label for which the fewest observations are found.
     * @param data data to examine
     * @return label with fewest number of samples
     */
    public static Integer findMinorityLabel(Data data) {
        return findMinorityLabel(labelFrequencies(data));
    }

    /**
     * Finds the majority label in a sample set, i.e. the label for which the most observations are found.
     * @param labelFreqs label frequencies map
     * @return label with the most number of samples
     */
    public static Integer findMajorityLabel(Map<Integer, Integer> labelFreqs) {
        Set<Integer> keys = labelFreqs.keySet();
        int majorityLabel = keys.iterator().next();
        int maxValue = labelFreqs.get(majorityLabel);
        for (Integer label : labelFreqs.keySet()) {
            int labelCount = labelFreqs.get(label);
            if (labelCount > maxValue) {
                maxValue = labelCount;
                majorityLabel = label;
            }
        }
        return majorityLabel;
    }

    /**
     * Finds the majority label in a sample set, i.e. the label for which the most observations are found.
     * @param data data to examine
     * @return label for which the most samples in data were found
     */
    public static Integer findMajorityLabel(Data data) {
        return findMajorityLabel(labelFrequencies(data));
    }

    /**
     * Balances imbalanced data with the SMOTE (Synthetic Minority Over-sampling TEchnique) as per
     * https://www.cs.cmu.edu/afs/cs/project/jair/pub/volume16/chawla02a-html/node6.html .  The data are examined and
     * the majority and minority labels are identified as the label found for the most and fewest samples respectively.
     * The ratio of these sample subsets determines how many synthetic samples are generated.
     * @param orig Original (possibly) imbalanced data
     * @param distanceMetric distance metric to use for finding nearest neighbors
     * @param nn number of nearest neighbors to find
     * @return new data consisting of both the original and synthetic data
     */
    public static Data balanceUp(Data orig, Distance<double[]> distanceMetric, int nn) {
        LinearSearch<double[]> ls = new LinearSearch<>(orig.samples, distanceMetric);
        List<double[]> oversamples = new ArrayList<>();
        List<Integer> oversampleLabels = new ArrayList<>();
        Map<Integer, Integer> labelFreqs = labelFrequencies(orig);
        int minorityLabel = findMinorityLabel(labelFreqs);
        int majorityLabel = findMajorityLabel(labelFreqs);
        int sampleFactor = labelFreqs.get(majorityLabel) / labelFreqs.get(minorityLabel);
        Random rnd = new Random();
        for (int i=0; i<orig.numSamples(); i++) {
            double[] sample = orig.samples[i];
            oversamples.add(sample);
            oversampleLabels.add(orig.labels[i]);
            if (orig.labels[i] == minorityLabel) {
                Neighbor<double[], double[]>[] neighbors = ls.knn(sample, nn);
                for (int j=0; j<sampleFactor; j++) {
                    int rndIdx = rnd.nextInt(neighbors.length);
                    double[] neighbor = neighbors[rndIdx].value;
                    double[] synthSample = new double[orig.numFeatures()];
                    double featureMult = rnd.nextDouble();
                    for (int feature=0; feature<orig.numFeatures(); feature++) {
                        double delta = neighbor[feature] - sample[feature];
                        synthSample[feature] = sample[feature] + featureMult * delta;
                    }
                    oversamples.add(synthSample);
                    oversampleLabels.add(minorityLabel);
                }
            }
        }
        double[][] samples = oversamples.toArray(new double[0][]);
        int[] labels = oversampleLabels.stream().mapToInt(i -> i).toArray();
        return new Data(samples, labels);
    }

    /**
     * Balances imbalanced data with the SMOTE (Synthetic Minority Over-sampling TEchnique) as per
     * https://www.cs.cmu.edu/afs/cs/project/jair/pub/volume16/chawla02a-html/node6.html .  The data are examined and
     * the majority and minority labels are identified as the label found for the most and fewest samples respectively.
     * The ratio of these sample subsets determines how many synthetic samples are generated.  Neighbors are determined
     * with simple linear (Euclidean) distance.
     * @param orig data to balance
     * @param nn number of nearest neighbors to use
     * @return new data consisting of both the original and synthetic data
     */
    public static Data balanceUp(Data orig, int nn) {
        return balanceUp(orig, new EuclideanDistance(), nn);
    }

    /**
     * Balances imbalanced data with the SMOTE (Synthetic Minority Over-sampling TEchnique) as per
     * https://www.cs.cmu.edu/afs/cs/project/jair/pub/volume16/chawla02a-html/node6.html .  The data are examined and
     * the majority and minority labels are identified as the label found for the most and fewest samples respectively.
     * The ratio of these sample subsets determines how many synthetic samples are generated.  Neighbors are determined
     * with simple linear (Euclidean) distance, and as per the original paper five nearest neighbors are chosen.
     * @param orig data to balance
     * @return new data consisting of both the original and synthetic data
     */
    public static Data balanceUp(Data orig) {
        return balanceUp(orig,5);
    }

    /**
     * Helper class to partition input data
     */
    public static class TrainTestSubsets {
        public Data training;
        public Data testing;

        public TrainTestSubsets() {
        }

        public TrainTestSubsets(Data training, Data testing) {
            this.training = training;
            this.testing = testing;
        }
    }

    /**
     * Evaluates the specified models for accuracy i.e. the proportion of true results
     * (both true positives and true negatives) in the overall results.
     *
     * @param numRounds number of rounds of testing
     * @param models    models to test
     * @param upSample if true, upsample training data to try to balance minority / majority classes
     * @return the accuracy measurements for each model for each of the rounds of testing
     * @throws Exception if an error occurs training the models
     */
    public Map<MLROIFinder, List<Double>> evalModels(int numRounds, MLROIFinder[] models, boolean upSample) throws Exception {
        Map<MLROIFinder, List<Double>> modelAccuracies = new HashMap<>();
        for (MLROIFinder model : models) {
            modelAccuracies.put(model, new ArrayList<>());
        }
        for (int round = 0; round < numRounds; round++) {
            TrainTestSubsets data = getTrainTestSubset();
            if (upSample) {
                data.training = balanceUp(data.training);
            }
            for (MLROIFinder model : models) {
                model.train(data.training.samples, data.training.labels);
                int[] truths = data.testing.labels;
                int[] predictions = new int[truths.length];
                for (int i = 0; i < truths.length; i++) {
                    predictions[i] = (int) model.predict(data.testing.samples[i]);
                }
                modelAccuracies.get(model).add(new Accuracy().measure(truths, predictions));
            }
        }
        return modelAccuracies;
    }


    /**
     * Evaluates the specified models for accuracy i.e. the proportion of true results
     * (both true positives and true negatives) in the overall results.
     *
     * @param numRounds number of rounds of testing
     * @param models    models to test
     * @return the accuracy measurements for each model for each of the rounds of testing
     * @throws Exception if an error occurs training the models
     */
    public Map<MLROIFinder, List<Double>> evalModels(int numRounds, MLROIFinder[] models) throws Exception {
        return evalModels(numRounds, models, false);
    }

    /**
     * Evaluates the specified models for mean accuracy (ratio of true results / total results).
     *
     * @param numRounds number of rounds of testing
     * @param models    models to test
     * @param upSample if true, upsample training data to try to balance minority / majority classes
     * @return average(mean) accuracy for each model
     * @throws Exception if an error occurs
     */
    public Map<MLROIFinder, Double> eval(int numRounds, MLROIFinder[] models, boolean upSample) throws Exception {
        Map<MLROIFinder, List<Double>> accuracies = evalModels(numRounds, models, upSample);
        Map<MLROIFinder, Double> meanAccuracies = new HashMap<>();
        for (MLROIFinder model : accuracies.keySet()) {
            double mean = 0;
            for (Double accuracy : accuracies.get(model)) {
                mean += accuracy;
            }
            meanAccuracies.put(model, mean / accuracies.get(model).size());
        }
        return meanAccuracies;
    }

    /**
     * Evaluates the specified models for mean accuracy (ratio of true results / total results).
     *
     * @param numRounds number of rounds of testing
     * @param models    models to test
     * @return average(mean) accuracy for each model
     * @throws Exception if an error occurs
     */
    public Map<MLROIFinder, Double> eval(int numRounds, MLROIFinder[] models) throws Exception {
        return eval(numRounds, models, false);
    }

    /**
     * Evaluates the specified models and returns the most accurate (ratio of true results / total results) model.
     *
     * @param numRounds number of rounds of testing
     * @param models    models to test
     * @param upSample if true, upsample training data to try to balance minority / majority classes
     * @return the most accurate model found and its mean accuracy score.
     * @throws Exception if an error occurs
     */
    public Map.Entry<MLROIFinder, Double> findBestModel(int numRounds, MLROIFinder[] models, boolean upSample) throws Exception {
        Map<MLROIFinder, Double> meanModelAccuracies = eval(numRounds, models, upSample);
        MLROIFinder bestModel = null;
        Double bestAccuracy = Double.MIN_VALUE;
        for (MLROIFinder model : meanModelAccuracies.keySet()) {
            Double currentAccuracy = meanModelAccuracies.get(model);
            if (currentAccuracy > bestAccuracy) {
                bestAccuracy = currentAccuracy;
                bestModel = model;
            }
        }
        return new AbstractMap.SimpleEntry<>(bestModel, bestAccuracy);
    }

    /**
     * Evaluates the specified models and returns the most accurate (ratio of true results / total results) model.
     *
     * @param numRounds number of rounds of testing
     * @param models    models to test
     * @return the most accurate model found and its mean accuracy score.
     * @throws Exception if an error occurs
     */
    public Map.Entry<MLROIFinder, Double> findBestModel(int numRounds, MLROIFinder[] models) throws Exception {
        return findBestModel(numRounds, models, false);
    }
}
