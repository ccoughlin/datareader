/*
 * com.emphysic.myriad.core.ml.MLDataCompiler
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.ml;

import com.emphysic.myriad.core.data.io.IODataset;
import com.emphysic.myriad.core.data.ops.DatasetOperation;
import com.emphysic.myriad.core.data.util.FileSniffer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * MLDataCompiler - gather and prepare labelled data for cross validation
 */
public class MLDataCompiler {
    /**
     * Folder containing positive samples
     */
    private File posFolder;
    /**
     * Folder containing negative samples
     */
    private File negFolder;
    /**
     * Value of the positive label
     */
    private double posLabel;
    /**
     * Value of the negative label
     */
    private double negLabel;

    /**
     * Constructs a new data compiler.
     *
     * @param positiveSamplesFolder File folder containing positive (e.g. is a flaw) samples.
     * @param negativeSamplesFolder File folder containing negative (e.g. is not a flaw) samples.
     * @param posLabel value assigned to positive samples
     * @param negLabel value assigned to negative samples
     */
    public MLDataCompiler(File positiveSamplesFolder, File negativeSamplesFolder, double posLabel, double negLabel) {
        this.posFolder = positiveSamplesFolder;
        this.negFolder = negativeSamplesFolder;
        this.posLabel = posLabel;
        this.negLabel = negLabel;
    }

    /**
     * Constructs a new data compiler, using 1.0 as the positive label value and 0.0 as the negative samples folder.
     * @param positiveSamplesFolder positive samples folder
     * @param negativeSamplesFolder negative samples folder
     */
    public MLDataCompiler(File positiveSamplesFolder, File negativeSamplesFolder) {
        this(positiveSamplesFolder, negativeSamplesFolder, 1, 0);
    }

    /**
     * Reads the positive and negative samples and compiles the resultant data.
     * @param op operation to perform on data during compilation process
     * @return complete Data
     * @throws Exception if an error occurs reading folders, etc.
     */
    public CrossValidation.Data readData(DatasetOperation op) throws Exception {
        List<IODataset> samples = new ArrayList<>();
        List<Integer> sampleLabels = new ArrayList<>();
        List<Integer> indices = new ArrayList<>();
        List<IODataset> positiveSamples = readFolder(posFolder);
        List<IODataset> negativeSamples = readFolder(negFolder);
        for (int i = 0; i < positiveSamples.size() + negativeSamples.size(); i++) {
            indices.add(i);
        }
        Collections.shuffle(indices);
        samples.addAll(positiveSamples);
        samples.addAll(negativeSamples);
        for (int i = 0; i < positiveSamples.size(); i++) {
            sampleLabels.add((int) posLabel);
        }
        for (int i = 0; i < negativeSamples.size(); i++) {
            sampleLabels.add((int) negLabel);
        }
        double[][] X = new double[samples.size()][samples.get(0).getSize()];
        int[] y = new int[sampleLabels.size()];
        for (int i = 0; i < samples.size(); i++) {
            int idx = indices.remove(0);
            if (op != null) {
                X[i] = op.run(samples.get(idx)).getData();
            }
            y[i] = sampleLabels.get(idx);
        }
        return new CrossValidation.Data(X, y);
    }

    /**
     * Reads the positive and negative samples and compiles the resultant data.
     * @return complete Data
     * @throws Exception if an error occurs reading folders, etc.
     */
    public CrossValidation.Data readData() throws Exception {
        return readData(null);
    }

    /**
     * Reads all the Datasets from a given file folder.
     *
     * @param folder folder to load
     * @return List of all Datasets load from specified folder
     * @throws Exception if an I/O error occurs
     */
    public static List<IODataset> readFolder(File folder) throws Exception {
        File[] files = folder.listFiles();
        assert (files != null);
        List<IODataset> datasets = new ArrayList<>();
        for (File f : files) {
            IODataset d = FileSniffer.read(f, true);
            if (d != null) {
                datasets.add(d);
            }
        }
        return datasets;
    }
}
