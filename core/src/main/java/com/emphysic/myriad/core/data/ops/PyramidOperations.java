/*
 * com.emphysic.myriad.core.data.ops.PyramidOperations
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.io.IODataset;
import com.emphysic.myriad.core.data.util.FileSniffer;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * PyramidOperations - general purpose pyramid operations.
 */
public class PyramidOperations {
    /**
     * Upscaling operation
     */
    private static final UpscaleOperation upo = new UpscaleOperation();
    /**
     * Power of two operation
     */
    private static final PowerOfTwoOperation pto = new PowerOfTwoOperation();

    /**
     * Blends the levels of a Laplacian pyramid into a single composite.
     *
     * @param pyramidLevels levels of the pyramid
     * @return blended result
     * @see PyramidOperations#generateLaplacian(List) to generate the Laplacian pyramid from a Gaussian pyramid
     */
    public static Dataset collapse(List<Dataset> pyramidLevels) {
        Dataset current;
        current = pyramidLevels.get(pyramidLevels.size() - 1);
        for (int i = pyramidLevels.size() - 2; i >= 0; i--) {
            Dataset up = expand(current);
            Dataset merged = new Dataset(up.getWidth(), up.getHeight());
            Dataset currentLevel = pyramidLevels.get(i);
            for (int x = 0; x < merged.getWidth(); x++) {
                for (int y = 0; y < merged.getHeight(); y++) {
                    merged.set(x, y, up.get(x, y) + currentLevel.get(x, y));
                }
            }
            current = merged;
        }
        return current;
    }

    /**
     * Generates the Laplacian Pyramid of an input and blends the levels into a single composite result.
     * @param input original Dataset
     * @param gaussianPyramidOperation pyramid operation to perform
     * @return blended Dataset
     */
    public static Dataset collapse(Dataset input, GaussianPyramidOperation gaussianPyramidOperation) {
        return collapse(generateLaplacian(input, gaussianPyramidOperation));
    }

    /**
     * Generates the Laplacian Pyramid of an input and blends the levels into a single composite result.
     * @param input original Dataset
     * @return blended Dataset
     */
    public static Dataset collapse(Dataset input) {
        return collapse(generateLaplacian(input));
    }

    /**
     * Generates the Laplacian pyramid from a list of Gaussian pyramid steps.  Note that the first element
     * in the list must be the original dataset for the complete pyramid, e.g. the list is generated as
     * <p>
     * GaussianPyramidOperation = new GaussianPyramidOperation();
     * Dataset current = new Dataset(imageDataset);
     * do {
     * steps.add(current);
     * } while ((current = gpo.run(current)) != null);
     *
     * @param steps complete Gaussian pyramid
     * @return approximation to the Laplacian operator for the original Dataset
     */
    public static List<Dataset> generateLaplacian(List<Dataset> steps) {
        List<Dataset> laplacianPyramid = new ArrayList<>();
        for (int i = 0; i < steps.size() - 1; i++) {
            Dataset current = pto.run(steps.get(i));
            Dataset up = expand(pto.run(steps.get(i + 1)));
            Dataset L = new Dataset(current.getWidth(), current.getHeight());
            for (int x = 0; x < current.getWidth(); x++) {
                for (int y = 0; y < current.getHeight(); y++) {
                    L.set(x, y, current.get(x, y) - up.get(x, y));
                }
            }
            laplacianPyramid.add(L);
        }
        laplacianPyramid.add(steps.get(steps.size() - 1));
        return laplacianPyramid;
    }

    /**
     * Generates the Laplacian pyramid from an original Dataset and a Gaussian Pyramid operation.
     *
     * @param input                    original Dataset
     * @param gaussianPyramidOperation pyramid operation to perform
     * @return approximation to the Laplacian operator for the original Dataset
     */
    public static List<Dataset> generateLaplacian(Dataset input,
                                                  GaussianPyramidOperation gaussianPyramidOperation) {
        List<Dataset> pyramid = new ArrayList<>();
        Dataset current = new Dataset(input);
        do {
            pyramid.add(current);
        } while ((current = gaussianPyramidOperation.run(current)) != null);
        return generateLaplacian(pyramid);
    }

    public static List<Dataset> generateLaplacian(Dataset input) {
        return generateLaplacian(input, new GaussianPyramidOperation());
    }

    /**
     * Returns the next step in the specified Gaussian pyramid
     *
     * @param input                    original input
     * @param gaussianPyramidOperation pyramid operation
     * @return next step in the pyramid
     */
    public static Dataset reduce(Dataset input, GaussianPyramidOperation gaussianPyramidOperation) {
        return gaussianPyramidOperation.run(input);
    }

    /**
     * Returns a reduction of the specified Dataset, 1/2 the size in each dimension and Gaussian blurred
     *
     * @param input original input
     * @return Dataset
     */
    public static Dataset reduce(Dataset input) {
        return new GaussianPyramidOperation().run(input);
    }

    /**
     * Returns an upscaled version of the input
     *
     * @param input            original input
     * @param upscaleOperation upscaling operation
     * @return enlarged Dataset
     */
    public static Dataset expand(Dataset input, UpscaleOperation upscaleOperation) {
        return upscaleOperation.run(input);
    }

    /**
     * Returns a Dataset twice as large in each dimension
     *
     * @param input original input
     * @return enlarged Dataset
     */
    public static Dataset expand(Dataset input) {
        return expand(input, upo);
    }

    // Demo of usage - reconstruct an image from its Laplacian pyramid
    public static void main(String[] args) throws Exception {
        URL cscanUrl = Thread.currentThread().getContextClassLoader().getResource("data/io/sampledata/140.csv");
        File inputFile = new File(cscanUrl.getPath());
        IODataset cScanSlice = new FileSniffer().sniff(inputFile);
        cScanSlice.read();
        Dataset result = PyramidOperations.collapse(cScanSlice);
        result.write("/home/ccoughlin/upscaledCscan.txt");
    }
}
