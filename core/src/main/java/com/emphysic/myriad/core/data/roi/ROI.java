/*
 * com.emphysic.myriad.core.data.roi.ROI
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Region Of Interest (ROI) - an object, signal, anomaly, etc. in a larger dataset.  The purpose of a typical Myriad
 * application is to find ROI(s) in this data automatically.
 *
 * Examples of ROIs include:
 *
 * - NDE flaw signals e.g. corrosion pitting, cracks, delaminations;
 * - Faces and people in images;
 * - Anomalies in acoustic data or video frames.
 */
public class ROI implements Serializable {
    /**
     * The underlying data that represents a region of interest
     */
    protected Dataset dataset;
    /**
     * A list of coordinates for the ROI, used to define a "bounding box" that describes the position of the ROI.
     */
    protected List<Integer> origin;
    /**
     * A text tag for including descriptive metadata
     */
    protected String metadata;

    /**
     * Creates a new Region of Interest from the specified data.
     * @param dataset data containing the ROI
     * @param start the starting coordinate of the ROI within the data
     * @param coords the remainder of the ROI bounding box
     */
    public ROI(Dataset dataset, int start, int... coords) {
        this.dataset = dataset;
        setOrigin(start, coords);
    }

    /**
     * Sets the bounding box origin for the ROI.
     * Examples:
     * setOrigin(left) : 1D data, starts at "left"
     * setOrigin(x, y) : 2D data, bounding box is (x, y)-(x + dataset.getWidth(), y + dataset.getHeight())
     *
     * @param start  coordinate of the first dimension in the bounding box e.g. left or x-coordinate of top left corner.
     * @param coords additional (start, stop) coordinates for the remaining dimensions of the bounding box.
     */
    public void setOrigin(int start, int... coords) {
        origin = new ArrayList<>();
        origin.add(start);
        for (int coord : coords) {
            origin.add(coord);
        }
    }

    /**
     * Returns the origin for the ROI
     * @return list of indices
     */
    public List<Integer> getOrigin() {
        return origin;
    }

    /**
     * Sets the descriptive metadata for this ROI
     * @param metadata new metadata
     */
    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    /**
     * Returns the current metadata for this ROI
     * @return current metadata
     */
    public String getMetadata() {
        return metadata;
    }

    /**
     * Retrieve the ROI data
     * @return Dataset that contains the ROI
     */
    public Dataset getDataset() {
        return dataset;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(metadata);
        if (!origin.isEmpty()) {
            sb.append("&roiorigin=").append(origin.get(0));
            for (int i=1; i<origin.size(); i++) {
                sb.append("x").append(origin.get(i));
            }
        }
        return sb.toString();
    }
}
