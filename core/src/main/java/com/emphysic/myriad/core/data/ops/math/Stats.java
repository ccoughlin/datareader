/*
 * com.emphysic.myriad.core.data.ops.math.Stats
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops.math;

import com.emphysic.myriad.core.data.io.Dataset;

/**
 * Stats - general purpose statistical functions.
 */
public class Stats {
    /**
     * Calculates the mean of the input data
     *
     * @param data elements
     * @return mean
     * @throws IllegalArgumentException if input array was null
     */
    public static double mean(double[] data) {
        if (data != null) {
            if (data.length == 1) {
                return data[0];
            }
            double res = 0;
            for (double el : data) {
                res += el;
            }
            return res / data.length;
        } else {
            throw new IllegalArgumentException("Input data was null");
        }
    }

    /**
     * Calculates the mean value of the input
     * @param dataset input
     * @return mean
     * @throws IllegalArgumentException if input array was null
     */
    public static double mean(Dataset dataset) {
        return mean(dataset.getData());
    }

    /**
     * Calculates the variance of the input data
     *
     * @param data elements
     * @return variance
     * @throws IllegalArgumentException if input array was null
     */
    public static double variance(double[] data) {
        if (data != null) {
            double mean = mean(data);
            double[] meandiffs = new double[data.length];
            for (int i = 0; i < data.length; i++) {
                meandiffs[i] = Math.pow(data[i] - mean, 2);
            }
            return mean(meandiffs);
        } else {
            throw new IllegalArgumentException("Input data was null");
        }
    }

    /**
     * Calculates the variance of the input data
     *
     * @param dataset elements
     * @return variance
     * @throws IllegalArgumentException if input array was null
     */
    public static double variance(Dataset dataset) {
        return variance(dataset.getData());
    }

    /**
     * Calculates the standard deviation of the data
     *
     * @param data input data
     * @return standard deviation
     * @throws IllegalArgumentException if input array was null
     */
    public static double stddev(double[] data) {
        if (data != null) {
            return Math.sqrt(variance(data));
        } else {
            throw new IllegalArgumentException("Input data was null");
        }
    }

    /**
     * Calculates the standard deviation of the data
     *
     * @param dataset input data
     * @return standard deviation
     * @throws IllegalArgumentException if input array was null
     */
    public static double stddev(Dataset dataset) {
        return stddev(dataset.getData());
    }

    /**
     * Returns the minimum of the data
     * @param data array to examine
     * @return minimum value, or Double.MAX_VALUE if data was null or contained no elements
     */
    public static double min(double[] data) {
        double min = Double.MAX_VALUE;
        if (data != null && data.length > 0) {
            for (double aData : data) {
                if (aData < min) {
                    min = aData;
                }
            }
        }
        return min;
    }

    /**
     * Returns the minimum of the data
     * @param dataset Dataset to examine
     * @return minimum value, or Double.MAX_VALUE if data was null or contained no elements
     */
    public static double min(Dataset dataset) {
        return min(dataset.getData());
    }

    /**
     * Returns the maximum of the data
     * @param data array to examine
     * @return maximum value, or Double.MIN_VALUE if data was null or contained no elements
     */
    public static double max(double[] data) {
        double max = Double.MIN_VALUE;
        if (data != null && data.length > 0) {
            for (double aData : data) {
                if (aData > max) {
                    max = aData;
                }
            }
        }
        return max;
    }

    /**
     * Returns the maximum of the data
     * @param dataset Dataset to examine
     * @return maximum value, or Double.MIN_VALUE if data was null or contained no elements
     */
    public static double max(Dataset dataset) {
        return max(dataset.getData());
    }

}
