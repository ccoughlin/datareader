/*
 * com.emphysic.myriad.core.data.io.ImageDataset
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.io;

import com.pulispace.ui.panorama.util.BigBufferedImage;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * ImageDataset - IODatasets from image files.
 */
@Slf4j
public class ImageDataset extends IODataset {
    /**
     * Image reader
     */
    private ImageReader reader;
    /**
     * The number of frames in the image (e.g. multi-page TIFF or GIF files)
     */
    private int numFrames;
    /**
     * Marks the current frame in the image.
     */
    private int frame = 0;

    private long BIGFILE = 1000000000;

    /**
     * Creates a new ImageDataset from the specified file.
     * @param dataSource source of data
     * @throws IOException if an I/O error occurs.
     */
    public ImageDataset(File dataSource) throws IOException {
        ImageInputStream is = ImageIO.createImageInputStream(dataSource);
        if (is == null || is.length() == 0) {
            throw new IOException("Encountered error opening input stream to " + dataSource);
        }
        Iterator<ImageReader> iterator = ImageIO.getImageReaders(is);
        if (iterator == null || !iterator.hasNext()) {
            throw new IOException("Image file format not supported by ImageIO: " + dataSource);
        }
        // We are just looking for the first reader compatible:
        reader = iterator.next();
        reader.setInput(is);
        this.source = dataSource;
        this.width = reader.getWidth(0);
        this.height = reader.getHeight(0);
        this.data = new double[width * height];
    }

    /**
     * Creates a new ImageDataset from the specified file.
     * @param dataFileName name of source file
     * @throws IOException if an I/O error occurs.
     */
    public ImageDataset(String dataFileName) throws IOException {
        this(new File(dataFileName));
    }

    /**
     * Empty constructor for (de)serialization.
     */
    public ImageDataset() {
    }

    /**
     * Returns the pixel data from an image.
     * @param img image to read
     * @return pixel data
     */
    public static int[] getPixelData(BufferedImage img) {
        Raster r = img.getData();
        return r.getPixels(0, 0, r.getWidth(), r.getHeight(), (int[]) null);
    }

    /**
     * Retrieves the data from the image.  If the input image contains multiple images
     * e.g. GIF or multipage TIFF, reads the data from the current frame and then
     * advances so that the data can be read iteratively:
     * <p>
     * for (int i=0; i&lt;instance.getNumFrames(); i++) {
     * instance.read(); // instance's data is now the contents of the current frame
     * }
     *
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void read() throws IOException {
        if (frame < getNumFrames()) {
            data = read(frame);
            frame++;
        }
    }

    /**
     * Reads a specified region in an image and returns the RGB data.
     *
     * @param image image to process
     * @param x     x-coordinate of top left corner of bounding box to read
     * @param y     y-coordinate of top left corner of bounding box to read
     * @param w     width of region
     * @param h     height of region
     * @return 1D array of RGB values
     * @throws IndexOutOfBoundsException if provided an invalid region
     */
    public static double[] getData(BufferedImage image, int x, int y, int w, int h) {
        double[] ddata = new double[w * h];
        if (x < 0 || y < 0 || w < 0 || h < 0) {
            throw new IndexOutOfBoundsException("Invalid coordinates specified");
        }
        for (int i = x; i < x + w; i++) {
            for (int j = y; j < h; j++) {
                ddata[j * w + i] = image.getRGB(i, j);
            }
        }
        return ddata;
    }

    /**
     * For multi-image file formats, reads the image at the specified index.
     *
     * @param idx index to read
     * @return image at the specified index or null if the image couldn't be read
     * @throws IOException               if an I/O error occurs
     * @throws IndexOutOfBoundsException if specified index is less than 0 or greater than the number of frames in the image
     */
    public double[] read(int idx) throws IOException {
        if (idx >= 0 && idx < getNumFrames()) {
            BufferedImage image;
            if (source.length() < BIGFILE) {
                image = reader.read(idx);
            } else {
                log.info("File size exceeds " + BIGFILE + " bytes, going big");
                image = BigBufferedImage.create(source,
                        new File(System.getProperty("java.io.tmpdir")),
                        BufferedImage.TYPE_INT_RGB,
                        idx);
            }
            if (image != null) {
                this.width = image.getWidth();
                this.height = image.getHeight();
                BufferedImage greyImage = new BufferedImage(width, height,
                        BufferedImage.TYPE_BYTE_GRAY);
                Graphics g = greyImage.getGraphics();
                g.drawImage(image, 0, 0, null);
                g.dispose();
                return getData(greyImage, 0, 0, width, height);
            } else {
                return null;
            }
        } else {
            throw new IndexOutOfBoundsException("Invalid frame specified, must be between 0 " + " and " + getNumFrames());
        }
    }

    /**
     * Resets the current frame to the first frame.
     */
    public void rewind() {
        frame = 0;
    }

    /**
     * Returns the current position in the image file
     *
     * @return current frame number
     */
    public int getCurrentFrame() {
        return frame;
    }

    /**
     * Returns the number of frames in the image file
     *
     * @return number of frames
     * @throws IOException if an I/O error occurs
     */
    public int getNumFrames() throws IOException {
        if (numFrames == 0) {
            numFrames = reader.getNumImages(true);
        }
        return numFrames;
    }

    /**
     * Size in bytes for "big" images.  Files bigger than this value are read with memory-mapped databuffers
     * @see BigBufferedImage .  Files smaller than this value are read as conventional
     * @see BufferedImage .
     * @return size in bytes
     */
    public long getBigFileSize() {
        return BIGFILE;
    }

    /**
     * Sets the threshold size in bytes for "big" images.  Files bigger than this value are read with memory-mapped
     * databuffers @see BigBufferedImage .  Files smaller than this value are read as conventional
     * @see BufferedImage .
     * @param bytes threshold filesize in bytes
     */
    public void setBigFileSize(long bytes) {
        this.BIGFILE = bytes;
    }
}
