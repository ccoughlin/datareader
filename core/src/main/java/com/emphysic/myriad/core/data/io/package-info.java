/**
 * This package contains classes for reading and writing data.
 * Copyright (c) 2016 Emphysic LLC.  All rights reserved.
 */
package com.emphysic.myriad.core.data.io;