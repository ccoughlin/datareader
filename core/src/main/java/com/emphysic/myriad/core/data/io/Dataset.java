/*
 * com.emphysic.myriad.core.data.io.Dataset
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.io;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Dataset-the fundamental data type in Myriad.  Consists of a 1D double array and associated functions to treat
 * as a two-dimensional entity.
 */
public class Dataset implements Serializable {
    /**
     * The width of a 2D dataset.  For 1D data set one of @link #width or @link #height to 1.
     */
    protected int width;
    /**
     * The height of a 2D dataset.  For 1D data set one of @link #width or @link #height to 1.
     */
    protected int height;
    /**
     * The array that backs the contents of the dataset.
     */
    protected double[] data;

    /**
     * Create a new initialized but empty Dataset
     * @param width width of the data
     * @param height height of the data
     */
    public Dataset(int width, int height) {
        this(new double[height * width], width, height);
    }

    /**
     * Create a new 1D Dataset of width data.length and height 1.
     * @param data array to copy
     */
    public Dataset(double[] data) {
        this.width = data.length;
        this.height = 1;
        this.data = Arrays.copyOf(data, data.length);
    }

    /**
     * Create a new 2D Dataset from an input array.
     * @param data array to copy
     * @param width width of data
     * @param height height of data
     */
    public Dataset(double[] data, int width, int height) {
        this.width = width;
        this.height = height;
        this.data = Arrays.copyOf(data, data.length);
    }

    /**
     * Create a new uninitialized Dataset.  Primarily for (de)serialization.
     */
    public Dataset() {
    }

    /**
     * Create a new Dataset from an original.  Array contents are copied.
     * @param orig original to copy
     */
    public Dataset(Dataset orig) {
        this.width = orig.getWidth();
        this.height = orig.getHeight();
        this.data = Arrays.copyOf(orig.getData(), orig.getSize());
    }

    /**
     * Retrieve the underlying array
     * @return this Dataset's 1D data.
     */
    public double[] getData() {
        return data;
    }

    /**
     * Sets this Dataset's data to a copy of the specified's data.  Width and height parameters are also updated.
     * @param dataSet dataset to copy
     */
    public void setData(Dataset dataSet) {
        this.width = dataSet.getWidth();
        this.height = dataSet.getHeight();
        this.data = Arrays.copyOf(dataSet.getData(), dataSet.getSize());
    }

    /**
     * Sets this Dataset's data to a copy of the specified data.
     * @param data array to copy
     * @param width new width of the Dataset
     * @param height new height of the Dataset
     */
    public void setData(double[] data, int width, int height) {
        this.width = width;
        this.height = height;
        this.data = Arrays.copyOf(data, data.length);
    }

    /**
     * Retrieves the value of the data at a given point.
     * @param x X-coordinate of the point
     * @param y Y-coordinate of the point
     * @return the value of the data at point (x, y)
     */
    public double get(int x, int y) {
        return data[y * width + x];
    }

    /**
     * Sets the value of a point.
     * @param x X-coordinate of the point
     * @param y Y-coordinate of the point
     * @param value new value for the point (x, y)
     */
    public void set(int x, int y, double value) {
        data[y * width + x] = value;
    }

    /**
     * Retrieves the current width of this Dataset.
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Retrieves the current height of this Dataset.
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the current size of this Dataset.  Equivalent to width * height.
     * @return size
     */
    public int getSize() {
        return getWidth() * getHeight();
    }

    /**
     * Convenience method for printing this Dataset to a PrintStream
     * @param out stream
     */
    public void prettyPrint(PrintStream out) {
        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                out.print(get(j, i) + " ");
            }
            out.println("");
        }
    }

    /**
     * Convenience method for printing this Dataset to standard output
     */
    public void prettyPrint() {
        prettyPrint(System.out);
    }

    /**
     * Writes this Dataset to a space-delimited text file.
     *
     * @param outputFile name of the output file
     * @throws IOException if an I/O error occurs
     */
    public void write(File outputFile) throws IOException {
        PrintStream printStream = new PrintStream(outputFile);
        prettyPrint(printStream);
        printStream.close();
    }

    /**
     * Writes this Dataset to a space-delimited text file.
     * @param outputFileName filename of the output file
     * @throws IOException if an error occurs
     */
    public void write(String outputFileName) throws IOException {
        write(new File(outputFileName));
    }
}
