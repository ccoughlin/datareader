/**
 * This package contains classes that operate on data.
 * Copyright (c) 2016 Emphysic LLC.  All rights reserved.
 */
package com.emphysic.myriad.core.data.ops;