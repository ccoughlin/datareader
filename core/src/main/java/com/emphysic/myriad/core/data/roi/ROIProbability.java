/*
 * com.emphysic.myriad.core.data.roi.ROIProbability
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.Vector;

/**
 * ROIProbability - the implementing class can return probabilities of a given sample being in a given class.
 * Created by ccoughlin on 9/21/2016.
 */
public interface ROIProbability {

    /**
     * Returns the probability of the sample being in each of the classes recognized by the current model.
     * Element 0 is the probability of the "negative" class i.e. no ROI.
     *
     * @param data sample data to classify
     * @return array of probabilities between 0 and 1 of the sample being in the specified class.
     */
    default double[] predict_proba(double[] data) {
        DenseVector d = new DenseVector(data);
        Vector scores = classify(d);
        double[] probabilities = new double[1 + scores.size()];
        for (int i = 0; i < scores.size(); i++) {
            probabilities[0] = scores.get(i);
        }
        return probabilities;
    }

    /**
     * Returns the probability of the sample being in each of the classes recognized by the current model.
     * Element 0 is the probability of the "negative" class i.e. no ROI.
     *
     * @param dataset sample data to classify
     * @return array of probabilities between 0 and 1 of the sample being in the specified class.
     */
    default double[] predict_proba(Dataset dataset) {
        return predict_proba(dataset.getData());
    }

    Vector classify(DenseVector d);
}
