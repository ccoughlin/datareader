/*
 * com.emphysic.myriad.core.examples.Blur
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.examples;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.BoxBlur;
import com.emphysic.myriad.core.data.ops.GaussianBlur;
import com.emphysic.myriad.core.data.util.FileSniffer;

import java.io.File;

/**
 * Demonstrates box and Gaussian blur
 * Created by ccoughlin on 9/9/16.
 */
public class Blur {

    public static void main(String[] args) {
        Dataset input;
        if (args == null || args.length == 0) {
            System.out.println("Using default input data");
            input = getDefaultDataset();
        } else {
            System.out.print("Attempting to read " + args[0] + "...");
            try {
                input = FileSniffer.read(new File(args[0]), true);
            } catch (Exception e) {
                System.out.println("failed! Using default data");
                input = getDefaultDataset();
            }
        }
        System.out.println("Input data " + input.getWidth() + "x" + input.getHeight() + ":");
        input.prettyPrint();
        System.out.println();
        System.out.println("Box blur radius 2:");
        BoxBlur bb = new BoxBlur(2);
        bb.run(input).prettyPrint();
        System.out.println();
        System.out.println("Gaussian blur radius 2:");
        GaussianBlur gb = new GaussianBlur(2);
        gb.run(input).prettyPrint();
        System.out.println();
    }

    /**
     * Returns a default Dataset for the demo
     * @return Dataset
     */
    private static Dataset getDefaultDataset() {
        return new Dataset(demoData, 5, 5);
    }

    /**
     * Default input data
     */
    private static double[] demoData = {
            0.0,  0.0,  0.0,  0.0, 0.0,
            0.0, 50.0, 50.0, 50.0, 0.0,
            0.0, 50.0, 50.0, 50.0, 0.0,
            0.0, 50.0, 50.0, 50.0, 0.0,
            0.0,  0.0,  0.0,  0.0, 0.0
    };
}
