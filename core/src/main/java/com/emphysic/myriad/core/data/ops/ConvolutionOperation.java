/*
 * com.emphysic.myriad.core.data.ops.ConvolutionOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.DatasetUtils;

import java.util.Map;

/**
 * ConvolutionOperation - performs convolution on a Dataset.
 */
public class ConvolutionOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * The convolution kernel
     */
    protected double[][] kernel;

    /**
     * Creates a new ConvolutionOperation with the identity kernel.
     */
    public ConvolutionOperation() {
        double[][] idKernel = {
                {0.0, 0.0, 0.0},
                {0.0, 1.0, 0.0},
                {0.0, 0.0, 0.0}
        };
        setKernel(idKernel);
    }

    /**
     * Creates a new ConvolutionOperation with the specified kernel.
     * @param k kernel to use
     */
    public ConvolutionOperation(double[][] k) {
        setKernel(k);
    }

    /**
     * Creates a new ConvolutionOperation with the specified kernel.
     * @param kernel kernel to use
     */
    public ConvolutionOperation(Dataset kernel) {
        double[][] k = new double[kernel.getWidth()][kernel.getHeight()];
        setKernel(k);
    }

    /**
     * Returns the convolution kernel.
     * @return convolution kernel
     */
    public double[][] getKernel() {
        return kernel;
    }

    /**
     * Sets the convolution kernel
     * @param k kernel to use
     */
    private void setKernel(double[][] k) {
        kernel = new double[k.length][k[0].length];
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                kernel[i][j] = k[i][j];
            }
        }
    }

    /**
     * Returns the width of the kernel i.e. kernel.length.
     * @return width of kernel
     */
    public int getWidth() {
        return kernel.length;
    }

    /**
     * Returns the height of the kernel i.e. kernel[0].length.
     * @return height of kernel
     */
    public int getHeight() {
        return kernel[0].length;
    }

    /**
     * Convolves a Dataset.
     * @param input Dataset to convolve
     * @return convolved Dataset
     */
    @Override
    public Dataset run(Dataset input) {
        if (input == null) {
            return null;
        }
        int w = input.getWidth();
        int h = input.getHeight();
        Dataset output = new Dataset(w, h);
        for (int i = w - 1; i >= 0; i--) {
            for (int j = h - 1; j >= 0; j--) {
                double val = 0.0;
                for (int kw = getWidth() - 1; kw >= 0; kw--) {
                    for (int kh = getHeight() - 1; kh >= 0; kh--) {
                        val += kernel[kw][kh] * input.get(
                                DatasetUtils.safeIdx(i + kw - getWidth() / 2, w),
                                DatasetUtils.safeIdx(j + kh - getHeight() / 2, h));
                    }
                }
                output.set(i, j, val);
            }
        }
        return output;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("kernel", kernel);
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        kernel = (double[][]) objectMap.getOrDefault("kernel", kernel);
    }
}
