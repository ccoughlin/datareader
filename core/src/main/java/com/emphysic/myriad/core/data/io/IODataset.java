/*
 * com.emphysic.myriad.core.data.io.IODataset
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.io;

import java.io.File;
import java.io.IOException;

/**
 * IODataset - lazy loading Datasets read from input files.
 */
public abstract class IODataset extends Dataset {
    /**
     * Source of the data
     */
    protected File source;

    /**
     * Creates a new Dataset from the specified file.  By default IODatasets are lazy loading, @see #IODataset.read()
     * @param dataSource file to read
     * @param width width of the data
     * @param height height of the data
     * @throws IOException if an I/O error occurs
     */
    public IODataset(File dataSource, int width, int height) throws IOException {
        super(width, height);
        this.source = dataSource;
    }

    /**
     * Creates a new Dataset from the specified file.  By default IODatasets are lazy loading, @see #IODataset.read()
     * @param dataFileName file to read
     * @param width width of the data
     * @param height height of the data
     * @throws IOException if an I/O error occurs
     */
    public IODataset(String dataFileName, int width, int height) throws IOException {
        this(new File(dataFileName), width, height);
    }

    /**
     * Empty constructor - used for (de)serialization.
     */
    public IODataset() {
    }

    /**
     * By default IODatasets are lazy readers and won't read the contents of the file until directed.
     * @throws IOException if an I/O error occurs
     */
    public abstract void read() throws IOException;
}
