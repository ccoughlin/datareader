/*
 * com.emphysic.myriad.core.data.ops.ScalingOperation
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.math.Stats;

import java.util.Map;

/**
 * ScalingOperation - scales a Dataset by substracting its mean and dividing by the standard deviation.  Differs
 * from FeatureScalingOperation in that ScalingOperation is per-Dataset while FeatureScalingOperation scales across
 * Datasets by feature (index position in the Dataset's underlying array).
 */
public class ScalingOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Mean element value
     */
    private double mean;
    /**
     * Standard deviation
     */
    private double std;

    /**
     * Creates a new ScalingOperation with the specified mean and standard deviation.
     * @param mean mean
     * @param std standard deviation
     */
    public ScalingOperation(double mean, double std) {
        this.mean = mean;
        this.std = std;
    }

    /**
     * Initializes a new ScalingOperation.  Mean and standard deviation are calculated from the input Dataset.
     */
    public ScalingOperation() {
        this(Double.MIN_VALUE, Double.MIN_VALUE);
    }

    /**
     * Scales an input Dataset
     * @param input Dataset on which to operate
     * @return scaled Dataset, or null if input was null.
     */
    @Override
    public Dataset run(Dataset input) {
        if (input != null) {
            Dataset standardized = new Dataset(input.getWidth(), input.getHeight());
            if (mean == Double.MIN_VALUE) {
                mean = Stats.mean(input);
            }
            if (std == Double.MIN_VALUE) {
                std = Stats.stddev(input);
            }
            if (std == 0) {
                std = 1;
            }
            for (int i = 0; i < input.getWidth(); i++) {
                for (int j = 0; j < input.getHeight(); j++) {
                    standardized.set(i, j, (input.get(i, j) - mean) / std);
                }
            }
            return standardized;
        }
        return null;
    }

    /**
     * Returns the mean value used in normalization.
     * @return mean value
     */
    public double getMean() {
        return mean;
    }

    /**
     * Sets the mean value used in normalization.
     * @param mean mean value
     */
    public void setMean(double mean) {
        this.mean = mean;
    }

    /**
     * Returns the standard deviation used in normalization.
     * @return standard deviation
     */
    public double getStd() {
        return std;
    }

    /**
     * Sets the standard deviation used in normalization.
     * @param std standard deviation.
     */
    public void setStd(double std) {
        this.std = std;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = DatasetOperation.super.getObjectMap();
        map.put("mean", mean);
        map.put("std", std);
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        mean = (double) objectMap.getOrDefault("mean", mean);
        std = (double) objectMap.getOrDefault("std", std);
    }
}
