/**
 * This package contains classes with general-purpose mathematical functions.
 * Copyright (c) 2016 Emphysic LLC.  All rights reserved.
 */
package com.emphysic.myriad.core.data.ops.math;