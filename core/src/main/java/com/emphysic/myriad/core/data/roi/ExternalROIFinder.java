/*
 * com.emphysic.myriad.core.data.roi.ExternalROIFinder
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.ExternalProcess;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ExternalROIFinder - demonstrates one way to call external ROI applications by redirecting their standard input
 * and output streams.
 */
@Slf4j
public class ExternalROIFinder implements ROIFinder {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    /**
     * Manages the external application process
     */
    private ExternalProcess processRunner;
    /**
     * In combination with #ExternalROIFinder.timeoutUnits, sets how long to wait for the external application to
     * close before shutting it down.
     */
    private long timeout = 10;
    /**
     * In combination with #ExternalROIFinder.timeout, sets how long to wait for the external application to
     * close before shutting it down.
     */
    private TimeUnit timeoutUnits = TimeUnit.SECONDS;

    private Pattern isMyriadMsgPattern = Pattern.compile("^Myriad:.*", Pattern.CASE_INSENSITIVE);
    private Pattern isROIPattern = Pattern.compile("(?<true>true)|(?<false>false)", Pattern.CASE_INSENSITIVE);
    private Pattern isLogPattern = Pattern.compile("(?<debug>\\[*debug\\]*)|(?<info>\\[*info\\]*)|(?<warn>\\[*warn\\]*)|(?<error>\\[*error\\]*)",
            Pattern.CASE_INSENSITIVE);

    /**
     * Constructor for an empty ROIFinder (serialization)
     */
    public ExternalROIFinder() {}

    /**
     * Creates a new ExternalROIFinder with the specified external process manager.
     * @param externalProcess external process manager
     */
    public ExternalROIFinder(ExternalProcess externalProcess) {
        this.processRunner = externalProcess;
    }

    /**
     * Starts external application and has it classify data as from a flaw or not.
     * Data are passed as space-delimited numbers to the app's standard input; the external application
     * reports whether a flaw was found or not by printing a line "myriad:T" to its standard output.
     * <p>
     * In a "real" deployment the external app would likely run indefinitely, would communicate over a network
     * connection, etc.
     *
     * @param data raw data to examine
     * @return true if the app found a flaw, false if not or if an error occurred
     * @see ExternalROIFinder#setTimeout(long, TimeUnit) for information on setting the timeout delay
     */
    @Override
    public boolean isROI(double[] data) {
        boolean result = false;
        try {
            if (processRunner.getProcess() == null || !processRunner.getProcess().isAlive()) {
                processRunner.start();
            }
            OutputStream stdin = processRunner.getProcess().getOutputStream();
            InputStream stdout = processRunner.getProcess().getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(stdout));
            PrintStream ps = new PrintStream(stdin);
            for (double d : data) {
                ps.print(d + " ");
            }
            ps.println();
            stdin.close();
            String line;
            while ((line = in.readLine()) != null) {
                if (isMyriadMessage(line)) {
                    // Found an output message directed to Myriad
                    if (isLogMessage(line)) {
                        // Appears to be a log message - determine type and file appropriately
                        handleLogMessage(line);
                    } else if (isROIMessage(line)) {
                        // Appears to be an ROI determination
                        result = ROIfound(line);
                    } else {
                        // Handle unknown messages with a warning
                        log.warn("Unknown message received: " + line);
                    }
                }
            }
            boolean completed = processRunner.getProcess().waitFor(timeout, timeoutUnits);
            if (!completed) {
                log.warn("External process timed out before completion");
            }
        } catch (IOException ioe) {
            log.error(ioe.getMessage());
        } catch (InterruptedException ie) {
            log.warn(ie.getMessage());
        }
        return result;
    }

    /**
     * Determines whether the specified message appears to be directed to Myriad, i.e. starts with "Myriad:".
     * @param input message to check
     * @return true if the message matches the regex ^Myriad (case insensitive).
     */
    boolean isMyriadMessage(String input) {
        return isMyriadMsgPattern.matcher(input).matches();
    }

    /**
     * Determines whether the specified message appears to be a logging message, i.e. a Myriad message with one of
     * DEBUG, INFO, WARN, or ERROR (case insensitive) defined.
     * @param input message to check
     * @return true if the message matches the regex ^Myriad (case insensitive).
     */
    boolean isLogMessage(String input) {
        return isMyriadMessage(input) && isLogPattern.matcher(input).find();
    }

    /**
     * Determines whether the specified message appears to be a Region Of Interest determination, i.e. starts with
     * "Myriad:" and has one of TRUE or FALSE (case insensitive).
     * @param input message to check
     * @return true if the message appears to be an ROI call
     */
    boolean isROIMessage(String input) {
        return isMyriadMessage(input) && isROIPattern.matcher(input).find();
    }

    /**
     * Determines whether the specified message indicates a Region of Interest was found by the external ROI finder.
     * @param input message to check
     * @return true if the external ROI finder found an ROI, false if not or if the message was invalid
     */
    boolean ROIfound(String input) {
        if (isMyriadMessage(input)) {
            Matcher matcher = isROIPattern.matcher(input);
            if (matcher.find()) {
                String group = matcher.group();
                if (group != null && group.length() > 0) {
                    String g = group.toLowerCase();
                    if (g.contains("true")) {
                        return true;
                    } else if (g.contains("false")) {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Determines the type of log message and records it in Myriad's log, e.g. a string of the form
     *
     * Myriad: 10:13:07,341 |-WARN in c.q.l.core.rolling.TimeBasedRollingPolicy@1165897474 - Subcomponent did not start.
     *
     * is recorded as a warning in Myriad's log file.
     *
     * @param input message to check
     */
    private void handleLogMessage(String input) {
        if (isMyriadMessage(input)) {
            Matcher matcher = isLogPattern.matcher(input);
            if (matcher.find()) {
                String group = matcher.group();
                if (group != null && group.length() > 0) {
                    String g = group.toLowerCase();
                    String msg = "External process: " + input.substring(matcher.start());
                    if (g.contains("debug")) {
                        log.debug(msg);
                    } else if (g.contains("info")) {
                        log.info(msg);
                    } else if (g.contains("warn")) {
                        log.warn(msg);
                    } else if (g.contains("error")) {
                        log.error(msg);
                    }
                }
            }
        }
    }

    /**
     * Returns the number of seconds, milliseconds, etc. before the external process times out.
     *
     * @return value of timeout
     * @see ExternalROIFinder#getTimeoutUnits()
     */
    public long getTimeout() {
        return timeout;
    }

    /**
     * Returns the units of the timeout delay.
     *
     * @return timeout units
     * @see ExternalROIFinder#getTimeout()
     */
    public TimeUnit getTimeoutUnits() {
        return timeoutUnits;
    }

    /**
     * Sets the external process timeout.  Defaults to 10 seconds.
     *
     * @param timeout      amount of time
     * @param timeoutUnits units of time
     */
    public void setTimeout(long timeout, TimeUnit timeoutUnits) {
        this.timeout = timeout;
        this.timeoutUnits = timeoutUnits;
    }

    /**
     * Predict whether the specified sample appears to contain a region of interest.
     * @param dataset data to examine
     * @return true if the sample appears to contain an ROI, false otherwise
     */
    @Override
    public boolean isROI(Dataset dataset) {
        return isROI(dataset.getData());
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Map<String, Object> getObjectMap() {
        Map<String, Object> map = ROIFinder.super.getObjectMap();
        List<String> procArgs = processRunner.getArgs();
        map.put("args", procArgs);
        Map<String, String> procEnv = processRunner.getEnv();
        map.put("env", procEnv);
        File workingFolder = processRunner.getWorkingFolder();
        String procFolder = null;
        if (workingFolder != null) {
            procFolder = workingFolder.getAbsolutePath();
        }
        map.put("dir", procFolder);
        map.put("timeout", timeout);
        map.put("tunits", timeoutUnits);
        return map;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {
        File workingFolder = null;
        String wdir = (String)objectMap.get("dir");
        if (wdir != null) {
            workingFolder = new File(wdir);
        }
        List<String> commandAndArgs = (List<String>) objectMap.get("args");
        processRunner = new ExternalProcess(
                commandAndArgs.get(0),
                commandAndArgs.subList(1, commandAndArgs.size()),
                (Map<String, String>) objectMap.get("env"),
                workingFolder
        );
        timeout = (long) objectMap.getOrDefault("timeout", timeout);
        timeoutUnits = (TimeUnit) objectMap.getOrDefault("tunits", timeoutUnits);
    }

    /**
     * Convenience method for building an external ROI finder
     *
     * @param cmd           full path and filename of the application to execute
     * @param args          list of command-line arguments to provide the application
     * @param workingFolder the working folder for the application
     * @return an ROI finder that calls the specified application to classify data
     */
    public static ExternalROIFinder buildROIFinder(String cmd, List<String> args, File workingFolder) {
        return buildROIFinder(cmd, args, null, workingFolder);
    }

    /**
     * Convenience method for building an external ROI finder
     *
     * @param cmd full path and filename of the application to execute
     * @param args list of command-line arguments to provide the application
     * @return an ROI finder that calls the specified application to classify data
     */
    public static ExternalROIFinder buildROIFinder(String cmd, List<String> args) {
        return buildROIFinder(cmd, args, null);
    }

    /**
     * Convenience method for building an external ROI finder
     *
     * @param cmd full path and filename of the application to execute
     * @return an ROI finder that calls the specified application to classify data
     */
    public static ExternalROIFinder buildROIFinder(String cmd) {
        return buildROIFinder(cmd, null);
    }

    /**
     * Convenience method for building an external ROI finder
     * @param cmd full path and filename of the application to execute
     * @param args list of command-line arguments to provide the application
     * @param environment environment variables for the application, or null to use the current system environment
     * @param workingFolder the working folder for the application, or null for "user.dir" working folder
     * @return an ROI finder that calls the specified application to classify data
     */
    public static ExternalROIFinder buildROIFinder(String cmd, List<String> args, Map<String, String> environment, File workingFolder) {
        ExternalProcess rp = new ExternalProcess(cmd, args, environment, workingFolder);
        rp.setWorkingFolder(workingFolder);
        return new ExternalROIFinder(rp);
    }

    public void legacyWrite(Kryo kryo, Output output) {
        kryo.writeObject(output, processRunner);
        kryo.writeObject(output, new Long(timeout));
        kryo.writeObject(output, timeoutUnits);
    }

    public void legacyRead(Kryo kryo, Input input) {
        this.setProcessRunner(kryo.readObject(input, ExternalProcess.class));
        this.setTimeout(kryo.readObject(input, Long.class), kryo.readObject(input, TimeUnit.class));
    }

    /**
     * Returns the external application for this ROIFinder
     * @return external application
     */
    public ExternalProcess getProcessRunner() {
        return processRunner;
    }

    /**
     * Sets the external application for this ROIFinder
     * @param processRunner external application
     */
    public void setProcessRunner(ExternalProcess processRunner) {
        this.processRunner = processRunner;
    }
}
