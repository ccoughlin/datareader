/*
 * com.emphysic.myriad.core.ml.MonteCarloCV
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.ml;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * MonteCarloCV - Monte Carlo cross-validation
 */
public class MonteCarloCV extends CrossValidation {
    private double ratio; // Ratio of training size: test size
    private Integer[] indices;

    /**
     * Create a new Monte Carlo cross validator.
     *
     * @param samples N samples of M features
     * @param labels  N labels
     * @param ratio   fraction of the N samples to use for training. Remainder used for testing.
     * @throws IllegalArgumentException if ratio &lt; 0 or ratio &gt; 1
     */
    public MonteCarloCV(double[][] samples, int[] labels, double ratio) {
        super(samples, labels);
        indices = new Integer[X.length];
        for (int i = 0; i < X.length; i++) {
            indices[i] = i;
        }
        if (ratio > 0 && ratio < 1) {
            this.ratio = ratio;
        } else {
            throw new IllegalArgumentException("Ratio must be between 0 and 1.");
        }
    }

    public MonteCarloCV(double[][] samples, int[] labels) {
        this(samples, labels, 0.75);
    }

    public MonteCarloCV(Data data, double ratio) {
        this(data.samples, data.labels, ratio);
    }

    public MonteCarloCV(Data data) {
        this(data, 0.75);
    }

    public TrainTestSubsets getTrainTestSubset() {
        int trainingSize = (int) (ratio * X.length);
        int testingSize = X.length - trainingSize;
        return new TrainTestSubsets(getSubset(trainingSize), getSubset(testingSize));
    }

    private Data getSubset(int numPoints) {
        ArrayList<Integer> availableIndices = new ArrayList<>(Arrays.asList(indices));
        Data subset = new Data(numPoints, X[0].length);
        for (int i = 0; i < numPoints; i++) {
            int idx = availableIndices.remove(random.nextInt(availableIndices.size()));
            subset.samples[i] = X[idx];
            subset.labels[i] = y[idx];
        }
        return subset;
    }

    public double getRatio() {
        return ratio;
    }
}
