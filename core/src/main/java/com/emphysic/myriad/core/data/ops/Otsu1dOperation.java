/*
 * com.emphysic.myriad.core.data.ops.Otsu1dOperation
 *
 * Copyright (c) 2018 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.math.Histogram;

import java.util.Map;

/**
 * Otsu's global thresholding algorithm (1D).  Based on Balarini and Nesmachnow,
 * "A C++ Implementation of Otsu’s Image Segmentation Method, Image Processing On Line, 6 (2016), pp. 155–164"
 * https://doi.org/10.5201/ipol.2016.158
 */
public class Otsu1dOperation implements DatasetOperation {
    private static final long serialVersionUID = 1L; // try never to change - indicates backwards compatibility is broken
    private static final int VERSION = 1; // current implementation version

    @Override
    public Dataset run(Dataset input) {
        if (input != null && input.getSize() > 0) {
            Dataset copy = Histogram.grayScale(input);
            Dataset result = new Dataset(copy.getWidth(), copy.getHeight());
            int threshold = calcThreshold(copy);
            for (int i=0; i<copy.getWidth(); i++) {
                for (int j=0; j<copy.getHeight(); j++) {
                    double val = copy.get(i, j);
                    double out = 0;
                    if (val > threshold) {
                        out = 1;
                    }
                    result.set(i, j, out);
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public long getSerializationVersion() {
        return serialVersionUID;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public void initCurrentVersion(Map<String, Object> objectMap) {}

    /**
     * Calculates Otsu's threshold for a given Dataset.
     * @param input Dataset to threshold
     * @return threshold value between 0 and 255
     */
    public static int calcThreshold(Dataset input) {
        return calcThreshold(input, false);
    }

    /**
     * Calculates Otsu's threshold for a given Dataset.
     * @param input Dataset to threshold
     * @param normalize if true, convert input data to gray scale prior to calculating the threshold
     * @return threshold value between 0 and 255
     */
    public static int calcThreshold(Dataset input, boolean normalize) {
        int threshold = 0;
        int[] hist = Histogram.hist1d(input, normalize);
        int suma = 0;
        for (int i=0; i<hist.length; i++) {
            suma += i * hist[i];
        }
        int q1 = 0;
        int q2;
        int sumb = 0;
        double u1;
        double u2;
        double sigma;
        double var_max = 0;
        for (int j=0; j<hist.length; j++) {
            q1 += hist[j];
            if (q1 == 0) {
                continue;
            }
            q2 = input.getSize() - q1;
            if (q2 == 0) {
                break;
            }
            sumb += j * hist[j];
            u1 = sumb / q1;
            u2 = (suma - sumb) / q2;
            sigma = q1 * q2 * Math.pow(u1 - u2, 2);
            if (sigma > var_max) {
                threshold = j;
                var_max = sigma;
            }
        }
        return threshold;
    }
}
