/*
 * com.emphysic.myriad.core.data.util.ExternalProcessTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.util.*;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ProcessBuilder.class, Process.class, ExternalProcess.class})
public class ExternalProcessTest {
    Process mockProcess;
    ProcessBuilder mockProcessBuilder;
    String cmd;
    List<String> args;
    Map<String, String> env;
    Random random;
    ExternalProcess eProc;

    @Before
    public void setUp() throws Exception {
        mockProcess = mock(Process.class);
        when(mockProcess.isAlive()).thenReturn(true);

        mockProcessBuilder = mock(ProcessBuilder.class);
        when(mockProcessBuilder.start()).thenReturn(mockProcess);
        PowerMockito.whenNew(ProcessBuilder.class).withAnyArguments().thenReturn(mockProcessBuilder);

        random = new Random();
        args = new ArrayList<>();
        env = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            args.add(Integer.toString(random.nextInt()));
            env.put(Integer.toString(i + 1), Integer.toString(random.nextInt(10)));
        }
        eProc = new ExternalProcess(cmd, args, env, new File(System.getProperty("user.dir")));
    }

    @Test
    public void addEnvironmentVariable() throws Exception {
        String key = "k";
        assertFalse(eProc.getEnv().containsKey(key));
        String val = "value";
        eProc.addEnvironmentVariable(key, val);
        assertEquals(eProc.getEnv().get(key), val);
    }

    @Test
    public void delEnvironmentVariable() throws Exception {
        Set<String> keys = eProc.getEnv().keySet();
        int idx = random.nextInt(keys.size());
        String keyToRemove = (String) keys.toArray()[idx];
        eProc.delEnvironmentVariable(keyToRemove);
        assertFalse(eProc.getEnv().containsKey(keyToRemove));
    }

    @Test
    public void addArgument() throws Exception {
        String newArg = "arg";
        assertFalse(eProc.getArgs().contains(newArg));
        eProc.addArgument(newArg, 1);
        assertEquals(eProc.getArgs().indexOf(newArg), 1);
        assertEquals(args.size() + 2, eProc.getArgs().size());
    }

    @Test
    public void addArgument1() throws Exception {
        String newArg = "arg";
        assertFalse(eProc.getArgs().contains(newArg));
        eProc.addArgument(newArg);
        assertEquals(eProc.getArgs().indexOf(newArg), eProc.getArgs().size() - 1);
        assertEquals(args.size() + 2, eProc.getArgs().size());
    }

    @Test
    public void delArgument() throws Exception {
        int idx = random.nextInt(args.size());
        String argToRemove = args.get(idx);
        assertTrue(eProc.getArgs().contains(argToRemove));
        eProc.delArgument(argToRemove);
        assertFalse(eProc.getArgs().contains(argToRemove));
    }

    @Test
    public void getWorkingFolder() throws Exception {
        assertEquals(System.getProperty("user.dir"), eProc.getWorkingFolder().getAbsolutePath());
    }

    @Test
    public void run() throws Exception {
        eProc.start();
        assertEquals(mockProcess, eProc.getProcess());
        assertTrue(eProc.getProcess().isAlive());
    }

    @Test
    public void getEnv() throws Exception {
        Map<String, String> procEnvironment = new HashMap<>(eProc.getEnv());
        for (String k : procEnvironment.keySet()) {
            assertEquals(env.get(k), procEnvironment.get(k));
        }
    }

    @Test
    public void setEnv() throws Exception {
        env.clear();
        env.put("k", "v");
        eProc.setEnv(env);
        Map<String, String> procEnvironment = new HashMap<>(eProc.getEnv());
        assertEquals(env.size(), procEnvironment.size());
        for (String k : procEnvironment.keySet()) {
            assertEquals(env.get(k), procEnvironment.get(k));
        }
    }

    @Test
    public void getArgs() throws Exception {
        List<String> arguments = eProc.getArgs();
        assertEquals(args.size() + 1, arguments.size());
        for (int i = 1; i < arguments.size() - 1; i++) {
            assertEquals(args.get(i - 1), arguments.get(i));
        }
        assertEquals(arguments.get(0), eProc.getCmd());
    }

    @Test
    public void setArgs() throws Exception {
        List<String> a = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            a.add("Argument " + (i + 1));
        }
        eProc.setArgs(a);
        assertEquals(a.size(), eProc.getArgs().size());
        for (int i = 0; i < a.size(); i++) {
            assertEquals(a.get(i), eProc.getArgs().get(i));
        }
    }

    @Test
    public void setWorkingFolder() throws Exception {
        String tmpFolder = System.getProperty("java.io.tmpdir");
        File newFolder = new File(tmpFolder);
        eProc.setWorkingFolder(newFolder);
        assertEquals(newFolder.getAbsolutePath(), eProc.getWorkingFolder().getAbsolutePath());
    }

    @Test
    public void getCmd() throws Exception {
        assertEquals(cmd, eProc.getCmd());
    }

    @Test
    public void setCmd() throws Exception {
        String c = "myriad";
        eProc.setCmd(c);
        assertEquals(c, eProc.getCmd());
    }

}