/*
 * com.emphysic.myriad.core.data.ops.math.GradientVectorTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops.math;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.ConvolutionOperation;
import com.emphysic.myriad.core.data.ops.SlidingWindowOperation;
import com.emphysic.myriad.core.data.util.FileSniffer;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;

import static org.junit.Assert.assertEquals;

/**
 * GradientVectorTest - tests the GradientVector class
 * Created by chris on 8/25/2016.
 */
public class GradientVectorTest {
    private URL testJpgURL = Thread.currentThread().getContextClassLoader().getResource("data/io/test.jpg");
    private File img = new File(testJpgURL.getPath());
    private GradientVector gv;

    /**
     * Dataset contents of test file
     */
    private Dataset orig;

    /**
     * Horizontal gradient kernel
     */
    private double[][] hKernel = new double[][] {
            {-1, 0, 1}
    };

    /**
     * Vertical gradient kernel
     */
    private double[][] vKernel = new double[][] {
            {-1},
            {0},
            {1}
    };

    @Before
    public void setUp() throws Exception {
        orig = FileSniffer.read(img, true);
        gv = new GradientVector(orig);
    }

    /**
     * Utility method for checking two Datasets are equal
     * @param expected expected results
     * @param actual actual results
     */
    public void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i=0; i<expected.getWidth(); i++) {
            for (int j=0; j<expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), actual.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

    /**
     * Utility method for checking two arrays are (nearly) equal
     * @param expected expected results
     * @param actual actual results
     */
    public void assertArraysEqual(double[] expected, double[] actual) {
        assertEquals(expected.length, actual.length);
        for (int i=0; i<expected.length; i++) {
            assertEquals(expected[i], actual[i], 0.05 * expected[i]);
        }
    }

    @Test
    public void hGrad() throws Exception {
        ConvolutionOperation convolutionOperation = new ConvolutionOperation(hKernel);
        assertDatasetsEqual(convolutionOperation.run(orig), gv.hGrad(orig));
    }

    @Test
    public void vGrad() throws Exception {
        ConvolutionOperation convolutionOperation = new ConvolutionOperation(vKernel);
        assertDatasetsEqual(convolutionOperation.run(orig), gv.vGrad(orig));
    }

    @Test
    public void calc() throws Exception {
        double[] angles = {
                 0.5880026035475675,  1.5707963267948966,  2.356194490192345, -1.5707963267948966, -1.1071487177940904,
                -0.7853981633974483, -1.5707963267948966,  0.7853981633974483, 2.677945044588987,  -0.4636476090008061,
                 1.5707963267948966,  0.8960553845713439,  3.141592653589793, -1.5707963267948966,  1.1071487177940904,
                 0.5404195002705842, -0.7853981633974483, -2.677945044588987,  1.2490457723982544,  3.141592653589793,
                -0.5880026035475675, -2.0344439357957027, -0.7853981633974483, 2.356194490192345,  3.141592653589793,
        };
        double[] mags = {
                237220, 263172, 186091, 131586, 147118,
                 93045,  65793, 186091, 147118, 147118,
                 65793, 421281,  65793, 131586, 147118,
                383636, 186091, 294235, 208056,  65793,
                237220, 294235,  93045,  93045,  65793
        };
        SlidingWindowOperation slide = new SlidingWindowOperation(5, 5);
        Dataset window = slide.run(orig);
        gv = new GradientVector(window);
        assertArraysEqual(angles, gv.getAngles().getData());
        assertArraysEqual(mags, gv.getMagnitudes().getData());
    }
}