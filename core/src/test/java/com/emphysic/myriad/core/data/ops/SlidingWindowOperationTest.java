/*
 * com.emphysic.myriad.core.data.ops.SlidingWindowOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class SlidingWindowOperationTest {
    private double[] data = {
            1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0,
            8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0,
            15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0,
            22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0,
            29.0, 30.0, 31.0, 32.0, 33.0, 34.0, 35.0
    };

    private double[][] step1window2 = {
            {1.0, 2.0, 8.0, 9.0},
            {2.0, 3.0, 9.0, 10.0},
            {3.0, 4.0, 10.0, 11.0},
            {4.0, 5.0, 11.0, 12.0},
            {5.0, 6.0, 12.0, 13.0},
            {6.0, 7.0, 13.0, 14.0},
            {8.0, 9.0, 15.0, 16.0},
            {9.0, 10.0, 16.0, 17.0},
            {10.0, 11.0, 17.0, 18.0},
            {11.0, 12.0, 18.0, 19.0},
            {12.0, 13.0, 19.0, 20.0},
            {13.0, 14.0, 20.0, 21.0},
            {15.0, 16.0, 22.0, 23.0},
            {16.0, 17.0, 23.0, 24.0},
            {17.0, 18.0, 24.0, 25.0},
            {18.0, 19.0, 25.0, 26.0},
            {19.0, 20.0, 26.0, 27.0},
            {20.0, 21.0, 27.0, 28.0},
            {22.0, 23.0, 29.0, 30.0},
            {23.0, 24.0, 30.0, 31.0},
            {24.0, 25.0, 31.0, 32.0},
            {25.0, 26.0, 32.0, 33.0},
            {26.0, 27.0, 33.0, 34.0},
            {27.0, 28.0, 34.0, 35.0}
    };

    private double[][] step3window1 = {
            {1.0},
            {4.0},
            {7.0},
            {22.0},
            {25.0},
            {28.0}
    };

    private double[][] step2window3 = {
            {1.0, 2.0, 3.0, 8.0, 9.0, 10.0, 15.0, 16.0, 17.0},
            {3.0, 4.0, 5.0, 10.0, 11.0, 12.0, 17.0, 18.0, 19.0},
            {5.0, 6.0, 7.0, 12.0, 13.0, 14.0, 19.0, 20.0, 21.0},
            {15.0, 16.0, 17.0, 22.0, 23.0, 24.0, 29.0, 30.0, 31.0},
            {17.0, 18.0, 19.0, 24.0, 25.0, 26.0, 31.0, 32.0, 33.0},
            {19.0, 20.0, 21.0, 26.0, 27.0, 28.0, 33.0, 34.0, 35.0}
    };

    /**
     * Expected windows for step size 3, 4x5 window
     */
    private double[][] hslide345 = {
            { 1.0,  2.0,  3.0,  4.0,
              8.0,  9.0, 10.0, 11.0,
             15.0, 16.0, 17.0, 18.0,
             22.0, 23.0, 24.0, 25.0,
             29.0, 30.0, 31.0, 32.0},
            { 4.0,  5.0,  6.0,  7.0,
             11.0, 12.0, 13.0, 14.0,
             18.0, 19.0, 20.0, 21.0,
             25.0, 26.0, 27.0, 28.0,
             32.0, 33.0, 34.0, 35.0}
    };

    public Dataset getOriginalDataset() {
        return new Dataset(data, 7, 5);
    }

    /**
     * Verify that two arrays are nearly equal i.e. have the same length and the maximum delta between two
     * corresponding elements is 5%.
     *
     * @param expected expected results
     * @param actual   actual results
     */
    public void assertResultsEqual(double[] expected, double[] actual) {
        assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], actual[i], 0.05 * expected[i]);
        }
    }

    /**
     * Verify sliding across an input with step size 1 and window size 2 (2x2 window)
     */
    @Test
    public void testStep1Window2() throws Exception {
        SlidingWindowOperation slidingWindow = new SlidingWindowOperation(1, 2);
        Dataset current = getOriginalDataset();
        for (double[] aStep1window2 : step1window2) {
            Dataset window = slidingWindow.run(current);
            assertResultsEqual(aStep1window2, window.getData());
        }
    }

    /**
     * Verify sliding across an input with step size 3 and window size 1 (1 element window)
     */
    @Test
    public void testStep3Window1() throws Exception {
        SlidingWindowOperation slidingWindow = new SlidingWindowOperation(3, 1);
        Dataset current = getOriginalDataset();
        for (double[] aStep3window1 : step3window1) {
            Dataset window = slidingWindow.run(current);
            assertResultsEqual(aStep3window1, window.getData());
        }
    }

    /**
     * Verify sliding across an input with step size 2 and window size 3 (3x3 window)
     */
    @Test
    public void testStep2Window3() throws Exception {
        SlidingWindowOperation slidingWindow = new SlidingWindowOperation(2, 3);
        Dataset current = getOriginalDataset();
        for (double[] aStep2window3 : step2window3) {
            Dataset window = slidingWindow.run(current);
            assertResultsEqual(aStep2window3, window.getData());
        }
    }

    /**
     * Verify performing a specified operation on each slide
     */
    @Test
    public void testOp() throws Exception {
        BoxBlur blur = new BoxBlur(2);
        SlidingWindowOperation slidingWindow = new SlidingWindowOperation(1, 2, blur);
        Dataset current = getOriginalDataset();
        for (double[] aStep1window2 : step1window2) {
            Dataset window = slidingWindow.run(current);
            Dataset orig = new Dataset(aStep1window2, 2, 2);
            Dataset expected = blur.run(orig);
            assertResultsEqual(expected.getData(), window.getData());
        }
    }

    /**
     * Verify sliding across a 1D Dataset (Wx1).
     */
    @Test
    public void testHSlide() throws Exception {
        Dataset hData = new Dataset(data, data.length, 1);
        SlidingWindowOperation slidingWindow = new SlidingWindowOperation(1, 10);
        for (int slide = 0; slide < 26; slide++) {
            Dataset window = slidingWindow.run(hData);
            for (int el = 0; el < 10; el++) {
                double expected = hData.get(slide + el, 0);
                double actual = window.get(el, 0);
                assertEquals(expected, actual, 0.05 * expected);
            }
        }
    }

    /**
     * Verify sliding across a 1D Dataset (1xH).
     */
    @Test
    public void testVSlide() throws Exception {
        Dataset vData = new Dataset(data, 1, data.length);
        SlidingWindowOperation slidingWindow = new SlidingWindowOperation(1, 10);
        for (int slide = 0; slide < 26; slide++) {
            Dataset window = slidingWindow.run(vData);
            for (int el = 0; el < 10; el++) {
                double expected = vData.get(0, slide + el);
                double actual = window.get(0, el);
                assertEquals(expected, actual, 0.05 * expected);
            }
        }
    }

    @Test
    public void testRect() throws Exception {
        Dataset orig = getOriginalDataset();
        SlidingWindowOperation slider = new SlidingWindowOperation(3, 4, 5);
        Dataset current;
        int windowNum = 0;
        while ((current = slider.run(orig)) != null) {
            assertResultsEqual(hslide345[windowNum], current.getData());
            windowNum++;
        }
    }

    @Test
    public void serialize() throws Exception {
        Dataset orig = getOriginalDataset();
        SlidingWindowOperation slider = new SlidingWindowOperation(3, 4, 5, new BoxBlur(3));
        File out = File.createTempFile("tmpslider", "dat");
        slider.save(out);
        SlidingWindowOperation read = new SlidingWindowOperation();
        read.load(out);
        Dataset expected = slider.run(orig);
        Dataset actual = read.run(orig);
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        assertResultsEqual(expected.getData(), actual.getData());
    }
}