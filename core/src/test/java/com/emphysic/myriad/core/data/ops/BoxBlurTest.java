/*
 * com.emphysic.myriad.core.data.ops.BoxBlurTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class BoxBlurTest {
    double[] originalData = {
            0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 50.0, 50.0, 50.0, 0.0,
            0.0, 50.0, 50.0, 50.0, 0.0,
            0.0, 50.0, 50.0, 50.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0
    };
    BoxBlur boxBlur;

    public Dataset getOriginalDataset() {
        return new Dataset(originalData, 5, 5);
    }

    /**
     * Verify two Datasets appear equal i.e. dimensions are the same and that there is no more than a 5% difference
     * between an expected element value and the actual element value.
     *
     * @param expected Expected results
     * @param returned Actual results
     */
    public void assertDatasetsEqual(Dataset expected, Dataset returned) {
        assertEquals(expected.getWidth(), returned.getWidth());
        assertEquals(expected.getHeight(), returned.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), returned.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

    @Before
    public void setUp() throws Exception {
        boxBlur = new BoxBlur(2);
    }

    /**
     * Verify the vertical blur operation
     */
    @Test
    public void vblur() throws Exception {
        double[] expectedData = {
                0.0, 0.0, 0.0, 0.0, 0.0,
                20.0, 30.0, 30.0, 30.0, 20.0,
                20.0, 30.0, 30.0, 30.0, 20.0,
                20.0, 30.0, 30.0, 30.0, 20.0,
                0.0, 0.0, 0.0, 0.0, 0.0
        };
        Dataset expectedDataset = new Dataset(expectedData, 5, 5);
        Dataset blurredDataset = boxBlur.VBlur(getOriginalDataset());
        assertDatasetsEqual(expectedDataset, blurredDataset);
    }

    /**
     * Verify the horizontal blur operation
     */
    @Test
    public void hblur() throws Exception {
        double[] expectedData = {
                0.0, 20.0, 20.0, 20.0, 0.0,
                0.0, 30.0, 30.0, 30.0, 0.0,
                0.0, 30.0, 30.0, 30.0, 0.0,
                0.0, 30.0, 30.0, 30.0, 0.0,
                0.0, 20.0, 20.0, 20.0, 0.0
        };
        Dataset expectedDataset = new Dataset(expectedData, 5, 5);
        Dataset blurredDataset = boxBlur.HBlur(getOriginalDataset());
        assertDatasetsEqual(expectedDataset, blurredDataset);
    }

    /**
     * Verify the final blur operation
     */
    @Test
    public void blur() throws Exception {
        double[] expectedData = {
                8.0, 12.0, 12.0, 12.0, 8.0,
                12.0, 18.0, 18.0, 18.0, 12.0,
                12.0, 18.0, 18.0, 18.0, 12.0,
                12.0, 18.0, 18.0, 18.0, 12.0,
                8.0, 12.0, 12.0, 12.0, 8.0
        };
        Dataset expectedDataset = new Dataset(expectedData, 5, 5);
        Dataset blurredDataset = boxBlur.run(getOriginalDataset());
        assertDatasetsEqual(expectedDataset, blurredDataset);
    }

    @Test
    public void serialize() throws Exception {
        Random random = new Random();
        double[] data = new double[]{-3, -2, -1, 0, 1, 2, 3};
        Dataset dataset = new Dataset(data, data.length, 1);
        int radius = random.nextInt(3);
        BoxBlur orig = new BoxBlur(radius);
        Dataset expected = orig.run(dataset);
        File out = File.createTempFile("tmpblur", "dat");
        orig.save(out);
        BoxBlur reread = new BoxBlur();
        reread.load(out);
        Dataset actual = reread.run(dataset);
        assertDatasetsEqual(expected, actual);
    }
}