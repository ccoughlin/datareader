/*
 * com.emphysic.myriad.core.data.roi.ExternalROIFinderTest
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.util.ExternalProcess;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ProcessBuilder.class, Process.class, ExternalROIFinder.class})
public class ExternalROIFinderTest {

    /**
     * Convenience method to mock an external process and assert correct input/output.
     *
     * @param procResponses    Simulated messages printed to external process' standard output
     * @param foundFlaw        true if a flaw should have been found, false otherwise
     * @param expectedResponse byte array that should have been sent to external process' standard input
     */
    public void assertExpectedResponse(String[] procResponses, boolean foundFlaw, char[] expectedResponse) throws Exception {
        for (String str : procResponses) {
            InputStream stdout = new ByteArrayInputStream(str.getBytes("UTF-8"));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Process mockProcess = mock(Process.class);
            when(mockProcess.getInputStream()).thenReturn(stdout);
            when(mockProcess.getOutputStream()).thenReturn(baos);

            ProcessBuilder mockProcessBuilder = mock(ProcessBuilder.class);
            when(mockProcessBuilder.start()).thenReturn(mockProcess);

            ExternalProcess mockExternalProcess = mock(ExternalProcess.class);
            when(mockExternalProcess.getProcess()).thenReturn(mockProcess);

            PowerMockito.whenNew(ProcessBuilder.class).withAnyArguments().thenReturn(mockProcessBuilder);

            ExternalROIFinder externalFlawFinder = new ExternalROIFinder(mockExternalProcess);
            double[] data = new double[]{1, 2, 3};
            assertEquals(foundFlaw, externalFlawFinder.isROI(data));
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            InputStreamReader in = new InputStreamReader(bais);
            char[] response = new char[bais.available()];
            in.read(response);
            assertArrayEquals(expectedResponse, response);
            stdout.close();
        }
    }

    @Test
    public void runFlawFinder() throws Exception {
        String[] foundFlaw = {"myriad: True", "myriad: true", "myriad: TRUE"};
        String[] noFlaw = {"myriad: False", "myriad: false", "myriad: FALSE"};
        String lineEnding = System.getProperty("line.separator");
        char[] expectedResponse = new char[12 + lineEnding.length()];
        char[] baseResponse = {
                '1', '.', '0', ' ',
                '2', '.', '0', ' ',
                '3', '.', '0', ' '
        };
        System.arraycopy(baseResponse, 0, expectedResponse, 0, baseResponse.length);
        // Need to handle platform-dependent line endings \r\n Windows, \n Linux / OS X, etc.
        for (int i=0; i<lineEnding.length(); i++) {
            expectedResponse[12 + i] = lineEnding.charAt(i);
        }
        assertExpectedResponse(foundFlaw, true, expectedResponse);
        assertExpectedResponse(noFlaw, false, expectedResponse);
    }

    @Test
    public void serialization() throws Exception {
        File tmpFile = File.createTempFile("tmpModel", ".myr");
        String cmd = "/path/to/nowhere";
        List<String> cargs = new ArrayList<>();
        cargs.add("Purple");
        cargs.add("Monkey");
        cargs.add("Dishwasher");
        Map<String, String> env = new HashMap<>();
        env.put("PurpleMonkey", "Dishwasher");
        ExternalROIFinder erf = ExternalROIFinder.buildROIFinder(cmd, cargs, env, null);
        erf.setTimeout(123, TimeUnit.DAYS);
        erf.save(tmpFile);

        ExternalROIFinder reread = new ExternalROIFinder();
        reread.load(tmpFile);
        assertModelsEqual(erf, reread);

        ExternalROIFinder read2 = (ExternalROIFinder) ROIFinder.fromFile(tmpFile, ExternalROIFinder.class);
        assertModelsEqual(erf, read2);
    }

    @Test
    public void testIsMyriadMessage() throws Exception {
        String[] shouldMatch = {
                "MYRIAD:123",
                "Myriad: Purple Monkey Dishwasher",
                "myriad:",
                "MYRIAD:  "
        };
        String[] shouldnotMatch = {
                "MRYIAD:",
                " Myriad:",
                "myriad",
                "  MYRIAD:"
        };
        ExternalROIFinder tst = new ExternalROIFinder();
        for (String s : shouldMatch) {
            assertTrue(tst.isMyriadMessage(s));
        }
        for (String s : shouldnotMatch) {
            assertFalse(tst.isMyriadMessage(s));
        }
    }

    @Test
    public void testIsLogMessage() throws Exception {
        String logMessages[] = {
                "10:13:07,341 |-ERROR in c.q.l.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy",
                "10:13:07,341 |-WARN in c.q.l.core.rolling.TimeBasedRollingPolicy@1165897474 - Subcomponent did not start.",
                "10:13:07,359 |-INFO in ch.qos.logback.classic.joran.action.RootLoggerAction - Setting level of ROOT logger to DEBUG",
                "log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.",
                "[debug] execute contextualize"
        };
        ExternalROIFinder tst = new ExternalROIFinder();
        for (String s : logMessages) {
            assertFalse(tst.isLogMessage(s));
            assertTrue(tst.isLogMessage("Myriad:" + s));
        }
    }

    @Test
    public void testIsROIMessage() throws Exception {
        String roiMessages[] = {
                "TRUE",
                "FALSE",
                " TrUe Blue",
                "~~~~ false pretense ~~~"
        };
        ExternalROIFinder tst = new ExternalROIFinder();
        for (String s : roiMessages) {
            assertFalse(tst.isROIMessage(s));
            assertTrue(tst.isROIMessage("Myriad: " + s));
        }
    }

    @Test
    public void testROIFound() throws Exception {
        String trueMessages[] = {
                "TRUE",
                "true",
                "!!!!True!!!!",
                "            True"
        };
        String falseMessages[] = {
                "FALSE",
                "false",
                ">>> False! <<<",
                "--- FALSE"
        };
        ExternalROIFinder tst = new ExternalROIFinder();
        for (String s : trueMessages) {
            assertFalse(tst.ROIfound(s));
            assertTrue(tst.ROIfound("myriad: " + s));
        }
        for (String s : falseMessages) {
            assertFalse(tst.ROIfound(s));
            assertFalse(tst.ROIfound("myriad: " + s));
        }
    }

    /**
     * Verify that two external process configurations are equivalent.
     * @param expected expected configuration
     * @param actual actual configuration
     */
    private void assertModelsEqual(ExternalROIFinder expected, ExternalROIFinder actual) {
        assertEquals(expected.getTimeout(), actual.getTimeout());
        assertEquals(expected.getTimeoutUnits(), actual.getTimeoutUnits());
        ExternalProcess expectedProc = expected.getProcessRunner();
        ExternalProcess actualProc = actual.getProcessRunner();
        assertEquals(expectedProc.getCmd(), actualProc.getCmd());
        assertEquals(expectedProc.getArgs(), actualProc.getArgs());
        assertEquals(expectedProc.getEnv(), actualProc.getEnv());
    }

}