/*
 * com.emphysic.myriad.core.data.ops.math.StatsTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops.math;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.FileSniffer;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;

import static org.junit.Assert.assertEquals;

public class StatsTest {
    private Dataset dataset;
    private double expectedMean = 0.76549;
    private double expectedVariance = 9.2412;
    private double expectedStdDev = 3.040;
    private double expectedMin = -9;
    private double expectedMax = 45;

    @Before
    public void setUp() throws Exception {
        URL sampleDataUrl = Thread.currentThread().getContextClassLoader().getResource("data/io/sampledata/140.csv");
        File sampleDataFile = new File(sampleDataUrl.getPath());
        dataset = FileSniffer.read(sampleDataFile, true);
    }

    @Test
    public void mean() throws Exception {
        double fromDataset = Stats.mean(dataset);
        double fromData = Stats.mean(dataset.getData());
        assertEquals(expectedMean, fromDataset, 0.05 * expectedMean);
        assertEquals(expectedMean, fromData, 0.05 * expectedMean);
    }

    @Test
    public void variance() throws Exception {
        double fromDataset = Stats.variance(dataset);
        double fromData = Stats.variance(dataset.getData());
        assertEquals(expectedVariance, fromDataset, 0.05 * expectedVariance);
        assertEquals(expectedVariance, fromData, 0.05 * expectedVariance);
    }

    @Test
    public void stddev() throws Exception {
        double fromDataset = Stats.stddev(dataset);
        double fromData = Stats.stddev(dataset.getData());
        assertEquals(expectedStdDev, fromDataset, 0.05 * expectedStdDev);
        assertEquals(expectedStdDev, fromData, 0.05 * expectedStdDev);
    }

    @Test
    public void min() throws Exception {
        double fromDataset = Stats.min(dataset);
        double fromData = Stats.min(dataset.getData());
        assertEquals(expectedMin, fromDataset, 0.05 * expectedMin);
        assertEquals(expectedMin, fromData, 0.05 * expectedMin);
    }

    @Test
    public void max() throws Exception {
        double fromDataset = Stats.max(dataset);
        double fromData = Stats.max(dataset.getData());
        assertEquals(expectedMax, fromDataset, 0.05 * expectedMax);
        assertEquals(expectedMax, fromData, 0.05 * expectedMax);
    }

}