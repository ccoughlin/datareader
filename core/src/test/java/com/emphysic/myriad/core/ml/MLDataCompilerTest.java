/*
 * com.emphysic.myriad.core.ml.MLDataCompilerTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.ml;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.io.IODataset;
import com.emphysic.myriad.core.data.ops.UpscaleOperation;
import com.emphysic.myriad.core.data.util.FileSniffer;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * MLDataCompilerTest - tests the MLDataCompiler class
 * Created by chris on 9/12/2016.
 */
public class MLDataCompilerTest {

    private File posFolder;
    private File negFolder;
    private MLDataCompiler mldc;

    @Before
    public void setUp() throws Exception {
        URL dataUrl = Thread.currentThread().getContextClassLoader().getResource("data/ml/generated");
        File generatedDataFolder = new File(dataUrl.getPath());
        posFolder = new File(generatedDataFolder, "signal");
        negFolder = new File(generatedDataFolder, "noise");
        mldc = new MLDataCompiler(posFolder, negFolder);
    }

    @Test
    public void testReadData() throws Exception {
        CrossValidation.Data sampleData = mldc.readData();
        File[] posSamples = posFolder.listFiles();
        File[] negSamples = negFolder.listFiles();
        assertEquals(posSamples.length + negSamples.length, sampleData.numSamples());
        IODataset rep = FileSniffer.read(posSamples[0], true);
        assertEquals(rep.getSize(), sampleData.numFeatures());
    }

    @Test
    public void testReadDataWithOp() throws Exception {
        UpscaleOperation up = new UpscaleOperation();
        CrossValidation.Data sampleData = mldc.readData(up);
        File[] posSamples = posFolder.listFiles();
        File[] negSamples = negFolder.listFiles();
        assertEquals(posSamples.length + negSamples.length, sampleData.numSamples());
        IODataset rep = FileSniffer.read(posSamples[0], true);
        assertEquals(rep.getSize() * 4, sampleData.numFeatures());
    }

    @Test
    public void testReadFolder() throws Exception {
        File[] fldrs = {posFolder, negFolder};
        for (File fldr : fldrs) {
            List<IODataset> read = MLDataCompiler.readFolder(fldr);
            List<IODataset> expected = new ArrayList<>();
            File[] files = fldr.listFiles();
            for (File f: files) {
                expected.add(FileSniffer.read(f, true));
            }
            assertEquals(expected.size(), read.size());
            for (int i=0; i<expected.size(); i++) {
                assertDatasetsEqual(expected.get(i), read.get(i));
            }
        }
    }

    /**
     * Helper method to check that two Datasets are the same dimensions and (nearly) the same values (w/i 5% of expected)
     * @param expected expected Dataset
     * @param actual actual Dataset
     */
    public void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i=0; i<expected.getWidth(); i++) {
            for (int j=0; j<expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), actual.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }
}