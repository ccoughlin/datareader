/*
 * com.emphysic.myriad.core.data.io.DicomDatasetTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.io;

import com.pixelmed.display.SourceImage;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import static org.junit.Assert.assertEquals;

public class DicomDatasetTest {

    File multiFrameDICONDEFile;
    DicomDataset dicomDataset;

    @Before
    public void setUp() throws Exception {
        URL dicondeUrl = Thread.currentThread().getContextClassLoader().getResource("data/io/dicondeEnhancedCtExampleImageE2767-13Typical.dcm");
        multiFrameDICONDEFile = new File(dicondeUrl.getPath());
        dicomDataset = new DicomDataset(multiFrameDICONDEFile);
    }

    /**
     * Helper method - verifies two arrays are equal
     *
     * @param expected expected results
     * @param actual   actual results
     */
    public void assertArraysEqual(double[] expected, double[] actual) {
        assertEquals(expected.length, actual.length);
        for (int j = 0; j < expected.length; j++) {
            assertEquals(expected[j], actual[j], 0.05 * expected[j]);
        }
    }

    @Test
    public void numframes() throws Exception {
        SourceImage sourceImage = new SourceImage(multiFrameDICONDEFile.getAbsolutePath());
        assertEquals(sourceImage.getNumberOfFrames(), dicomDataset.getNumFrames());
    }

    /**
     * Verify reading and iterating over multi-image DICOM
     */
    @Test
    public void readMultiFrameImages() throws Exception {
        SourceImage sourceImage = new SourceImage(multiFrameDICONDEFile.getAbsolutePath());
        for (int frame = 0; frame < sourceImage.getNumberOfFrames(); frame++) {
            BufferedImage expectedImage = sourceImage.getBufferedImage(frame);
            double[] expected = ImageDataset.getData(
                    expectedImage,
                    0,
                    0,
                    expectedImage.getWidth(),
                    expectedImage.getHeight()
            );
            assertArraysEqual(expected, dicomDataset.read(frame));
        }
    }
}