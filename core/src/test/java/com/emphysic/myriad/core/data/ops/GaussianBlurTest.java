/*
 * com.emphysic.myriad.core.data.ops.GaussianBlurTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class GaussianBlurTest {
    double[] originalData = {
            0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 50.0, 50.0, 50.0, 0.0,
            0.0, 50.0, 50.0, 50.0, 0.0,
            0.0, 50.0, 50.0, 50.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0
    };
    GaussianBlur blur;

    public Dataset getOriginalDataset() {
        return new Dataset(originalData, 5, 5);
    }

    /**
     * Runs a specified number of Box blurs on the input Dataset
     *
     * @param input     Dataset to blur
     * @param numPasses number of blurs to perform
     * @return blurred Dataset
     */
    public Dataset runBlurs(Dataset input, int numPasses) {
        BoxBlur bblur = new BoxBlur(2);
        Dataset blurredDataset = bblur.run(getOriginalDataset());
        for (int pass = 0; pass < numPasses - 1; pass++) {
            blurredDataset = bblur.run(blurredDataset);
        }
        return blurredDataset;
    }

    /**
     * Verify two Datasets appear equal i.e. dimensions are the same and that there is no more than a 5% difference
     * between an expected element value and the actual element value.
     *
     * @param expected Expected results
     * @param returned Actual results
     */
    public void assertDatasetsEqual(Dataset expected, Dataset returned) {
        assertEquals(expected.getWidth(), returned.getWidth());
        assertEquals(expected.getHeight(), returned.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), returned.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

    @Before
    public void setUp() throws Exception {
        blur = new GaussianBlur(2);
    }

    /**
     * Verify the Gaussian blur is equivalent to N passes of a box blur.
     */
    @Test
    public void testNumPassses() throws Exception {
        Random random = new Random();
        int numPasses = random.nextInt(20) + 1;
        Dataset expectedDataset = runBlurs(getOriginalDataset(), numPasses);
        GaussianBlur gb = new GaussianBlur(2, numPasses);
        Dataset returnedDataset = gb.run(getOriginalDataset());
        assertDatasetsEqual(expectedDataset, returnedDataset);
    }

    /**
     * Verify the default Gaussian blur is equivalent to 3 passes of a box blur.
     */
    @Test
    public void testDefault() throws Exception {
        Dataset expectedDataset = runBlurs(getOriginalDataset(), 3);
        Dataset returnedDataset = blur.run(getOriginalDataset());
        assertDatasetsEqual(expectedDataset, returnedDataset);
    }

    @Test
    public void serialize() throws Exception {
        Random random = new Random();
        double[] data = new double[]{-3, -2, -1, 0, 1, 2, 3};
        Dataset dataset = new Dataset(data, data.length, 1);
        int radius = random.nextInt(3);
        GaussianBlur orig = new GaussianBlur(radius);
        Dataset expected = orig.run(dataset);
        File out = File.createTempFile("tmpblur", "dat");
        orig.save(out);
        GaussianBlur reread = new GaussianBlur();
        reread.load(out);
        Dataset actual = reread.run(dataset);
        assertDatasetsEqual(expected, actual);
    }
}