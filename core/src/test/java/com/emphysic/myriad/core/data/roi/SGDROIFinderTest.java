/*
 * com.emphysic.myriad.core.data.roi.SGDROIFinderTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SGDROIFinderTest {
    private double[][] X;
    private int[] y;
    private SGDROIFinder sgdFlawFinder;
    private Random random;

    @Before
    public void setUp() throws Exception {
        X = new double[][]{{-1.0, -1.0}, {-2.0, -1.0}, {1.0, 1.0}, {2.0, 1.0}};
        y = new int[]{0, 0, 1, 1};
        sgdFlawFinder = new SGDROIFinder();
        sgdFlawFinder.train(X, y);
        random = new Random();
    }

    @Test
    public void isFlawSignal() throws Exception {
        int rndIdx = random.nextInt(y.length);
        double[] rndData = X[rndIdx];
        Dataset d = new Dataset(rndData.length, 1);
        for (int i = 0; i < d.getWidth(); i++) {
            for (int j = 0; j < d.getHeight(); j++) {
                d.set(i, j, rndData[i + j]);
            }
        }
        assertEquals(y[rndIdx] > 0, sgdFlawFinder.isROI(d));
        assertEquals(y[rndIdx] > 0, sgdFlawFinder.isROI(rndData));
    }

    @Test
    public void persist() throws Exception {
        File out = File.createTempFile("sgdmdl", null);
        sgdFlawFinder.save(out);
        SGDROIFinder read = new SGDROIFinder();
        read.load(out);
        assertModelsEqual(sgdFlawFinder, read);

        SGDROIFinder read2 = (SGDROIFinder)ROIFinder.fromFile(out, SGDROIFinder.class);
        assertModelsEqual(sgdFlawFinder, read2);
    }

    /**
     * Asserts that two SGD models are "equal" i.e. make the same predictions and have the same basic parameters.
     * @param expected expected model
     * @param actual actual model
     */
    private void assertModelsEqual(SGDROIFinder expected, SGDROIFinder actual) {
        assertEquals(expected.getNumFeatures(), actual.getNumFeatures());
        assertEquals(expected.positiveClass(), actual.positiveClass(), expected.positiveClass() * 0.05);
        assertEquals(expected.negativeClass(), actual.negativeClass(), expected.negativeClass() * 0.05);
        for (double[] aX : X) {
            assertEquals(expected.isROI(aX), actual.isROI(aX));
        }
    }

    @Test
    public void catchInvalidDimensions() throws Exception {
        double[][][] badSamples = new double[][][]{
                {{1}, {2}, {3}},
                {{-11}},
                {{3, 4, 5, 6}, {2, 3, 4}},
                {{1, 2, 3}, {1}}
        };
        for (double[][] badSample : badSamples) {
            try {
                sgdFlawFinder.train(badSample, new int[]{0});
                fail("Should not allow training with incorrect number of features");
            } catch (IllegalArgumentException ignore) {
            }
            for (int j = 0; j < badSamples[j].length; j++) {
                try {
                    sgdFlawFinder.predict(badSample[j]);
                    fail("Should not allow predictions with incorrect number of features");
                } catch (IllegalArgumentException ignore) {
                }
            }

        }
    }
}