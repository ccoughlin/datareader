/*
 * com.emphysic.myriad.core.data.ops.GaussianPyramidOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GaussianPyramidOperationTest {

    double[] originalData = {
            0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 50.0, 60.0, 50.0, 0.0,
            0.0, 60.0, 90.0, 60.0, 0.0,
            0.0, 50.0, 60.0, 50.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0
    };

    // Gaussian blur radius 2, scale factor 2, min 2x2 window, first step
    double[] step1Data = {
            4.800000000000001, 4.800000000000001, 4.800000000000001,
            4.800000000000001, 4.800000000000001, 4.800000000000001,
            4.800000000000001, 4.800000000000001, 4.800000000000001,
            4.800000000000001, 4.800000000000001, 4.800000000000001
    };

    // Gaussian blur radius 2, scale factor 2, min 2x2 window, first step
    double[] step2Data = {
            4.800000000000001, 4.800000000000001,
            4.800000000000001, 4.800000000000001
    };

    public Dataset getOriginalDataset() {
        return new Dataset(originalData, 5, 7);
    }

    /**
     * Verify two Datasets appear equal i.e. dimensions are the same and that there is no more than a 5% difference
     * between an expected element value and the actual element value.
     *
     * @param expected Expected results
     * @param returned Actual results
     */
    public void assertDatasetsEqual(Dataset expected, Dataset returned) {
        assertEquals(expected.getWidth(), returned.getWidth());
        assertEquals(expected.getHeight(), returned.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), returned.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

    /**
     * Verify catching invalid scaling factor
     */
    @Test
    public void checkInvalidScalingFactor() throws Exception {
        try {
            GaussianPyramidOperation po = new GaussianPyramidOperation(new GaussianBlur(1), 1, 1);
            fail("Shouldn't allow setting a scaling factor less than 2.");
        } catch (IllegalArgumentException iae) {
            assertEquals(iae.getMessage(),
                    "Scaling factor must be 2 or greater.");
        }
    }

    /**
     * Verify catching invalid window size
     */
    @Test
    public void checkInvalidWindowSize() throws Exception {
        try {
            GaussianPyramidOperation po = new GaussianPyramidOperation(new GaussianBlur(1), 2, -1);
        } catch (IllegalArgumentException iae) {
            assertEquals(iae.getMessage(),
                    "Window size must be 1 or greater.");
        }
    }

    /**
     * Verify the Pyramid operation returns expected datasets at each step.
     *
     * @throws Exception
     */
    @Test
    public void testPyramid() throws Exception {
        Dataset current = getOriginalDataset();
        GaussianPyramidOperation po = new GaussianPyramidOperation(new GaussianBlur(2), 2, 1);
        List<Dataset> steps = new ArrayList<>();
        while ((current = po.run(current)) != null) {
            steps.add(current);
        }
        assertTrue(steps.size() == 2);
        assertDatasetsEqual(new Dataset(step1Data, 3, 4), steps.get(0));
        assertDatasetsEqual(new Dataset(step2Data, 2, 2), steps.get(1));
    }

    /**
     * Verify performing Pyramid operation across 1-D horizontal Datasets
     */
    @Test
    public void test1DHorizontalPyramid() throws Exception {
        double[] h = {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        };
        double[] firstStep = new double[]{1, 3, 5, 7, 9};
        double[] secondStep = new double[]{1, 5, 9};
        double[] thirdStep = new double[]{1, 9};
        Dataset horizontal = new Dataset(h, h.length, 1);
        GaussianPyramidOperation po = new GaussianPyramidOperation(2, 1);
        List<Dataset> pyramidSteps = new ArrayList<>();
        Dataset result = new Dataset(horizontal);
        while ((result = po.run(result)) != null) {
            pyramidSteps.add(result);
        }
        assertEquals(pyramidSteps.size(), 3);
        assertDatasetsEqual(pyramidSteps.get(0), new Dataset(firstStep, 5, 1));
        assertDatasetsEqual(pyramidSteps.get(1), new Dataset(secondStep, 3, 1));
        assertDatasetsEqual(pyramidSteps.get(2), new Dataset(thirdStep, 2, 1));
    }

    /**
     * Verify performing Pyramid operation across 1-D vertical Datasets
     */
    @Test
    public void test1DVerticalPyramid() throws Exception {
        double[] v = {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        };
        double[] firstStep = new double[]{1, 3, 5, 7, 9};
        double[] secondStep = new double[]{1, 5, 9};
        double[] thirdStep = new double[]{1, 9};
        Dataset vertical = new Dataset(v, 1, v.length);
        GaussianPyramidOperation po = new GaussianPyramidOperation(2, 1);
        List<Dataset> pyramidSteps = new ArrayList<>();
        Dataset result = new Dataset(vertical);
        while ((result = po.run(result)) != null) {
            pyramidSteps.add(result);
        }
        assertEquals(pyramidSteps.size(), 3);
        assertDatasetsEqual(pyramidSteps.get(0), new Dataset(firstStep, 1, 5));
        assertDatasetsEqual(pyramidSteps.get(1), new Dataset(secondStep, 1, 3));
        assertDatasetsEqual(pyramidSteps.get(2), new Dataset(thirdStep, 1, 2));
    }

    @Test
    public void serialize() throws Exception {
        Dataset original = getOriginalDataset();
        GaussianPyramidOperation gpo = new GaussianPyramidOperation(new BoxBlur(2), 3, 2);
        File out = File.createTempFile("tmpgaussian", "dat");
        gpo.save(out);
        GaussianPyramidOperation read = new GaussianPyramidOperation();
        read.load(out);
        assertDatasetsEqual(gpo.run(original), read.run(original));
    }
}