/*
 * com.emphysic.myriad.core.data.roi.ROITest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * ROITest - tests the ROI class
 * Created by ccoughlin on 8/26/16.
 */
public class ROITest {
    private Dataset dataset;
    private ROI roi;

    @Before
    public void setUp() throws Exception {
        dataset = new Dataset(10, 10);
    }

    @Test
    public void testConstructor() throws Exception {
        int[] coords = {10, -10, 21, -11};
        // Check 1-D coordinates
        roi = new ROI(dataset, coords[0]);
        List<Integer> oneD = roi.getOrigin();
        assertTrue(oneD.size() == 1);
        assertCoordinatesEqual(coords, oneD);

        // Check when sent (x, y) coordinates
        roi = new ROI(dataset, coords[0], coords[1]);
        List<Integer> twoD = roi.getOrigin();
        assertTrue(twoD.size() == 2);
        assertCoordinatesEqual(coords, twoD);

        // Check 3-D origin (x, y)
        roi = new ROI(dataset, coords[0], coords[1], coords[2]);
        List<Integer> threeD = roi.getOrigin();
        assertTrue(threeD.size() == 3);
        assertCoordinatesEqual(coords, threeD);

        // Check 4-D origin (x, y, z, t)
        roi = new ROI(dataset, coords[0], coords[1], coords[2], coords[3]);
        List<Integer> fourD = roi.getOrigin();
        assertTrue(fourD.size() == 4);
        assertCoordinatesEqual(coords, fourD);
    }


    private void assertCoordinatesEqual(int[] expected, List<Integer> origin) {
        for (int i=0; i<origin.size(); i++) {
            assertTrue(expected[i] == origin.get(i));
        }
    }

    @Test
    public void setSource() throws Exception {
        roi = new ROI(dataset, 0);
        String src = Long.toString(new Date().getTime());
        roi.setMetadata(src);
        assertEquals(src, roi.getMetadata());
    }

    @Test
    public void getDataset() throws Exception {
        double[] data = {
                1, 2, 3, 4, 5,
                5, 4, 0, 2, 1,
                1, 1, 1, 1, 1
        } ;
        dataset = new Dataset(data, data.length, 1);
        roi = new ROI(dataset, 0);
        Dataset returned = roi.getDataset();
        assertEquals(dataset.getWidth(), returned.getWidth());
        assertEquals(dataset.getHeight(), returned.getHeight());
        double[] actual = returned.getData();
        for (int i=0; i<data.length; i++) {
            assertEquals(data[i], actual[i], 0.05 * data[i]);
        }
    }

}