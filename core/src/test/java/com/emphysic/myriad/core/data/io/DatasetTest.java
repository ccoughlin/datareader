/*
 * com.emphysic.myriad.core.data.io.DatasetTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.io;

import com.emphysic.myriad.core.data.util.FileSniffer;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DatasetTest {
    Random random;
    Dataset dataset;

    @Before
    public void setUp() throws Exception {
        random = new Random();
    }

    public int rndLength() {
        return random.nextInt(5) + 1;
    }

    public double[] rndData(int w, int h) {
        double[] data = new double[w * h];
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextDouble() * 10;
        }
        return data;
    }

    @Test
    public void getData() throws Exception {
        int w = rndLength();
        int h = rndLength();
        double[] data = rndData(w, h);
        dataset = new Dataset(data, w, h);
        for (int i = 0; i < dataset.getWidth(); i++) {
            for (int j = 0; j < dataset.getHeight(); j++) {
                assertEquals(data[j * w + i], dataset.get(i, j), 0.05 * data[j * w + i]);
            }
        }
    }

    @Test
    public void setDataDataset() throws Exception {
        int w = rndLength();
        int h = rndLength();
        double[] data = rndData(w, h);
        Dataset orig = new Dataset(data, w, h);
        dataset = new Dataset(w, h);
        dataset.setData(orig);
        for (int i = 0; i < orig.getWidth(); i++) {
            for (int j = 0; j < orig.getHeight(); j++) {
                assertEquals(orig.get(i, j), dataset.get(i, j), 0.05 * orig.get(i, j));
            }
        }
    }

    @Test
    public void setDataArray() throws Exception {
        int w = rndLength();
        int h = rndLength();
        double[] data = rndData(w, h);
        dataset = new Dataset(w, h);
        dataset.setData(data, w, h);
        for (int i = 0; i < dataset.getWidth(); i++) {
            for (int j = 0; j < dataset.getHeight(); j++) {
                assertEquals(data[j * w + i], dataset.get(i, j), 0.05 * data[j * w + i]);
            }
        }
    }

    @Test
    public void get() throws Exception {
        int w = rndLength();
        int h = rndLength();
        double[] data = rndData(w, h);
        dataset = new Dataset(data, w, h);
        int x = random.nextInt(w);
        int y = random.nextInt(h);
        assertEquals(data[y * w + x], dataset.get(x, y), 0.05 * data[y * w + x]);
    }

    @Test
    public void set() throws Exception {
        int w = rndLength();
        int h = rndLength();
        double[] data = rndData(w, h);
        dataset = new Dataset(data, w, h);
        int x = random.nextInt(w);
        int y = random.nextInt(h);
        double newValue = random.nextDouble() * 100;
        dataset.set(x, y, newValue);
        assertEquals(newValue, dataset.get(x, y), 0.05 * newValue);
    }

    @Test
    public void getWidthHeightSize() throws Exception {
        int w = rndLength();
        int h = rndLength();
        double[] data = rndData(w, h);
        dataset = new Dataset(data, w, h);
        assertEquals(w, dataset.getWidth());
        assertEquals(h, dataset.getHeight());
        assertEquals(w * h, dataset.getSize());
    }

    @Test
    public void write() throws Exception {
        int w = rndLength();
        int h = rndLength();
        double[] data = rndData(w, h);
        dataset = new Dataset(data, w, h);
        File out = File.createTempFile("tmpdataset", null);
        dataset.write(out);
        FileSniffer fileSniffer = new FileSniffer();
        TextDataset tds = fileSniffer.readAsText(out);
        tds.read();
        assertNotNull(tds);
        assertEquals(tds.getWidth(), dataset.getWidth());
        assertEquals(tds.getHeight(), dataset.getHeight());
        for (int i = 0; i < tds.getWidth(); i++) {
            for (int j = 0; j < tds.getHeight(); j++) {
                assertEquals(dataset.get(i, j), tds.get(i, j), 0.05 * dataset.get(i, j));
            }
        }
    }

}