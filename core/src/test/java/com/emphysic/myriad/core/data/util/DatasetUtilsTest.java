/*
 * com.emphysic.myriad.core.data.util.DatasetUtilsTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * DatasetUtilsTest - test the DatasetUtils class.
 * Created by ccoughlin on 8/26/16.
 */
public class DatasetUtilsTest {

    @Test
    public void safeIdx() throws Exception {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int w = random.nextInt(10);
            for (int widx=-w; widx<2*w; widx++) {
                int safewidx = DatasetUtils.safeIdx(widx, w);
                assertTrue(safewidx >= 0 && safewidx < w);
            }
        }
        // Confirm returning -1 for 0-length dimension
        assertEquals(-1, DatasetUtils.safeIdx(0, 0));
    }

}