/*
 * com.emphysic.myriad.core.data.roi.RESTROIFinderTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.core.data.roi;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * RESTROIFinderTest - tests the REST API ROI finder implementation.
 * Created by ccoughlin on 10/17/16.
 */
public class RESTROIFinderTest {
    private RESTROIFinder finder;

    @Before
    public void setUp() throws Exception {
        finder = new RESTROIFinder("127.0.0.1:1080/mock/isroi");
    }

    @After
    public void tearDown() throws Exception {
        finder.close();
    }

    @Test
    public void serialize_with_creds() throws Exception {
        File out = File.createTempFile("tmp_rst", "dat");
        finder.setSaveCredentials(true);
        finder.save(out);
        RESTROIFinder read = new RESTROIFinder();
        read.load(out);
        assertModelsEqual(finder, read);

        RESTROIFinder read2 = (RESTROIFinder) ROIFinder.fromFile(out, RESTROIFinder.class);
        assertModelsEqual(finder, read2);
    }

    @Test
    public void serialize_no_creds() throws Exception {
        File out = File.createTempFile("tmp_rst", "dat");
        finder.save(out);
        RESTROIFinder read = new RESTROIFinder();
        read.load(out);
        assertModelsEqual(finder, read);

        RESTROIFinder read2 = (RESTROIFinder) ROIFinder.fromFile(out, RESTROIFinder.class);
        assertModelsEqual(finder, read2);
    }

    private void assertModelsEqual(RESTROIFinder expected, RESTROIFinder actual) {
        assertEquals(expected.getUrl(), actual.getUrl());
        assertEquals(expected.getConnectionTimeout(), actual.getConnectionTimeout());
        assertEquals(expected.getSocketTimeout(), actual.getSocketTimeout());
        assertEquals(expected.saveCredentials(), actual.saveCredentials());
        if (expected.saveCredentials()) {
            assertEquals(expected.getUsername(), actual.getUsername());
            assertEquals(expected.getPassword(), actual.getPassword());
        } else {
            assertNotEquals(expected.getUsername(), actual.getUsername());
            assertNotEquals(expected.getPassword(), actual.getPassword());
        }
    }

    @Test
    public void encode_decode() throws Exception {
        double[] testArray = {
                -1.25, -1.15, -1.05, -0.95, -0.85,
                -0.75, -0.65, -0.55, -0.45, -0.35,
                -0.25, -0.15, -0.05,  0.05,  0.15,
                 0.25,  0.35,  0.45,  0.55,  0.65,
                 0.75,  0.85,  0.95,  1.05,  1.15
        };
        String encoded = finder.encodeData(testArray);
        double[] decoded = finder.decodeData(encoded);
        for (int i=0; i<testArray.length; i++) {
            assertEquals(testArray[i], decoded[i], 0.05 * testArray[i]);
        }
    }

}