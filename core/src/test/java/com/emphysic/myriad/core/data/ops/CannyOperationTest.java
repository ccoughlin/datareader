/*
 * com.emphysic.myriad.core.data.ops.CannyOperationTest
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.math.GradientVector;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * CannyOperationTest - tests the CannyOperation edge detector.
 * Created by ccoughlin on 8/26/16.
 */
public class CannyOperationTest {
    private double[] data = {
            0,   0,   0,   0,   0,   0,   0,
            0,   0,   0, 100,   0,   0,   0,
            0,   0, 100, 100, 100,   0,   0,
            0, 100, 100, 100, 100, 100,   0,
            0,   0, 100, 100, 100,   0,   0,
            0,   0,   0, 100,   0,   0,   0,
            0,   0,   0,   0,   0,   0,   0
    };
    private Dataset orig;
    private CannyOperation canny;

    @Before
    public void setUp() throws Exception {
        orig = new Dataset(data, 7, 7);
        canny = new CannyOperation(1);
    }

    @Test
    public void run() throws Exception {
        double[] edged = {
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
        };
        Dataset result = canny.run(orig);
        assertEquals(orig.getWidth(), result.getWidth());
        assertEquals(orig.getHeight(), result.getHeight());
        assertArraysEqual(edged, result.getData());
    }

    @Test
    public void hysteresis() throws Exception {
        double[] thresholded = {
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0,
                0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0,
                0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
        };
        Dataset result = canny.hysteresis(orig);
        assertEquals(orig.getWidth(), result.getWidth());
        assertEquals(orig.getHeight(), result.getHeight());
        assertArraysEqual(thresholded, result.getData());
    }

    @Test
    public void roundToCardinal() throws Exception {
        double[] to0 = {0, 11, 22.4, 157.5, 165, 180, 202.4, 337.5, 359, -11, -22.4, -180};
        assertCardinality(to0);
        double[] to45 = {45, 22.5, 30, 67.4, 202.5, 225, -135, 247.4};
        assertCardinality(to45);
        double[] to135 = {135, 112.5, 157.4, 292.5, 337.4, 300, -45};
        assertCardinality(to135);
        double[] to90 = {90, 67.5, 112.4, 247.5, 292.4, 270, -90, -270};
        assertCardinality(to90);
    }

    /**
     * Utility method for asserting two arrays are (nearly) equal.
     * @param expected expected results
     * @param actual actual results
     */
    private void assertArraysEqual(double[] expected, double[] actual) {
        assertEquals(expected.length, actual.length);
        for (int i=0; i<expected.length; i++) {
            assertEquals(expected[i], actual[i], 0.05 * expected[i]);
        }
    }

    /**
     * Utility method for asserting cardinality.  Array argument is a list of angles in degrees that should be rounded
     * to the cardinal angle element 0 i.e. roundToCardinal(Ai) = A0.
     * @param arr array to check
     */
    private void assertCardinality(double[] arr) {
        double expected = arr[0];
        for (int i=0; i<arr.length; i++) {
            double rad = arr[i] * Math.PI / 180;
            assertEquals(expected, canny.roundToCardinal(rad), 0.05 * expected);
        }
    }

    @Test
    public void gradMax() throws Exception {
        double[] grads = {
                4.9E-324,   4.9E-324,   4.9E-324,   4.9E-324, 4.9E-324, 4.9E-324, 4.9E-324,
                4.9E-324,   4.9E-324,   4.9E-324, 100.0,      4.9E-324, 4.9E-324, 4.9E-324,
                4.9E-324,   4.9E-324, 100.0,      100.0,    100.0,      4.9E-324, 4.9E-324,
                4.9E-324, 100.0,      100.0,      100.0,    100.0,    100.0,      4.9E-324,
                4.9E-324,   4.9E-324, 100.0,      100.0,    100.0,      4.9E-324, 4.9E-324,
                4.9E-324,   4.9E-324,   4.9E-324, 100.0,      4.9E-324, 4.9E-324, 4.9E-324,
                4.9E-324,   4.9E-324,   4.9E-324,   4.9E-324, 4.9E-324, 4.9E-324, 4.9E-324,
        };
        GradientVector gv = new GradientVector(orig);
        Dataset result = new Dataset(orig.getWidth(), orig.getHeight());
        for (int i=0; i<result.getWidth(); i++) {
            for (int j=0; j<result.getHeight(); j++) {
                result.set(i, j, canny.gradMax(orig, i, j, gv.getAngles().get(i, j)));
            }
        }
        assertEquals(orig.getWidth(), result.getWidth());
        assertEquals(orig.getHeight(), result.getHeight());
        assertArraysEqual(grads, result.getData());
    }

    @Test
    public void serialize() throws Exception {
        Random random = new Random();
        int blurRadius = random.nextInt(3);
        CannyOperation original = new CannyOperation(blurRadius);
        original.setAutoThreshold(CannyOperation.AutoThreshold.MEDIAN);
        File out = File.createTempFile("tmpcanny", "dat");
        original.save(out);
        CannyOperation read = new CannyOperation();
        read.load(out);
        assertEquals(original.getAutoThreshold(), read.getAutoThreshold());
        assertEquals(original.getSigmaThreshold(), read.getSigmaThreshold(), 0.05 * original.getSigmaThreshold());
        assertEquals(original.getUpperThreshold(), read.getUpperThreshold(), 0.05 * original.getUpperThreshold());
        assertEquals(original.getLowerThreshold(), read.getLowerThreshold(), 0.05 * original.getLowerThreshold());
        assertArraysEqual(original.run(orig).getData(), read.run(orig).getData());
    }
}