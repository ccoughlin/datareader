/*
 * com.emphysic.myriad.core.data.ops.ScalingOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.math.Stats;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ScalingOperationTest {

    private Dataset rnd;
    private Random random = new Random();
    private ScalingOperation scaler = new ScalingOperation();

    @Before
    public void setUp() throws Exception {
        rnd = genData(5, 5);
    }

    public Dataset genData(int w, int h) {
        double[] data = new double[w * h];
        for (int i = 0; i < data.length; i++) {
            int flip = random.nextInt(2);
            double val = random.nextDouble() * 10;
            if (flip == 0) {
                val *= -1;
            }
            data[i] = val;
        }
        return new Dataset(data, w, h);
    }

    public void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), actual.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

    @Test
    public void run() throws Exception {
        Dataset expected = new Dataset(rnd.getWidth(), rnd.getHeight());
        double mean = Stats.mean(rnd);
        double std = Stats.stddev(rnd);
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                expected.set(i, j, (rnd.get(i, j) - mean) / std);
            }
        }
        assertDatasetsEqual(expected, scaler.run(rnd));
    }

    @Test
    public void getMean() throws Exception {
        scaler.run(rnd);
        double mean = Stats.mean(rnd);
        assertEquals(mean, scaler.getMean(), 0.05 * mean);
    }

    @Test
    public void setMean() throws Exception {
        double rndMean = Stats.mean(rnd);
        assertNotEquals(rndMean, scaler.getMean(), 0.05 * rndMean);
        scaler.setMean(rndMean);
        assertEquals(rndMean, scaler.getMean(), 0.05 * rndMean);
    }

    @Test
    public void getStd() throws Exception {
        scaler.run(rnd);
        double std = Stats.stddev(rnd);
        assertEquals(std, scaler.getStd(), 0.05 * std);
    }

    @Test
    public void setStd() throws Exception {
        double rndstd = Stats.mean(rnd);
        assertNotEquals(rndstd, scaler.getStd(), 0.05 * rndstd);
        scaler.setStd(rndstd);
        assertEquals(rndstd, scaler.getStd(), 0.05 * rndstd);
    }

    @Test
    public void serialize() throws Exception {
        File out = File.createTempFile("tmpscaler", "dat");
        scaler.save(out);
        ScalingOperation read = new ScalingOperation();
        read.load(out);
        assertEquals(scaler.getStd(), read.getStd(), 0.05 * scaler.getStd());
        assertEquals(scaler.getMean(), read.getMean(), 0.05 * scaler.getMean());
    }

}