/*
 * com.emphysic.myriad.core.data.io.TextDatasetTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.io;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;

@Slf4j
public class TextDatasetTest {

    /**
     * Picks a delimiter at random
     *
     * @return chosen delimiter
     */
    public String getDelimiter() {
        String[] delims = {",", " ", "\t"};
        return delims[new Random().nextInt(delims.length)];
    }

    /**
     * Generates a random dataset
     *
     * @param numCols number of elements per row
     * @param numRows number of rows
     * @return dataset
     */
    public List<List<Double>> genData(int numCols, int numRows) {
        ArrayList<List<Double>> dataArray = new ArrayList<>();
        for (int row = 0; row < numRows; row++) {
            ArrayList<Double> newRow = new ArrayList<>(numCols);
            for (int j = 0; j < numCols; j++) {
                newRow.add(Math.asin(Math.random()) * 1000);
            }
            dataArray.add(newRow);
        }
        return dataArray;
    }

    /**
     * Creates a temporary delimited text file.  File is automatically deleted when the VM exits.
     *
     * @param data  data to write to the file
     * @param delim delimiter to use to separate elements in the file
     * @return instance of the created file
     */
    public File genTempDataFile(List<List<Double>> data, String delim) {
        try {
            File tempFile = File.createTempFile("sample", ".tsv");
            tempFile.deleteOnExit();
            FileWriter fileWriter = new FileWriter(tempFile);
            for (List<Double> row : data) {
                StringJoiner joiner = new StringJoiner(delim);
                for (Double el : row) {
                    joiner.add(el.toString());
                }
                fileWriter.write(joiner.toString() + "\n");
            }
            fileWriter.close();
            return tempFile;
        } catch (IOException ioException) {
            log.error("Encountered an error creating test file: " + ioException.getLocalizedMessage());
        }
        return null;
    }

    @Test
    public void getData() throws Exception {
        int w = 10;
        int h = 13;
        List<List<Double>> expectedData = genData(w, h);
        String delim = getDelimiter();
        File separatedVariableData = genTempDataFile(expectedData, delim);
        TextDataset tds = new TextDataset(separatedVariableData, delim, w, h);
        tds.read();
        assertEquals(expectedData.size() * expectedData.get(0).size(), tds.getSize());
        for (int i=0; i<tds.getWidth(); i++) {
            for (int j=0; j<tds.getHeight(); j++) {
                assertEquals(expectedData.get(j).get(i), tds.get(i, j), expectedData.get(j).get(i) * 0.05);
            }
        }
    }

    @Test
    public void readAsString() throws Exception {
        int w = 123;
        int h = 8;
        List<List<Double>> data = genData(w, h);
        String delim = getDelimiter();
        File tempDataFile = genTempDataFile(data, delim);
        TextDataset tds = new TextDataset(tempDataFile, delim, w, h);
        tds.read();
        List<List<String>> returnedData = tds.getStringData();
        Scanner scanner = new Scanner(tempDataFile);
        int numLines = 0;
        while (scanner.hasNextLine()) {
            String expectedLine = scanner.nextLine();
            StringJoiner deTokenized = new StringJoiner(delim);
            returnedData.get(numLines).forEach(deTokenized::add);
            assertEquals(expectedLine, deTokenized.toString());
            numLines++;
        }
        assertEquals(numLines, returnedData.size());
    }
}