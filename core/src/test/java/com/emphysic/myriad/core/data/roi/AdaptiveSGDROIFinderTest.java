/*
 * com.emphysic.myriad.core.data.roi.AdaptiveSGDROIFinderTest
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import org.apache.mahout.classifier.sgd.AdaptiveLogisticRegression;
import org.apache.mahout.classifier.sgd.UniformPrior;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.Vector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * AdaptiveSGDROIFinderTest - tests the AdaptiveSGDROIFinder.
 * Created by ccoughlin on 8/26/16.
 */
public class AdaptiveSGDROIFinderTest {
    private double[][] X = {{-1.0, -1.0}, {-2.0, -1.0}, {1.0, 1.0}, {2.0, 1.0}};
    private int[] y = {0, 0, 1, 1};
    private AdaptiveSGDROIFinder model;

    @Before
    public void setUp() throws Exception {
        model = new AdaptiveSGDROIFinder();
    }

    private void train() throws Exception {
        model.train(X, y);
        model.close();
    }

    @Test
    public void testConstructor() throws Exception {
        int numCats = 3;
        int poolSize = 4;
        int cores = 20;
        AdaptiveSGDROIFinder mdl = new AdaptiveSGDROIFinder(numCats, new UniformPrior(), cores, poolSize);
        assertEquals(UniformPrior.class, mdl.getPriorFunction().getClass());
        assertEquals(numCats, mdl.getNumCategories());
        assertEquals(poolSize, mdl.getPoolSize());
        assertEquals(cores, mdl.getThreadCount());
    }

    @Test
    public void predict() throws Exception {
        train();
        for (int i = 0; i < X.length; i++) {
            double prediction = model.predict(X[i]);
            assertEquals(y[i], prediction, 0.05 * y[i]);
        }
    }

    @Test
    public void predict1() throws Exception {
        train();
        for (int i=0; i<X.length; i++) {
            Dataset d = new Dataset(X[i], X[i].length, 1);
            double prediction = model.predict(d);
            assertEquals(y[i], prediction, 0.05 * y[i]);
        }
    }

    @Test
    public void isROI() throws Exception {
        train();
        for (int i = 0; i < X.length; i++) {
            boolean prediction = model.isROI(X[i]);
            assertEquals(y[i] == (int)model.positiveClass(), prediction);
        }
    }

    @Test
    public void isROI1() throws Exception {
        train();
        for (int i = 0; i < X.length; i++) {
            Dataset d = new Dataset(X[i], X[i].length, 1);
            boolean prediction = model.isROI(d);
            assertEquals(y[i] == (int)model.positiveClass(), prediction);
        }
    }

    @Test
    public void confidenceThreshold() throws Exception {
        train();
        // Verify throwing exception for bad confidence levels
        double[] badThresh = {-1, 1.01, -0.1, 75};
        for (double bad : badThresh) {
            try {
                model.setConfidenceThreshold(bad);
                Assert.fail("Expected exception to be thrown");
            } catch (Exception e) {
                assertTrue(e instanceof IllegalArgumentException);
            }
        }
        // Verify reporting ROI iff confidence threshold is met
        double[] confThresh = {0, 0.25, 0.5, 0.75, 1};
        for (double t : confThresh) {
            model.setConfidenceThreshold(t);
            assertEquals(t, model.getConfidenceThreshold(), 0.05 * t);
            for (double[] aX : X) {
                Vector scores = model.classify(new DenseVector(aX));
                double maxConf = scores.maxValue();
                int idx = scores.maxValueIndex();
                if (idx > 0 && maxConf < t) {
                    idx = 0;
                }
                assertEquals(idx == model.positiveClass(), model.isROI(aX));
            }
        }
    }

    @Test
    public void serialize() throws Exception {
        File out = File.createTempFile("tmp", "dat");
        train();
        Random random = new Random();
        model.setConfidenceThreshold(random.nextDouble());
        model.save(out);
        AdaptiveSGDROIFinder read = new AdaptiveSGDROIFinder();
        read.load(out);
        assertModelsEqual(model, read);

        AdaptiveSGDROIFinder read2 = (AdaptiveSGDROIFinder) ROIFinder.fromFile(out, AdaptiveSGDROIFinder.class);
        assertModelsEqual(model, read2);
    }

    /**
     * Ensure that two AdaptiveSGD models are equivalent i.e. make the same predictions and have the same basic
     * settings.
     * @param expected expected model
     * @param actual actual model
     */
    private void assertModelsEqual(AdaptiveSGDROIFinder expected, AdaptiveSGDROIFinder actual) {
        for (int i = 0; i < X.length; i++) {
            assertEquals(expected.predict(X[i]), actual.predict(X[i]), 0.05 * y[i]);
        }
        assertEquals(expected.getConfidenceThreshold(), actual.getConfidenceThreshold(),
                0.05 * expected.getConfidenceThreshold());
        assertEquals(expected.getNumCategories(), actual.getNumCategories());
        assertEquals(expected.getNumFeatures(), actual.getNumFeatures());
        assertEquals(expected.getPoolSize(), actual.getPoolSize());
        assertEquals(expected.getThreadCount(), actual.getThreadCount());
        assertEquals(expected.positiveClass(), actual.positiveClass(), expected.positiveClass() * 0.05);
        assertEquals(expected.negativeClass(), actual.negativeClass(), expected.negativeClass() * 0.05);
        AdaptiveLogisticRegression expectedModel = expected.getModel();
        AdaptiveLogisticRegression actualModel = actual.getModel();
        assertEquals(expectedModel.getMaxInterval(), actualModel.getMaxInterval());
        assertEquals(expectedModel.getMinInterval(), actualModel.getMinInterval());
        assertEquals(expectedModel.getNumCategories(), actualModel.getNumCategories());
        assertEquals(expectedModel.getNumFeatures(), actualModel.getNumFeatures());
        assertEquals(expectedModel.getRecord(), actualModel.getRecord());
    }

}