/*
 * com.emphysic.myriad.core.data.ops.AbsoluteValueOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AbsoluteValueOperationTest {
    @Test
    public void run() throws Exception {
        AbsoluteValueOperation avo = new AbsoluteValueOperation();
        assertNull(avo.run(null));
        int width = 3;
        int height = 5;
        Dataset dataset = new Dataset(genData(width * height), width, height);
        Dataset abs = avo.run(dataset);
        assertDatasetsEqual(dataset, abs);
    }

    @Test
    public void serialize() throws Exception {
        AbsoluteValueOperation orig = new AbsoluteValueOperation();
        int width = 2;
        int height = 2;
        Dataset dataset = new Dataset(genData(width * height), width, height);
        Dataset abs = orig.run(dataset);
        File out = File.createTempFile("tmpabs", "dat");
        orig.save(out);
        AbsoluteValueOperation reread = new AbsoluteValueOperation();
        reread.load(out);
        Dataset results = reread.run(dataset);
        assertDatasetsEqual(abs, results);
    }

    /**
     * Convenience method for verifying two datasets are equivalent
     * @param expected expected dataset
     * @param actual actual dataset
     */
    public void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getWidth(); j++) {
                double expectedValue = Math.abs(expected.get(i, j));
                assertEquals(expectedValue, actual.get(i, j), 0.05 * expectedValue);
            }
        }
    }

    /**
     * Convenience method for generating random data
     * @param n number of elements in the array
     * @return array of n elements of random values
     */
    private double[] genData(int n) {
        Random random = new Random();
        double[] d = new double[n];
        for (int i = 0; i < d.length; i++) {
            int roll = random.nextInt(10);
            d[i] = random.nextInt(Integer.MAX_VALUE);
            if (roll < 5) {
                d[i] *= -1;
            }
        }
        return d;
    }
}