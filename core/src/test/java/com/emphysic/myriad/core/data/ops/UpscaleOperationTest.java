/*
 * com.emphysic.myriad.core.data.ops.UpscaleOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class UpscaleOperationTest {
    double[] origData = new double[]{
            0, 0, 0, 0,
            0, 1, 1, 0,
            0, 0, 0, 0,
            0, 2, 2, 0,
            0, 0, 0, 0
    };

    double[] paddedUpscaledData = new double[]{
            0.25, 1.25, 2.75, 3.75, 3.75, 2.75, 1.25, 0.25,
            1.25, 6.0, 12.75, 17.0, 16.75, 12.0, 5.25, 1.0,
            2.25, 11.25, 23.25, 30.25, 29.25, 20.25, 8.25, 1.25,
            2.25, 11.0, 21.25, 27.0, 25.25, 17.0, 6.25, 1.0,
            1.75, 7.75, 15.25, 19.25, 18.25, 12.25, 4.75, 0.75,
            2.75, 13.0, 27.25, 36.0, 35.25, 25.0, 10.75, 2.0,
            4.5, 22.5, 46.5, 60.5, 58.5, 40.5, 16.5, 2.5,
            4.5, 22.0, 42.5, 54.0, 50.5, 34.0, 12.5, 2.0,
            2.5, 10.5, 19.5, 23.5, 21.5, 13.5, 4.5, 0.5,
            0.5, 2.0, 3.5, 4.0, 3.5, 2.0, 0.5, 0.0
    };

    @Test
    public void run() throws Exception {
        Dataset input = new Dataset(origData, 4, 5);
        Dataset expected = new Dataset(paddedUpscaledData, 8, 10);
        UpscaleOperation upo = new UpscaleOperation();
        Dataset output = upo.run(input);
        assertDatasetsEqual(expected, output);
    }

    @Test
    public void serialize() throws Exception {
        UpscaleOperation upo = new UpscaleOperation(new PrewittOperation());
        File out = File.createTempFile("tmpupo", "dat");
        upo.save(out);
        UpscaleOperation read = new UpscaleOperation();
        read.load(out);
        Dataset input = new Dataset(origData, 4, 5);
        assertDatasetsEqual(upo.run(input), read.run(input));
    }

    /**
     * Convenience method for verifying two Datasets are equivalent
     * @param expected expected results
     * @param actual actual results
     */
    public void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), actual.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }
}