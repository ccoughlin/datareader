/*
 * com.emphysic.myriad.core.data.ops.DifferenceOfGaussiansOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DifferenceOfGaussiansOperationTest {
    double[] X;
    double[] expectedDog;

    @Before
    public void setUp() throws Exception {
        X = new double[]{
                0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 50.0, 50.0, 50.0, 0.0,
                0.0, 50.0, 50.0, 50.0, 0.0,
                0.0, 50.0, 50.0, 50.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0
        };
        expectedDog = new double[]{
                1.1422727023319528, 3.824531138545943, 5.744970096021936, 3.824531138545943, 1.142272702331951,
                3.824531138545943, 7.12087352537722, 9.452835116598067, 7.120873525377217, 3.824531138545945,
                5.744970096021937, 9.45283511659807, 12.059145130315489, 9.452835116598067, 5.744970096021936,
                3.824531138545943, 7.12087352537722, 9.452835116598067, 7.120873525377217, 3.8245311385459413,
                1.1422727023319563, 3.824531138545943, 5.744970096021936, 3.8245311385459395, 1.1422727023319563
        };
    }

    @Test
    public void run() throws Exception {
        Dataset input = new Dataset(X, 5, 5);
        Dataset dogged = new DifferenceOfGaussiansOperation(1, 2).run(input);
        Dataset expected = new Dataset(expectedDog, 5, 5);
        assertEquals(expected.getWidth(), dogged.getWidth());
        assertEquals(expected.getHeight(), dogged.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), dogged.get(i, j), expected.get(i, j));
            }
        }
    }
}