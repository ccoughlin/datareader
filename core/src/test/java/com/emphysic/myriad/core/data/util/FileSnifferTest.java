/*
 * com.emphysic.myriad.core.data.util.FileSnifferTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

import com.emphysic.myriad.core.data.io.DicomDataset;
import com.emphysic.myriad.core.data.io.IODataset;
import com.emphysic.myriad.core.data.io.ImageDataset;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.Random;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class FileSnifferTest {

    private File jpgFile;
    private File tifFile;
    private File dicomFile;
    private File csvFile;
    private File txtFile;
    private FileSniffer fileSniffer;

    @Before
    public void setUp() throws Exception {
        URL jpgURL = Thread.currentThread().getContextClassLoader().getResource("data/io/test.jpg");
        jpgFile = new File(jpgURL.getPath());
        URL tiffURL = Thread.currentThread().getContextClassLoader().getResource("data/io/multipage_tiff_example.tif");
        tifFile = new File(tiffURL.getPath());
        URL dicomURL = Thread.currentThread().getContextClassLoader().getResource("data/io/dicondeEnhancedCtExampleImageE2767-13Typical.dcm");
        dicomFile = new File(dicomURL.getPath());
        URL csvURL = Thread.currentThread().getContextClassLoader().getResource("data/io/140.csv");
        csvFile = new File(csvURL.getPath());
        URL txtURL = Thread.currentThread().getContextClassLoader().getResource("data/io/ascan.txt");
        txtFile = new File(txtURL.getPath());
    }

    @Test
    public void sniff() throws Exception {
        fileSniffer = new FileSniffer(true);
        Random random = new Random();
        File[] samplesFiles = {jpgFile, tifFile, dicomFile, csvFile, txtFile};
        File inputFile = samplesFiles[random.nextInt(samplesFiles.length)];
        IODataset d = fileSniffer.sniff(inputFile);
        assertNotEquals(d, null);
        d.read();
        assertNotEquals(d.getWidth(), 0);
        assertNotEquals(d.getHeight(), 0);
    }

    @Test
    public void readAsText() throws Exception {
        fileSniffer = new FileSniffer();
        File[] textFiles = {csvFile, txtFile};
        File[] otherFiles = {jpgFile, tifFile, dicomFile};
        for (File textFile : textFiles) {
            IODataset tds = fileSniffer.readAsText(textFile);
            assertNotEquals(tds, null);
        }
        for (File other : otherFiles) {
            IODataset n = fileSniffer.readAsText(other);
            assertNull(n);
        }
    }

    @Test
    public void readAsImage() throws Exception {
        fileSniffer = new FileSniffer();
        File[] imageFiles = {jpgFile, tifFile};
        File[] otherFiles = {csvFile, txtFile, dicomFile};
        for (File img : imageFiles) {
            ImageDataset i = fileSniffer.readAsImage(img);
            assertNotEquals(i, null);
        }
        for (File other : otherFiles) {
            ImageDataset i = fileSniffer.readAsImage(other);
            assertNull(i);
        }
    }

    @Test
    public void readAsDICOM() throws Exception {
        fileSniffer = new FileSniffer();
        File[] others = {csvFile, tifFile, jpgFile, txtFile};
        for (File other : others) {
            DicomDataset d = fileSniffer.readAsDICOM(other);
            assertNull(d);
        }
        DicomDataset dds = fileSniffer.readAsDICOM(dicomFile);
        assertNotEquals(dds, null);
    }

    @Test
    public void read() throws Exception {
        Random random = new Random();
        File[] samplesFiles = {jpgFile, tifFile, dicomFile, csvFile, txtFile};
        File inputFile = samplesFiles[random.nextInt(samplesFiles.length)];
        IODataset d = FileSniffer.read(inputFile, true);
        assertNotEquals(d, null);
        fileSniffer = new FileSniffer();
        IODataset direct = fileSniffer.sniff(inputFile);
        direct.read();
        assertEquals(direct.getWidth(), d.getWidth());
        assertEquals(direct.getHeight(), d.getHeight());
        assertTrue(Arrays.equals(direct.getData(), d.getData()));
    }

    @Test
    public void isText() throws Exception {
        File[] textFiles = {csvFile, txtFile};
        File[] otherFiles = {jpgFile, tifFile, dicomFile};
        for (File textFile : textFiles) {
            assertTrue(FileSniffer.isText(textFile));
        }
        for (File other : otherFiles) {
            assertFalse(FileSniffer.isText(other));
        }
    }
}