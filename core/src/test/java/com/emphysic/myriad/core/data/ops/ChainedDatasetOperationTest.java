/*
 * com.emphysic.myriad.core.data.ops.ChainedDatasetOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.FileSniffer;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ChainedDatasetOperationTest {

    private PowerOfTwoOperation pto = new PowerOfTwoOperation();
    private BoxBlur blur = new BoxBlur(5);
    private NormalizeSignalOperation nso = new NormalizeSignalOperation();
    private BinarizeOperation binarize = new BinarizeOperation(0.5);
    private ChainedDatasetOperation chain = new ChainedDatasetOperation();
    List<DatasetOperation> ops = new ArrayList<>();
    Dataset dataset;
    File singleImageFile;

    @Before
    public void setUp() throws Exception {
        URL testJpgURL = Thread.currentThread().getContextClassLoader().getResource("data/io/test.jpg");
        singleImageFile = new File(testJpgURL.getPath());
        dataset = FileSniffer.read(singleImageFile, true);
        ops.add(pto);
        ops.add(blur);
        ops.add(nso);
        ops.add(binarize);
    }

    public void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), actual.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

    @Test
    public void run() throws Exception {
        chain.withOp(pto).withOp(blur).withOp(nso).withOp(binarize);
        Dataset result = new Dataset(dataset);
        for (DatasetOperation op : ops) {
            result = op.run(result);
        }
        assertDatasetsEqual(result, chain.run(dataset));
    }

    @Test
    public void withOp() throws Exception {
        Dataset result = new Dataset(dataset);
        for (DatasetOperation op : ops) {
            chain.withOp(op);
            result = op.run(result);
            assertDatasetsEqual(result, chain.run(dataset));
        }
    }

    @Test
    public void withOp1() throws Exception {
        // Verify adding as first operation if no operations present
        chain.withOp(1, pto);
        assertDatasetsEqual(pto.run(dataset), chain.run(dataset));
        chain.withOp(0, blur); // shift pto right, insert blur as first op
        assertDatasetsEqual(pto.run(blur.run(dataset)), chain.run(dataset));
        chain.withOp(1, nso); // shift pto right, insert nso as second op
        assertDatasetsEqual(pto.run(nso.run(blur.run(dataset))), chain.run(dataset));
    }

    @Test
    public void serialize() throws Exception {
        chain.withOp(pto).withOp(blur).withOp(nso).withOp(binarize);
        File out = File.createTempFile("tmpchain", "dat");
        chain.save(out);
        ChainedDatasetOperation read = new ChainedDatasetOperation();
        read.load(out);
        assertDatasetsEqual(chain.run(dataset), read.run(dataset));
    }
}