/*
 * com.emphysic.myriad.core.data.roi.SVMROIFinderTest
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * SVMROIFinderTest - tests the SVMROIFinder class.
 * Created by ccoughlin on 5/13/17.
 */
public class SVMROIFinderTest {
    private double[][] X = {{-1.0, -1.0}, {-2.0, -1.0}, {1.0, 1.0}, {2.0, 1.0}};
    private int[] y = {0, 0, 1, 1};
    private SVMROIFinder model;

    @Before
    public void setUp() throws Exception {
        model = new SVMROIFinder(0.123);
        model.train(X, y);
    }

    @Test
    public void predict() throws Exception {
        for (int i = 0; i < X.length; i++) {
            double prediction = model.predict(X[i]);
            double expected = y[i];
            assertEquals(expected, prediction, 0.05 * expected);
        }
    }

    @Test
    public void predict1() throws Exception {
        for (int i=0; i<X.length; i++) {
            Dataset d = new Dataset(X[i], X[i].length, 1);
            double prediction = model.predict(d);
            assertEquals(y[i], prediction, 0.05 * y[i]);
        }
    }

    @Test
    public void isROI() throws Exception {
        for (int i = 0; i < X.length; i++) {
            boolean prediction = model.isROI(X[i]);
            assertEquals(y[i] == (int)model.positiveClass(), prediction);
        }
    }

    @Test
    public void isROI1() throws Exception {
        for (int i = 0; i < X.length; i++) {
            Dataset d = new Dataset(X[i], X[i].length, 1);
            boolean prediction = model.isROI(d);
            assertEquals(y[i] == (int)model.positiveClass(), prediction);
        }
    }

    @Test
    public void serialize() throws Exception {
        File out = File.createTempFile("tmp_svm", "dat");
        model.save(out);
        SVMROIFinder read = new SVMROIFinder();
        read.load(out);
        assertModelsEqual(model, read);

        SVMROIFinder read2 = (SVMROIFinder) ROIFinder.fromFile(out, SVMROIFinder.class);
        assertModelsEqual(model, read2);
    }

    /**
     * Verify that two SVM models are "equal" i.e. make the same predictions and have the same basic
     * settings.
     * @param expected expected model
     * @param actual actual model
     */
    public void assertModelsEqual(SVMROIFinder expected, SVMROIFinder actual) {
        assertEquals(expected.getNumFeatures(), actual.getNumFeatures());
        assertEquals(expected.positiveClass(), actual.positiveClass(), expected.positiveClass() * 0.05);
        assertEquals(expected.negativeClass(), actual.negativeClass(), expected.negativeClass() * 0.05);
        assertEquals(expected.getC(), actual.getC(), expected.getC() * 0.05);
        assertEquals(expected.getCp(), actual.getCp(), expected.getCp() * 0.05);
        assertEquals(expected.getCn(), actual.getCn(), expected.getCn() * 0.05);
        for (int i = 0; i < X.length; i++) {
            assertEquals(expected.predict(X[i]), actual.predict(X[i]), 0.05 * y[i]);
        }
    }
}