/*
 * com.emphysic.myriad.core.data.roi.ROIBundleTest
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.*;
import com.emphysic.myriad.core.ml.CrossValidation;
import com.emphysic.myriad.core.ml.MLDataCompiler;
import com.emphysic.myriad.core.ml.MonteCarloCV;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Tests the ROIBundle
 * Created by ccoughlin on 4/16/17.
 */
public class ROIBundleTest {

    static DatasetOperation[] ops = {
            new AbsoluteValueOperation(),
            new BoxBlur(3),
            new BinarizeOperation(0.5),
            new CannyOperation(new BoxBlur(5), new PrewittOperation()),
            new ConvolutionOperation(),
            new DifferenceOfGaussiansOperation(3, 4),
            new GaussianBlur(2),
            new NormalizeSignalOperation(),
            new PrewittOperation(),
            new SobelOperation(),
    };
    static DatasetOperation preproc;

    static MLROIFinder[] finders = {
            new AdaptiveSGDROIFinder(),
            new PassiveAggressiveROIFinder(),
            new SGDROIFinder()
    };
    static MLROIFinder finder;

    static ROIBundle bundle;

    static int opidx;
    static int fidx;

    static MLDataCompiler mldc;
    static MonteCarloCV mcv;

    @BeforeClass
    public static void setUpClass() throws Exception {
        Random random = new Random();
        opidx = random.nextInt(ops.length);
        fidx = random.nextInt(finders.length);
        finder = finders[fidx];
        preproc = ops[opidx];
        URL dataUrl = Thread.currentThread().getContextClassLoader().getResource("data/ml/generated");
        File generatedDataFolder = new File(dataUrl.getPath());
        File posFolder = new File(generatedDataFolder, "signal");
        File negFolder = new File(generatedDataFolder, "noise");
        mldc = new MLDataCompiler(posFolder, negFolder);
        mcv = new MonteCarloCV(mldc.readData(preproc));
        Map.Entry<MLROIFinder, Double> model = mcv.findBestModel(1, new MLROIFinder[]{finder});
        finder = model.getKey();
        bundle = new ROIBundle(finder, preproc);
    }

    @Test
    public void testGetSetMetadataEntry() throws Exception {
        String key = "date";
        String val = new Date().toString();
        bundle.setMetadataEntry(key, val);
        assertEquals(bundle.getMetadataEntry(key), val);
    }

    @Test
    public void isROIArray() throws Exception {
        CrossValidation.Data data = mldc.readData(preproc);
        for (int sample=0;sample<data.numSamples();sample++) {
            double[] arr = data.samples[sample];
            // ROIBundle assumes arrays have already been preprocessed
            assertEquals(finder.isROI(arr), bundle.isROI(arr));
        }
    }

    @Test
    public void isROIDataset() throws Exception {
        CrossValidation.Data data = mldc.readData();
        for (int sample=0;sample<data.numSamples();sample++) {
            double[] arr = data.samples[sample];
            Dataset raw = new Dataset(arr, 15, 15);
            Dataset preprocessed = preproc.run(raw);
            // ROIBundle assumes Datasets need to be preprocessed
            assertEquals(finder.isROI(preprocessed), bundle.isROI(raw));
        }
    }

    @Test
    public void serialize() throws Exception {
        File out = File.createTempFile("tmpbundle", "out");
        String key = "date";
        String val = new Date().toString();
        bundle.setMetadataEntry(key, val);
        bundle.save(out);
        ROIBundle reread = new ROIBundle();
        reread.load(out);
        assertEquals(bundle.getMetadata().keySet(), reread.getMetadata().keySet());
        assertEquals(bundle.getMetadataEntry(key), reread.getMetadataEntry(key));
        CrossValidation.Data data = mldc.readData(preproc);
        for (int sample=0;sample<data.numSamples();sample++) {
            double[] arr = data.samples[sample];
            assertEquals(bundle.isROI(arr), reread.isROI(arr));
        }
    }

}