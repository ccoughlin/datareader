/*
 * com.emphysic.myriad.core.data.ops.ConvolutionOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * ConvolutionOperationTest - tests the ConvolutionOperation.
 * Created by chris on 8/25/2016.
 */
public class ConvolutionOperationTest {
    /**
     * Simple edge detection kernel
     */
    private double[][] kernel = {
            { 1, 0, -1},
            { 0, 0,  0},
            {-1, 0,  1}
    };
    /**
     * Convolution operation
     */
    private ConvolutionOperation op;

    private void assertKernelsEqual(double[][] expected, double[][] actual) {
        assertEquals(expected.length, actual.length);
        for (int i=0; i<expected.length; i++) {
            assertEquals(expected[i].length, actual[i].length);
            for (int j=0; j<expected[i].length; j++) {
                assertEquals(expected[i][j], actual[i][j], 0.05 * expected[i][j]);
            }
        }
    }

    @Test
    public void getKernel() throws Exception {
        op = new ConvolutionOperation();
        double[][] id = {
                {0.0, 0.0, 0.0},
                {0.0, 1.0, 0.0},
                {0.0, 0.0, 0.0}
        };
        assertKernelsEqual(id, op.getKernel());
        op = new ConvolutionOperation(kernel);
        assertKernelsEqual(kernel, op.getKernel());
    }

    @Test
    public void getWidth() throws Exception {
        op = new ConvolutionOperation(kernel);
        assertEquals(op.getWidth(), kernel.length);
    }

    @Test
    public void getHeight() throws Exception {
        op = new ConvolutionOperation(kernel);
        assertEquals(op.getHeight(), kernel[0].length);
    }

    @Test
    public void run() throws Exception {
        Dataset dataset = getDataset();
        op = new ConvolutionOperation();
        Dataset result = op.run(dataset);
        // New and original should be the same - used identity kernel
        assertDatasetsEqual(dataset, result);
    }

    @Test
    public void serialize() throws Exception {
        op = new ConvolutionOperation(kernel);
        Dataset dataset = getDataset();
        File out = File.createTempFile("tmpconv", "dat");
        op.save(out);
        ConvolutionOperation read = new ConvolutionOperation();
        read.load(out);
        assertDatasetsEqual(op.run(dataset), read.run(dataset));
    }

    /**
     * Convenience method for returning a static 5x6 Dataset.
     * @return Dataset width 5 height 6
     */
    private Dataset getDataset() {
        double[] data = {
                0, 0, 0, 0, 0,
                0, 1, 1, 1, 0,
                0, 1, 2, 1, 0,
                0, 1, 2, 1, 0,
                0, 1, 1, 1, 0,
                0, 0, 0, 0, 0
        };
        return new Dataset(data, 5, 6);
    }

    /**
     * Convenience method for verifying two Datasets are equivalent.
     * @param expected expected results
     * @param actual actual results
     */
    private void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i=0; i<expected.getWidth(); i++) {
            for (int j=0; j<expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), actual.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }
}