/*
 * com.emphysic.myriad.core.data.ops.HOGOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.math.GradientVector;
import com.emphysic.myriad.core.data.util.FileSniffer;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;

import static org.junit.Assert.assertEquals;

/**
 * HOGOperationTest - tests the Histogram of Oriented Gradients (HOG) Operation.
 * Created by chris on 8/25/2016.
 */
public class HOGOperationTest {
    private HOGOperation hog;
    private URL testJpgURL = Thread.currentThread().getContextClassLoader().getResource("data/io/test.jpg");
    private File img = new File(testJpgURL.getPath());
    /**
     * Dataset contents of test file
     */
    private Dataset orig;

    /**
     * Utility method for checking two Datasets are equal
     * @param expected expected results
     * @param actual actual results
     */
    public void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i=0; i<expected.getWidth(); i++) {
            for (int j=0; j<expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), actual.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

    /**
     * Utility method for checking two arrays are (nearly) equal
     * @param expected expected results
     * @param actual actual results
     */
    public void assertArraysEqual(double[] expected, double[] actual) {
        assertEquals(expected.length, actual.length);
        for (int i=0; i<expected.length; i++) {
            assertEquals(expected[i], actual[i], 0.05 * expected[i]);
        }
    }

    @Before
    public void setUp() throws Exception {
        orig = FileSniffer.read(img, true);
        hog = new HOGOperation();
    }

    @Test
    public void run() throws Exception {
        SlidingWindowOperation slide = new SlidingWindowOperation(20, 20);
        Dataset window;
        int sliceno = 0;
        while ((window = slide.run(orig)) != null) {
            String fname = "swo20x20hog_slice" + sliceno + ".dat";
            URL expectedHOGSliceURL = Thread.currentThread().getContextClassLoader().getResource("data/io/hog/" + fname);
            File expectedHOGSliceFile = new File(expectedHOGSliceURL.getPath());
            Dataset expected = FileSniffer.read(expectedHOGSliceFile, true);
            Dataset actual = hog.run(window);
            assertDatasetsEqual(expected, actual);
            sliceno++;
        }
    }

    @Test
    public void serialize() throws Exception {
        HOGOperation original = new HOGOperation(3, 2, 2);
        File out = File.createTempFile("tmphog", "dat");
        original.save(out);
        HOGOperation reread = new HOGOperation();
        reread.load(out);
        SlidingWindowOperation slide = new SlidingWindowOperation(20, 20);
        Dataset window = slide.run(orig);
        Dataset expected = original.run(window);
        Dataset actual = reread.run(window);
        assertDatasetsEqual(expected, actual);
    }

    @Test
    public void gradientVector() throws Exception {
        double[] angles = {
                 0.5880026035475675,  1.5707963267948966,  2.356194490192345, -1.5707963267948966, -1.1071487177940904,
                -0.7853981633974483, -1.5707963267948966,  0.7853981633974483, 2.677945044588987,  -0.4636476090008061,
                 1.5707963267948966,  0.8960553845713439,  3.141592653589793, -1.5707963267948966,  1.1071487177940904,
                 0.5404195002705842, -0.7853981633974483, -2.677945044588987,  1.2490457723982544,  3.141592653589793,
                -0.5880026035475675, -2.0344439357957027, -0.7853981633974483, 2.356194490192345,  3.141592653589793
        };
        double[] mags = {
                237220, 263172, 186091, 131586, 147118,
                 93045,  65793, 186091, 147118, 147118,
                 65793, 421281,  65793, 131586, 147118,
                383636, 186091, 294235, 208056,  65793,
                237220, 294235,  93045,  93045,  65793
        };
        SlidingWindowOperation slide = new SlidingWindowOperation(5, 5);
        Dataset window = slide.run(orig);
        GradientVector gv = hog.gradientVector(window);
        assertArraysEqual(angles, gv.getAngles().getData());
        assertArraysEqual(mags, gv.getMagnitudes().getData());
    }

    @Test
    public void descriptors() throws Exception {
        double[] descs = {
                0.03641657801918729, 0.2000313689730766,  0.26473176753900385,
                0.29243714209263694, 0.18926941435644665, 0.027350178243506346,
                0.0,                 0.0,                 0.0,
                0.10924973405756186, 0.2826047814019705,  0.20729149279195228,
                0.20197116076058896, 0.17792847786835236, 0.027350178243506346,
                0.0,                 0.0,                 0.0,
                0.0,                 0.25106649033351897, 0.45486072857505216,
                0.28885757989272354, 0.22725938776037125, 0.041425286992806336,
                0.0,                 0.0,                 0.0,
                0.03641657801918729, 0.03641657801918729, 0.14887450341005584,
                0.12146222333831669, 0.2666251910527576,  0.2027955335923396,
                0.0,                 0.0,                 0.0,
        };
        // Need a slightly larger slice here for descriptors
        SlidingWindowOperation slide = new SlidingWindowOperation(5, 6);
        HOGOperation hog2 = new HOGOperation(1, 2, 9);
        Dataset descriptor = hog2.descriptors(slide.run(orig));
        assertArraysEqual(descs, descriptor.getData());
    }

    @Test
    public void normalize() throws Exception {
        double[] data = {1, 2, 3};
        double[] expected = new double[data.length];
        double mag = 0;
        for (double el: data) {
            mag += el * el;
        }
        mag = Math.sqrt(mag);
        for (int i=0; i<data.length; i++) {
            expected[i] = data[i] / mag;
        }
        double[] actual = hog.normalize(data);
        assertArraysEqual(expected, actual);
    }

}