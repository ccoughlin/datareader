/*
 * com.emphysic.myriad.core.data.ops.PowerOfTwoOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class PowerOfTwoOperationTest {
    Random random;
    PowerOfTwoOperation pto;

    @Before
    public void setUp() throws Exception {
        random = new Random();
        pto = new PowerOfTwoOperation();
    }

    public int getX() {
        return random.nextInt(15) + 1;
    }

    public int randomPowerOf2() {
        int exponent = getX();
        return (int) Math.pow(2, exponent);
    }

    public int randomNotPowerOf2() {
        return randomPowerOf2() + 1;
    }

    @Test
    public void nextPowerOf2() throws Exception {
        assertEquals(0, PowerOfTwoOperation.nextPowerOf2(0));
        assertEquals(0, PowerOfTwoOperation.nextPowerOf2(1));
        int randomPower = getX();
        assertEquals(randomPower, PowerOfTwoOperation.nextPowerOf2((int) Math.pow(2, randomPower)));
    }

    @Test
    public void pad() throws Exception {
        assertEquals(1, PowerOfTwoOperation.pad(0));
        assertEquals(1, PowerOfTwoOperation.pad(1));
        assertEquals(2, PowerOfTwoOperation.pad(2));
        int exponent = getX();
        int power = (int) Math.pow(2, exponent);
        assertEquals(power, PowerOfTwoOperation.pad(power));
        assertEquals((int) Math.pow(2, exponent + 1), PowerOfTwoOperation.pad(power + 1));
    }

    @Test
    public void isPowerOf2() throws Exception {
        assertFalse(PowerOfTwoOperation.isPowerOf2(0));
        assertFalse(PowerOfTwoOperation.isPowerOf2(-1));
        assertTrue(PowerOfTwoOperation.isPowerOf2(1));
        assertTrue(PowerOfTwoOperation.isPowerOf2(randomPowerOf2()));
        assertFalse(PowerOfTwoOperation.isPowerOf2(randomNotPowerOf2()));
    }

    @Test
    public void run() throws Exception {
        int w = randomNotPowerOf2();
        int h = randomNotPowerOf2();
        Dataset toPad = new Dataset(w, h);
        Dataset padded = pto.run(toPad);
        assertEquals(PowerOfTwoOperation.pad(w), padded.getWidth());
        assertEquals(PowerOfTwoOperation.pad(h), padded.getHeight());
        Dataset notPadded = pto.run(padded);
        assertEquals(notPadded.getWidth(), padded.getWidth());
        assertEquals(notPadded.getHeight(), padded.getHeight());
    }

}