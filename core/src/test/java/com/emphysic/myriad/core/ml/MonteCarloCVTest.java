/*
 * com.emphysic.myriad.core.ml.MonteCarloCVTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.ml;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * MonteCarloCVTest - tests MonteCarloCV.
 * Created by ccoughlin on 8/26/16.
 */
public class MonteCarloCVTest {
    private Random random = new Random();

    @Test
    public void getTrainTestSubset() throws Exception {
        int numSamples = random.nextInt(100) + 1;
        int numFeatures = random.nextInt(100) + 1;
        double[][] data = new double[numSamples][numFeatures];
        int[] labels = new int[numSamples];
        MonteCarloCV mcv = new MonteCarloCV(data, labels);
        CrossValidation.TrainTestSubsets tts = mcv.getTrainTestSubset();
        double ratio = mcv.getRatio();
        double train = (int)(ratio * numSamples);
        double test = numSamples - train;
        assertEquals(tts.testing.numFeatures(), numFeatures);
        assertEquals(test, tts.testing.numSamples(), 0.05 * test);
        assertEquals(tts.training.numFeatures(), numFeatures);
        assertEquals(train, tts.training.numSamples(), 0.05 * test);
    }

}