/*
 * com.emphysic.myriad.core.data.roi.PassiveAggressiveROIFinderTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.Vector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * PassiveAggressiveROIFinderTest - unit tests for PassiveAggressiveROIFinder.
 * Created by ccoughlin on 8/26/16.
 */
public class PassiveAggressiveROIFinderTest {
    private double[][] X = {{-1.0, -1.0}, {-2.0, -1.0}, {1.0, 1.0}, {2.0, 1.0}};
    private int[] y = {0, 0, 1, 1};
    private PassiveAggressiveROIFinder model;

    @Before
    public void setUp() throws Exception {
        model = new PassiveAggressiveROIFinder();
    }

    public void train() throws Exception {
        model.train(X, y);
    }

    @Test
    public void predict() throws Exception {
        train();
        for (int i = 0; i < X.length; i++) {
            double prediction = model.predict(X[i]);
            assertEquals(y[i], prediction, 0.05 * y[i]);
        }
    }

    @Test
    public void predict1() throws Exception {
        train();
        for (int i=0; i<X.length; i++) {
            Dataset d = new Dataset(X[i], X[i].length, 1);
            double prediction = model.predict(d);
            assertEquals(y[i], prediction, 0.05 * y[i]);
        }
    }

    @Test
    public void isROI() throws Exception {
        train();
        for (int i = 0; i < X.length; i++) {
            boolean prediction = model.isROI(X[i]);
            assertEquals(y[i] == (int)model.positiveClass(), prediction);
        }
    }

    @Test
    public void isROI1() throws Exception {
        train();
        for (int i = 0; i < X.length; i++) {
            Dataset d = new Dataset(X[i], X[i].length, 1);
            boolean prediction = model.isROI(d);
            assertEquals(y[i] == (int)model.positiveClass(), prediction);
        }
    }

    @Test
    public void confidenceThreshold() throws Exception {
        train();
        // Verify throwing exception for bad confidence levels
        double[] badThresh = {-1, 1.01, -0.1, 75};
        for (double bad : badThresh) {
            try {
                model.setConfidenceThreshold(bad);
                Assert.fail("Expected exception to be thrown");
            } catch (Exception e) {
                assertTrue(e instanceof IllegalArgumentException);
            }
        }
        // Verify reporting ROI iff confidence threshold is met
        double[] confThresh = {0, 0.25, 0.5, 0.75, 1};
        for (double t : confThresh) {
            model.setConfidenceThreshold(t);
            assertEquals(t, model.getConfidenceThreshold(), 0.05 * t);
            for (double[] aX : X) {
                Vector scores = model.classify(new DenseVector(aX));
                double maxConf = scores.maxValue();
                int idx = scores.maxValueIndex();
                if (idx > 0 && maxConf < t) {
                    idx = 0;
                }
                assertEquals(idx == model.positiveClass(), model.isROI(aX));
            }
        }
    }

    @Test
    public void serialize() throws Exception {
        File out = File.createTempFile("tmp_pa", "dat");
        train();
        Random random = new Random();
        model.setConfidenceThreshold(random.nextDouble());
        model.save(out);
        PassiveAggressiveROIFinder read = new PassiveAggressiveROIFinder();
        read.load(out);
        assertModelsEqual(model, read);

        PassiveAggressiveROIFinder read2 = (PassiveAggressiveROIFinder) ROIFinder.fromFile(out, PassiveAggressiveROIFinder.class);
        assertModelsEqual(model, read2);
    }

    /**
     * Verify that two PassiveAggressive models are "equal" i.e. make the same predictions and have the same basic
     * settings.
     * @param expected expected model
     * @param actual actual model
     */
    public void assertModelsEqual(PassiveAggressiveROIFinder expected, PassiveAggressiveROIFinder actual) {
        for (int i = 0; i < X.length; i++) {
            assertEquals(expected.predict(X[i]), actual.predict(X[i]), 0.05 * y[i]);
        }
        assertEquals(expected.getConfidenceThreshold(), actual.getConfidenceThreshold(),
                0.05 * expected.getConfidenceThreshold());
        assertEquals(expected.getLearningRate(), actual.getLearningRate(), 0.05 * expected.getLearningRate());
        assertEquals(expected.getNumCategories(), actual.getNumCategories());
        assertEquals(expected.getNumFeatures(), actual.getNumFeatures());
        assertEquals(expected.positiveClass(), actual.positiveClass(), expected.positiveClass() * 0.05);
        assertEquals(expected.negativeClass(), actual.negativeClass(), expected.negativeClass() * 0.05);
    }
}