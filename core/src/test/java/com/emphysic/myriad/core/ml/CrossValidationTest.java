/*
 * com.emphysic.myriad.core.ml.CrossValidationTest
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.ml;

import com.emphysic.myriad.core.data.roi.MLROIFinder;
import com.emphysic.myriad.core.data.roi.SGDROIFinder;
import org.junit.Before;
import org.junit.Test;
import org.sgdtk.HingeLoss;
import org.sgdtk.LogLoss;
import org.sgdtk.Loss;
import smile.math.distance.EuclideanDistance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * CrossValidationTest - tests the CrossValidation class.
 * Created by chris on 8/26/2016.
 */
public class CrossValidationTest {
    private CrossValidation cv;

    public CrossValidation.Data getData() throws Exception{
        URL dataURL = Thread.currentThread().getContextClassLoader().getResource("data/ml/enchilada.txt");
        File samplesFile = new File(dataURL.getPath());
        BufferedReader br = new BufferedReader(new FileReader(samplesFile));
        String line;
        int numFeatures = 225;
        int numSamples = 458;
        double[][] samples = new double[numSamples][numFeatures];
        int[] labels = new int[numSamples];
        int sampleno = 0;
        while ((line = br.readLine()) != null) {
            String[] tokens = line.split(" ");
            labels[sampleno] = Integer.parseInt(tokens[0]);
            for (int i = 0; i< numFeatures; i++) {
                double val = Double.parseDouble(tokens[i + 1]);
                samples[sampleno][i] = val;
            }
            sampleno++;
        }
        return new CrossValidation.Data(samples, labels);
    }

    @Before
    public void setUp() throws Exception {
        CrossValidation.Data data = getData();
        cv = new MonteCarloCV(data.samples, data.labels);
    }

    @Test
    public void findMinorityLabel() throws Exception {
        CrossValidation.Data data = getData();
        assertTrue(CrossValidation.findMinorityLabel(data) == 1);
    }

    @Test
    public void findMajorityLabel() throws Exception {
        CrossValidation.Data data = getData();
        assertTrue(CrossValidation.findMajorityLabel(data) == -1);
    }

    @Test
    public void findLabelFrequencies() throws Exception {
        CrossValidation.Data data = getData();
        int numPos = 0;
        int numNeg = 0;
        for (int i=0; i<data.numSamples(); i++) {
            if (data.labels[i] == -1) {
                numNeg++;
            } else if (data.labels[i] == 1) {
                numPos++;
            }
        }
        Map<Integer, Integer> labelFreqs = CrossValidation.labelFrequencies(data);
        assertTrue(labelFreqs.get(-1) == numNeg);
        assertTrue(labelFreqs.get(1) == numPos);
    }

    @Test
    public void balanceUp() throws Exception {
        CrossValidation.Data data = getData();
        int numNeighbors = 5;
        CrossValidation.Data upSampled = CrossValidation.balanceUp(data, new EuclideanDistance(), numNeighbors);
        Map<Integer, Integer> origLabelFreqs = CrossValidation.labelFrequencies(data);
        int minorityLabel = CrossValidation.findMinorityLabel(origLabelFreqs);
        int majorityLabel = CrossValidation.findMajorityLabel(origLabelFreqs);
        int factor = origLabelFreqs.get(majorityLabel) / origLabelFreqs.get(minorityLabel);
        assertTrue(origLabelFreqs.get(minorityLabel) * factor +
                data.numSamples() == upSampled.numSamples());
        Map<Integer, Integer> newLabelFreqs = CrossValidation.labelFrequencies(upSampled);
        assertTrue(origLabelFreqs.keySet().equals(newLabelFreqs.keySet()));
        assertTrue(origLabelFreqs.get(minorityLabel) * (factor + 1) == newLabelFreqs.get(minorityLabel));
        for (Integer key : origLabelFreqs.keySet()) {
            if (key != minorityLabel) {
                assertTrue(origLabelFreqs.get(key).equals(newLabelFreqs.get(key)));
            }
        }
    }

    @Test
    public void evalModels() throws Exception {
        SGDROIFinder[] models = genModels();
        Map<MLROIFinder, List<Double>> evaluation = cv.evalModels(2, models);
        assertEquals(evaluation.keySet().size(), models.length);
        for (MLROIFinder model : evaluation.keySet()) {
            List<Double> accuracies = evaluation.get(model);
            int numCats = 2;
            assertEquals(accuracies.size(), numCats);
        }
    }

    @Test
    public void eval() throws Exception {
        SGDROIFinder[] models = genModels();
        int numRounds = 3;
        Map<MLROIFinder, Double> meanEvaluation = cv.eval(numRounds, models);
        assertEquals(meanEvaluation.keySet().size(), models.length);
        for (int i=0; i<models.length; i++) {
            MLROIFinder model = models[i];
            assertTrue(meanEvaluation.containsKey(model));
            double acc = meanEvaluation.get(model);
            assertTrue(acc >= 0 && acc <= 1);
        }
    }

    @Test
    public void findBestModel() throws Exception {
        SGDROIFinder[] models = genModels();
        Map.Entry<MLROIFinder, Double> returnedBest = cv.findBestModel(2, models);
        boolean found = false;
        for (SGDROIFinder model : models) {
            if (model == returnedBest.getKey()) {
                found = true;
            }
        }
        assertTrue(found);
        double acc = returnedBest.getValue();
        assertTrue(acc >= 0 && acc <= 1);
    }

    /**
     * Utility method for creating a grid of machine learning models.
     * @return array of untrained models with varying settings
     */
    private SGDROIFinder[] genModels() {
        double[] regularizations = new double[]{
                -5, -1.5, -.25, 0.25, 0.5, 0.75, 1, 5
        };
        Loss[] losses = new Loss[]{
                new HingeLoss(),
                new LogLoss(),
        };
        SGDROIFinder[] models = new SGDROIFinder[regularizations.length * losses.length];
        for (int i = 0; i < regularizations.length; i++) {
            for (int j = 0; j < losses.length; j++) {
                models[j * regularizations.length + i] = new SGDROIFinder(losses[j], regularizations[i]);
            }
        }
        return models;
    }

    /**
     * Calculates the mean value of a List of doubles.
     * @param data numbers
     * @return mean
     */
    private double mean(List<Double> data) {
        double sum = 0;
        int n = 0;
        for (double el : data) {
            sum += el;
            n++;
        }
        return sum / n;
    }


}