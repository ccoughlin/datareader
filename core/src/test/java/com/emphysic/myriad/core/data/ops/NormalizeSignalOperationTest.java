/*
 * com.emphysic.myriad.core.data.ops.NormalizeSignalOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class NormalizeSignalOperationTest {
    @Test
    public void run() throws Exception {
        Random random = new Random();
        double[] inputData = new double[12];
        double max = Double.MIN_VALUE;
        double min = Double.MAX_VALUE;
        for (int i = 0; i < inputData.length; i++) {
            double el = 10 * random.nextDouble();
            if (el > max) {
                max = el;
            }
            if (el < min) {
                min = el;
            }
            inputData[i] = el;
        }
        Dataset dataset = new Dataset(inputData, 3, 4);
        NormalizeSignalOperation norm = new NormalizeSignalOperation();
        Dataset actual = norm.run(dataset);
        assertEquals(dataset.getWidth(), actual.getWidth());
        assertEquals(dataset.getHeight(), actual.getHeight());
        for (int i = 0; i < dataset.getWidth(); i++) {
            for (int j = 0; j < dataset.getHeight(); j++) {
                assertEquals((dataset.get(i, j) - min) / (max - min), actual.get(i, j), 0.05 * dataset.get(i, j));
            }
        }
    }

}