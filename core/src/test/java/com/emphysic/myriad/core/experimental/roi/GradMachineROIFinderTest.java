/*
 * com.emphysic.myriad.core.experimental.roi.GradMachineROIFinderTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.experimental.roi;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.roi.ROIFinder;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Random;


/**
 * Tests the experimental GradMachineROIFinder implementation.
 * Created by ccoughlin on 11/8/2016.
 */
public class GradMachineROIFinderTest {
    private double[][] X = {{-1.0, -1.0}, {-2.0, -1.0}, {1.0, 1.0}, {2.0, 1.0}};
    private int[] y = {0, 0, 1, 1};
    private GradMachineROIFinder model;

    @Before
    public void setUp() throws Exception {
        model = new GradMachineROIFinder(2);
        model.train(X, y);
    }

    @Test
    public void predict() throws Exception {
        int[] acceptablePredictions = {(int) model.positiveClass(), (int) model.negativeClass()};
        for (double[] aX : X) {
            int prediction = (int) model.predict(aX);
            Assert.assertTrue(ArrayUtils.contains(acceptablePredictions, prediction));
        }
    }

    @Test
    public void predict1() throws Exception {
        int[] acceptablePredictions = {(int) model.positiveClass(), (int) model.negativeClass()};
        for (double[] aX : X) {
            Dataset d = new Dataset(aX, aX.length, 1);
            int prediction = (int) model.predict(d);
            Assert.assertTrue(ArrayUtils.contains(acceptablePredictions, prediction));
        }
    }

    @Test
    public void serialize() throws Exception {
        File out = File.createTempFile("tmp_gm", "dat");
        Random random = new Random();
        model.setConfidenceThreshold(random.nextDouble());
        model.save(out);
        GradMachineROIFinder read = new GradMachineROIFinder();
        read.load(out);
        assertModelsEqual(model, read);

        GradMachineROIFinder read2 = (GradMachineROIFinder) ROIFinder.fromFile(out, GradMachineROIFinder.class);
        assertModelsEqual(model, read2);
    }

    /**
     * Convenience method to verify two GradMachine ROI Finders are "equal" i.e. equivalent fields.
     * @param expected expected model
     * @param actual actual model
     */
    public void assertModelsEqual(GradMachineROIFinder expected, GradMachineROIFinder actual) {
        Assert.assertEquals(expected.getNumCategories(), actual.getNumCategories());
        Assert.assertEquals(expected.getLearningRate(), actual.getLearningRate(), expected.getLearningRate() * 0.05);
        Assert.assertEquals(expected.getNumFeatures(), actual.getNumFeatures());
        Assert.assertEquals(expected.getNumHidden(), actual.getNumHidden());
        Assert.assertEquals(expected.getConfidenceThreshold(), actual.getConfidenceThreshold(),
                expected.getConfidenceThreshold() * 0.05);
        Assert.assertEquals(expected.getRegularization(), actual.getRegularization(), expected.getRegularization() * 0.05);
        Assert.assertEquals(expected.getSparsity(), actual.getSparsity(), expected.getSparsity() * 0.05);
        for (int i = 0; i < X.length; i++) {
            Assert.assertEquals(expected.predict(X[i]), actual.predict(X[i]), 0.05 * y[i]);
        }
    }
}