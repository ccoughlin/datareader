/*
 * com.emphysic.myriad.core.data.ops.math.OnlineStatsTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops.math;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OnlineStatsTest {
    private double[][] samples = new double[][]{
            {1, -1, 2},
            {2, 0, 0},
            {0, 1, -1}
    };

    private double[] samples1d = new double[]{
            1, -1, 2,
            2, 0, 0,
            0, 1, -1
    };

    // Expected mean, variance, and standard deviation after updating each row in samples
    private double[] expectedMeans = new double[]{0.6666666666666666, 0.6666666666666667, 0.44444444444444453};
    private double[] expectedVars = new double[]{2.3333333333333335, 1.4666666666666666, 1.2777777777777777};
    private double[] expectedStds = new double[]{1.5275252316519468, 1.2110601416389966, 1.130388330520878};

    private OnlineStats online;

    @Before
    public void setUp() throws Exception {
        online = new OnlineStats();
    }

    @Test
    public void update() throws Exception {
        for (int i = 0; i < samples.length; i++) {
            online.update(samples[i]);
            assertEquals(expectedMeans[i], online.mean(), 0.05 * expectedMeans[i]);
            assertEquals(expectedVars[i], online.variance(), 0.05 * expectedVars[i]);
            assertEquals(expectedStds[i], online.stddev(), 0.05 * expectedStds[i]);
        }
    }

    @Test
    public void update1() throws Exception {
        for (int i = 0; i < samples.length; i++) {
            online.update(new Dataset(samples[i], 3, 1));
            assertEquals(expectedMeans[i], online.mean(), 0.05 * expectedMeans[i]);
            assertEquals(expectedVars[i], online.variance(), 0.05 * expectedVars[i]);
            assertEquals(expectedStds[i], online.stddev(), 0.05 * expectedStds[i]);
        }
    }

    @Test
    public void mean() throws Exception {
        assertEquals(0, online.mean(), 0.05 * online.mean());
        for (int i = 0; i < samples.length; i++) {
            double currentMean = online.mean(samples[i]);
            assertEquals(expectedMeans[i], currentMean, 0.05 * expectedMeans[i]);
        }
    }

    @Test
    public void mean1() throws Exception {
        assertEquals(0, online.mean(), 0.05 * online.mean());
        for (int i = 0; i < samples.length; i++) {
            double currentMean = online.mean(new Dataset(samples[i], 3, 1));
            assertEquals(expectedMeans[i], currentMean, 0.05 * expectedMeans[i]);
        }
    }

    @Test
    public void variance() throws Exception {
        assertEquals(Double.NaN, online.variance(), 0.05 * online.variance());
        for (int i = 0; i < samples.length; i++) {
            double currentVariance = online.variance(samples[i]);
            assertEquals(expectedVars[i], currentVariance, 0.05 * expectedVars[i]);
        }
    }

    @Test
    public void variance1() throws Exception {
        assertEquals(Double.NaN, online.variance(), 0.05 * online.variance());
        for (int i = 0; i < samples.length; i++) {
            double currentVariance = online.variance(new Dataset(samples[i], 3, 1));
            assertEquals(expectedVars[i], currentVariance, 0.05 * expectedVars[i]);
        }
    }

    @Test
    public void stddev() throws Exception {
        assertEquals(Double.NaN, online.stddev(), 0.05 * online.stddev());
        for (int i = 0; i < samples.length; i++) {
            double currentMean = online.stddev(samples[i]);
            assertEquals(expectedStds[i], currentMean, 0.05 * expectedStds[i]);
        }
    }

    @Test
    public void stddev1() throws Exception {
        assertEquals(Double.NaN, online.stddev(), 0.05 * online.stddev());
        for (int i = 0; i < samples.length; i++) {
            double currentMean = online.stddev(new Dataset(samples[i], 3, 1));
            assertEquals(expectedStds[i], currentMean, 0.05 * expectedStds[i]);
        }
    }

}