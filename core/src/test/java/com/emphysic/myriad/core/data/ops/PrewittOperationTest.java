/*
 * com.emphysic.myriad.core.data.ops.PrewittOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests the PrewittOperation.
 * Created by chris on 8/9/2016.
 */
public class PrewittOperationTest {
    @Test
    public void run() throws Exception {
        double[] inputData = new double[]{
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 1, 1, 1, 0, 0, 0,
                0, 1, 0, 1, 0, 0, 0,
                0, 1, 0, 1, 0, 0, 0,
                0, 1, 0, 1, 0, 0, 0,
                0, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
        };

        double[] expectedData = new double[]{
                0.0,  0.0, 0.0,  0.0,  0.0, 0.0, 0.0,
                3.16, 4.0, 3.16, 1.41, 0.0, 0.0, 0.0,
                2.83, 2.0, 2.83, 2.0,  0.0, 0.0, 0.0,
                2.83, 2.0, 2.83, 2.0,  0.0, 0.0, 0.0,
                2.0,  0.0, 2.0,  2.0,  0.0, 0.0, 0.0,
                2.83, 2.0, 2.83, 2.0,  0.0, 0.0, 0.0,
                2.83, 2.0, 2.83, 2.0,  0.0, 0.0, 0.0,
                3.16, 4.0, 3.16, 1.41, 0.0, 0.0, 0.0,
                0.0,  0.0, 0.0,  0.0,  0.0, 0.0, 0.0,
                0.0,  0.0, 0.0,  0.0,  0.0, 0.0, 0.0
        };
        Dataset inp = new Dataset(inputData, 7, 10);
        PrewittOperation edgeFinder = new PrewittOperation();
        Dataset out = edgeFinder.run(inp);
        Dataset expected = new Dataset(expectedData, 7, 10);
        assertEquals(expected.getWidth(), out.getWidth());
        assertEquals(expected.getHeight(), out.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), out.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

}