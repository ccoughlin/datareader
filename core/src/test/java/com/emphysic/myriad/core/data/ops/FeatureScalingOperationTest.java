/*
 * com.emphysic.myriad.core.data.ops.FeatureScalingOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FeatureScalingOperationTest {
    private List<Dataset> samples;
    private FeatureScalingOperation scaler;
    private Dataset test = new Dataset(new double[]{-1., 1., 0.}, 3, 1);

    @Before
    public void setUp() throws Exception {
        samples = new ArrayList<>();
        samples.add(new Dataset(new double[]{1, -1, 2}, 3, 1));
        samples.add(new Dataset(new double[]{2, 0, 0}, 3, 1));
        samples.add(new Dataset(new double[]{0, 1, -1}, 3, 1));
    }

    public void assertArraysEqual(double[] expected, double[] actual) {
        assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], actual[i], 0.05 * expected[i]);
        }
    }

    public double[] toArray(List<Double> input) {
        double[] output = new double[input.size()];
        for (int i = 0; i < output.length; i++) {
            output[i] = input.get(i);
        }
        return output;
    }

    @Test
    public void runnofit() throws Exception {
        try {
            scaler = new FeatureScalingOperation();
            scaler.run(test);
            fail("Should not run until fit");
        } catch (RuntimeException rte) {
            assertEquals(rte.getMessage(), "Must call fit prior to scaling a dataset");
        }
    }

    @Test
    public void run() throws Exception {
        scaler = new FeatureScalingOperation(samples);
        double[] expected = {-2.449489742783178, 1.224744871391589, -0.2672612419124244};
        double[] actual = scaler.run(test).getData();
        assertArraysEqual(expected, actual);
    }

    @Test
    public void fit() throws Exception {
        scaler = new FeatureScalingOperation();
        scaler.fit(samples);
        double[] expectedMeans = {1.0, 0.0, 0.3333333333333333};
        double[] expectedStds = {0.816496580927726, 0.816496580927726, 1.247219128924647};
        assertArraysEqual(expectedMeans, toArray(scaler.getFeatureMeans()));
        assertArraysEqual(expectedStds, toArray(scaler.getFeatureStds()));
    }

    @Test
    public void persist() throws Exception {
        File out = File.createTempFile("scaler", "serialized");
        scaler = new FeatureScalingOperation(samples);
        scaler.save(out);
        FeatureScalingOperation read = new FeatureScalingOperation();
        read.load(out);
        assertArraysEqual(toArray(scaler.getFeatureMeans()), toArray(read.getFeatureMeans()));
        assertArraysEqual(toArray(scaler.getFeatureStds()), toArray(read.getFeatureStds()));
    }

    @Test
    public void persist_partial_fit() throws Exception {
        File out = File.createTempFile("scaler", "serialized");
        FeatureScalingOperation onlineScaler = new FeatureScalingOperation();
        onlineScaler.partial_fit(samples);
        onlineScaler.save(out);
        FeatureScalingOperation read = new FeatureScalingOperation();
        read.load(out);
        assertArraysEqual(toArray(onlineScaler.getFeatureMeans()), toArray(onlineScaler.getFeatureMeans()));
        assertArraysEqual(toArray(onlineScaler.getFeatureStds()), toArray(onlineScaler.getFeatureStds()));
        assertEquals(onlineScaler.getOnlineStats().getN(), read.getOnlineStats().getN());
        assertEquals(onlineScaler.getOnlineStats().getSumSquaredMeanDiff(),
                read.getOnlineStats().getSumSquaredMeanDiff(),
                0.05 * onlineScaler.getOnlineStats().getSumSquaredMeanDiff());
        assertEquals(onlineScaler.getOnlineStats().mean(),
                read.getOnlineStats().mean(),
                0.05 * onlineScaler.getOnlineStats().mean());
        assertEquals(onlineScaler.getOnlineStats().variance(),
                read.getOnlineStats().variance(),
                0.05 * onlineScaler.getOnlineStats().variance());
        assertEquals(onlineScaler.getOnlineStats().stddev(),
                read.getOnlineStats().stddev(),
                0.05 * onlineScaler.getOnlineStats().stddev());
    }

    @Test
    public void partial_fit() throws Exception {
        scaler = new FeatureScalingOperation();
        // Verify 2 or more elements required
        try {
            scaler.partial_fit(samples.subList(1, samples.size() - 2));
            fail("Shouldn't work with less than 2 elements");
        } catch (AssertionError ignored) {
        }
        scaler = new FeatureScalingOperation();
        scaler.partial_fit(samples);
        double[] expectedScaledData = new double[]{-2.23606797749979, 0.33333333333333337, -0.4255431312120302};
        double[] approximatedScaledData = scaler.run(test).getData();
        assertArraysEqual(expectedScaledData, approximatedScaledData);
    }
}