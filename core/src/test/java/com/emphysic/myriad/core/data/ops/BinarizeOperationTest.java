/*
 * com.emphysic.myriad.core.data.ops.BinarizeOperationTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class BinarizeOperationTest {

    /**
     * Helper method - assert two arrays are of the same length and each corresponding element is no more than
     * 5% different with the expected value
     *
     * @param expected expected values
     * @param actual   actual values
     */
    private void assertArraysEqual(double[] expected, double[] actual) {
        assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], actual[i], 0.05 * expected[i]);
        }
    }

    /**
     * Verify binarizing with a specified threshold
     */
    @Test
    public void threshold() throws Exception {
        double[] data = new double[]{-3, -2, -1, 0, 1, 2, 3};
        Dataset dataset = new Dataset(data, data.length, 1);
        double t = -1;
        BinarizeOperation binarizeOperation = new BinarizeOperation(t);
        double[] expected = new double[]{0, 0, 1, 1, 1, 1, 1};
        double[] actual = binarizeOperation.run(dataset).getData();
        assertArraysEqual(expected, actual);
    }

    /**
     * Verify binarizing by sign
     */
    @Test
    public void zero() throws Exception {
        double[] data = new double[]{1, 2, 3, -1, 0, -3.141592654, 11, 0.001, -7e7};
        double[] expected = new double[]{1, 1, 1, 0, 1, 0, 1, 1, 0};
        BinarizeOperation binarizeOperation = new BinarizeOperation();
        double[] actual = binarizeOperation.run(new Dataset(data, 3, 3)).getData();
        assertArraysEqual(expected, actual);
    }

    @Test
    public void serialize() throws Exception {
        Random random = new Random();
        double[] data = new double[]{-3, -2, -1, 0, 1, 2, 3};
        Dataset dataset = new Dataset(data, data.length, 1);
        double t = data[random.nextInt(data.length)];
        BinarizeOperation orig = new BinarizeOperation(t);
        Dataset expected = orig.run(dataset);
        File out = File.createTempFile("tmpbin", "dat");
        orig.save(out);
        BinarizeOperation reread = new BinarizeOperation();
        reread.load(out);
        Dataset actual = reread.run(dataset);
        assertArraysEqual(expected.getData(), actual.getData());
    }
}