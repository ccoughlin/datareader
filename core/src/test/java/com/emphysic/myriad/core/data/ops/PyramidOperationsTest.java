/*
 * com.emphysic.myriad.core.data.ops.PyramidOperationsTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.ops;

import com.emphysic.myriad.core.data.io.Dataset;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PyramidOperationsTest {
    double[] originalData = {
            0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 50.0, 60.0, 50.0, 0.0,
            0.0, 60.0, 90.0, 60.0, 0.0,
            0.0, 50.0, 60.0, 50.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0
    };

    // First Laplacian level of originalData, blur radius 5, scale factor 2, min 2x2 window
    double[] lap1 = {
            -176.11501946588353, -167.2385765999591, -164.2797623113176, -164.2797623113176, -153.18011064817983, -108.78150399562875, -46.62181544976437, -8.14083737449628,
            -167.48446144389052, -152.67809575848653, -146.75636910047137, -148.2357762447921, -135.64032511440485, -94.13906153951237, -37.81094187555497, -6.673724472372109,
            -164.92725906700372, -97.12519636636853, -81.19117546615678, -91.19117546615678, -130.0505429956971, -88.44682740249988, -34.94228536302167, -5.206611570247937,
            -165.38624410900908, -88.27880609248012, -51.58459121644708, -83.07219452223217, -130.41117410012984, -89.43651389932386, -35.040639300594236, -5.96680554606926,
            -153.8050679598389, -85.90465132163106, -69.93784577556184, -79.93784577556181, -119.48773990847623, -80.66252305170413, -31.374906085649897, -4.483300321016325,
            -109.14418414042757, -94.1984837101291, -88.21938392186327, -89.71518338911278, -80.74448466634794, -53.832388498053426, -19.441294993511374, -2.9915989344990104,
            -47.86558295198417, -38.89078614848713, -35.89918721398813, -35.89918721398812, -32.15968854586436, -20.19329280786832, -6.731097602622773, -0.7478997336247526,
            -8.974796803497032, -6.731097602622773, -5.983197868998021, -5.983197868998021, -5.235298135373268, -2.9915989344990104, -0.7478997336247526, 0.0
    };

    // Second Laplacian level of originalData, blur radius 5, scale factor 2, min 2x2 window
    double[] lap2 = {
            -174.0658879428256, -165.14027341822919, -159.18986373516492, -153.22306349920999,
            -165.12393419644368, -150.24790733372583, -139.83468861832523, -127.93386836717762,
            -159.15720122193682, -139.0745552663091, -125.68612462922393, -108.59507760809522,
            -153.2232210326016, -127.19015034858566, -109.33890182897366, -83.30583114495771
    };

    // Third Laplacian level of originalData, blur radius 5, scale factor 2, min 2x2 window
    double[] lap3 = {
            2.975204841532139, 2.975204841532139,
            2.9752083816083545, 2.9752083816083545
    };

    // Merged result of combining Laplacian levels lap1, lap2, lap3
    double[] merged = {
            2.5579538487363607E-13, 2.5579538487363607E-13, 2.2737367544323206E-13, 2.5579538487363607E-13, 2.2737367544323206E-13, 1.7053025658242404E-13, 7.815970093361102E-14, 1.2434497875801753E-14,
            3.126388037344441E-13, 2.5579538487363607E-13, 2.2737367544323206E-13, 2.5579538487363607E-13, 2.2737367544323206E-13, 1.7053025658242404E-13, 7.105427357601002E-14, 1.2434497875801753E-14,
            3.694822225952521E-13, 50.00000000000031, 60.00000000000034, 50.00000000000031, 2.5579538487363607E-13, 1.9895196601282805E-13, 7.815970093361102E-14, 9.769962616701378E-15,
            4.547473508864641E-13, 60.000000000000455, 90.0000000000004, 60.00000000000037, 2.5579538487363607E-13, 1.4210854715202004E-13, 4.973799150320701E-14, 7.105427357601002E-15,
            5.400124791776761E-13, 50.00000000000054, 60.000000000000455, 50.00000000000037, 1.8474111129762605E-13, 7.105427357601002E-14, 1.0658141036401503E-14, 8.881784197001252E-16,
            4.831690603168681E-13, 4.263256414560601E-13, 3.552713678800501E-13, 2.4158453015843406E-13, 8.526512829121202E-14, 7.105427357601002E-15, 0.0, -8.881784197001252E-16,
            2.0605739337042905E-13, 1.7763568394002505E-13, 1.3500311979441904E-13, 8.526512829121202E-14, 2.1316282072803006E-14, -3.552713678800501E-15, -1.7763568394002505E-15, -2.220446049250313E-16,
            3.907985046680551E-14, 3.019806626980426E-14, 2.3092638912203256E-14, 1.2434497875801753E-14, 1.7763568394002505E-15, -8.881784197001252E-16, -2.220446049250313E-16, 0.0
    };

    public Dataset getDataset() {
        return new Dataset(originalData, 5, 5);
    }

    public Dataset getExpectedDataset() {
        return new Dataset(merged, 8, 8);
    }

    public List<Dataset> GaussianLevels() {
        List<Dataset> pyramid = new ArrayList<>();
        GaussianPyramidOperation gaussianPyramidOperation = new GaussianPyramidOperation();
        Dataset current = new Dataset(originalData, 5, 5);
        do {
            pyramid.add(current);
        } while ((current = gaussianPyramidOperation.run(current)) != null);
        return pyramid;
    }

    public List<Dataset> LaplacianLevels() {
        List<Dataset> pyramid = new ArrayList<>();
        pyramid.add(new Dataset(lap1, 8, 8));
        pyramid.add(new Dataset(lap2, 4, 4));
        pyramid.add(new Dataset(lap3, 2, 2));
        return pyramid;
    }

    public void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), actual.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

    public void assertDatasetListsEqual(List<Dataset> expected, List<Dataset> actual) {
        assertEquals(expected.size(), actual.size());
        for (int i = 0; i < expected.size(); i++) {
            assertDatasetsEqual(expected.get(i), actual.get(i));
        }
    }

    @Test
    public void collapse() throws Exception {
        Dataset actual = PyramidOperations.collapse(LaplacianLevels());
        assertDatasetsEqual(getExpectedDataset(), actual);
    }

    @Test
    public void collapse1() throws Exception {
        GaussianPyramidOperation gpo = new GaussianPyramidOperation();
        Dataset actual = PyramidOperations.collapse(getDataset(), gpo);
        assertDatasetsEqual(getExpectedDataset(), actual);
    }

    @Test
    public void collapse2() throws Exception {
        Dataset actual = PyramidOperations.collapse(getDataset());
        assertDatasetsEqual(getExpectedDataset(), actual);
    }

    @Test
    public void generateLaplacian() throws Exception {
        List<Dataset> actual = PyramidOperations.generateLaplacian(GaussianLevels());
        assertDatasetListsEqual(LaplacianLevels(), actual);
    }

    @Test
    public void generateLaplacian1() throws Exception {
        GaussianPyramidOperation gpo = new GaussianPyramidOperation();
        List<Dataset> actual = PyramidOperations.generateLaplacian(getDataset(), gpo);
        assertDatasetListsEqual(LaplacianLevels(), actual);
    }

    @Test
    public void generateLaplacian2() throws Exception {
        List<Dataset> actual = PyramidOperations.generateLaplacian(getDataset());
        assertDatasetListsEqual(LaplacianLevels(), actual);
    }

    @Test
    public void reduce() throws Exception {
        GaussianPyramidOperation gpo = new GaussianPyramidOperation(2, 2);
        List<Dataset> levels = new ArrayList<>();
        Dataset current = getDataset();
        do {
            levels.add(current);
        } while ((current = gpo.run(current)) != null);

        List<Dataset> reductions = new ArrayList<>();
        Dataset reduction = getDataset();
        do {
            reductions.add(reduction);
        } while ((reduction = PyramidOperations.reduce(reduction, gpo)) != null);
        assertDatasetListsEqual(levels, reductions);
    }

    @Test
    public void reduce1() throws Exception {
        List<Dataset> levels = GaussianLevels();
        List<Dataset> reductions = new ArrayList<>();
        Dataset reduction = getDataset();
        do {
            reductions.add(reduction);
        } while ((reduction = PyramidOperations.reduce(reduction)) != null);
        assertDatasetListsEqual(levels, reductions);
    }

    @Test
    public void expand1() throws Exception {
        UpscaleOperation upo = new UpscaleOperation();
        Dataset expected = upo.run(getDataset());
        Dataset actual1 = PyramidOperations.expand(getDataset(), upo);
        assertDatasetsEqual(expected, actual1);
    }

    @Test
    public void expand2() throws Exception {
        UpscaleOperation upo = new UpscaleOperation();
        Dataset expected = upo.run(getDataset());
        Dataset actual2 = PyramidOperations.expand(getDataset());
        assertDatasetsEqual(expected, actual2);
    }
}