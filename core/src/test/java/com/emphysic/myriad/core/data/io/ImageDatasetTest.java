/*
 * com.emphysic.myriad.core.data.io.ImageDatasetTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.io;

import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class ImageDatasetTest {

    private ImageDataset imgReader;
    private File singleImageFile;
    private File multiframeImageFile;
    private List<BufferedImage> imageFrames;
    private long bigFileBytes = 1000000000;

    @Before
    public void setUp() throws Exception {
        URL testJpgURL = Thread.currentThread().getContextClassLoader().getResource("data/io/test.jpg");
        singleImageFile = new File(testJpgURL.getPath());
        URL tstTiffURL = Thread.currentThread().getContextClassLoader().getResource("data/io/multipage_tiff_example.tif");
        multiframeImageFile = new File(tstTiffURL.getPath());
    }

    /**
     * Helper method - instantiates the ImageDataset and the image frames from the specified image
     *
     * @param imageFile image to read
     * @throws IOException if an I/O error occurs
     */
    public void setReaders(File imageFile) throws IOException {
        imgReader = new ImageDataset(imageFile);
        imgReader.setBigFileSize(bigFileBytes);
        imageFrames = readImage(imageFile);
    }

    /**
     * Helper method - iterates over an image file and returns a list of the images read
     *
     * @param dataSource image file to read
     * @return list of images read from the file
     * @throws IOException if an I/O error occurs
     */
    public List<BufferedImage> readImage(File dataSource) throws IOException {
        List<BufferedImage> imageFrames = new ArrayList<>();
        ImageInputStream is = ImageIO.createImageInputStream(dataSource);
        if (is == null || is.length() == 0) {
            throw new IOException("Encountered error opening input stream to " + dataSource);
        }
        Iterator<ImageReader> iterator = ImageIO.getImageReaders(is);
        if (iterator == null || !iterator.hasNext()) {
            throw new IOException("Image file format not supported by ImageIO: " + dataSource);
        }
        // We are just looking for the first reader compatible:
        ImageReader reader = iterator.next();
        reader.setInput(is);
        for (int frame = 0; frame < reader.getNumImages(true); frame++) {
            imageFrames.add(reader.read(frame));
        }
        return imageFrames;
    }

    /**
     * Helper method - returns the RGB data from an image
     *
     * @param image image to read
     * @return RGB data
     */
    public double[] img2double(BufferedImage image) {
        int w = image.getWidth();
        int h = image.getHeight();
        BufferedImage greyImage = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = greyImage.getGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        double[] ddata = new double[w * h];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                ddata[j * w + i] = greyImage.getRGB(i, j);
            }
        }
        return ddata;
    }

    /**
     * Helper method - verifies two arrays are equal
     *
     * @param expected expected results
     * @param actual   actual results
     */
    public void assertArraysEqual(double[] expected, double[] actual) {
        assertEquals(expected.length, actual.length);
        for (int j = 0; j < expected.length; j++) {
            assertEquals(expected[j], actual[j], 0.05 * expected[j]);
        }
    }

    /**
     * Verify reading and iterating over multi-image file formats e.g. GIF or TIFF
     */
    @Test
    public void readMultiFrameImages() throws Exception {
        setReaders(multiframeImageFile);
        assertEquals(imageFrames.size(), imgReader.getNumFrames());
        for (BufferedImage expectedImage : imageFrames) {
            imgReader.read();
            assertEquals(expectedImage.getWidth(), imgReader.getWidth());
            assertEquals(expectedImage.getHeight(), imgReader.getHeight());
            assertArraysEqual(img2double(expectedImage), imgReader.getData());
        }
    }

    /**
     * Verify reading a single-image file format e.g. PNG or JPEG
     */
    @Test
    public void readSingleFrameImages() throws Exception {
        setReaders(singleImageFile);
        imgReader.read();
        assertEquals(1, imgReader.getNumFrames());
        assertArraysEqual(img2double(imageFrames.get(0)), imgReader.getData());
        // Verify subsequent reads don't change the data
        for (int i = 0; i < 3; i++) {
            imgReader.read();
            assertArraysEqual(img2double(imageFrames.get(0)), imgReader.getData());
        }
    }

    /**
     * Verify reading a specified frame from an image
     */
    @Test
    public void readFrame() throws Exception {
        Random random = new Random();
        setReaders(multiframeImageFile);
        int frame = random.nextInt(imageFrames.size());
        double[] expectedData = img2double(imageFrames.get(frame));
        double[] returnedData = imgReader.read(frame);
        assertEquals(expectedData.length, returnedData.length);
        for (int i = 0; i < expectedData.length; i++) {
            assertEquals(expectedData[i], returnedData[i], 0.05 * expectedData[i]);
        }
        // Verify throwing an exception for a bad index number
        int[] badFrames = new int[]{-1, imgReader.getNumFrames() + 1};
        for (int badFrame : badFrames) {
            try {
                imgReader.read(badFrame);
                fail("Expected an IllegalArgumentException");
            } catch (IndexOutOfBoundsException oobe) {
                assertEquals(oobe.getMessage(),
                        "Invalid frame specified, must be between 0 " + " and " + imgReader.getNumFrames());
            }
        }
    }

    /**
     * Verify retrieving RGB values from an image
     */
    @Test
    public void getData() throws Exception {
        Random random = new Random();
        setReaders(multiframeImageFile);
        int frame = random.nextInt(imageFrames.size());
        BufferedImage randomFrame = imageFrames.get(frame);
        int left = random.nextInt(randomFrame.getWidth() - 11);
        int top = random.nextInt(randomFrame.getHeight() - 11);
        int width = 10;
        int height = 10;
        double[] data = ImageDataset.getData(randomFrame, left, top, width, height);
        double[] expected = new double[width * height];
        for (int i = left; i < width; i++) {
            for (int j = top; j < height; j++) {
                expected[i + j] = randomFrame.getRGB(i, j);
            }
        }
        assertArraysEqual(expected, data);
    }

    @Test
    public void getPixelData() throws Exception {
        setReaders(singleImageFile);
        BufferedImage img = imageFrames.get(0);
        int[] expected = img.getData().getPixels(0, 0, img.getWidth(), img.getHeight(), (int[]) null);
        int[] actual = ImageDataset.getPixelData(img);
        assertEquals(expected.length, actual.length);
        for (int i = 0; i < img.getWidth(); i++) {
            for (int j = 0; j < img.getHeight(); j++) {
                assertEquals(expected[i], actual[i]);
            }
        }
    }

    @Test
    public void testBigFile() throws Exception {
        long origBigFileBytes = bigFileBytes;
        bigFileBytes = 10;
        readSingleFrameImages();
        readMultiFrameImages();
        readFrame();
        bigFileBytes = origBigFileBytes;
    }
}