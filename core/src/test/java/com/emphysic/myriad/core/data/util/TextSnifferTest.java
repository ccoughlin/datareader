/*
 * com.emphysic.myriad.core.data.util.TextSnifferTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.core.data.util;

import com.emphysic.myriad.core.data.io.TextDataset;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringJoiner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Slf4j
public class TextSnifferTest {
    /**
     * Picks a delimiter at random
     *
     * @return chosen delimiter
     */
    public String getDelimiter() {
        String[] delims = {",", " ", "\t"};
        return delims[new Random().nextInt(delims.length)];
    }

    /**
     * Generates a random dataset
     *
     * @param numCols number of elements per row
     * @param numRows number of rows
     * @return dataset
     */
    public List<List<Double>> genData(int numCols, int numRows) {
        ArrayList<List<Double>> dataArray = new ArrayList<>();
        for (int row = 0; row < numRows; row++) {
            ArrayList<Double> newRow = new ArrayList<>(numCols);
            for (int j = 0; j < numCols; j++) {
                newRow.add(Math.asin(Math.random()) * 1000);
            }
            dataArray.add(newRow);
        }
        return dataArray;
    }

    /**
     * Creates a temporary delimited text file.  File is automatically deleted when the VM exits.
     *
     * @param data  data to write to the file
     * @param delim delimiter to use to separate elements in the file
     * @return instance of the created file
     */
    public File genTempDataFile(List<List<Double>> data, String delim) {
        try {
            File tempFile = File.createTempFile("sample", ".tsv");
            tempFile.deleteOnExit();
            FileWriter fileWriter = new FileWriter(tempFile);
            for (List<Double> row : data) {
                StringJoiner joiner = new StringJoiner(delim);
                for (Double el : row) {
                    joiner.add(el.toString());
                }
                fileWriter.write(joiner.toString() + "\n");
            }
            fileWriter.close();
            return tempFile;
        } catch (IOException ioException) {
            log.error("Encountered an error creating test file: " + ioException.getLocalizedMessage());
        }
        return null;
    }

    @Test
    public void testTextSniff() throws Exception {
        Random random = new Random();
        int[] widths = {1, random.nextInt(10) + 1, random.nextInt(10) + 1};
        int[] heights = {random.nextInt(10) + 1, random.nextInt(10) + 1, 1};
        TextSniffer sniffer = new TextSniffer();
        for (int i = 0; i < widths.length; i++) {
            int w = widths[i];
            int h = heights[i];
            List<List<Double>> sampleData = genData(h, w);
            File sampleFile = genTempDataFile(sampleData, getDelimiter());
            TextDataset tds = sniffer.sniff(sampleFile);
            assertNotEquals(tds, null);
            tds.read();
            assertEquals(tds.getWidth(), sampleData.get(0).size());
            assertEquals(tds.getHeight(), sampleData.size());
            for (int x = 0; x < tds.getWidth(); x++) {
                for (int y = 0; y < tds.getHeight(); y++) {
                    assertEquals(tds.get(x, y), sampleData.get(y).get(x), 0.05 * sampleData.get(y).get(x));
                }
            }
        }
    }
}