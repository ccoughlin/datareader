@echo off
REM installdeps - installs Myriad dependencies that aren't distributed as Maven projects i.e. runs
REM mvn install:install-file -Dfile=<path-to-file> -DgroupId=<group-id>  -DartifactId=<artifact-id> -Dversion=<version> -Dpackaging=jar

REM Maven requires JAVA_HOME environment variable
if "%JAVA_HOME%"=="" (
    echo Please set the environment variable JAVA_HOME to point to the JDK root folder e.g. "SET JAVA_HOME=C:\Program Files\Java\jdk1.8.0_91"
    EXIT /B 1
)
REM Check if Maven found in path
for %%X in (mvn) do (set FOUND=%%~$PATH:X)
if not defined FOUND (
    echo Unable to find Maven executable.
    echo Please add the Apache Maven folder to the path e.g. "SET PATH=%%PATH%%;C:\apache-maven-3.3.9\bin"
    EXIT /B 1
)

echo.
REM Run mvn install for each dep 
call:mvnInstall pixelmed.jar com.pixelmed dicomtoolkit
call:mvnInstall sgdtk-0.1.1.jar com.github sgdtk

EXIT /B %ERRORLEVEL%

REM Function to run mvn install
REM Usage call:mvnInstall JARfile groupID artifactID
:mvnInstall
echo.
echo %~0 %~1 %~2 %~3
echo Installing local maven artifacts for %~1
call mvn.cmd install:install-file -Dfile=%~1 -DgroupId=%~2  -DartifactId=%~3 -Dversion=1.0.0 -Dpackaging=jar
EXIT /B 0
