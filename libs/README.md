Installing Third Party Dependencies
==

The JAR archives in this folder are not registered as Maven projects in the Maven repositories.  To add them to a local
Maven repository:

mvn install:install-file -Dfile=<path-to-file> -DgroupId=<group-id>  -DartifactId=<artifact-id> -Dversion=<version> -Dpackaging=jar

The Myriad POM project files assume that they have been added to the local repository as follows.

<dependency>
    <groupId>com.github</groupId>
    <artifactId>sgdtk</artifactId>
    <version>1.0.0</version>
</dependency>

<dependency>
    <groupId>com.pixelmed</groupId>
    <artifactId>dicomtoolkit</artifactId>
    <version>1.0.0</version>
</dependency>

Alternatively, you can use the scripts installdeps.bat (Windows) or installdeps.sh (Bash) to do this automatically.