#!/bin/sh

# installdeps - installs Myriad dependencies that aren't distributed as Maven projects i.e. runs
# mvn install:install-file -Dfile=<path-to-file> -DgroupId=<group-id>  -DartifactId=<artifact-id> -Dversion=<version> -Dpackaging=jar

mvnInstall() {
	mvn install:install-file -Dfile=$1 -DgroupId=$2 -DartifactId=$3 -Dversion=1.0.0 -Dpackaging=jar
}


# Check if JAVA_HOME is set for maven
if [ -n "$JAVA_HOME" ];
then
	# Check for maven
	command -v mvn >/dev/null 2>&1 || { echo >&2 "mvn not found; please add to PATH"; exit 1; }
	# Run mvn install for each dep 
	mvnInstall pixelmed.jar com.pixelmed dicomtoolkit
	mvnInstall sgdtk-0.1.1.jar com.github sgdtk
else
	echo "Maven requires the enviroment variable JAVA_HOME to be set"
	echo "e.g. export JAVA_HOME=/home/username/jdk"
	exit 1
fi
