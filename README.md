Myriad Data Reduction Framework
==
Myriad is a library written in Java that provides tools for image / signal processing, machine learning, and fault-tolerant distributed computing. Its primary purpose is to assist with the development of large-scale Region Of Interest (ROI) detection applications by providing the parts required to train a model to detect ROI in large datasets.  

Example applications include face detection, pedestrian tracking, and anomalous signal identification.  Myriad’s [original application for NASA](http://sbir.nasa.gov/SBIR/abstracts/16/sbir/phase1/SBIR-16-1-H13.01-8360.html) was to detect indications of structural damage in nondestructive evaluation data.

For more details on Myriad, visit our [Myriad page](https://emphysic.com/myriad/) to get [code samples](https://emphysic.com/myriad/sample-code/), [video demonstrations](https://emphysic.com/myriad/downloads/), and [tutorials](http://myrdocs.azurewebsites.net/).

License
==
[Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)

Sister Projects
==
* [Myriad Trainer](https://gitlab.com/ccoughlin/MyriadTrainer) is a cross-platform GUI for training and testing machine learning models
* [Myriad Desktop](https://gitlab.com/ccoughlin/MyriadDesktop) is a cross-platform GUI for constructing and running ROI processing pipelines

Links
==
* [Installation](http://myrdocs.azurewebsites.net/install/)
* [Frequently Asked Questions](https://emphysic.com/myriad/faq/)
* [API Documentation](http://myrdocs.azurewebsites.net/api/)
* [About Emphysic](https://emphysic.com/about/)

Contact Information
==
Questions? Comments? Suggestions?  [Drop us a line.](https://emphysic.com/contact-us/)