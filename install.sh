#!/bin/sh

# install - installs the Myriad data reduction framework

# Check if JAVA_HOME is set for maven
if [ -n "$JAVA_HOME" ];
then
	# Check for maven
	command -v mvn >/dev/null 2>&1 || { echo >&2 "mvn not found; please add to PATH"; exit 1; }
	# Run installdeps for third-party libraries that aren't packaged as Maven projects
	cd libs
	./installdeps.sh
	cd ..
	mvn -DskipTests=true clean install
else
	echo "Maven requires the enviroment variable JAVA_HOME to be set"
	echo "e.g. export JAVA_HOME=/home/username/jdk"
	exit 1
fi

