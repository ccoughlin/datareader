/*
 * com.emphysic.myriad.network.DataIngestorActorTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.FileSniffer;
import com.emphysic.myriad.network.messages.DatasetMessage;
import com.emphysic.myriad.network.messages.FileMessage;
import org.junit.Test;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.io.File;
import java.net.URL;

import static org.junit.Assert.assertTrue;

/**
 * Unit tests for DataIngestorActor.
 * Created by ccoughlin on 9/13/2016.
 */
public class DataIngestorActorTest extends ActorTest {

    public DataIngestorActorTest() {
        super(DataIngestorActor.class);
    }

    @Test
    public void testIO() throws Exception {
        URL testJpgURL = Thread.currentThread().getContextClassLoader().getResource("data/test.jpg");
        File singleImageFile = new File(testJpgURL.getPath());
        Dataset expected = FileSniffer.read(singleImageFile, true);

        final Future<Object> future = akka.pattern.Patterns.ask(actorRef, new FileMessage(singleImageFile), 3000);
        assertTrue(future.isCompleted());
        Dataset actual = ((DatasetMessage) (Await.result(future, Duration.Zero()))).getDataset();
        assertDatasetsEqual(expected, actual);
    }
}