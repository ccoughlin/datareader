/*
 * com.emphysic.myriad.network.ActorTest
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.Actor;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.TestActorRef;
import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.util.FileSniffer;
import com.emphysic.myriad.network.messages.DatasetMessage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertEquals;

/**
 * Base class for deterministic single-threaded unit tests for Actors.
 * Created by ccoughlin on 9/13/2016.
 */
class ActorTest {
    final ActorSystem system;
    final Props props;
    final TestActorRef<Actor> actorRef;

    ActorTest(Class clz) {
        system = ActorSystem.create(clz.getSimpleName());
        props = Props.create(clz);
        actorRef = TestActorRef.create(system, props, "TestActorRef");
    }

    /**
     * Utility method to assert two Datasets are "equal" i.e. same dimensions and each element is w/i 5% of the
     * expected element's value.
     * @param expected expected results
     * @param actual actual results
     */
    void assertDatasetsEqual(Dataset expected, Dataset actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        for (int i = 0; i < expected.getWidth(); i++) {
            for (int j = 0; j < expected.getHeight(); j++) {
                assertEquals(expected.get(i, j), actual.get(i, j), 0.05 * expected.get(i, j));
            }
        }
    }

    /**
     * Utility method for returning a sample Dataset
     * @return Dataset for testing
     * @throws IOException if an I/O error occurs attempting to read the Dataset
     */
    Dataset getSampleDataset() throws IOException {
        URL datasetURL = Thread.currentThread().getContextClassLoader().getResource("data/140.csv");
        File datasetFile = new File(datasetURL.getPath());
        return FileSniffer.read(datasetFile, true);
    }

    /**
     * Utility method for returning a sample DatasetMessage
     * @return sample message
     * @throws IOException if an I/O error occurs attempting to read the Dataset
     */
    DatasetMessage getSampleDatasetMessage() throws IOException {
        return new DatasetMessage(getSampleDataset());
    }
}
