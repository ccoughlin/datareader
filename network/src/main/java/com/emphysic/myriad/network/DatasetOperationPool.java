/*
 * com.emphysic.myriad.network.DatasetOperationPool
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.ActorRef;
import akka.routing.Pool;
import com.emphysic.myriad.core.data.ops.DatasetOperation;
import com.emphysic.myriad.network.messages.DatasetMessage;
import com.emphysic.myriad.network.messages.ShutdownMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * DatasetOperationPool - linked worker pool for performing operations on Datasets.
 */
@Slf4j
public class DatasetOperationPool extends LinkedWorkerPool {

    /**
     * Creates a new pool with the specified number of workers.
     * @param workers number of workers
     * @param op operation each worker performs on a received Dataset
     */
    public DatasetOperationPool(int workers, DatasetOperation op) {
        start(workers, DatasetActor.class, op);
    }

    /**
     * Creates a new pool from an existing one
     * @param pool pool
     * @param op operation to perform
     */
    public DatasetOperationPool(Pool pool, DatasetOperation op) {
        start(pool, DatasetActor.class, op);
    }

    /**
     * Starts a new pool to perform the specified operation.
     * @param op operation to perform
     */
    public DatasetOperationPool(DatasetOperation op) {
        start(DatasetActor.class, op);
    }

    /**
     * Handled Messages
     * DatasetMessage - if received from worker pool, sent to the next link.  Otherwise sent to the worker pool for
     * processing.
     * ActorRef - sets the next link.
     * ShutdownMessage - initiates shutdown
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof DatasetMessage) {
            if (getSender().path().parent() == router.path()) {
                ActorRef recipient;
                // Received a processed Dataset
                log.info(genMessage("sending processed Dataset to " + getNextActor()));
                tellNextActor(message);
            } else {
                // Received a new Dataset - start processing
                log.info(genMessage("received new Dataset, processing"));
                tellRouter(message);
            }
        } else if (message instanceof ActorRef) {
            log.info(genMessage("sent next link in the processing chain " + message));
            this.next = (ActorRef) message;
        } else if (message instanceof ShutdownMessage) {
            shutdown();
        }
    }
}
