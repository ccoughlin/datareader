/*
 * com.emphysic.myriad.network.PyramidActor
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.UntypedActor;
import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.GaussianPyramidOperation;
import com.emphysic.myriad.network.messages.DatasetMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * PyramidActor - Akka actor that performs Gaussian Pyramid operations on Datasets.
 */
@Slf4j
public class PyramidActor extends UntypedActor {
    /**
     * Pyramid operation to perform
     */
    private GaussianPyramidOperation gpo;

    /**
     * Creates a new PyramidActor with the specified pyramid operation
     * @param gaussianPyramidOperation operation to perform
     */
    public PyramidActor(GaussianPyramidOperation gaussianPyramidOperation) {
        this.gpo = gaussianPyramidOperation;
    }

    /**
     * Creates a new PyramidActor with the default Gaussian Pyramid Operation
     */
    public PyramidActor() {
        this(new GaussianPyramidOperation());
    }

    /**
     * Handled Messages
     * DatasetMessage - while the current step is not null, perform the pyramid operation and send the result back to
     * the sender as a DatasetMessage
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof DatasetMessage) {
            if (gpo != null) {
                log.info(getSelf() + " received new Dataset");
                Dataset current = new Dataset(((DatasetMessage) message).getDataset());
                int step = 0;
                do {
                    String md = ((DatasetMessage) message).genMetadata("pyramid",
                            "pscale" + Integer.toString(gpo.getScaleFactor()) +
                            "pwsize" + Integer.toString(gpo.getWindowSize()) +
                            "pstep" + Integer.toString(step)
                    );
                    getSender().tell(new DatasetMessage(current, md), getSelf());
                    log.info(getSelf() + " sent pyramid step to " + getSender());
                    step++;
                } while ((current = gpo.run(current)) != null);
            }
        }
    }
}
