/*
 * com.emphysic.myriad.network.DataIngestorActor
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.UntypedActor;
import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.io.ImageDataset;
import com.emphysic.myriad.core.data.ops.NormalizeSignalOperation;
import com.emphysic.myriad.core.data.util.FileSniffer;
import com.emphysic.myriad.network.messages.DatasetMessage;
import com.emphysic.myriad.network.messages.FileMessage;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

/**
 * DataIngestorActor - Akka actor for reading data files
 */
@Slf4j
public class DataIngestorActor extends UntypedActor {

    private boolean normalize;
    private NormalizeSignalOperation norm;

    /**
     * Constructor
     * @param normalizeData true if data should be normalized between 0 and 1 after reading
     */
    public DataIngestorActor(boolean normalizeData) {
        normalize = normalizeData;
        norm = new NormalizeSignalOperation();
    }

    /**
     * Default constructor.  Data are not normalized after reading.
     */
    public DataIngestorActor() {
        this(false);
    }

    /**
     * Sets the data normalization policy.
     * @param norm true if data should be normalized between 0 and 1 after reading, false if not.
     */
    public void Normalize(boolean norm) {
        normalize = norm;
    }

    /**
     * Returns the current data normalization policy.
     * @return true if data are normalized, false otherwise.
     */
    public boolean Normalize() {
        return normalize;
    }

    /**
     * Handled Messages:
     * FileMessage - attempts to read a Dataset from the message's payload, sends a DatasetMessage response back to
     * the sender.
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof FileMessage) {
            File f = ((FileMessage) message).getFile();
            Dataset dataset = FileSniffer.read(f, true);
            if (dataset != null) {
                if (normalize) {
                    dataset = norm.run(dataset);
                }
                log.info(getSelf() + ": successfully read " + f + " sending to " + getSender());
                DatasetMessage response = new DatasetMessage(dataset, ((FileMessage) message).getMetadata());
                getSender().tell(response, getSelf());
                if (dataset instanceof ImageDataset) {
                    // Multi-frame
                    int numFrames = ((ImageDataset) dataset).getNumFrames();
                    int currentFrame;
                    while ((currentFrame = ((ImageDataset) dataset).getCurrentFrame()) < numFrames) {
                        log.info(getSelf() + " sending frame " + currentFrame + " of " + numFrames);
                        DatasetMessage nextFrame = new DatasetMessage(dataset,
                                DatasetMessage.genMetadata(response, "frame", Integer.toString(currentFrame)));
                        getSender().tell(nextFrame, getSelf());
                    }
                }
            } else {
                log.warn(getSelf() + ": unable to read " + f);
            }
        }
    }
}
