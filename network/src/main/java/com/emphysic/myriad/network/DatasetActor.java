/*
 * com.emphysic.myriad.network.DatasetActor
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.UntypedActor;
import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.DatasetOperation;
import com.emphysic.myriad.network.messages.DatasetMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * DatasetActor - Akka actor that performs a DatasetOperation on a Dataset.
 */
@Slf4j
public class DatasetActor extends UntypedActor {
    /**
     * Operation to perform
     */
    DatasetOperation op;

    /**
     * Creates a new DatasetActor with a DatasetOperation
     * @param operation operation to perform on each Dataset
     */
    public DatasetActor(DatasetOperation operation) {
        setOperation(operation);
    }

    /**
     * Sets the operation to perform on each Dataset
     * @param operation new operation to perform
     */
    public void setOperation(DatasetOperation operation) {
        this.op = operation;
    }

    /**
     * Handled Messages
     * DatasetMessage - performs the DatasetOperation on the message payload, sends DatasetMessage with result back
     * to sender.
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof DatasetMessage) {
            log.info(getSelf() + " received DatasetMessage");
            Dataset dataset = ((DatasetMessage) message).getDataset();
            Dataset result;
            if (op != null) {
                log.info(getSelf() + " performing DatasetOperation");
                result = op.run(dataset);
            } else {
                log.info(getSelf() + " no operation defined, sending dataset back");
                result = dataset;
            }
            // Perform operation and send results to the next operation in the chain
            log.info(getSelf() + " sending result to " + getSender());
            String md = ((DatasetMessage) message).genMetadata("op", op.toString());
            getSender().tell(new DatasetMessage(result, md), getSelf());
        }
    }
}
