/*
 * com.emphysic.myriad.network.SlidingWindowActor
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.DatasetOperation;
import com.emphysic.myriad.core.data.ops.SlidingWindowOperation;
import com.emphysic.myriad.network.messages.DatasetMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * SlidingWindowActor - performs sliding window operation on Datasets.
 */
@Slf4j
public class SlidingWindowActor extends UntypedActor {
    /**
     * Sliding window operation to perform
     */
    private DatasetOperation windowOp;
    /**
     * Step size between windows
     */
    private int stepSize;
    /**
     * Width of window
     */
    private int windowWidth;

    /**
     * Height of window
     */
    private int windowHeight;

    /**
     * Next link in the chain - current window is sent to this Actor.  Since sliding windows require keeping state,
     * the next link has to be kept here rather than in the LinkedWorkerPool implementation as is normally done.
     */
    private ActorRef next;


    /**
     * Creates a new rectangular sliding window actor
     * @param stepSize step size between windows
     * @param windowWidth width of window
     * @param windowHeight height of window
     * @param op DatasetOperation to perform on each window
     */
    public SlidingWindowActor(int stepSize, int windowWidth, int windowHeight, DatasetOperation op) {
        this.stepSize = stepSize;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        this.windowOp = op;
    }

    /**
     * Creates a new sliding window actor
     * @param stepSize step size between windows
     * @param windowSize dimension of square window
     * @param op DatasetOperation to perform on each window
     */
    public SlidingWindowActor(int stepSize, int windowSize, DatasetOperation op) {
        this(stepSize, windowSize, windowSize, op);
    }

    /**
     * Creates a new sliding window actor with no operation performed on the window.
     * @param stepSize step size between windows
     * @param windowSize dimension of square window
     */
    public SlidingWindowActor(int stepSize, int windowSize) {
        this(stepSize, windowSize, null);
    }

    /**
     * Handled Messages
     * DatasetMessage - starts sliding window.  Each window is then sent as a new DatasetMessage to this actor's next
     * link.
     * ActorRef - sets the next link in the chain.
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof DatasetMessage) {
            log.info(getSelf() + " received Dataset");
            SlidingWindowOperation slider = new SlidingWindowOperation(stepSize, windowWidth, windowHeight, windowOp);
            Dataset current;
            int steps = 0;
            while ((current = slider.run(((DatasetMessage) message).getDataset())) != null) {
                log.info(getSelf() + ": offset " + slider.getXoffset() + "," + slider.getYoffset() + " " + ((DatasetMessage) message).getDataset().getSize());
                if (next != null) {
                    String md = ((DatasetMessage) message).genMetadata("window",
                            "xoff" + slider.getXoffset() + "yoff" + slider.getYoffset() + "w" + slider.getWindowWidth() + "h" + slider.getWindowHeight());
                    log.info(getSelf() + " sending window to " + next);
                    next.tell(new DatasetMessage(current, md), getSelf());
                } else {
                    log.info(getSelf() + " no recipient configured!");
                }
                steps++;
            }
            log.info(getSelf() + " completed processing in " + steps + " steps");
        } else if (message instanceof ActorRef) {
            log.info(getSelf() + " setting next link to " + message);
            next = (ActorRef) message;
        }
    }
}
