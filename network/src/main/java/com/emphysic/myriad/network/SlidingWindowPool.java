/*
 * com.emphysic.myriad.network.SlidingWindowPool
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.ActorRef;
import akka.routing.Broadcast;
import com.emphysic.myriad.core.data.ops.DatasetOperation;
import com.emphysic.myriad.network.messages.DatasetMessage;
import com.emphysic.myriad.network.messages.ShutdownMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * SlidingWindowPool - a linked worker pool performing sliding windows.
 */
@Slf4j
public class SlidingWindowPool extends LinkedWorkerPool {

    /**
     * Creates a new pool
     * @param workers number of workers
     * @param stepSize step size between windows
     * @param windowWidth width of window
     * @param windowHeight height of window
     * @param operation operation to perform on each window
     */
    public SlidingWindowPool(int workers, int stepSize, int windowWidth, int windowHeight, DatasetOperation operation) {
        start(workers, SlidingWindowActor.class, stepSize, windowWidth, windowHeight, operation);
    }

    /**
     * Creates a new pool
     * @param workers number of workers
     * @param stepSize step size between windows
     * @param windowWidth width of window
     * @param windowHeight height of window
     */
    public SlidingWindowPool(int workers, int stepSize, int windowWidth, int windowHeight) {
        this(workers, stepSize, windowWidth, windowHeight, null);
    }


    /**
     * Creates a new pool
     * @param workers number of workers
     * @param stepSize step size between windows
     * @param windowSize dimension of square window
     * @param op operation to perform on each window
     */
    public SlidingWindowPool(int workers, int stepSize, int windowSize, DatasetOperation op) {
        this(workers, stepSize, windowSize, windowSize, op);
    }

    /**
     * Creates a new pool
     * @param stepSize step size between windows
     * @param windowSize dimension of square window
     * @param operation operation to perform on each window
     */
    public SlidingWindowPool(int stepSize, int windowSize, DatasetOperation operation) {
        this(WorkerPool.DEFAULTWORKERS, stepSize, windowSize, null);
    }

    /**
     * Creates a new pool with no DatasetOperation performed on each window.
     * @param workers number of workers in the pool
     * @param stepSize step size between windows
     * @param windowSize dimension of square window
     */
    public SlidingWindowPool(int workers, int stepSize, int windowSize) {
        this(workers, stepSize, windowSize, windowSize, null);
    }

    /**
     * Creates a new pool
     * @param stepSize step size between windows
     * @param windowSize dimension of square window
     */
    public SlidingWindowPool(int stepSize, int windowSize) {
        this(WorkerPool.DEFAULTWORKERS, stepSize, windowSize, null);
    }

    /**
     * Handled Messages
     * DatasetMessage - sends the message to the worker pool for processing
     * ActorRef - sets the next link in the processing chain for the worker pool
     * ShutdownMessage - initiates shutdown
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof DatasetMessage) {
            log.info(genMessage("received DatasetMessage, sliding"));
            tellRouter(message);
        } else if (message instanceof ActorRef) {
            log.info(genMessage("setting next link to " + message));
            // Since sliding requires keeping state, each worker is responsible for sending to the next
            // link - need to tell each of them about the link
            tellRouter(new Broadcast(message));
        } else if (message instanceof ShutdownMessage) {
            shutdown();
        }
    }
}
