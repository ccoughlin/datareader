/**
 * This package contains definitions of immutable messages for the Myriad network.
 * Copyright (c) 2016 Emphysic LLC.  All rights reserved.
 */
package com.emphysic.myriad.network.messages;