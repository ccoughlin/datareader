/*
 * com.emphysic.myriad.network.messages.ROIMessage
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network.messages;

import com.emphysic.myriad.core.data.roi.ROI;
import lombok.extern.slf4j.Slf4j;

/**
 * ROIMessage - immutable message with a Region of Interest (ROI) payload.
 */
@Slf4j
public class ROIMessage extends ImmutableMessage {
    /**
     * Payload
     */
    protected final ROI ROI;

    /**
     * Creates a new ROIMessage
     * @param ROI Region Of Interest payload
     * @param metadata metadata
     */
    public ROIMessage(ROI ROI, String metadata) {
        super(metadata);
        this.ROI = ROI;
    }

    /**
     * Creates a new empty message
     */
    public ROIMessage() {
        this(null, null);
    }

    /**
     * Creates a new message with no ROI
     * @param metadata metadata
     */
    public ROIMessage(String metadata) {
        this(null, metadata);
    }

    /**
     * Retrieves this message's payload
     * @return payload
     */
    public ROI getROI() {
        return ROI;
    }

    @Override
    public String toString() {
        return metadata;
    }
}
