/*
 * com.emphysic.myriad.network.WorkerPool
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.Broadcast;
import akka.routing.Pool;
import akka.routing.SmallestMailboxPool;
import lombok.extern.slf4j.Slf4j;

/**
 * WorkerPool - a pool of identical worker processes
 */
@Slf4j
public abstract class WorkerPool extends UntypedActor {
    /**
     * Manages the workers
     */
    protected ActorRef router;
    /**
     * Default number of workers in a pool
     */
    protected static final int DEFAULTWORKERS = 10;

    /**
     * Creates a new pool of workers
     *
     * @param pool        Router actor that creates workers
     * @param workerClass the type of worker in the pool
     * @param args        constructor arguments for the worker class
     */
    public void start(Pool pool, Class<? extends UntypedActor> workerClass, Object... args) {
        this.router = getContext().actorOf(pool.props(
                Props.create(workerClass, args)));
    }

    /**
     * Creates a new worker pool that tries to send to the non-suspended routee with fewest messages in mailbox.
     * The selection is done in this order:
     * <ul>
     * <li>pick any idle routee (not processing message) with empty mailbox</li>
     * <li>pick any routee with empty mailbox</li>
     * <li>pick routee with fewest pending messages in mailbox</li>
     * <li>pick any remote routee, remote actors are consider lowest priority,
     * since their mailbox size is unknown</li>
     * </ul>
     *
     * @param numWorkers  number of workers in the pool
     * @param workerClass class of worker
     * @param args        optional constructor arguments for the worker
     */
    public void start(int numWorkers, Class<? extends UntypedActor> workerClass, Object... args) {
        start(new SmallestMailboxPool(numWorkers), workerClass, args);
    }

    /**
     * Creates a new worker pool that tries to send to the non-suspended routee with fewest messages in mailbox.
     * The selection is done in this order:
     * <ul>
     * <li>pick any idle routee (not processing message) with empty mailbox</li>
     * <li>pick any routee with empty mailbox</li>
     * <li>pick routee with fewest pending messages in mailbox</li>
     * <li>pick any remote routee, remote actors are consider lowest priority,
     * since their mailbox size is unknown</li>
     * </ul>
     *
     * @param workerClass class of worker
     * @param args        optional constructor arguments for the worker
     */
    public void start(Class<? extends UntypedActor> workerClass, Object... args) {
        start(DEFAULTWORKERS, workerClass, args);
    }

    /**
     * Prepends this instance's ID to a String.  Primarily used in logging.
     *
     * @param orig original message
     * @return ID + ": " + original message
     */
    public String genMessage(String orig) {
        return getSelf() + ": " + orig;
    }

    /**
     * Retrieves the worker manager Actor
     * @return router
     */
    public ActorRef getRouter() {
        return router;
    }

    /**
     * Sets the worker manager
     * @param router new router
     */
    public void setRouter(ActorRef router) {
        this.router = router;
    }

    /**
     * Sends an Akka message to the worker pool
     * @param message message to send
     * @param sender message's sender
     */
    public void tellRouter(Object message, ActorRef sender) {
        if (router != null) {
            router.tell(message, sender);
        } else {
            log.error(genMessage("no router configured!"));
        }
    }

    /**
     * Sends an Akka message to the worker pool from this instance
     * @param message message to send
     */
    public void tellRouter(Object message) {
        tellRouter(message, getSelf());
    }

    /**
     * Initiates shutdown
     */
    public void shutdown() {
        log.info(genMessage("instructing workers to finish up for shutdown"));
        tellRouter(new Broadcast(PoisonPill.getInstance()));
        tellRouter(PoisonPill.getInstance());
        getContext().stop(getSelf());
    }
}
