/*
 * com.emphysic.myriad.network.DataIngestorPool
 *
 * Copyright (c) 2018 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.Pool;
import com.emphysic.myriad.network.messages.DatasetMessage;
import com.emphysic.myriad.network.messages.FileMessage;
import com.emphysic.myriad.network.messages.ShutdownMessage;
import lombok.extern.slf4j.Slf4j;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static akka.pattern.Patterns.gracefulStop;

/**
 * DataIngestorPool - a linked pool for reading Dataset input files.
 */
@Slf4j
public class DataIngestorPool extends LinkedWorkerPool {

    /**
     * Creates a new pool.
     * @param numWorkers number of workers to create
     * @param normalize true if data should be normalized between 0 and 1
     */
    public DataIngestorPool(int numWorkers, boolean normalize) {
        start(numWorkers, DataIngestorActor.class, normalize);
    }

    /**
     * Creates a new pool from an existing pool.
     * @param pool pool
     * @param normalize true if data should be normalized between 0 and 1
     */
    public DataIngestorPool(Pool pool, boolean normalize) {
        start(pool, DataIngestorActor.class, normalize);
    }

    /**
     * Starts a new pool with the default number of workers.
     */
    public DataIngestorPool(boolean normalize) {
        start(DataIngestorPool.class, normalize);
    }

    /**
     * Creates a new pool.  Data are not normalized.
     * @param numWorkers number of workers to create
     */
    public DataIngestorPool(int numWorkers) {
        this(numWorkers, false);
    }

    /**
     * Creates a new pool from an existing pool.  Data are not normalized.
     * @param pool pool
     */
    public DataIngestorPool(Pool pool) {
        this(pool, false);
    }

    /**
     * Starts a new pool with the default number of workers.  Data are not normalized.
     */
    public DataIngestorPool() {
        this(false);
    }

    /**
     * Handled Messages
     * FileMessage - sends to pool for reading
     * DatasetMessage - sends to the next link
     * ActorRef - sets the next link
     * ShutdownMessage - initiates shutdown
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof FileMessage) {
            log.info(genMessage("received FileMessage"));
            tellRouter(message);
        } else if (message instanceof DatasetMessage) {
            tellNextActor(message);
            log.info(genMessage("sending data to " + getNextActor()));
        } else if (message instanceof ActorRef) {
            log.info(genMessage("received next Actor " + message));
            this.next = (ActorRef) message;
        } else if (message instanceof ShutdownMessage) {
            shutdown();
        }
    }

    public static void main(String[] args) {
        final ActorSystem ingestorDemo = ActorSystem.create("DataIngestor");
        final ActorRef ingestor = ingestorDemo.actorOf(Props.create(DataIngestorPool.class, 5));
        URL dataUrl = Thread.currentThread().getContextClassLoader().getResource("data/io/sampledata");
        File dataFolder = new File(dataUrl.getPath());
        File[] dataFiles = dataFolder.listFiles();
        for (File f : dataFiles) {
            ingestor.tell(new FileMessage(f), ingestor);
        }
        try {
            Future<Boolean> stopped = gracefulStop(ingestor, Duration.create(15, TimeUnit.SECONDS), ingestorDemo);
            Await.result(stopped, Duration.create(20, TimeUnit.SECONDS));
            // the actor has been stopped
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        ingestorDemo.terminate();
    }
}
