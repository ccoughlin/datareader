/*
 * com.emphysic.myriad.network.ReporterActorPool
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.ActorRef;
import com.emphysic.myriad.network.messages.ROIMessage;
import com.emphysic.myriad.network.messages.ShutdownMessage;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * ReporterActorPool - linked worker pool for examining Region Of Interest (ROI) results
 * TODO: convert to streaming reporting
 */
@Slf4j
public class ReporterActorPool extends LinkedWorkerPool {
    /**
     * List of Regions Of Interest (ROI) detected
     */
    List<ROIMessage> rois = new ArrayList<>();

    /**
     * Creates a new pool.
     * @param numWorkers number of workers in the pool
     * @param reporter type of reporter to use
     * @param args constructor arguments for the reporter
     */
    public ReporterActorPool(int numWorkers, Class<? extends ReporterActor> reporter, Object... args) {
        start(numWorkers, reporter, args);
    }

    /**
     * Creates a new pool
     * @param numWorkers number of workers in the pool
     * @param reporter type of reporter to use
     */
    public ReporterActorPool(int numWorkers, Class<? extends ReporterActor> reporter) {
        start(numWorkers, reporter);
    }

    /**
     * Creates a new pool with the default ReporterActor
     * @param numWorkers number of workers in the pool
     */
    public ReporterActorPool(int numWorkers) {
        start(numWorkers, ReporterActor.class);
    }

    /**
     * Creates a new pool
     */
    public ReporterActorPool() {
        start(ReporterActor.class);
    }

    /**
     * Handled Messages
     * ROIMessage - if from worker pool, adds to ROI.  Otherwise sends to pool for processing.
     * ShutdownMessage - initiates shutdown
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ROIMessage) {
            if (getSender().path().parent() == router.path()) {
                if (((ROIMessage) message).getROI() != null) {
                    rois.add((ROIMessage) message);
                    log.info("Found flaw, sending to " + next);
                    tellNextActor(message);
                } else {
                    log.info("No flaw found");
                }
            } else {
                log.info(genMessage("received a ROI, processing"));
                tellRouter(message);
            }
        } else if (message instanceof ActorRef) {
            this.next = (ActorRef) message;
        } else if (message instanceof ShutdownMessage) {
            if (rois.size() > 0) {
                log.info("**** Final ROI Find Report ****");
                for (ROIMessage f : rois) {
                    log.info(f.getMetadata());
                }
            } else {
                log.info("No flaws");
            }
            shutdown();
        }
    }

    /**
     * Returns the current list of ROIs
     * @return list of ROIs
     */
    public List<ROIMessage> getROIS() {
        return rois;
    }
}
