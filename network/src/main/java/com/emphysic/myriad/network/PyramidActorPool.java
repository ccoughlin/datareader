/*
 * com.emphysic.myriad.network.PyramidActorPool
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.ActorRef;
import com.emphysic.myriad.core.data.ops.GaussianPyramidOperation;
import com.emphysic.myriad.network.messages.DatasetMessage;
import com.emphysic.myriad.network.messages.ShutdownMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * PyramidActorPool - linked worker pool for performing Gaussian Pyramid operations on Datasets.
 */
@Slf4j
public class PyramidActorPool extends LinkedWorkerPool {

    /**
     * Creates a new pool.
     * @param numWorkers Number of workers in the pool
     * @param gpo pyramid operation to perform
     */
    public PyramidActorPool(int numWorkers, GaussianPyramidOperation gpo) {
        start(numWorkers, PyramidActor.class, gpo);
    }

    /**
     * Creates a new pool with the default Gaussian Pyramid operation
     * @param numWorkers number of workers in the pool
     */
    public PyramidActorPool(int numWorkers) {
        start(numWorkers, PyramidActor.class);
    }

    /**
     * Creates a new pool
     */
    public PyramidActorPool() {
        start(PyramidActor.class);
    }

    /**
     * Handled Messages
     * DatasetMessage - if received from worker pool, sends results to next link in the chain.  Otherwise sends to
     * worker pool for processing.
     * ActorRef - sets the next link in the chain
     * ShutdownMessage - initiates shutdown
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof DatasetMessage) {
            if (getSender().path().parent() == router.path()) {
                // Received a processed Dataset
                tellNextActor(message);
                log.info(genMessage("received processed Dataset sending to " + getNextActor()));
            } else {
                // Received a new Dataset - start processing
                log.info(genMessage("received a new Dataset, processing"));
                tellRouter(message);
            }
        } else if (message instanceof ActorRef) {
            this.next = (ActorRef) message;
            log.info(genMessage("set next recipient"));
        } else if (message instanceof ShutdownMessage) {
            shutdown();
        }
    }
}
