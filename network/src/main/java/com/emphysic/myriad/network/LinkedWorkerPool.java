/*
 * com.emphysic.myriad.network.LinkedWorkerPool
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.routing.Broadcast;
import com.emphysic.myriad.network.messages.ShutdownMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * LinkedWorkerPool - an Akka worker pool that sends its results to another Actor.  Conceptually speaking, a linked
 * list of processing steps.
 */
@Slf4j
public abstract class LinkedWorkerPool extends WorkerPool {
    /**
     * Next link in the chain
     */
    protected ActorRef next;

    /**
     * Determines the recipient for a message.
     *
     * @return next link or this Actor's parent if the link is null.
     */
    public ActorRef getNextActor() {
        return next != null ? next : getContext().parent();
    }

    /**
     * Sends a message to the next Actor in the chain from this Actor.
     *
     * @param message message to send to the next Actor
     */
    public void tellNextActor(Object message) {
        tellNextActor(message, getSelf());
    }

    /**
     * Sends a message to the next Actor in the chain, or to this instance's parent if next is null.
     *
     * @param message object to send
     * @param sender  sender of the message
     */
    public void tellNextActor(Object message, ActorRef sender) {
        getNextActor().tell(message, sender);
    }

    /**
     * Returns the next link in the chain
     * @return next link
     */
    public ActorRef getNext() {
        return next;
    }

    /**
     * Sets the next link in the chain
     * @param next new next
     */
    public void setNext(ActorRef next) {
        this.next = next;
    }

    /**
     * Initiates shutdown procedure.
     */
    @Override
    public void shutdown() {
        log.info(genMessage("instructing workers to finish up for shutdown"));
        tellRouter(new Broadcast(PoisonPill.getInstance()));
        if (next != null) {
            log.info(genMessage("instructing next link to finish up for shutdown"));
            tellNextActor(new ShutdownMessage());
        }
        getContext().stop(getSelf());
    }
}
