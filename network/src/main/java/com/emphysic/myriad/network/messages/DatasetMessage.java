/*
 * com.emphysic.myriad.network.messages.DatasetMessage
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network.messages;

import com.emphysic.myriad.core.data.io.Dataset;

/**
 * DatasetMessage - immutable message for transmitting a Dataset payload
 */
public class DatasetMessage extends ImmutableMessage {
    /**
     * Dataset payload
     */
    protected final Dataset dataset;

    /**
     * Creates a new DatasetMessage
     * @param dataset payload
     * @param metadata metadata
     */
    public DatasetMessage(Dataset dataset, String metadata) {
        super(metadata);
        this.dataset = dataset;
    }

    /**
     * Creates a new DatasetMessage with no metadata
     * @param dataset payload
     */
    public DatasetMessage(Dataset dataset) {
        this(dataset, null);
    }

    /**
     * Copy constructor
     * @param orig message to copy
     */
    public DatasetMessage(DatasetMessage orig) {
        this(orig.getDataset(), orig.getMetadata());
    }

    /**
     * Creates a copy of a DatasetMessage with additional metadata
     * @param orig message to copy
     * @param addlMetadata additional metadata
     */
    public DatasetMessage(DatasetMessage orig, String addlMetadata) {
        this(orig.getDataset(), orig.getMetadata() == null ? addlMetadata: orig.getMetadata() + addlMetadata);
    }

    /**
     * Retrieves the Dataset payload
     * @return payload
     */
    public Dataset getDataset() {
        return dataset;
    }
}
