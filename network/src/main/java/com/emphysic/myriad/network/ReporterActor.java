/*
 * com.emphysic.myriad.network.ReporterActor
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.UntypedActor;
import com.emphysic.myriad.core.data.roi.ROI;
import com.emphysic.myriad.network.messages.ROIMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * ReporterActor - Akka actor that examines and acts upon ROIMessages
 */
@Slf4j
public class ReporterActor extends UntypedActor {

    /**
     * Handled Messages
     * ROIMessage - examines the message and logs the result.
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ROIMessage) {
            ROI f = ((ROIMessage) message).getROI();
            if (f != null) {
                StringBuilder sb = new StringBuilder("Received ROI report from " + getSender() + ": ");
                sb.append("** FLAW FOUND ** ").append(((ROIMessage) message).getMetadata());
                getSender().tell(message, getSelf());
                log.info(sb.toString());
            }
        }
    }
}
