/*
 * com.emphysic.myriad.network.messages.ImmutableMessage
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network.messages;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * ImmutableMessage - base class for Myriad messages.
 * Created by ccoughlin on 9/28/2016.
 */
public abstract class ImmutableMessage implements Serializable {
    /**
     * Metadata
     */
    final String metadata;

    ImmutableMessage(String metadata) {
        this.metadata = metadata;
    }

    /**
     * Retrieves the metadata
     * @return metadata associated with this payload, or null if none
     */
    public String getMetadata() {
        return metadata;
    }

    /**
     * Convenience function to split metadata of the form &amp;key1=val1&amp;key2=val2... into a Map
     * @param mData original metadata
     * @return key:value map of metadata
     */
    public static Map<String, String> getMetadata(String mData) {
        HashMap<String, String> md = new HashMap<>();
        String[] entries = mData.split("&");
        for (String entry : entries) {
            String[] pair = entry.split("=");
            if (pair.length == 2) {
                md.put(pair[0], pair[1]);
            }
        }
        return md;
    }

    /**
     * Convenience function to create a new metadata String from this message's metadata and a new
     * key=val pair.  This instance's metadata is not modified.
     * @param key key
     * @param val value
     * @return new metadata String of the form this.getMetadata();&amp;key=val
     */
    public String genMetadata(String key, String val) {
        return genMetadata(this, key, val);
    }

    /**
     * Convenience function to create a new metadata String from a message's metadata and a new
     * key=val pair.  The original message is not modified.
     * @param orig metadata of existing metadata
     * @param key key
     * @param val value
     * @return String of the form orig.getMetadata()&amp;key=val
     */
    public static String genMetadata(ImmutableMessage orig, String key, String val) {
        StringBuilder sb = new StringBuilder();
        if (orig != null) {
            String md = orig.getMetadata();
            if (md != null) {
                sb.append(md);
            }
        }
        sb.append("&");
        sb.append(genMetadataEntry(key, val));
        return sb.toString();
    }

    /**
     * Convenience function to generate a key=val pair.
     * @param key key
     * @param val value
     * @return String of the form key=val
     */
    public static String genMetadataEntry(String key, String val) {
        return key + "=" + val;
    }
}
