/*
 * com.emphysic.myriad.network.ROIFinderPool
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.ActorRef;
import com.emphysic.myriad.core.data.roi.ROIFinder;
import com.emphysic.myriad.network.messages.DatasetMessage;
import com.emphysic.myriad.network.messages.ROIMessage;
import com.emphysic.myriad.network.messages.ShutdownMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * ROIFinderPool - linked worker pool of Region Of Interest (ROI) detectors
 */
@Slf4j
public class ROIFinderPool extends LinkedWorkerPool {

    /**
     * Creates a new ROIFinderPool.
     * @param numWorkers number of workers in the pool
     * @param finder ROI detector to use
     */
    public ROIFinderPool(int numWorkers, ROIFinder finder) {
        start(numWorkers, ROIActor.class, finder);
    }

    /**
     * Creates a new ROIFinderPool
     * @param finder ROI detector to use
     */
    public ROIFinderPool(ROIFinder finder) {
        start(ROIActor.class, finder);
    }

    /**
     * Handled Messages
     * DatasetMessage - sends to router for ROI detection
     * ROIMessage - sends to next link in the processing chain
     * ActorRef - sets next link in the processing chain
     * ShutdownMessage - initiates shutdown
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof DatasetMessage) {
            // Received new Dataset to search
            log.info(genMessage("received new Dataset to scan for flaws"));
            tellRouter(message);
        } else if (message instanceof ROIMessage) {
            tellNextActor(message);
            log.info(genMessage("sending ROI result to " + getNextActor()));
        } else if (message instanceof ActorRef) {
            this.next = (ActorRef) message;
        } else if (message instanceof ShutdownMessage) {
            shutdown();
        }
    }
}
