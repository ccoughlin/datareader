/*
 * com.emphysic.myriad.network.messages.FileMessage
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network.messages;

import java.io.File;

/**
 * FileMessage - immutable message with a File payload
 */
public class FileMessage extends ImmutableMessage {
    /**
     * Payload
     */
    private final File file;

    public FileMessage(File file, String metadata) {
        super(metadata);
        this.file = file;
    }

    /**
     * Creates a new FileMessage
     * @param file payload
     */
    public FileMessage(File file) {
        this(file, genMetadata(null, "source", file.getAbsolutePath()));
    }

    /**
     * Creates a new FileMessage
     * @param fileName payload
     */
    public FileMessage(String fileName) {
        this(new File(fileName));
    }

    /**
     * Retrieves this message's payload
     * @return payload
     */
    public File getFile() {
        return file;
    }
}
