/*
 * com.emphysic.myriad.network.ROIActor
 *
 * Copyright (c) 2016 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emphysic.myriad.network;

import akka.actor.UntypedActor;
import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.roi.ROI;
import com.emphysic.myriad.core.data.roi.ROIFinder;
import com.emphysic.myriad.network.messages.DatasetMessage;
import com.emphysic.myriad.network.messages.ROIMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * ROIActor - Akka actor that looks for Regions of Interest (ROI) in Datasets.
 */
@Slf4j
public class ROIActor extends UntypedActor {
    /**
     * ROI finder
     */
    protected ROIFinder ROIFinder;

    /**
     * Creates a new ROIFinder with a specified ROI detector
     * @param ROIFinder detector
     */
    public ROIActor(ROIFinder ROIFinder) {
        this.ROIFinder = ROIFinder;
    }

    /**
     * Handled Messages
     * DatasetMessage - sends message payload to ROI detector, creates new ROIMessage from result and sends back to
     * sender
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof DatasetMessage) {
            StringBuilder sb = new StringBuilder(getSelf() + " received dataset, ");
            if (ROIFinder != null) {
                // Look for ROI
                ROIMessage ROIMessage;
                Dataset payload = ((DatasetMessage) message).getDataset();
                String md = ((DatasetMessage) message).genMetadata("roifinder", ROIFinder.toString());
                ROI ROI = null;
                if (ROIFinder.isROI(payload)) {
                    sb.append("ROI found");
                    // TODO: extract coordinates from metadata if present
                    ROI = new ROI(payload, 0);
                } else {
                    sb.append("no ROI found");
                }
                ROIMessage = new ROIMessage(ROI, md);
                log.info(sb.toString());
                getSender().tell(ROIMessage, getSelf());
            } else {
                log.info(getSelf() + " couldn't scan for flaws, no ROI finder set");
            }
        }
    }
}
